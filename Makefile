CPLUSPLUS := mpicxx -w -O3 -g 
#CPLUSPLUS := tau_cxx.sh -w -O3 -g 

CPLUSPLUSLINK := $(CPLUSPLUS)

MODULES		:= util problems drivers algorithms 
SRC_DIR		:= $(MODULES)
BUILD_DIR   := $(addprefix build/, $(MODULES))
CPLUSPLUSFLAGS := $(addprefix -I ./, $(MODULES)) -I $$TACC_GSL_INC -I $$TACC_BOOST_INC
CPLUSPLUSLINKFLAGS := $(addprefix -L/build/, $(MODULES)) -L$$TACC_GSL_LIB -lgsl -lgslcblas 


SRC			:= $(foreach sdir, $(SRC_DIR), $(wildcard $(sdir)/*.cpp))
OBJ			:= $(patsubst %.cpp, build/%.o, $(SRC))

obj := o

.SUFFIXES: .cpp .$(obj)

CPLUSPLUSSOURCE := $(wildcard *.cpp)
CPLUSPLUSOBJ := $(patsubst %.cpp, %.$(obj), $(CPLUSPLUSSOURCE))
CPLUSPLUSEXE := a.out

all: $(BUILD_DIR)  $(CPLUSPLUSEXE)

build/%.$(obj) : %.cpp
	@echo ""
	@echo "Compiling program $*.cpp:"
	$(CPLUSPLUS) -c $(CPLUSPLUSFLAGS) $*.cpp -o build/$*.$(obj)

$(CPLUSPLUSEXE): $(OBJ)
#	@echo $(SRC_DIR)
#	@echo $(SRC)
#	@echo $(OBJ)
	@echo ""
	@echo "Linking program $@:"
	$(CPLUSPLUSLINK) $(OBJ) -o $@ $(CPLUSPLUSFLAGS) $(CPLUSPLUSLINKFLAGS) 
	@echo ""

$(BUILD_DIR):
	mkdir -p $@

.PHONY: clean
clean:
	$(RM) -f $(CPLUSPLUSEXE) $(OBJ)

