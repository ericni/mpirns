
/**
 * @file EtaFunc.h
 *
 * @brief Defines functions used to calculate the constant Eta for Good Selection Algorithm
 *
 */
#ifndef ETAFUNC_H_
#define ETAFUNC_H_

/**
 * @brief Set to 1 to approximate eta, set to 0 to use Gauss-Laguerre quadrature to find eta.
 */
#define APPROX_ETA 0
#include "SOAlgorithm.h"
#include<cmath>
#include<iostream>

using namespace std;

/**
 * @brief Functions used to calculate Eta constant
 */
class EtaFunc {
private:
	///@{
	/**
	 * Weights and abscissas used by the Gauss-Laguerre Quadrature Rule
	 */
	static double *W;
	static double *X;
	static double *WEX;
	///@}

	/**
	 * @brief An approximation of the eta function
	 * @param x
	 * @param n1 Stage-1 sample size
	 * @param alpha1 Type-I error rate
	 * @param k Number of Systems
	 * @return
	 */
	static double f_eta_approx(double x, int n1, double alpha1, long k);

	/**
	 * @brief A precise evaluation of the eta function using Gauss-Laguerre Quadrature
	 * @param x
	 * @param n1 Stage-1 sample size
	 * @param alpha1 Type-I error rate
	 * @param k Number of Systems
	 * @return
	 */
	static double f_eta_integral(double x, int n1, double alpha1, long k) ;

public:
	/**
	 * @brief Computes the Gamma function using Lanczos approximation
	 *
	 * @param x
	 *
	 * @return Gamma(x)
	 */
	static double la_gamma(double x);

	/**
	 * @brief Using binary search to find parameter eta
	 * @param N Stage-1 sample size
	 * @param alpha1 Type-I error rate
	 * @param nSys Number of systems
	 * @return
	 */
	static double find_eta(int N, double alpha1, long nSys) ;
};

#endif /* ETAFUNC_H_ */
