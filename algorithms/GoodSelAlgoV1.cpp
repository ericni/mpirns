/**
 * @file GoodSelAlgoV1.cpp
 *
 * @brief Defines a parallel SO algorithm with good selection guarantee [Ni et al., 2014]
 */
extern int bsize_GLOBAL, useTimeEst, s1max_GLOBAL, seed_GLOBAL;
extern int n0_GLOBAL, n1_GLOBAL, screenVersion, outputLevel, RB_global;
extern double delta_GLOBAL, burnin_param;

#include "GoodSelAlgoV1.h"
#include "RinottFunc.h"
#include "EtaFunc.h"

GoodSelAlgoV1::GoodSelAlgoV1() {
	//	alpha = 0.05; delta = 0.01; t0 = 10.; t=10.; num_systems_eliminated = 0; lambda = alpha/2.;
	//	nP = 2; a = 100.0; myRank = 0; S = 100; n0=5; rinott_h = 0.;
	this->currentSeed = new unsigned long [6];
	this->tempSeed    = new unsigned long [6];
	this->myRank = MPI::COMM_WORLD.Get_rank();	//get process rank
	this->nP = MPI::COMM_WORLD.Get_size();     	//get number of processes
}

GoodSelAlgoV1::~GoodSelAlgoV1() {
	delete [] currentSeed; delete [] tempSeed; problem->clear();
#if BEST_MAX > 0
	MPI_Type_free(&best_type);
	delete [] best_buffer;
#endif
	MPI_Type_free(&singleresult_type);
}

void GoodSelAlgoV1::initialize(SOProb * prob) {
	SOAlgorithm::initialize(prob);
	S_Total = problem->getNumSystems();
	alpha = 0.05;
	delta = delta_GLOBAL;
	n0 = n0_GLOBAL;
	n1 = n1_GLOBAL;
	bestmax = (nP-1 <= BEST_MAX || BEST_MAX == 0) ? (nP-1):BEST_MAX;
	eta = EtaFunc::find_eta(n1, alpha/2., S_Total);
	if (outputLevel > 0 && myRank == 0) cout << "eta = " << eta << endl;
	//	a  = 	((double)bmax-1.0)/2.0/delta
	//			*(pow(2.0-2.0*pow(1.0-alpha,1.0/((double)S_Total-1.0) ), -2.0/(bmax-1.0) ) -1.0);

	// a_u, upper bound in Hong(2006)
	//	a= 	((double)n0-1.0)/2.0/delta
	//			*(pow(1.0-1.0*pow(1.0-alpha,1.0/((double)S-1.0) ), -2.0/(n0-1.0) ) -1.0);
	bsize = bsize_GLOBAL;
	batchmax = s1max_GLOBAL / bsize;
	S2.resize(S_Total, 0.0); // every core keeps a copy of S2
	dummy = 0; worker_flag = 0;
	system_eliminated.clear(); system_eliminated.resize(S_Total, 0);
	num_systems_eliminated = 0;
	worker_last_batch_recvd = -1; worker_last_batch_screened = -1;
	screen_t = 0.0;
	//	CORE_FLAGS_MIN = TAGS_MAX + 1;
	//	CORE_FLAGS_MAX = CORE_FLAGS_MIN + (nP * MAX_SEND_RECV_GRP) - 1;
	//	SYS_FLAGS_MIN  = CORE_FLAGS_MAX + 1;
	//	SYS_FLAGS_MAX  = SYS_FLAGS_MIN  + (S_Total * MAX_SEND_RECV_GRP) - 1;
	Phase1_Batch_Sizes.resize(S_Total);
	Phase1_Max_Sizes.resize(S_Total);
	core_eliminated.clear(); core_eliminated.resize(nP-1, 0);
	Master_Phase1_Best_Results_ID.clear(); Master_Phase1_Best_Results_ID.resize(nP-1,-1);

#if BEST_MAX > 0
	int _max = (batchmax + 1) * bestmax;
	best_buffer = new BESTRESULT_T [_max];
	for (int m1 = 0; m1 <= batchmax; ++m1) {
		for (int m2 = 0; m2 < bestmax; ++m2) {
			int j = m1 * bestmax + m2;
			best_buffer[j].id = -1;
			best_buffer[j].Xbar = ( -1. - bestmax + m2 ) * this->minmax;
			best_buffer[j].size = -1;
		}
	}
	MPI_Aint 	 offsets[2], extent;
	int			 blocks[2];
	MPI_Datatype oldtypes[2];
	MPI_Type_extent(MPI_DOUBLE, &extent);
	offsets	[0] = 0;
	offsets	[1] = extent;
	blocks	[0] = 1;
	blocks	[1] = 2;
	oldtypes[0] = MPI_DOUBLE;
	oldtypes[1] = MPI_INT;
	MPI_Type_struct(2, blocks, offsets, oldtypes, &best_type);
	MPI_Type_commit(&best_type);
#endif

	MPI_Aint 	 offsets1[2], extent1;
	int			 blocks1[2];
	MPI_Datatype oldtypes1[2];
	MPI_Type_extent(MPI_INT, &extent1);
	offsets1	[0] = 0;
	offsets1	[1] = extent1 * 2;
	blocks1		[0] = 2;
	blocks1		[1] = 3;
	oldtypes1	[0] = MPI_INT;
	oldtypes1	[1] = MPI_DOUBLE;
	MPI_Type_struct(2, blocks1, offsets1, oldtypes1, &singleresult_type);
	MPI_Type_commit(&singleresult_type);

	if (myRank > 0) {
		worker_phase1_results_keep.resize(S_Total);
		return;
	}

	// the following are used by master only
	Master_Phase1_Best_Results.clear(); Master_Phase1_Best_Results.resize(nP-1);
	master_phase1_current_batch = 0;
	master_phase1_current_sys   = S_Total - 1;
	//	Master_best_answers.clear(); Master_best_answers.resize(nP-1);
	//	Master_batches_sent_each_worker.clear();
	Master_last_batch_screened_each_worker.clear();
	//	Master_batches_sent_each_worker.resize(nP-1, 0);
	Master_last_batch_screened_each_worker.resize(nP-1, -1);
	//	Master_batches_recv_count.resize(nP-1);
	Master_num_elim_each_core.clear();	Master_num_elim_each_core.resize(nP-1,0);
	Master_simcount_phases.clear(); 	Master_simcount_phases.resize(4,0);
	Master_timing_phases.clear(); 	Master_timing_phases.resize(4,0);
	//	if (outputLevel > 0)
	//		cout << "n0 = " << n0 << endl;

	Simulation_Times.resize(S_Total, 0.0);
	Master_elim_sys_idx.resize(S_Total);
	Master_tmp_elim_sys_simcount.resize(S_Total);
	Worker2ReqID.resize(nP - 1);
	WtoMmsg.resize(nP - 1);
	//	//	master_tmp_elim_sys_idx.resize(S_Total, 0);
	//	deque<SOAnswer> dso; dso.clear();
	Master_Results.resize(S_Total);
	Master_Temp_Results.resize(S_Total);
	list<int> lst;
	this->Core2SysLists.clear(); this->Core2SysLists.resize(nP-1, lst);
	//	this->Sys2CoreLists.clear(); this->Sys2CoreLists.resize(S_Total, l);

	for (int i = 0; i < 6; i++) currentSeed[i] = (unsigned long) seed_GLOBAL;
	for (int i = 0; i < 6; i++)    tempSeed[i] = 0;
	if (SORngStream::SetPackageSeed(currentSeed)==false)
		cerr << "Error seed for Master routine" << endl;
	this->pMasterStream = new SORngStream();
	ReadyRecvRequests.resize(nP-1);
}

void GoodSelAlgoV1::run() {
	if (myRank == 0) {
		// the master core creates seeds and solutions, sends to workers
		Master_Routine();
	}
	else {
		// the worker cores receive seeds and run simulation
		Worker_Routine();
	}
}

void GoodSelAlgoV1::Master_Routine() {
	t0 = MPI::Wtime();

#ifdef TEST_CONFIGURES
	problem->readSystem();
#endif

	if(outputLevel > 1)
		cout << toString_Param();

	if(useTimeEst) {
		// Phase 00
		Master_Phase00_PrepareList();
		Master_Phase00_0_Send(true);
		Master_Phase00_0_Recv(true);
		// End Phase 00
		// Phase 0
		Master_Phase0_PrepareList();
		Master_Phase00_0_Send(false);
		Master_Phase00_0_Recv(false);
	}
	else {
		Master_Phase00_PrepareList();
		Master_Phase00_0_Send(false);
		Master_Phase00_0_Recv(false);
	}

	// Phase 1
	if (screenVersion == 5 || screenVersion == 6) {
		//		Master_report_phase0();
		if (outputLevel >= 1)
			this->Master_PrintPhase0Summary();
	}
	else {
		Master_Phase1_Initial_Send();
		Master_Phase1_PrepareList();
		//		Master_Phase1_Send();
		//Phase 2

		Master_Phase1_Execute();
		Master_Phase1_Clean();

		Master_Phase2_Execute();

		this->report_winner();
		//	Master_PrintSummary();
	}
	//stop here
	//	int stop = 0;
	//	for (int j = 1; j <= nP-1; j++ )
	//		MPI::COMM_WORLD.Send(&stop, 1, MPI::INT, j, flag_tag);
	delete this->pMasterStream;
	cout << "End of Master Routine" << endl;
}

void GoodSelAlgoV1::Master_Phase00_PrepareList() {
	//in phase 00, distribute systems randomly
	//perform Fisher-Yates shuffle to get a randomized core2syslist
	int tmp_i, tmp_j, s;
	rnd_perm.resize(S_Total);
	for (int i = 0; i < S_Total; i++) rnd_perm[i]=i;
	for (int i = S_Total - 1; i >= 1; i--) {
		// use the next substream of pMasterStream to generate a random integer between 0 and i
		tmp_j = pMasterStream->RandInt(0, i);
		tmp_i = rnd_perm[tmp_j]; rnd_perm[tmp_j] = rnd_perm[i]; rnd_perm[i] = tmp_i;
	}
	pMasterStream->ResetNextSubstream();

	int num_systems_per_core = S_Total/(nP-1);
	int num_systems_extra    = S_Total%(nP-1); // number of cores to get  num_systems_per_core+1 systems

	//compile core2syslist
	int curr_sys = 0;
	for (int i = 1; i < nP; ++i) {
		s = (i<=num_systems_extra)?(num_systems_per_core+1):(num_systems_per_core);
		for (int j = 1; j <= s; ++j){
			this->Core2SysLists[i-1].push_back(rnd_perm[curr_sys++]);
		}
	}

	if (outputLevel > 0)
		cout << "Phase 00 list prepared by Master" <<", t= " << this->getCurrentTime() << endl;
	//debug
	//	cout << this->toString_Core2Sys() << endl;
}

void GoodSelAlgoV1::Master_Phase0_PrepareList() {
	//in phase 0, distribute systems evenly according to the runtime
	for (int i = 0; i<Core2SysLists.size(); i++) Core2SysLists[i].clear();

	TotalSimulationTime = 0.0;
	for (int i = 0; i < S_Total; ++i) {
		TotalSimulationTime += Simulation_Times[i];
	}
	timePerCore = TotalSimulationTime / (nP-1); double cumTime = 0.0;
	int currCore = 1;
	//debug
	//	cout << "2" << endl;

	for (int i = 0; i < S_Total; ++i) {
		cumTime += Simulation_Times[i];
		if (cumTime > timePerCore * currCore) currCore += 1;
		Core2SysLists[min(nP-1,currCore) - 1].push_back(i);
	}
	//debug
	if (outputLevel >= 4)
		cout << this->toString_Core2Sys() << endl;
	if (outputLevel > 0)
		cout << "Phase 0 list prepared by Master" <<", t= " << this->getCurrentTime() << endl;
	this->Master_simcount_phases[1] = (double) ( S_Total * this->n1 );
}

void GoodSelAlgoV1::Master_Phase00_0_Send(bool phase00) {
	//given the Core2SysLists, send job to respective cores, each system simulated numRep times

	list<int>::iterator iter; int idx, temp_s, numRep = phase00?n0:n1;
	Master_simcount_phases[phase00?0:1] = (unsigned long) (S_Total * numRep);
	//send a go flag and the number of systems to each core
	int go = phase00?1:2;
	for (int i = 1; i <= nP-1; i++) {
		temp_s = Core2SysLists[i-1].size();
		MPI::COMM_WORLD.Send(&go,		1, MPI::INT, i, flag_tag);

		MPI::COMM_WORLD.Send(&temp_s,	1, MPI::INT, i, sys_tag);

		int v1 [temp_s];
		int v2 [temp_s];
		unsigned long seed_to_send[temp_s*6];
		int ss, k = 0;
		for (iter = Core2SysLists[i-1].begin(); iter!=Core2SysLists[i-1].end(); ++iter, ++k) {
			//create a new stream and get its seed
			SORngStream rs;
			rs.GetState(tempSeed);
			for (ss = 0; ss < 6; ss++) {
				seed_to_send[k*6+ss]=tempSeed[ss];
			}

			//get the next system to simulate
			//			vector<int> temp_disc = problem->Idx2System_Disc(*iter);

			//send seed to the worker
			//			MPI::COMM_WORLD.Send(tempSeed, 6, MPI::UNSIGNED_LONG, i, seed_tag);

			idx = *iter;
			v1[k] = idx;
			v2[k] = numRep;
			//send system index to the worker
			//			MPI::COMM_WORLD.Send(&idx, 1, MPI::INT, i, idx_tag);

			//send number of replications required for each system
			//			MPI::COMM_WORLD.Send(&numRep, 1, MPI::INT, i, size_tag);

			//send discrete decision variables to worker
			//			if (problem->prop.d_disc > 0)
			//				MPI::COMM_WORLD.Send(&temp_disc[0], problem->prop.d_disc,
			//						MPI::INT, i, x_disc_tag);

		}
		MPI::COMM_WORLD.Send(v1, temp_s, MPI::INT, i, idx_tag);
		MPI::COMM_WORLD.Send(v2, temp_s, MPI::INT, i, size_tag);
		MPI::COMM_WORLD.Send(seed_to_send, 6*temp_s, MPI::UNSIGNED_LONG, i, seed_tag);
		//		cout << "Master sending to core " << i << endl;
	}

	if (outputLevel > 0) {
		if (phase00)
			cout << "Phase 00 send finished by Master" <<", t= " << this->getCurrentTime() << endl;
		else
			cout << "Phase 0 send finished by Master"  <<", t= " << this->getCurrentTime() << endl;
	}
}



void GoodSelAlgoV1::Master_Phase00_0_Recv(bool phase00) {
	// send buffered "ready to receive" message to workers
	for (int i=1; i<= nP-1; ++i ) {
		ReadyRecvRequests[i-1] = MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,i, ready_master_tag);
	}
	for (int i=1; i<= nP-1; ++i ) ReadyRecvRequests[i-1].Wait();

	//receives Answers from cores
	int nRecv = 0, from, numSys;
	//	int count, nextIdx, nextSize;
	double nextFn, nextSumSq, nextTime, s;
	list<int>::iterator iter;
	MPI::Status status;

	while (nRecv < S_Total) {
		// iterate over cores to find the next core that has message to send
		if (MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE, sys_tag, status)) {
			from = status.Get_source();
			MPI::COMM_WORLD.Recv(&numSys,		1, MPI::INT	,	from, sys_tag);
			nRecv += numSys;
			if (phase00) {
				double master_times[numSys];
				MPI::COMM_WORLD.Recv(master_times,	numSys, MPI::DOUBLE,	from, runtime_tag);
				int k=0;
				for (iter = Core2SysLists[from-1].begin();
						iter!=Core2SysLists[from-1].end(); iter++, k++){
					Simulation_Times[*iter] = master_times[k];
					Master_timing_phases[0] += master_times[k];
				}
			} else {
				// this is not used since workers screen in phase 1, so use with_screening()
				double master_sumsq[numSys];
				double master_fn[numSys];
				double master_t[numSys];
				MPI::COMM_WORLD.Recv(master_fn,		numSys, MPI::DOUBLE,	from, fn_tag);
				MPI::COMM_WORLD.Recv(master_sumsq,	numSys, MPI::DOUBLE,	from, sumsq_tag);
				MPI::COMM_WORLD.Recv(master_t,		numSys, MPI::DOUBLE,	from, runtime_tag);
				int k=0;
				for (iter = Core2SysLists[from-1].begin();
						iter!=Core2SysLists[from-1].end(); iter++, k++){
					Master_timing_phases[1] += master_t[k];
					int i = *iter;
					Master_Results[i].resize(2); Master_Results[i][0].clear();
					Master_Results[i][0].addSample(n1, master_fn[k]*n1, master_sumsq[k]);
					Master_Results[i][1] = Master_Results[i][0];
					s = Master_Results[i][0].getSampleStdev();
					S2[i] = s*s;
					Master_Phase1_UpdateBestAnsBuffer(&Master_Results[i][0],
							i, 0);
				}
			}
			if (outputLevel > 0 ) {
				cout << "Received result from node " << setw(2) << setfill('0') << from
						<<  ", time: " << this->getCurrentTime()<< endl;
				cout << nRecv << " systems recvd\n";
			}
		} //end if
	} // end while
	if (outputLevel > 0) {
		if (phase00)
			cout << "Phase 00 recv finished by Master" <<", t= " << this->getCurrentTime() << endl;
		else
			cout << "Phase 0 recv finished by Master"  <<", t= " << this->getCurrentTime() << endl;
	}
}

void GoodSelAlgoV1::Master_Phase1_Initial_Send() {
	// in the beginning of Phase 1, master distributes
	list<int>::iterator iter; double s2;
	for (int i = 1; i < nP; ++i) {
		//send a flag to continue
		int go = 3;
		MPI::COMM_WORLD.Send(&go, 1, MPI::INT, i, flag_tag);
	}

	SORngStream rs;
	rs.GetState(tempSeed);
	MPI_Bcast(tempSeed,6,MPI_UNSIGNED_LONG,0,MPI_COMM_WORLD);
	MPI_Bcast(&S2[0],S_Total,MPI_DOUBLE,0,MPI_COMM_WORLD);
}

void GoodSelAlgoV1::Master_Phase1_PrepareList() {
	if (outputLevel > 0){
		cout << "Master preparing phase 1 list" << endl;
	}

	// in phase 1, determine a sequence of systems and their sample sizes
	// find the average variance
	double S_avg = 0.0;
	for (int i = 0; i < S_Total; ++i) {
		S_avg += sqrt(S2[i] / (useTimeEst?Simulation_Times[i]:1));
	}
	S_avg = S_avg/S_Total;
	ostringstream oss;
	//	cout << "aaa\n" ;
	for (int i = 0; i < S_Total; ++i) {
		Phase1_Batch_Sizes[i] = (int) ceil(sqrt(S2[i]/(useTimeEst?Simulation_Times[i]:1))/S_avg*bsize);
		Phase1_Max_Sizes[i] = Phase1_Batch_Sizes[i] * batchmax + n1;
		if (outputLevel > 0)
			oss << "Sys," << i << "," << S2[i] << "," << Phase1_Batch_Sizes[i] << endl;
	}
	if (outputLevel > 0 ) cout << oss.str();

	MPI_Bcast(&Phase1_Batch_Sizes[0],		S_Total,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&Phase1_Max_Sizes[0],	S_Total,MPI_INT,0,MPI_COMM_WORLD);

	for (int i = 0; i<Core2SysLists.size(); i++) Core2SysLists[i].clear();
	Sys2CoreList.resize(S_Total);
	Master_phase1_num_sys_each_core.resize(nP-1);

	//	cout << "aaa\n" ;
	//	//perform Fisher-Yates shuffle to get a randomized core2syslist
	int tmp_i, tmp_j, s; list<int>::iterator iter;
	rnd_perm.resize(S_Total);
	for (int i = 0; i < S_Total; i++) rnd_perm[i]=i;
	for (int i = S_Total - 1; i >= 1; i--) {
		// use the next substream of pMasterStream to generate a random integer between 0 and i
		tmp_j = pMasterStream->RandInt(0, i);
		tmp_i = rnd_perm[tmp_j]; rnd_perm[tmp_j] = rnd_perm[i]; rnd_perm[i] = tmp_i;
	}

	pMasterStream->ResetNextSubstream();

	//	cout << "aaa\n" ;
	//	cout << num_systems_remain_after_phase0 << endl;

	MPI_Bcast(&S_Total,1,MPI_INT,0,MPI_COMM_WORLD);
	int perms[S_Total];
	int num_systems_per_core = (S_Total)/(nP-1);
	int num_systems_extra    = (S_Total)%(nP-1);
	//compile core2syslist
	int perm_sys = 0, curr_sys = 0;
	for (int w = 1; w < nP; ++w) {
		//		cout << i  << endl;
		s = (w<=num_systems_extra)?(num_systems_per_core+1):(num_systems_per_core);
		Master_phase1_num_sys_each_core [w-1] = s;
		//distribute systems
		for (int j = 1; j <= s; ++j){
			// skip eliminated systems
			while(this->system_eliminated[rnd_perm[curr_sys]]) curr_sys += 1;
			this->Core2SysLists[w-1].push_back(rnd_perm[curr_sys]);
			this->Sys2CoreList[rnd_perm[curr_sys]] = w;
			//			this->Sys2CoreLists[rnd_perm[curr_sys  ]].push_back(i-1);
			perms[perm_sys] = rnd_perm[curr_sys];
			curr_sys += 1; perm_sys += 1;
		}
		//		Master_batches_recv_count[i-1].clear();
		//		Master_batches_recv_count[i-1].push_back(s);
	}
	MPI_Bcast(perms,S_Total,MPI_INT,0,MPI_COMM_WORLD);

	if (outputLevel > 0) {
		cout << "Phase 1 list prepared by Master" <<", t= " << this->getCurrentTime() << endl;
		cout << S_Total << " systems remain after phase 0\n";
		if (outputLevel >= 4)
			cout << this->toString_Core2Sys();
	}
}

void GoodSelAlgoV1::Master_Phase1_Execute() {
	// Master Phase 1 routine
	// keep watching message queues, receive message and send the most recent
	// "best system from other cores" list to the workers

	Cores_Hibernated.clear();
	Cores_Hibernated.resize(nP-1, 0);
	//
	// send buffered "ready to receive" message to workers
	int exec, from, flag = 4;
	int numNewBatchesAvail = 0;
	num_core_terminated = 0;
	for ( int i = 1; i < nP; ++i ) {
		//send flag 4 to kick off phase 1 simulation at workers
		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,i, flag_tag);
	}

	MPI::Request * reqs_new;
	reqs_ptr = new MPI::Request[nP-1];
	//	MPI::Request reqs[nP-1];
	for ( int i = 1; i < nP; ++i ) {
		// post irecv request
		//		reqs[i-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[i-1], 1, MPI::INT, i, ready_worker_tag);
		reqs_ptr[i-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[i-1], 1, MPI::INT, i, ready_worker_tag);
		Worker2ReqID[i-1] = i;
	}

	//	Master_Recvd_Workers_List.clear();
	MPI::Status status;

	while (this->num_systems_eliminated < S_Total - 1 && this->num_core_terminated < nP - 1 ) {
		flag = 5;
		MPI::Request::Waitany(nP-1, reqs_ptr, status);
		from = status.Get_source();
		if (!(from > 0 && from < nP)) {
			ostringstream oss1;
			oss1 << "From worker " << from << ", core terminated " << num_core_terminated << endl;
			cout << oss1.str();
			assert(from > 0 && from < nP);
		}
		this->Master_Phase1_SendContFlag(from);
		if (outputLevel >= 3){
			ostringstream oss1;
			oss1 << "Master done waiting at t = " << this->getCurrentTime() << ", source = " << from << endl;
			if (from < 1 || from > nP-1) {
				ostringstream oss;
				oss << "error 1, from = " << from << endl;
				cout << oss.str();
			} else {
				cout << oss1.str();
			}
		}

		switch (WtoMmsg[from-1]) {
		case 1:
			// receive answer
			Master_Phase1_RecvResult(from);
			break;
		case 2:
			Master_Phase1_RecvScreening(from);
			break;
		case 0:
			break;
		}
		// if all systems but one are eliminated, terminate the worker
		if (this->num_systems_eliminated >= S_Total - 1) {
			exec = 0;
			this->Master_Phase1_SendTerminateExec(from);
			this->num_core_terminated++;
		} else {
			// if screening up to batch batchmax have completed on the worker
			// or if all systems on the worker have been eliminated
			if (Master_last_batch_screened_each_worker[from-1] == batchmax ||
					Master_num_elim_each_core[from-1] == Master_phase1_num_sys_each_core[from-1]) {
				// 0=there exists some system to be simulated
				dummy = Master_Phase1_findNextSimSystem();
				if(outputLevel >= 3) {
					int disp = dummy < 0 ? dummy : master_phase1_current_sys;
					ostringstream oss;
					oss << "Master finds next simulation system in Phase 1 to be "
							<< disp << ", worker screened all\n";
					cout << oss.str();
				}
				if (dummy == 0) {
					exec = 1;
					Master_Phase1_SendSimExec(from);
					Master_Phase1_SendSimSys(from);
				}
				// else, terminate the worker
				else {
					exec = 0;
					this->Master_Phase1_SendTerminateExec(from);
					this->num_core_terminated++;
				}
			}
			else
			{
				numNewBatchesAvail = Master_Phase1_getNumNewBatchesAvail(from);
				// if new answers are available for screening
				if (numNewBatchesAvail > 0) {
					exec = 2;
					this->Master_Phase1_SendScreenExec(from);
					Master_Phase1_SendScreenAns(from, numNewBatchesAvail);
				}
				else
				{
					// 0=there exists some system to be simulated
					dummy = Master_Phase1_findNextSimSystem();
					if(outputLevel >= 3) {
						int disp = dummy < 0 ? dummy : master_phase1_current_sys;
						ostringstream oss;
						oss << "Master finds next simulation system in Phase 1 to be " << disp << "\n";
						cout << oss.str();
					}
					if (dummy == 0) {
						exec = 1;
						this->Master_Phase1_SendSimExec(from);
						Master_Phase1_SendSimSys(from);
					}
					// else, set worker to hibernate
					else
					{
						if(outputLevel >= 3) {
							ostringstream oss;
							oss << "Master putting worker " << from << " to hibernate\n";
							cout << oss.str();
						}
						exec = 0;
						Cores_Hibernated[from-1] = 1;
					}
				}
			}
		}
		Master_Phase1_MoveReq(from, exec);
	}

	if (outputLevel >=3 ) {
		ostringstream os;
		os << "Master terminate the hibernated cores when bmax is reached\n";
		cout << os.str();
	}

	Master_Phase1_TerminateWorkers();

	delete[] reqs_ptr;

	if (outputLevel >=3 ) {
		ostringstream os;
		os << "End of Master Phase 1 Exec.\n";
		cout << os.str();
	}
}

void GoodSelAlgoV1::Master_Phase1_SendContFlag(const int & from) {
	int flag = 5;
	MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	from, flag_tag);
}


void GoodSelAlgoV1::Master_SendExec(const int & from, const int & exec) {
	MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,	from, exec_tag);
}

void GoodSelAlgoV1::Master_Phase1_SendSimExec(const int & from) {
	Master_SendExec(from, 1);
}

void GoodSelAlgoV1::Master_Phase1_SendTerminateExec(const int & from) {
	Master_SendExec(from, 0);
}

void GoodSelAlgoV1::Master_Phase1_SendScreenExec(const int & from) {
	Master_SendExec(from, 2);
}

void GoodSelAlgoV1::Master_Phase1_TerminateWorkers() {
	// terminate the hibernated cores when bmax is reached
	// and some system(s) survive phase 1
	MPI::Status status;
	int flag, from, exec = 0;
	for (from = 1; from <= nP-1; ++from) {
		if(Cores_Hibernated[from-1]) {
			Cores_Hibernated[from-1] = 0;
			MPI::COMM_WORLD.Send(&exec,	1, MPI::INT, from, exec_tag);
			++num_core_terminated;
			assert(Master_last_batch_screened_each_worker[from-1] == batchmax);
		}
	}

	if (outputLevel >=3 ) {
		ostringstream os;
		os << "Master terminate other cores when the best system is found before bmax is reached\n";
		cout << os.str();
	}
	// terminate other cores when the best system is found before bmax is reached
	while (num_core_terminated < nP-1) {
		MPI::Request::Waitany(nP-1, reqs_ptr, status);
		from = status.Get_source();
		if (from < 1 || from > nP-1) {
			ostringstream oss;
			oss << "error 111, from = " << from << endl;
			cout << oss.str();
		}
		flag = 6;
		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	from, flag_tag);
		reqs_ptr[Worker2ReqID[from-1]-1] = MPI::REQUEST_NULL;
		++num_core_terminated;
		if (outputLevel >= 3) {
			ostringstream oss1;
			oss1 << "Master sending termination instruction to core " << from << ", t = " << this->getCurrentTime() << endl;
			cout << oss1.str();
		}
	}
}

void GoodSelAlgoV1::Master_Phase1_MoveReq(const int & from, const int & exec) {
	int w;
	MPI::Request * reqs_new = new MPI::Request[nP-1];
	w = 1;
	while (w < nP - 1) {
		if (w >= Worker2ReqID[from-1]) {
			reqs_new[w-1] = reqs_ptr[w];
		} else {
			reqs_new[w-1] = reqs_ptr[w-1];
		}
		w++;
	}
	for (w = 0; w < nP; w++) {
		if (Worker2ReqID[w] > Worker2ReqID[from-1])
			Worker2ReqID[w] -= 1;
	}
	if (exec > 0) {
		reqs_new[nP - 2] = MPI::COMM_WORLD.Irecv(&WtoMmsg[from-1], 1,
				MPI::INT, from, ready_worker_tag);
	} else {
		if(outputLevel >= 3) {
			ostringstream oss;
			oss << "New NULL request for worker " << from << endl;
			cout << oss.str();
		}
		reqs_new[nP - 2] = MPI::REQUEST_NULL;
	}
	Worker2ReqID[from-1] = nP - 1;
	delete [] reqs_ptr;
	reqs_ptr = reqs_new;
	if (outputLevel >= 3){
		ostringstream os;
		os << "Req_workers: ";
		for (int ww = 0; ww < nP-1; ww++) {
			os << Worker2ReqID[ww] <<" ";
		}
		os << endl;
		cout << os.str();
	}
}

void GoodSelAlgoV1::Master_Phase2_Execute() {
	if(outputLevel >= 2)
		cout << "================== Master Phase 2 ==================\n";
	num_systems_remain_after_phase1 = S_Total - this->num_systems_eliminated;
	int flag = ( this->num_systems_eliminated >= this->S_Total - 1 )? 0 : 7;
	for (int i = 1; i <= nP-1; ++i)
		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	i, flag_tag);
	if (flag == 0) return;

	// gather number of surviving systems from each core
	Master_Phase2_Num_Sys.resize(nP);
	int master_dummy_count = 0;
	MPI_Gather(&master_dummy_count, 1, MPI_INT,
			&Master_Phase2_Num_Sys[0], 1, MPI_INT,
			0, MPI_COMM_WORLD);
	assert(Master_Phase2_Num_Sys[0] == 0);

	// check number of surviving systems is correct
	Master_Phase2_nSys = 0;
	// compute total number of surviving systems
	for (int i = 1; i <= nP-1; ++i) Master_Phase2_nSys += Master_Phase2_Num_Sys[i];
	assert(Master_Phase2_nSys == S_Total - num_systems_eliminated);
	Master_Phase2_SentSize.clear();
	Master_Phase2_ID2Order.clear();
	Master_Phase2_ID.resize(Master_Phase2_nSys);
	Master_Phase2_Mean.resize(Master_Phase2_nSys);
	Master_Phase2_Phase1Size.resize(Master_Phase2_nSys);
	Master_Phase2_AdditionalSize.resize(Master_Phase2_nSys);
	Master_Phase2_SentSize.resize(Master_Phase2_nSys, 0);
	Master_Phase2_TotalSize.resize(Master_Phase2_nSys);
	Master_Phase2_ID2Order.resize(S_Total, -1);

	// compute Rinott constant
	rinott_h = RinottFunc::rinott(S_Total, 1. - alpha/2., n1 - 1);

	if(outputLevel >= 3) {
		ostringstream oss;
		oss << "Number of systems in Stage 3 = " << Master_Phase2_nSys << endl;
		oss << "rinott_h = " << rinott_h << endl;
		cout << oss.str();
	}
	// gather sys id, mean, sample sizes from all workers
	int displs [nP];
	displs[0] = 0;
	for (int i=1; i<nP; ++i)
		displs[i] = displs[i-1]+Master_Phase2_Num_Sys[i-1];

	MPI_Gatherv( NULL, 0, MPI_INT,
			&Master_Phase2_ID[0], &Master_Phase2_Num_Sys[0],
			displs, MPI_INT, 0, MPI_COMM_WORLD);

	MPI_Gatherv( NULL, 0, MPI_DOUBLE,
			&Master_Phase2_Mean[0], &Master_Phase2_Num_Sys[0],
			displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	MPI_Gatherv( NULL, 0, MPI_INT,
			&Master_Phase2_Phase1Size[0], &Master_Phase2_Num_Sys[0],
			displs, MPI_INT, 0, MPI_COMM_WORLD);

	int rinottSize, addn;
	// calculate additional sizes required to form Rinott sample
	for (int i = 0; i < Master_Phase2_nSys; ++i) {
		rinottSize = (int) ceil( rinott_h * rinott_h *
				S2[ Master_Phase2_ID[i] ] / (delta * delta) );
		assert(Master_Phase2_Phase1Size[i] ==
				batchmax * Phase1_Batch_Sizes[ Master_Phase2_ID[i] ] + n1);
		addn = rinottSize - Master_Phase2_Phase1Size[i];
		Master_Phase2_AdditionalSize[i] = (addn>0) ? addn:0;
		Master_Phase2_TotalSize[i] =
				Master_Phase2_AdditionalSize[i] + Master_Phase2_Phase1Size[i];
		Master_Phase2_ID2Order[ Master_Phase2_ID[i] ] = i;
	}

	if(outputLevel >= 3) {
		ostringstream oss;
		oss << "Master gathered sys id, mean, sample sizes from all workers in Phase 2\n";
		oss << "id,mean,S2,phase1size,additionalSize\n";
		for (int s = 0; s < Master_Phase2_nSys; ++s) {
			oss << Master_Phase2_ID[s] << "," << Master_Phase2_Mean[s] << "," <<
					S2[ Master_Phase2_ID[s] ] << "," <<
					Master_Phase2_Phase1Size[s] << "," <<
					Master_Phase2_AdditionalSize[s] <<  "\n";
		}
		cout << oss.str();
	}

	MPI::Request * reqs_new;
	reqs_ptr = new MPI::Request[nP-1];
	int w;
	//	MPI::Request reqs[nP-1];
	for ( int i = 1; i < nP; ++i ) {
		// post irecv request
		reqs_ptr[i-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[i-1], 1, MPI::INT, i, ready_worker_tag);
		Worker2ReqID[i-1] = i;
	}
	// end new
	num_core_terminated = 0;
	num_sys_completed_phase2 = 0;
	master_phase2_current_order = 0;
	int from, exec;
	MPI::Status status;
	bool cont = true;
	while (num_core_terminated < nP-1) {
		flag = 5;
		MPI::Request::Waitany(nP-1, reqs_ptr, status);
		from = status.Get_source();
		assert(from > 0 && from < nP);
		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	from, flag_tag);
		if (outputLevel >= 3){
			ostringstream oss1;
			oss1 << "Master done waiting in Phase 2 at t = " << this->getCurrentTime() << ", source = " << from << endl;
			if (from < 1 || from > nP-1) {
				ostringstream oss;
				oss << "error 1, from = " << from << endl;
				cout << oss.str();
			} else {
				cout << oss1.str();
			}
		}

		switch (WtoMmsg[from-1]) {
		case 3:
			// receive answer
			Master_Phase2_RecvResult(from);
			break;
		case 0:
			break;
		}
		// if all systems in phase 2 have been received, terminate the worker
		if (num_sys_completed_phase2 >= Master_Phase2_nSys) {
			exec = 0;
			MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,from, exec_tag);
			if(outputLevel >= 3) {
				ostringstream oss;
				oss << "Master sends termination to core " << from << " in phase 2\n";
				cout << oss.str();
			}
			this->num_core_terminated++;
		} else {
			// 0=there exists some system to be simulated
			dummy = Master_Phase2_findNextSimSystem();
			if(outputLevel >= 3) {
				ostringstream oss;
				oss << "Master finds next simulation system to be " << dummy << "\n";
				cout << oss.str();
			}
			if (dummy == 0) {
				exec = 3;
				MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,from, exec_tag);
				Master_Phase2_SendSimSys(from);
			}
			// else, terminate the worker
			else {
				exec = 0;
				MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,from, exec_tag);
				this->num_core_terminated++;
			}
		}
		//mark the worker as received
		reqs_new = new MPI::Request[nP-1];
		w = 1;
		while (w < nP - 1) {
			if (w >= Worker2ReqID[from-1]) {
				reqs_new[w-1] = reqs_ptr[w];
			} else {
				reqs_new[w-1] = reqs_ptr[w-1];
			}
			w++;
		}
		for (w = 0; w < nP; w++) {
			if (Worker2ReqID[w] > Worker2ReqID[from-1])
				Worker2ReqID[w] -= 1;
		}
		if (exec > 0) {
			reqs_new[nP - 2] = MPI::COMM_WORLD.Irecv(&WtoMmsg[from-1], 1,
					MPI::INT, from, ready_worker_tag);
		} else {
			reqs_new[nP - 2] = MPI::REQUEST_NULL;
		}
		Worker2ReqID[from-1] = nP - 1;
		delete [] reqs_ptr;
		reqs_ptr = reqs_new;
		if (outputLevel >= 3){
			ostringstream os;
			os << "Req_workers: ";
			for (int ww = 0; ww < nP-1; ww++) {
				os << Worker2ReqID[ww] <<" ";
			}
			os << endl;
			cout << os.str();
		}
	}
	//
	//	while (num_core_terminated < nP-1) {
	//		MPI::Request::Waitany(nP-1, reqs_ptr, status);
	//		from = status.Get_source();
	//		if (from < 1 || from > nP-1) {
	//			ostringstream oss;
	//			oss << "error 111, from = " << from << endl;
	//			cout << oss.str();
	//		}
	//		flag = 8;
	//		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	from, flag_tag);
	//		reqs_ptr[Worker2ReqID[from-1]-1] = MPI::REQUEST_NULL;
	//		++num_core_terminated;
	//		if (outputLevel >= 3) {
	//			ostringstream oss1;
	//			oss1 << "Master sending termination instruction to core " << from << ", t = " << this->getCurrentTime() << endl;
	//			cout << oss1.str();
	//		}
	//	}
	delete[] reqs_ptr;
	Master_Phase2_Find_Winner();
}

int GoodSelAlgoV1::Master_Phase1_findNextSimSystem() {
	if (master_phase1_current_batch > batchmax) return -1;
	// finds the next system to simulate
	master_phase1_current_sys++;
	// if the next system exceeds S_Total, increase batch count by 1
	if (master_phase1_current_sys == S_Total) {
		master_phase1_current_batch++;
		master_phase1_current_sys = 0;
		if (outputLevel >= 4) {
			cout << "Master start sending batch " << master_phase1_current_batch << endl;
		}
	}
	while (system_eliminated[rnd_perm[master_phase1_current_sys]]) {
		master_phase1_current_sys++ ;
		if (master_phase1_current_sys == S_Total) {
			master_phase1_current_batch++;
			master_phase1_current_sys = 0;
			if (outputLevel >= 4) {
				cout << "Master start sending batch " << master_phase1_current_batch << endl;
			}
		}
	}
	if (master_phase1_current_batch > batchmax) return -1;
	return 0;
}

void GoodSelAlgoV1::Master_Phase1_SendSimSys(const int & worker) {
	assert(worker > 0 && worker < nP);
	dummy = rnd_perm[master_phase1_current_sys];
	MPI::COMM_WORLD.Send(&dummy,	1, MPI::INT,worker, idx_tag);
	MPI::COMM_WORLD.Send(&master_phase1_current_batch,	1, MPI::INT,worker, batch_tag);
	this->Master_simcount_phases[2] += (unsigned long) (Phase1_Batch_Sizes[dummy]);
	if (outputLevel >= 3)
	{
		ostringstream oss;
		oss << "Master send sys " << dummy << " batch "<<
				master_phase1_current_batch << " to worker " << worker << endl;
		cout << oss.str();
	}
	return;
}

void GoodSelAlgoV1::Master_Phase1_RecvResult(const int & worker) {
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Master receiving answer from worker " << worker << endl;
		cout << oss.str();
	}
	int sysid, w_batch;
	double w_fn, w_sumsq, w_t;

	MPI_Recv(&tmpResult, 1, singleresult_type, worker, result_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	sysid = tmpResult.idx;
	w_batch = tmpResult.batch;
	w_fn = tmpResult.fn;
	w_sumsq = tmpResult.sumsq;
	w_t = tmpResult.runtime;

	//	MPI::COMM_WORLD.Recv(&sysid,	1, MPI::INT,	worker, idx_tag);
	//	MPI::COMM_WORLD.Recv(&w_batch,	1, MPI::INT,	worker, batch_tag);
	//	MPI::COMM_WORLD.Recv(&w_fn,		1, MPI::DOUBLE,	worker, fn_tag);
	//	MPI::COMM_WORLD.Recv(&w_sumsq,	1, MPI::DOUBLE,	worker, sumsq_tag);
	//	MPI::COMM_WORLD.Recv(&w_t,		1, MPI::DOUBLE,	worker, runtime_tag);

	Master_timing_phases[2] += w_t;
	// if system is already marked as eliminated, do nothing to collect the data
	if (this->system_eliminated[sysid])	return;

	int otherWorker = this->Sys2CoreList[sysid];
	Master_Phase1_StoreResult(otherWorker, sysid, w_batch, w_fn, w_sumsq);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master received answer from worker " << worker << " for sys " << sysid <<endl;
		cout << oss.str();
	}

	Master_Phase1_WakeWorker(otherWorker);
}

void GoodSelAlgoV1::Master_Phase1_StoreResult(const int & otherWorker,
		const int & sysid, const int & w_batch,
		const double & w_fn, const double & w_sumsq) {
	int sz;
	assert(otherWorker > 0 && otherWorker < nP);
	map<int, SOResult>::iterator it, it2;
	//	cout << "aaa\n";
	//	cout << w_sysidx << " " << w_batch << " " << Master_Answers[w_sysidx].size() <<
	//			" " << Master_batches_screened_each_worker[worker-1] << endl;
	sz = Master_Results[sysid].size() + Master_last_batch_screened_each_worker[otherWorker-1];
	if (sz == w_batch) {
		//		cout << "ccc\n";
		// if every batch prior to this received one is present, add the new batch to the back
		Master_Results[sysid].push_back(Master_Results[sysid].back());
		Master_Results[sysid].back().addSample(Phase1_Batch_Sizes[sysid],
				w_fn*Phase1_Batch_Sizes[sysid], w_sumsq);

#if BEST_MAX > 0
		int curr_batch = w_batch;
		Master_Phase1_UpdateBestAnsBuffer( &(Master_Results[sysid].back()),
				sysid, curr_batch );
#endif
		//		cout << "ddd\n";
		//		if (Master_batches_recv_count[worker-1].size() <= w_batch) {
		//			Master_batches_recv_count[worker-1].resize(w_batch+1, 0);
		//		}
		//		Master_batches_recv_count[worker-1][w_batch] += 1;
		sz++;

		//		cout << "eee\n";
		it = Master_Temp_Results[sysid].begin();
		// check if the temp_answers has any further batch to add
		while( !Master_Temp_Results[sysid].empty() && it->first <= sz) {
			//			cout << "ggg\n";
			assert( it->first == sz );
			Master_Results[sysid].push_back(Master_Results[sysid].back());
			sz++;
			Master_Results[sysid].back().addSample(it->second);
			Master_Temp_Results[sysid].erase(it);
			it = Master_Temp_Results[sysid].begin();
#if BEST_MAX > 0
			++curr_batch;
			Master_Phase1_UpdateBestAnsBuffer( &(Master_Results[sysid].back()),
					sysid, curr_batch );
#endif
		}
		//		cout << "fff\n";
	} else {
		//		cout << "bbb\n";
		// if w_batch > sz, that is, there is a 'gap' between collected answer and this batch, add
		// the batch to the Master_Temp_Answers map.
		if (!(w_batch > sz)) {
			cout << "w_batch " << w_batch << " sz " << sz << "  " << Master_Results[sysid].size() <<
					" " << Master_last_batch_screened_each_worker[otherWorker-1] << " sys " <<
					sysid << " belong to worker " << otherWorker << endl;
			assert (w_batch > sz);
		}
		assert(Master_Temp_Results[sysid].find(w_batch) == Master_Temp_Results[sysid].end());
		SOResult tmpans;
		tmpans.addSample(Phase1_Batch_Sizes[sysid], w_fn*Phase1_Batch_Sizes[sysid], w_sumsq);
		Master_Temp_Results[sysid][w_batch] = tmpans;
	}
}

void GoodSelAlgoV1::Master_Phase1_WakeWorker(const int & otherWorker ) {
	//if the received system belongs to a hibernated worker, check if the work can resume screening
	if( Cores_Hibernated[otherWorker-1] ) {
		int numBatches = Master_Phase1_getNumNewBatchesAvail(otherWorker);
		if(numBatches > 0) {
			int exec = 2;
			MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,	otherWorker, exec_tag);
			Master_Phase1_SendScreenAns(otherWorker, numBatches);
			assert (reqs_ptr[ Worker2ReqID[otherWorker-1]-1 ] == MPI::REQUEST_NULL);
			reqs_ptr[ Worker2ReqID[otherWorker-1]-1 ] =
					MPI::COMM_WORLD.Irecv(&WtoMmsg[otherWorker-1], 1,
							MPI::INT, otherWorker, ready_worker_tag);
			Cores_Hibernated[otherWorker-1] = 0;
			if(outputLevel >= 3) {
				ostringstream oss;
				oss << "Master wakes worker " << otherWorker << " from hibernate\n";
				cout << oss.str();
			}
		}
	}
}

void GoodSelAlgoV1::Master_Phase1_UpdateBestAnsBuffer( SOResult * result,
		const int & sysid, const int & currBatch){
#if BEST_MAX > 0
	// check if the answer is better than the worst in the bestans buffer
	if ( (result->getSampleMean() - best_buffer[currBatch * bestmax].Xbar) * this->minmax > 0 ) {
		int firstpos = currBatch * bestmax;
		int lastpos = (currBatch + 1) * bestmax - 1;
		pop_heap( best_buffer + ( firstpos ),
				best_buffer + ( lastpos + 1 ),
				greater_result() );
		best_buffer[ lastpos ].Xbar = result->getSampleMean();
		best_buffer[ lastpos ].id = sysid;
		best_buffer[ lastpos ].size = result->sample_size;
		push_heap( best_buffer + ( firstpos ),
				best_buffer + ( lastpos + 1 ),
				greater_result() );
	}
#endif
}

void GoodSelAlgoV1::Master_Phase1_SendScreenAns(const int & worker,
		const int & nNewBatches) {
	assert(worker > 0 && worker < nP);
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Master send screening answers to worker " << worker << ", " <<
				nNewBatches << " new batches" << endl;
		oss << "Worker has " <<Core2SysLists[worker-1].size() <<
				" sys remaining\n";
		cout << oss.str();
	}
	assert(Core2SysLists[worker-1].size()>0);
	list<int>::iterator it;

	if (outputLevel >= 4)	cout << "MM 1111\n";
	MPI::COMM_WORLD.Send(&nNewBatches,	1, MPI::INT,	worker, batch_tag);

	int nSys2Send = Core2SysLists[worker-1].size();
	MPI::COMM_WORLD.Send(&nSys2Send,	1, MPI::INT,	worker, sys_tag);
	int sys_idx_to_send	[nSys2Send];
	double fns_to_send  [nSys2Send*nNewBatches];
	double sumsq_to_send[nSys2Send*nNewBatches];
	int szs_to_send   	[nSys2Send*nNewBatches];
	int curr_batch, curr_sys = 0;
	SOResult *tmp_ans ;
	if (outputLevel >= 4)	cout << "MM 2222\n";
	for (it = Core2SysLists[worker-1].begin();
			it != Core2SysLists[worker-1].end(); it++) {
		assert(!this->system_eliminated[*it]);
		assert( (int)Master_Results[*it].size() >= nNewBatches );
		sys_idx_to_send[curr_sys] = *it;
		for (curr_batch = 0; curr_batch < nNewBatches; curr_batch++) {
			tmp_ans = &(Master_Results[*it][curr_batch+1]);
			//			tmp_ans = &(Master_Answers[*it][curr_batch+Master_batches_screened_each_worker[worker-1]+1]);
			fns_to_send  [curr_sys*nNewBatches+curr_batch]= tmp_ans->getSampleMean();
			sumsq_to_send[curr_sys*nNewBatches+curr_batch]= tmp_ans->getSampleSumsq();
			szs_to_send  [curr_sys*nNewBatches+curr_batch]= tmp_ans->sample_size;
		}
		curr_sys++;
		Master_Results[*it].erase (Master_Results[*it].begin(),
				Master_Results[*it].begin()+nNewBatches);
	}

	if (outputLevel >= 4)	cout << "MM 3333\n";
	Master_last_batch_screened_each_worker[worker-1] += nNewBatches;
	MPI::COMM_WORLD.Send(sys_idx_to_send,	nSys2Send, MPI::INT,	worker, idx_tag);
	MPI::COMM_WORLD.Send(fns_to_send,		nSys2Send*nNewBatches, MPI::DOUBLE,	worker, fn_tag);
	MPI::COMM_WORLD.Send(sumsq_to_send,	nSys2Send*nNewBatches, MPI::DOUBLE,	worker, sumsq_tag);
	MPI::COMM_WORLD.Send(szs_to_send,		nSys2Send*nNewBatches, MPI::INT,		worker, size_tag);

	if (outputLevel >= 4)	cout << "MM 4444\n";
	//	this->Master_Phase1_Recv_Best(worker);
	this->Master_Phase1_Send_OtherBest(worker);

	if (outputLevel >= 4)	cout << "MM 5555\n";
	return;
}

void GoodSelAlgoV1::Master_Phase1_RecvScreening(const int & worker) {
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Master receiving screening results from worker " << worker << endl;
		cout << oss.str();
	}
	Master_Phase1_Recv_Eliminated(worker);
#if BEST_MAX == 0
	Master_Phase1_Recv_Best(worker);
#endif
	//	Master_Phase1_Send_OtherBest(worker);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master finished receiving screening results from worker " << worker << endl;
		cout << oss.str();
	}
}

int GoodSelAlgoV1::Master_Phase1_getNumNewBatchesAvail(const int worker) {
	if (Core2SysLists[worker-1].size() == 0) return 0;
	int last_completed_batch = Master_last_batch_screened_each_worker[worker-1];
	list<int>::iterator it;
	int sz, minbatch = 99999;
	for (it = Core2SysLists[worker-1].begin();
			it != Core2SysLists[worker-1].end(); ++it) {
		sz = (int)Master_Results[*it].size()-1;
		minbatch = (minbatch > sz) ? sz : minbatch;
	}
	last_completed_batch += minbatch;
	//	bool isBatchComplete;
	//	do {
	//		isBatchComplete = true;
	//		for (it = Core2SysLists[worker-1].begin(); it != Core2SysLists[worker-1].end(); it++) {
	//			if (
	//					//	!system_eliminated[*it] &&
	//					(int)Master_Results[*it].size() +
	//					Master_batches_screened_each_worker[worker-1]  <=
	//					last_completed_batch + 1) {
	//				isBatchComplete=false;
	//				break;
	//			}
	//		}
	//		if (isBatchComplete)	last_completed_batch += 1;
	//	} while(isBatchComplete);
	assert(last_completed_batch <= batchmax);
	if (outputLevel >= 4) {
		ostringstream os1;
		os1 << "Worker " << worker << " has last completed " << last_completed_batch <<
				" screened " << Master_last_batch_screened_each_worker[worker-1] << endl;
		cout << os1.str();
	}
	return (last_completed_batch-Master_last_batch_screened_each_worker[worker-1]);
}
//
//void GoodSelAlgoV1::Master_Phase1_SendTermination(const int & worker) {
//	//	int exec = 0;
//	//	MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,worker, exec_tag);
//}

void GoodSelAlgoV1::Master_Phase1_Clean() {
	Master_Results.clear();
	Master_Temp_Results.clear();
	Master_Phase1_Best_Results_ID.clear();
	Master_Phase1_Best_Results.clear();
	Simulation_Times.clear();
	Cores_Hibernated.clear();
	Master_last_batch_screened_each_worker.clear();
	Master_num_elim_each_core.clear();
}

//void GoodSelAlgoV1::Master_Phase1_WrapUp() {
//	int from, flag; list<int>::iterator iter;
//	// send ready flag to remaining workers
//	for (iter=Master_Recvd_Workers_List.begin();iter!=Master_Recvd_Workers_List.end();iter++) {
//		from = *iter;
//		MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,from, ready_master_tag);
//	}
//	Master_Recvd_Workers_List.clear();
//
//	assert (this->num_systems_eliminated >= S_Total - 1);
//	flag = 6;
//	for (int i = 1; i < nP; ++i) {
//		if (!core_terminated[i-1]) {
//			MPI::COMM_WORLD.Recv(&dummy, 	1, MPI::INT,	i, ready_worker_tag);
//			MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	i, flag_tag);
//		}
//	}
//}
int GoodSelAlgoV1::Master_Phase2_findNextSimSystem() {
	if (master_phase2_current_order >= Master_Phase2_nSys) return -1;

	// finds the next system to simulate
	while(master_phase2_current_order < Master_Phase2_nSys &&
			Master_Phase2_SentSize[master_phase2_current_order] >=
			Master_Phase2_AdditionalSize[master_phase2_current_order] )
		++master_phase2_current_order;

	if(master_phase2_current_order >= Master_Phase2_nSys) return -1;

	master_phase2_current_size = Master_Phase2_AdditionalSize[master_phase2_current_order] -
			Master_Phase2_SentSize[master_phase2_current_order];
	master_phase2_current_size = ( master_phase2_current_size > bsize * 5 )?
			bsize * 5:master_phase2_current_size;
	assert(master_phase2_current_size > 0);
	Master_Phase2_SentSize[master_phase2_current_order] += master_phase2_current_size;
	return 0;
}

void GoodSelAlgoV1::Master_Phase2_SendSimSys(const int & worker) {
	assert(worker > 0 && worker < nP);
	dummy = Master_Phase2_ID[master_phase2_current_order];
	MPI::COMM_WORLD.Send(&dummy,	1, MPI::INT,worker, idx_tag);
	MPI::COMM_WORLD.Send(&master_phase2_current_size,	1, MPI::INT,worker, size_tag);
	this->Master_simcount_phases[3] += (unsigned long) (master_phase2_current_size);
	if (outputLevel >= 3)
	{
		ostringstream oss;
		oss << "Master send sys " << dummy << " of size "<<
				master_phase2_current_size << " to worker " << worker << endl;
		cout << oss.str();
	}
	return;
}

void GoodSelAlgoV1::Master_Phase2_RecvResult(const int & worker) {
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Master receiving answer in Phase 2 from worker " << worker << endl;
		cout << oss.str();
	}
	int sysid, w_size;
	double w_fn, w_t;
	//	map<int, SOAnswer>::iterator it, it2;

	MPI_Recv(&tmpResult, 1, singleresult_type, worker, result_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	sysid = tmpResult.idx;
	w_size = tmpResult.batch;
	w_fn = tmpResult.fn;
	w_t = tmpResult.runtime;

	//	MPI::COMM_WORLD.Recv(&sysid,	1, MPI::INT,	worker, idx_tag);
	//	MPI::COMM_WORLD.Recv(&w_size,	1, MPI::INT,	worker, size_tag);
	//	MPI::COMM_WORLD.Recv(&w_fn,		1, MPI::DOUBLE,	worker, fn_tag);
	//	MPI::COMM_WORLD.Recv(&w_t,		1, MPI::DOUBLE,	worker, runtime_tag);

	Master_timing_phases[3] += w_t;

	int order = Master_Phase2_ID2Order[sysid];
	assert(order >= 0);
	double old_fn = Master_Phase2_Mean[order];
	int old_sz = Master_Phase2_TotalSize[order];
	int new_sz = w_size + old_sz;
	double fn_new = ( old_fn * old_sz + w_fn * w_size ) / new_sz;
	Master_Phase2_TotalSize[order] = new_sz;
	Master_Phase2_Mean[order] = fn_new;

	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master received answer in Phase 2 from worker " <<
				worker << " for sys " << sysid << endl;
		cout << oss.str();
	}

	if (new_sz == Master_Phase2_AdditionalSize[order] + Master_Phase2_Phase1Size[order]) {
		++num_sys_completed_phase2;
		if (outputLevel >= 4) {
			ostringstream oss;
			oss << "Master finished receiving all answers for sys " <<
					sysid << " in Phase 2."<< endl;
			cout << oss.str();
		}
	}
}

void GoodSelAlgoV1::Master_Phase2_Find_Winner() {
	int i, bestorder = -1;
	double bestmean = -9e99 * this->minmax;
	for (i = 0; i < Master_Phase2_nSys; ++i) {
		if ( ( Master_Phase2_Mean[i] - bestmean ) * this->minmax > 0) {
			bestmean = Master_Phase2_Mean[i];
			bestorder = i;
			if (outputLevel > 3)
				cout << "i= " << i << ", bestmean = " << bestmean <<
				", bestorder = " << bestorder << endl;
		}
	}
	int bestid = Master_Phase2_ID[bestorder];
	if (outputLevel > 3)
		cout << "best system id = " << Master_Phase2_ID[bestorder] << endl;
	for (i = 0; i < Master_Phase2_nSys; ++i)
		if( bestid != Master_Phase2_ID[i] ) {
			system_eliminated[Master_Phase2_ID[i]] = true;
			++num_systems_eliminated;
		}
	if (outputLevel > 3)
		cout << num_systems_eliminated << "," << Master_Phase2_nSys <<
		"," << Master_Phase2_ID.size() << endl;
}

void GoodSelAlgoV1::Worker_Routine() {
	// the worker cores receive seeds and run simulation
	bool cont = true;
	map<int, SOResult>::iterator iter_ans;
#ifdef TEST_CONFIGURES
	problem->readSystem();
#endif

	//	worker_batches_simulated = 0;
	//	worker_phase0_results_keep.clear();

	do {
		//worker receives a flag from master
		MPI::COMM_WORLD.Recv(&worker_flag, 1, MPI::INT, 0, flag_tag);
		switch (worker_flag) {
		case 1:
			Worker_Phase00_0_Routine();
			break;
		case 2:
			Worker_Phase00_0_Routine();
			//			Worker_Phase0_Routine_with_Screening();
			break;

			//		case 2: {
			//			switch (screenVersion) {
			//			case 1:
			//			case 3:
			//			case 5:
			//				Worker_Phase00_0_Routine();
			//				break;
			//			case 2:
			//			case 4:
			//			case 6:
			//			case 8:
			//				Worker_Phase0_Routine_Version2_4();
			//				break;
			//			default:
			//				cerr << "Incorrect screen version, terminating..." << endl;
			//				return;
			//				break;
			//			}
			//			break;
			//		}
		case 3:
			// flag is 3: a signal to receive and initialize the system list for phase 1
			Worker_Phase1_Prepare();
			break;
		case 4:
			// flag is 4: a signal to execute the subroutine for phase 1
			Worker_Phase1_Execute();
			if (outputLevel >= 2) {
				ostringstream oss;
				oss << "Worker " << myRank << " terminating from Phase 1\n";
				cout << oss.str();
			}
			break;
		case 7:
			// flag is 7: enters Phase 2
			Worker_Phase2_Execute();
			return;
		case 0:
			return;
		default:
			cout << "Worker " << myRank <<" unexpectedly received flag "
			<< worker_flag << endl;
			break;
		}
		//		if (worker_flag == 1) {
		//			Worker_Phase00_0_Routine();
		//		} else if (worker_flag == 2) {
		//			if (screenVersion == 1 || screenVersion == 3 || screenVersion == 5) {
		//				Worker_Phase00_0_Routine();
		//			} else if (screenVersion == 2 || screenVersion == 4 || screenVersion == 6) {
		//				Worker_Phase0_Routine_Version2_4();
		//			} else {
		//				cerr << "Incorrect screen version, terminating..." << endl;
		//				return;
		//			}
		//		} else if (worker_flag == 3) {
		//			// flag is 3: a signal to receive and initialize the system list for phase 1
		//			Worker_Phase1_Prepare();
		//		} else if (worker_flag == 4){
		//			// flag is 4: a signal to execute the subroutine for phase 1
		//			Worker_Phase1_Execute();
		//			if (outputLevel >=2) {
		//				ostringstream oss;
		//				oss << "Worker " << myRank << " terminating from routine\n";
		//				cout << oss.str();
		//			}
		//			return;
		//		} else if (worker_flag == 0) {
		//			return;
		//		} else{
		//			cout << "Worker " << myRank <<" unexpectedly received flag "
		//					<< worker_flag << endl;
		//		}
	} while(worker_flag >= 0);
	//
	//	//	cout << "Worker " << myRank <<" received worker_flag " << worker_flag << endl;
	//
	//	// phase 1: receive system list and batch sizes from master
	//	if(worker_flag == 5)
	//		// flag is 5: a signal to receive and initialize the system list for phase 1
	//		Worker_Phase1_Prepare();
	//	else if (worker_flag == 0)
	//		return;
	//	else cout << "Worker " << myRank <<" did not receive flag 5 as expected" << endl;
	//
	//	//	MPI::COMM_WORLD.Recv(&flag, 1, MPI::INT, 0, flag_tag);
	//	//	cout << "Worker " << myRank <<" received flag " << flag << endl;
	//	//	return;
	//
	//	MPI::COMM_WORLD.Recv(&worker_flag, 	1, MPI::INT,    0, flag_tag);
	//	if(worker_flag == 4)
	//		// flag is 4: a signal to execute the subroutine for phase 1
	//		Worker_Phase1_Execute();
	//	else if (worker_flag == 0)
	//		return;
	//	else
	//		cout << "Worker " << myRank <<" did not receive flag 4 as expected" << endl;
	//


	return;

	/*
	// phase 2: simulate each system 1 time before sending back, while listening to master's updates
	if(flag == 2) {
		// phase 2 routine

		// receives the number of systems for the worker
		this->Worker_Recv_NumSys();
		s_curr = S_worker-1;
		//worker receives systems and seeds from master
		this->Worker_Recv_ListSys();
		// finish receiving systems and seeds from the master

		while (flag == 2) {
			bool more_message = true;
			while (more_message) {
				// iterate over cores to find the next core that has message to send
				if (MPI::COMM_WORLD.Iprobe(0 , flag_tag, status)) {
					// if there is a flag_tag message from the master, it must be an update to systems,
					// or a signal to stop
					MPI::COMM_WORLD.Recv(&flag, 1, MPI::INT,    0, flag_tag);
					if (flag == 2) {
						// a signal to begin receiving a new list of systems.

						// receives the number of systems for the worker
						this->Worker_Recv_NumSys();
						s_curr = S_worker-1;
						//worker receives systems and seeds from master
						this->Worker_Recv_ListSys();
						// finish receiving systems and seeds from the master
					} else {
						// a signal to stop. quite the inner and the outer while loop
						more_message = false;
					}
				} else more_message = false;
			}
			if (flag == 2) {
				// no more message and no stop flag, continue to simulate the next system once and send to master
				this->Worker_Simulate_Multiple_System();
			}
		}
	}

	//exit here when flag != 2, in which case phase 2 is completed
	return;
	 */
}

void GoodSelAlgoV1::Worker_Recv_NumSys() {
	//worker receives the number of systems to simulate from the master
	assert(this->myRank>0);
	MPI::COMM_WORLD.Recv(&S_worker, 1, MPI::INT, 0, sys_tag);
}

void GoodSelAlgoV1::Worker_Recv_ListSys() {
	//worker receives the list of systems and seeds from master
	//			unsigned long mySeed[s][6]; int idx[s];
	//			Matrix<int> init_disc(s, problem->prop.d_disc, 0);
	//			double * init_cont;
	//			int n[s];
	problem->clear();
	workerSeeds.clear(); workerSeeds.resize(S_worker, 6, 0);
	//	init_disc.clear(); init_disc.resize(S_worker, problem->prop.d_disc, 0);
	worker_system_idx.clear(); worker_system_idx.resize(S_worker, 0);
	worker_batch_sizes.clear(); worker_batch_sizes.resize(S_worker, 0);
	//	for (int j = 0; j < S_worker; j++) {
	//		MPI::COMM_WORLD.Recv(mySeed[j], 6, MPI::UNSIGNED_LONG, 0, seed_tag);
	//		MPI::COMM_WORLD.Recv(&worker_system_idx[j]  , 1, MPI::INT,           0, idx_tag);
	//		MPI::COMM_WORLD.Recv(&worker_batch_sizes[j]    , 1, MPI::INT,           0, size_tag);
	//receive discrete decision variables from master
	//		if (problem->prop.d_disc > 0) {
	//			MPI::COMM_WORLD.Recv(init_disc[j], problem->prop.d_disc, MPI::INT, 0, x_disc_tag);
	//		}

	//		//receive continuous decision variables from master
	//		if (problem->prop.d_cont > 0) {
	//			init_cont = new double [problem->prop.d_cont];
	//			MPI::COMM_WORLD.Recv(init_cont, problem->prop.d_cont, MPI::DOUBLE, 0, x_cont_tag);
	//		}
	//	}
	MPI::COMM_WORLD.Recv(&worker_system_idx[0]	, S_worker, MPI::INT,	0, idx_tag);
	MPI::COMM_WORLD.Recv(&worker_batch_sizes[0]	, S_worker, MPI::INT,	0, size_tag);
	MPI::COMM_WORLD.Recv(workerSeeds[0], 6*S_worker, MPI::UNSIGNED_LONG, 0, seed_tag);
	//	vector <int> v;
	//	for (int j = 0; j < S_worker; j++) {
	//		//compute discrete decision variable using index
	//		v = problem->Idx2System_Disc(worker_system_idx[j]);
	//		for (int k = 0 ; k < problem->prop.d_disc; k++) {
	//			init_disc[j][k] = v[k];
	//		}
	//	}
	//	if (outputLevel >= 3) {
	//		ostringstream oss;
	//		oss << "Worker " << myRank << " finished receiving \n";
	//		cout << oss.str();
	//	}

}
//
//void GoodSelAlgoV1::Worker_Recv_S2() {
//	// worker receives vector S2 from master
//	for (int k=0; k < S_Total; k++) {
//		MPI::COMM_WORLD.Recv(&S2[k], 1, MPI::DOUBLE, 0, s2_tag);
//	}
//}

void GoodSelAlgoV1::Worker_Phase00_0_Routine() {
	int sz;
	double fn, sumsq, simtime, s2;
	MPI::Status status; SOResult tmp_answer;
	// receives the number of systems for the worker
	this->Worker_Recv_NumSys();
	//worker receives systems and seeds from master
	this->Worker_Recv_ListSys();
	// finish receiving systems and seeds from the master

	// clear answers collected by worker
	if (worker_flag == 1) {
		//phase 0
		worker_phase00_send.clear();
		worker_phase00_send.resize(S_worker);
		worker_phase0_t_send.clear();
		worker_phase0_t_send.resize(S_worker);
		worker_phase00_s2_send.clear();
		worker_phase00_s2_send.resize(S_worker);
	} else if (worker_flag == 2){
		worker_phase00_send.clear();
		worker_phase0_fn_send.clear();
		worker_phase0_fn_send.resize(S_worker);
		worker_phase0_sumsq_send = worker_phase0_fn_send;
		worker_phase0_t_send.clear();
		worker_phase0_t_send.resize(S_worker);
	}
	//	worker_phase0_sys_sent = 0;

	// performs simulation, one replication for each system, and send results immediately back to the master
	//	bool message_received = false;
	for (int j=0; j < S_worker; j++) {
		//		double _t = MPI_Wtime();
		//simulate a single system
		this->Worker_Simulate_Single_System(j, worker_batch_sizes[j]);
		//		_t = MPI_Wtime() - _t;
		sz = worker_batch_sizes[j];
		fn = problem->getFn();
		sumsq = problem->getFnSumSq();
		simtime = problem->getSimulationTime();
		tmp_answer.clear(); tmp_answer.addSample(sz, fn*sz, sumsq, simtime);
		s2 = tmp_answer.getSampleStdev();
		s2 = s2 * s2;
		if ( outputLevel >= 4 ) {
			ostringstream oss;
			oss << "Simulated system " << worker_system_idx[j] <<
					",fn = " << fn << ",s2 = " << s2 << endl;
		}
		//		if (algo == 2)
		//		{
		if  (worker_flag == 1) {
			// phase 00
			worker_phase00_send[j] = simtime;
			//			worker_phase00_s2_send[j] = s2;
			//          worker_phase00_t_send[j] = _t;
		}
		else {
			// phase 0
			worker_phase0_fn_send[j] = fn;
			worker_phase0_sumsq_send[j] = sumsq;
			worker_phase0_t_send[j] = simtime;
		}
		//			worker_batch_send[worker_system_idx[j]] = tmp_answer;
		//		}
		//		else
		//		{
		//			Worker_Isend_Single_Answer(worker_system_idx[j], tmp_answer);
		//		}

		ostringstream oss;
		if (outputLevel >= 4) {
			oss << "Worker " << myRank << " finished its sys " << j << " out of " << S_worker << endl;
			cout << oss.str();
		}
	}//end for
	//	if (algo == 2){
	// after finishing simulating all the systems, send remaining answers whenever master
	// is ready to receive

	//		if (!worker_batch_send.empty()) {
	MPI::COMM_WORLD.Recv(&dummy,      1, MPI::INT,    0, ready_master_tag);
	Worker_Send_CurrentBatch();
	//		}
	//	}
}

//
//void GoodSelAlgoV1::Worker_Phase0_Routine_with_Screening() {
//	bool cont = true; dummy = 1; double s;
//	// receives the number of systems for the worker
//	Worker_Recv_NumSys();
//	//worker receives systems and seeds from master
//	Worker_Recv_ListSys();
//
//	MPI_Bcast(&Simulation_Times[0], S_Total, MPI_DOUBLE, 0, MPI_COMM_WORLD);
//
//	//	Worker_Initilize_Stages();
//	//	Worker_Simulate_Stage();
//	//	double S2_send[S_worker];
//	int   elim_send[S_worker];
//	worker_phase0_fn_send.resize(S_worker);
//	worker_phase0_sumsq_send = worker_phase0_fn_send;
//	int best_idx, best_sz; double fn0, best_ssq, best_st, best_fn=-1.0;
//	int best_idxs[nP];
//	int best_szs [nP];
//	double best_sts [nP];
//	double best_ssqs[nP];
//	double best_fns[nP];
//	worker_phase0_results_keep.resize(1);
//	worker_phase0_results_keep[0].resize(S_worker);
//	double Sttmp[S_worker];
//	double Sttmp2[S_Total];
//	for (int i = 0; i < S_worker ; ++i) {
//		this->Worker_Simulate_Single_System(i, worker_batch_sizes[i]);
//		dummy = worker_system_idx[i];
//		SOResult *ans = &(this->worker_phase0_results_keep[0][i]);
//		ans->clear();
//		ans->addSample(worker_batch_sizes[i],
//				worker_batch_sizes[i] * problem->getFn(),
//				problem->getFnSumSq());
//		s = ans->getSampleStdev();
//		S2[dummy] = s * s;
//		Sttmp[i] = s / (useTimeEst?sqrt(Simulation_Times[dummy]):1);
//		fn0 = ans->getSampleMean();
//		if (fn0 > best_fn) {
//			best_fn = fn0;
//			best_idx= dummy;
//			best_sz = ans->sample_size;
//			best_ssq= ans->getSampleSumsq();
//			best_st = s / (useTimeEst?sqrt(Simulation_Times[dummy]):1);
//		}
//	}
//	MPI::COMM_WORLD.Allgather(&best_fn , 1, MPI::DOUBLE, best_fns , 1,MPI::DOUBLE);
//	MPI::COMM_WORLD.Allgather(&best_idx, 1, MPI::INT   , best_idxs, 1,MPI::INT   );
//	MPI::COMM_WORLD.Allgather(&best_sz , 1, MPI::INT   , best_szs , 1,MPI::INT   );
//	MPI::COMM_WORLD.Allgather(&best_ssq, 1, MPI::DOUBLE, best_ssqs, 1,MPI::DOUBLE);
//	MPI::COMM_WORLD.Allgather(&best_st,  1, MPI::DOUBLE, best_sts, 1,MPI::DOUBLE);
//	int nSys[nP];
//	int displ[nP];
//	int d = 0;
//	MPI::COMM_WORLD.Allgather(&S_worker, 	  1, MPI::INT   , nSys,    1,MPI::INT   );
//	displ[0] = 0;
//	for (int dd = 1; dd < nP; ++dd) {
//		displ[dd] = displ[dd-1] + nSys[dd];
//	}
//	MPI::COMM_WORLD.Allgatherv(Sttmp, 	  S_worker, MPI::DOUBLE   , Sttmp2,    nSys, displ, MPI::DOUBLE   );
//	double S_avg;
//	for (int ss = 0; ss < S_Total; ++ss) {
//		S_avg += (Sttmp2[ss]);
//	}
//	S_avg /= S_Total;
//
//	map_worker_results_received.clear();
//	deque<SOResult> vans_temp;
//	//	SOAnswer ans_temp;
//	for (int i = 1; i <= nP-1; ++i) {
//		vans_temp.clear(); vans_temp.resize(1); vans_temp[0].clear();
//		vans_temp[0].addSample(best_szs[i], best_fns[i]*best_szs[i], best_ssqs[i]);
//		map_worker_results_received[best_idxs[i]]=vans_temp;
//		s = vans_temp[0].getSampleStdev();
//		S2[best_idxs[i]] = s * s;
//		Phase1_Batch_Sizes[best_idxs[i]] = (int) ceil(best_sts[i]/S_avg*bsize);
//	}
//	for (int i = 0; i < S_Total; ++i) {
//		Phase1_Batch_Sizes[i] = (int) ceil(sqrt(S2[i]/(useTimeEst?Simulation_Times[i]:1))/S_avg*bsize);
//		Phase1_Max_Sizes[i] = Phase1_Batch_Sizes[i] * batchmax + n1;
//	}
//	// so far, only one stage simulated: use skipping screening procedure will only screen with
//	// stage 0 information
//	this->worker_batches_simulated=1;
//	Worker_pairwise_screen_skipping();
//	int sys_left = S_worker - this->num_systems_eliminated;
//
//	// count the number of sys left in the system_eliminated vector to double check
//	int s_tmp = 0;
//	for (int i = 0; i < S_worker; ++i) {
//		if (!this->system_eliminated[this->worker_system_idx[i]]) {
//			s_tmp+=1;
//		}
//	}
//	assert(s_tmp == sys_left);
//
//	//report phase 0 results (including screening) back to master
//	SOResult *temp_ans; double fn, sumsq, simtime; int sz, temp_idx; bool isEliminated;
//	//	MPI::COMM_WORLD.Send(&dummy,		1, MPI::INT,0, ready_worker_tag);
//	MPI::COMM_WORLD.Isend(&S_worker,		1, MPI::INT,0, sys_tag);
//	for (int i = 0; i < S_worker; ++i) {
//		temp_idx = this->worker_system_idx[i];
//		temp_ans = &(this->worker_phase0_results_keep[0][i]);
//		//		simtime = temp_ans.getSampleTime();
//		//		sz = temp_ans.sample_size;
//		worker_phase0_sumsq_send[i] = temp_ans->getSampleSumsq();
//		worker_phase0_fn_send[i]    = temp_ans->getSampleMean();
//		elim_send[i] = this->system_eliminated[temp_idx];
//	}
//	MPI::COMM_WORLD.Send( &worker_phase0_fn_send[0]		, S_worker, MPI::DOUBLE, 0, fn_tag);
//	MPI::COMM_WORLD.Send( &worker_phase0_sumsq_send[0]	, S_worker, MPI::DOUBLE, 0, sumsq_tag);
//	MPI::COMM_WORLD.Send( elim_send	, S_worker, MPI::INT	, 0, elimidx_tag);
//}
void GoodSelAlgoV1::Worker_Phase1_Prepare() {
	if (outputLevel > 0) {
		ostringstream oss;
		oss << "worker " << myRank << " preparing phase 1" << endl;
		cout << oss.str();
	}

	// receives the seed from Master
	MPI_Bcast(tempSeed,6,MPI_UNSIGNED_LONG,0,MPI_COMM_WORLD);

	// receives the vector S2 from Master
	MPI_Bcast(&S2[0],S_Total,MPI_DOUBLE,0,MPI_COMM_WORLD);

	// receives the batch sizes from Master
	MPI_Bcast(&Phase1_Batch_Sizes[0],S_Total,MPI_INT,0,MPI_COMM_WORLD);

	// receives the max sizes from Master
	MPI_Bcast(&Phase1_Max_Sizes[0],S_Total,MPI_INT,0,MPI_COMM_WORLD);

	if (SORngStream::SetPackageSeed(tempSeed)==false)
		cerr << "Error seed for Worker routine\n";

	for (int i = 0; i <= myRank; ++i) {
		SORngStream rs;
	}

	SORngStream rs;
	WorkerStreams.resize(S_Total);

	for (int i = 0; i < S_Total; ++i) {
		rs.ResetNextSubstream();
		rs.GetState(tempSeed);
		WorkerStreams[i].SetSeed(tempSeed);
	}

	MPI_Bcast(&num_systems_remain_after_phase1,1,MPI_INT,0,MPI_COMM_WORLD);
	int perms[num_systems_remain_after_phase1];
	int num_systems_per_core = (num_systems_remain_after_phase1)/(nP-1);
	int num_systems_extra    = (num_systems_remain_after_phase1)%(nP-1);
	//	cout << "Worker" << num_systems_remain_after_phase0 << " " << num_systems_per_core << " " <<
	//			num_systems_extra << " "<< endl;
	MPI_Bcast(perms,num_systems_remain_after_phase1,MPI_INT,0,MPI_COMM_WORLD);
	int curr_sys = 0;
	for (int i = 1; i < myRank; ++i) {
		curr_sys += (num_systems_per_core+((i<=num_systems_extra)?1:0));
	}
	worker_system_idx.resize(num_systems_per_core+ ((myRank<=num_systems_extra)?1:0) );
	for(int i = 0; i < worker_system_idx.size(); ++i) {
		worker_system_idx[i] = perms[curr_sys + i];
	}
	if(outputLevel >= 4) {
		ostringstream oss;
		oss << "Worker " << myRank << " lists: " ;
		for (int i = 0; i < worker_system_idx.size(); i++) {
			oss << worker_system_idx[i] << ",";
		}
		oss << endl;
		cout << oss.str();
	}
	S_worker = worker_system_idx.size();
	//	worker_phase0_results_keep.clear();
	worker_phase1_results_keep.clear();
	worker_phase1_results_keep.resize(S_Total);

	if (outputLevel >= 3){
		ostringstream oss;
		oss << "worker " << myRank << " finishes initializing in phase 1\n";
		cout << oss.str();
	}
	if (screenVersion == 3 || screenVersion == 4 || screenVersion == 8) {
		worker_otherbest_batches_screened.clear();
		worker_otherbest_batches_screened.resize(S_worker);
	}
}

void GoodSelAlgoV1::Worker_Phase1_Execute() {
	bool cont = true;  MPI::Status status;
	//begins simulating batch by batch
	//worker_batches_toSend = 0;
	this->num_systems_eliminated = 0; // set to zero because a new set of systems arrive
	//	this->worker_phase0_results_keep.clear();
	this->worker_eliminated_system_to_send.clear();
	this->worker_eliminated_system_simcount.clear();
	this->worker_phase00_s2_send.clear();
	this->worker_phase00_send.clear();
	this->worker_phase0_fn_send.clear();
	this->worker_phase0_sumsq_send.clear();
	this->worker_phase0_t_send.clear();
	this->map_worker_results_received.clear();

	int flag, exec = 0;
	MPI::COMM_WORLD.Send(&exec, 1, MPI::INT, 0, ready_worker_tag);
	flag  = Worker_Phase1_RecvInitFlag();
	while(flag == 5 && cont) {
		switch(exec) {
		case 1:
			Worker_Phase1_SendResult();
			break;
		case 2:
			Worker_Phase1_SendScreening();
			break;
		case 0:
			break;
		}
		exec = Worker_Phase1_RecvExecute();
		switch(exec) {
		case 1:
			Worker_Phase1_RecvSimSys();
			break;
		case 2:
			Worker_Phase1_RecvScreenAns();
			break;
		case 0:
			cont = false;
			Worker_Phase1_Clean();
			return;
		}
		MPI::COMM_WORLD.Send(&exec, 1, MPI::INT, 0, ready_worker_tag);
		flag  = Worker_Phase1_RecvContFlag();
	}
	Worker_Phase1_Clean();
	return;
}

int GoodSelAlgoV1::Worker_Phase1_RecvInitFlag() {
	int flag = 0;
	MPI::COMM_WORLD.Recv(&flag,	1, MPI::INT, 0, flag_tag);
	return flag;
}

int GoodSelAlgoV1::Worker_Phase1_RecvContFlag() {
	int flag = 0;
	MPI::COMM_WORLD.Recv(&flag,	1, MPI::INT, 0, flag_tag);
	return flag;
}

int GoodSelAlgoV1::Worker_Phase1_RecvExecute() {
	int exec = 0;
	MPI::COMM_WORLD.Recv(&exec,	1, MPI::INT, 0, exec_tag);
	return exec;
}

void GoodSelAlgoV1::Worker_Phase2_Execute() {
	int num_remaining = S_worker - this->num_systems_eliminated;
	// send the number of remaining systems to the master
	MPI_Gather(&num_remaining, 1, MPI_INT,
			NULL, 1, MPI_INT,
			0, MPI_COMM_WORLD);

	int idx_i, i, num_remaining_count = 0;
	for ( idx_i = 0; idx_i < S_worker; idx_i++){
		i = worker_system_idx[idx_i];
		if(!this->system_eliminated[i])
			++num_remaining_count;
	}
	if (num_remaining_count != num_remaining) {
		ostringstream oss;
		oss << "num_remaining_count = " << num_remaining_count <<
				",num_remaining = " << num_remaining << endl;
		cout << oss.str();
		assert(num_remaining_count == num_remaining);
	}

	// compile a list of surviving systems, their means and sizes
	vector<int> w_ids;
	vector<double> w_means;
	vector<int> w_sizes;
	w_ids.resize(num_remaining);
	w_means.resize(num_remaining);
	w_sizes.resize(num_remaining);

	int j = 0;
	for ( idx_i = 0; idx_i < S_worker; idx_i++ ){
		i = worker_system_idx[idx_i];
		if(!this->system_eliminated[i]) {
			w_ids[j] = i;
			assert(worker_phase1_results_keep[i].size() == batchmax + 1);
			w_means[j] = worker_phase1_results_keep[i].back().getSampleMean();
			w_sizes[j] = worker_phase1_results_keep[i].back().sample_size;
			++j;
		}
	}

	if(outputLevel >= 3) {
		ostringstream oss;
		oss << "Master gathering info in phase 2\n";
	}
	// send the lists to the master
	MPI_Gatherv( &w_ids[0], num_remaining, MPI_INT,
			NULL, NULL,
			NULL, MPI_INT, 0, MPI_COMM_WORLD);

	MPI_Gatherv( &w_means[0], num_remaining, MPI_DOUBLE,
			NULL, NULL,
			NULL, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	MPI_Gatherv( &w_sizes[0], num_remaining, MPI_INT,
			NULL, NULL,
			NULL, MPI_INT, 0, MPI_COMM_WORLD);

	bool cont = true;
	int flag, exec = 0;
	MPI::COMM_WORLD.Send(&exec, 1, MPI::INT, 0, ready_worker_tag);
	MPI::COMM_WORLD.Recv(&flag,	1, MPI::INT, 0, flag_tag);
	while(flag == 5 && cont) {
		switch(exec) {
		case 3:
			Worker_Phase2_SendResult();
			break;
		case 0:
			break;
		}
		MPI::COMM_WORLD.Recv(&exec,	1, MPI::INT, 0, exec_tag);
		switch(exec) {
		case 3:
			Worker_Phase2_RecvSimSys();
			break;
		case 0:
			cont = false;
			Worker_Phase2_Clean();
			return;
		}
		MPI::COMM_WORLD.Send(&exec, 1, MPI::INT, 0, ready_worker_tag);
		MPI::COMM_WORLD.Recv(&flag,	1, MPI::INT, 0, flag_tag);
	}
	Worker_Phase2_Clean();
}

void GoodSelAlgoV1::Worker_Phase1_SendResult() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " send answer of sys " <<
				worker_phase1_sys << " batch " << worker_phase1_batch <<
				" to master\n";
		cout << oss.str();
	}
	tmpResult.idx = worker_phase1_sys;
	tmpResult.batch = worker_phase1_batch;
	tmpResult.fn = worker_phase1_fn;
	tmpResult.sumsq = worker_phase1_sumsq;
	tmpResult.runtime = worker_phase1_t;

	MPI_Send(&tmpResult, 1, singleresult_type, 0, result_tag, MPI_COMM_WORLD);

	//	MPI::COMM_WORLD.Send(&worker_phase1_sys, 	1, MPI::INT,	0, idx_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase1_batch,	1, MPI::INT,	0, batch_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase1_fn,		1, MPI::DOUBLE,	0, fn_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase1_sumsq,	1, MPI::DOUBLE,	0, sumsq_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase1_t,		1, MPI::DOUBLE,	0, runtime_tag);
}

void GoodSelAlgoV1::Worker_Phase1_SendScreening() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " send screening results to master\n";
		cout << oss.str();
	}
	Worker_Phase1_Send_Eliminated();
#if BEST_MAX == 0
	Worker_Phase1_Send_Best();
#endif
}

void GoodSelAlgoV1::Worker_Phase1_RecvSimSys() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " receiving a sys to simulate\n";
		cout << oss.str();
	}
	MPI::COMM_WORLD.Recv(&worker_phase1_sys  ,	1, MPI::INT,0, idx_tag);
	MPI::COMM_WORLD.Recv(&worker_phase1_batch,	1, MPI::INT,0, batch_tag);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " received sys " <<
				worker_phase1_sys << " batch " << worker_phase1_batch <<
				" from master to simulate\n";
		cout << oss.str();
	}
	worker_phase1_t = MPI_Wtime();
	problem->Initialize_Single_System(worker_phase1_sys,
			Phase1_Batch_Sizes[worker_phase1_sys], &WorkerStreams[worker_phase1_sys]);
	problem->Objective();

	worker_phase1_fn = problem->getFn();
	worker_phase1_sumsq = problem->getFnSumSq();
	worker_phase1_t = MPI_Wtime() - worker_phase1_t;
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " finished simulating sys " <<
				worker_phase1_sys << " batch " << worker_phase1_batch <<
				"\n";
		cout << oss.str();
	}
}

void GoodSelAlgoV1::Worker_Phase1_RecvScreenAns() {
	int numBatches, numSys, curr_batch, curr_sys;
	// receiving this worker's own systems
	MPI::COMM_WORLD.Recv(&numBatches,		1, MPI::INT,	0, batch_tag);
	MPI::COMM_WORLD.Recv(&numSys,	1, MPI::INT,	0, sys_tag);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << myRank  << " " << worker_last_batch_recvd << " WW 1111\n";
		oss << "this worker " << myRank << " has " << S_worker - this->num_systems_eliminated <<
				" Sys remaining\n";
		cout << oss.str();
	}
	assert (numSys == S_worker - this->num_systems_eliminated );
	int    sys_idx_to_recv[numSys];
	double fns_to_recv  [numSys*numBatches];
	double sumsq_to_recv[numSys*numBatches];
	int    szs_to_recv  [numSys*numBatches];
	MPI::COMM_WORLD.Recv(sys_idx_to_recv,	numSys, MPI::INT,	0, idx_tag);
	MPI::COMM_WORLD.Recv(fns_to_recv,	numSys*numBatches, MPI::DOUBLE,	0, fn_tag);
	if (outputLevel >= 4){
		cout << "WW 2222\n";
	}
	MPI::COMM_WORLD.Recv(sumsq_to_recv,	numSys*numBatches, MPI::DOUBLE,	0, sumsq_tag);
	MPI::COMM_WORLD.Recv(szs_to_recv,	numSys*numBatches, MPI::INT,		0, size_tag);
	if (outputLevel >= 4)	cout << "WW 3333\n";
	SOResult tmp_ans;
	tmp_ans.clear();
	for (curr_sys = 0; curr_sys < numSys; curr_sys++){
		for (curr_batch = 0; curr_batch < numBatches; curr_batch++) {
			worker_phase1_results_keep[sys_idx_to_recv[curr_sys]].push_back(tmp_ans);
			worker_phase1_results_keep[sys_idx_to_recv[curr_sys]].back().addSample(
					szs_to_recv[curr_sys * numBatches + curr_batch],
					fns_to_recv[curr_sys * numBatches + curr_batch]*
					szs_to_recv[curr_sys * numBatches + curr_batch],
					sumsq_to_recv[curr_sys * numBatches + curr_batch] );
		}
	}
	if (outputLevel >= 4)
		cout << "WW 4444\n";
	this->worker_last_batch_recvd += numBatches;
	assert(worker_last_batch_recvd <= batchmax);
	// receiving others' best
	Worker_Phase1_Recv_OtherBest();
	if (outputLevel >= 4) {
		cout << "WW 5555now\n";
	}
	screen_t = MPI_Wtime();
	// performs pairwise screening
	Worker_pairwise_screen_no_skipping();
	screen_t = MPI_Wtime() - screen_t;
	if (outputLevel >= 4)	cout << "WW 6666\n";
}

void GoodSelAlgoV1::Worker_Phase1_Clean() {
	//	this->worker_phase0_results_keep.clear();

	//do not clear this as phase 2 uses it
	//this->worker_phase1_Results_keep.clear();
	//this->worker_system_idx.clear();

	worker_phase00_send.clear();
	worker_phase00_s2_send.clear();
	worker_phase0_t_send.clear();
	map_worker_results_received.clear();
	worker_eliminated_system_to_send.clear();
	worker_eliminated_system_simcount.clear();
	worker_otherbest_batches_screened.clear();
	//	this->init_disc.clear();
	worker_batch_sizes.clear();
	if (outputLevel >= 2) {
		ostringstream oss;
		oss << "Worker " << myRank <<
				" received Phase 1 termination instruction from master" << endl;
		cout << oss.str();
	}
}

void GoodSelAlgoV1::Worker_Phase2_RecvSimSys() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " receiving a sys to simulate\n";
		cout << oss.str();
	}
	MPI::COMM_WORLD.Recv(&worker_phase2_sys,	1, MPI::INT,0, idx_tag);
	MPI::COMM_WORLD.Recv(&worker_phase2_size,	1, MPI::INT,0, size_tag);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " received sys " <<
				worker_phase2_sys << " batch " << worker_phase2_size <<
				" from master to simulate in Phase 2\n";
		cout << oss.str();
	}
	worker_phase2_t = MPI_Wtime();
	problem->Initialize_Single_System(worker_phase2_sys,
			worker_phase2_size, &WorkerStreams[worker_phase2_sys]);
	problem->Objective();
	worker_phase2_t = MPI_Wtime() - worker_phase2_t;
	worker_phase2_fn = problem->getFn();
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " finished simulating sys " <<
				worker_phase2_sys << " size " << worker_phase2_size <<
				"in Phase 2.\n";
		cout << oss.str();
	}
}

void GoodSelAlgoV1::Worker_Phase2_SendResult() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " send answer of sys " <<
				worker_phase1_sys << " batch " << worker_phase1_batch <<
				" to master\n";
		cout << oss.str();
	}

	tmpResult.idx = worker_phase2_sys;
	tmpResult.batch = worker_phase2_size;
	tmpResult.fn = worker_phase2_fn;
	tmpResult.sumsq = 0.0;
	tmpResult.runtime = worker_phase2_t;

	MPI_Send(&tmpResult, 1, singleresult_type, 0, result_tag, MPI_COMM_WORLD);

	//	MPI::COMM_WORLD.Send(&worker_phase2_sys, 	1, MPI::INT,	0, idx_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase2_size,	1, MPI::INT,	0, size_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase2_fn,	1, MPI::DOUBLE,	0, fn_tag);
	//	MPI::COMM_WORLD.Send(&worker_phase2_t,		1, MPI::DOUBLE,	0, runtime_tag);
}

void GoodSelAlgoV1::Worker_Phase2_Clean() {
	if (outputLevel >= 2) {
		ostringstream oss;
		oss << "Worker " << myRank <<
				" received Phase 2 termination instruction from master" << endl;
		cout << oss.str();
	}
}

void GoodSelAlgoV1::Worker_pairwise_screen_no_skipping() {
	// performs pairwise screening at the worker level
	if (outputLevel >= 4)	{
		cout << "WW 5555x\n";
	}
	if (outputLevel >= 3) {
		ostringstream o0, o1,o2,o3,o4;
		o0 << "Worker " << this->myRank << " begins pairwise screen\n" ;
		cout << o0.str().c_str();
		o1 <<"S_worker = " << this->S_worker << "\n";
		cout << o1.str().c_str();
		o2 << "num_systems_eliminated = " << this->num_systems_eliminated << "\n";
		cout << o2.str().c_str();
		o3 << "worker_batches_screened = " << this->worker_last_batch_screened << "\n";
		cout << o3.str().c_str();
		o4 << "worker_batches_recvd = " << this->worker_last_batch_recvd << "\n";
		cout << o4.str().c_str();
	}
	//	assert(S_worker == worker_system_idx.size());
	// return if all systems eliminated
	if (this->num_systems_eliminated == this->S_worker) {
		return;
	}
	if (this->num_systems_eliminated >= this->S_worker - 1 &&
			(int)this->map_worker_results_received.size() == 0) {
		return;
	}
	map<int, deque<SOResult> >::iterator iter_other;
	list<int>::iterator iter;
	double Xbar_i, Xbar_j, ni, nj, tau, Y, tij, aij;
	int idx_i, idx_j, i, j, otherBatch, int_ni, int_nj;
	int batch_screening;
	for ( idx_i = 0; idx_i < S_worker; idx_i++){
		i = worker_system_idx[idx_i];
		if (!(system_eliminated[i]) ) {
			if (outputLevel >= 4){
				ostringstream osss;
				osss << "Sys i " << i << endl;
				cout << osss.str();
			}
			for ( idx_j = 0; idx_j < S_worker; idx_j++){
				if (system_eliminated[i]) break;
				j = worker_system_idx[idx_j];
				if (i != j && !system_eliminated[j])  {
					if (outputLevel >= 4){
						ostringstream osss;
						osss << "Sys j " << j << endl;
						cout << osss.str();
					}
					for (batch_screening = worker_last_batch_screened + 1;
							batch_screening <= worker_last_batch_recvd; ++batch_screening) {
						if (system_eliminated[i]) break;
						Xbar_i = worker_phase1_results_keep[i][batch_screening].getSampleMean() * this->minmax;
						Xbar_j = worker_phase1_results_keep[j][batch_screening].getSampleMean() * this->minmax;

						if (outputLevel >= 4){
							ostringstream osss;
							osss << "batch_screening = " << batch_screening << endl;
							osss << "minmax = " << this->minmax << endl;
							osss << "Xbar_i = " << Xbar_i << endl;
							osss << "Xbar_j = " << Xbar_j << endl;
							cout << osss.str();
						}
						if (Xbar_j > Xbar_i) {
							ni = int_ni = worker_phase1_results_keep[i][batch_screening].sample_size;
							nj = int_nj = worker_phase1_results_keep[j][batch_screening].sample_size;
							tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
							Y = tau*(Xbar_i-Xbar_j);
							// old
							//							assert(int_ni % (batch_screening + 1) == 0);
							//							assert(int_nj % (batch_screening + 1) == 0);
							//							bi = ni / (batch_screening + 1);
							//							bj = nj / (batch_screening + 1);
							//							tij = bi*bj*bmax / (S2[i]*bj + S2[j]*bi);
							tij = 1. / ( S2[i] / Phase1_Max_Sizes[i] + S2[j] / Phase1_Max_Sizes[j]);
							aij = sqrt( (double) (tij * (n1-1)) ) * eta;
							if (Y < -aij) {
								// system i eliminated by system j
								if(outputLevel >= 4) {
									ostringstream oss;
									oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
											<< setw(7)	<< setfill('0')  << i <<","
											<< " on core " << myRank
											<< "; " << Xbar_j << "," << nj << "," << S2[j] <<
											"," << Xbar_i << "," << ni << "," << S2[i] <<
											endl;
									cout << oss.str();
								}
								this->system_eliminated[i] = 1;
								this->num_systems_eliminated += 1;
								worker_eliminated_system_to_send.push_back(i);
								worker_eliminated_system_simcount.push_back(ni);
							}
						}
					}

				}
			}
		}
	}

	if (outputLevel >= 4)  {
		ostringstream oss;
		oss << "Worker " << myRank << " done self screening\n";
		cout << oss.str();
	}
	// use best results from other workers to screen sys i
#if BEST_MAX == 0
	int ijscreened;
	map<int,int>::iterator it;
	//	cout << "111" << endl;
	if (!map_worker_results_received.empty()){
		for ( int idx_i = 0; idx_i < S_worker; idx_i++){
			i = this->worker_system_idx[idx_i];
			if (!(this->system_eliminated[i]) ) {
				if (outputLevel >= 4){
					ostringstream os;
					os << "Sys i " << i << endl;
					cout << os.str();
				}
				for ( iter_other = map_worker_results_received.begin();
						iter_other !=map_worker_results_received.end(); iter_other++ ) {
					if (this->system_eliminated[i]) break;
					j = iter_other->first;
					//					cout << "222" << endl;
					if (i != j) {
						if (outputLevel >= 4){
							ostringstream osss;
							osss << "Other Sys j " << j << endl;
							cout << osss.str();
						}
						//						cout << "555" << endl;
						otherBatch = min((int)iter_other->second.size()-1, worker_last_batch_recvd);
						//						cout << "otherbatch = " << otherBatch <<endl;
						//						cout << "666" << endl;
						if (otherBatch >= 0) {
							it = worker_otherbest_batches_screened[idx_i].find(j);
							if (it == worker_otherbest_batches_screened[idx_i].end()) {
								worker_otherbest_batches_screened[idx_i][j] = -1;
								ijscreened = -1;
							} else {
								ijscreened = it->second;
							}
							for ( batch_screening = ijscreened+1;
									batch_screening <= otherBatch; batch_screening++) {

								//	cout << "Batch " << batch_screening << endl;
								if (system_eliminated[i] ) break;
								Xbar_j = iter_other->second[batch_screening].getSampleMean() * this->minmax;
								Xbar_i = worker_phase1_results_keep[i][batch_screening].getSampleMean() * this->minmax;
								if (Xbar_j > Xbar_i) {
									ni = int_ni = worker_phase1_results_keep[i][batch_screening].sample_size;
									nj = int_nj = iter_other->second[batch_screening].sample_size;
									tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
									Y = tau*(Xbar_i-Xbar_j);
									// old
									//									assert(int_ni % (batch_screening + 1) == 0);
									//									assert(int_nj % (batch_screening + 1) == 0);
									//									bi = ni / (batch_screening + 1);
									//									bj = nj / (batch_screening + 1);
									//									tij = bi*bj*bmax / (S2[i]*bj + S2[j]*bi);

									tij = 1. / ( S2[i] / Phase1_Max_Sizes[i] + S2[j] / Phase1_Max_Sizes[j]);
									aij = sqrt( (double) (tij * (n1-1)) ) * eta;
									//								cout << "777" << endl;
									if (Y < -aij) {
										// system i eliminated by system j
										system_eliminated[i] = 1;
										num_systems_eliminated += 1;
										worker_eliminated_system_to_send.push_back(i);
										worker_eliminated_system_simcount.push_back(ni);
										if(outputLevel >= 4 || (outputLevel == 3 &&
												this->num_systems_eliminated >= S_worker - 1)) {
											ostringstream oss;
											oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
													<< setw(7)	<< setfill('0')  << i <<","
													<< " on core " << myRank
													<< " at batch " << batch_screening << " of "
													<< otherBatch << ", "
													<< "othersize=" << (int)iter_other->second.size()
													<< ", workersize=" << worker_last_batch_recvd-1 << ", "
													<< Xbar_j << "," << nj << "," << S2[j] << ","
													<< Xbar_i << "," << ni << "," << S2[i] << endl;
											cout << oss.str();
										}
									}// end if
								}
							}//end for
							it->second = otherBatch;
						}// end if
					}
				}
			}
		}
	}

#else
	if (outputLevel >= 4){
		ostringstream os;
		os << "Worker enters screening with the best step. S_worker = " << S_worker << endl;
		cout << os.str();
	}
	for ( int idx_i = 0; idx_i < S_worker; ++idx_i ){
		if (outputLevel >= 4){
			ostringstream os;
			os << "Starting worker idx = " << idx_i << endl;
			cout << os.str();
		}
		i = this->worker_system_idx[idx_i];
		if (outputLevel >= 4){
			ostringstream os;
			os << "Starting System " << i << " with worker idx" << idx_i << endl;
			cout << os.str();
		}
		if (!(this->system_eliminated[i]) ) {
			if (outputLevel >= 4){
				ostringstream os;
				os << "Sys i " << i << " screening against best systems" << endl;
				cout << os.str();
			}
			if (outputLevel >= 4){
				ostringstream os;
				os << "batchmax = " << batchmax << endl;
				os << "bestmax  = " << bestmax << endl;
				cout << os.str();
			}
			for (int currBatch = 0; currBatch <= batchmax; ++currBatch){
				if (outputLevel >= 4){
					ostringstream os;
					os << "Starting batch " << currBatch << " of screen with best." << endl;
					cout << os.str();
				}
				if (system_eliminated[i] || currBatch > worker_last_batch_recvd) break;
				for (int idx_j = 0; idx_j < bestmax; ++idx_j){
					if (this->system_eliminated[i]) break;
					int _tmp = currBatch * bestmax + idx_j;
					j = best_buffer[ _tmp ].id;
					if (outputLevel >= 4){
						ostringstream os;
						os << "Best id = " << j << endl;
						os << "Best Xbar = " << best_buffer[ _tmp ].Xbar << endl;
						os << "Best size = " << best_buffer[ _tmp ].size << endl;
						cout << os.str();
					}
					if(i != j && j >= 0) {
						Xbar_i = worker_phase1_results_keep[i][currBatch].getSampleMean();
						Xbar_j = best_buffer[ _tmp ].Xbar;
						if (outputLevel >= 4){
							ostringstream os;
							os << "Xbar_i = " << Xbar_i << endl;
							os << "Xbar_j = " << Xbar_j << endl;
							cout << os.str();
						}
						if(Xbar_j > Xbar_i) {
							ni = worker_phase1_results_keep[i][currBatch].sample_size;
							nj = best_buffer[ _tmp ].size;
							tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
							Y = tau*(Xbar_i - Xbar_j);
							tij = 1. / ( S2[i] / Phase1_Max_Sizes[i] + S2[j] / Phase1_Max_Sizes[j]);
							aij = sqrt( (double) (tij * (n1-1)) ) * eta;
							//								cout << "777" << endl;
							if (Y < -aij) {
								// system i eliminated by system j
								this->system_eliminated[i] = 1;
								this->num_systems_eliminated += 1;
								worker_eliminated_system_to_send.push_back(i);
								worker_eliminated_system_simcount.push_back(ni);
								if(outputLevel >= 4 ||
										(outputLevel == 3 &&
												this->num_systems_eliminated >= S_worker - 1)) {
									ostringstream oss;
									oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
											<< setw(7)	<< setfill('0')  << i <<","
											<< " on core " << myRank
											<< " at batch " << batch_screening << " of "
											<< otherBatch << ", "
											<< "othersize=" << (int)iter_other->second.size()
											<< ", workersize=" << worker_last_batch_recvd-1 << ", "
											<< Xbar_j << "," << nj << "," << S2[j] <<
											"," << Xbar_i << "," << ni << "," << S2[i] << endl;
									cout << oss.str();
								}
							}// end if
						}// end if
					} // end if
				}// end for

				if (outputLevel >= 4){
					ostringstream os;
					os << "Finished batch " << currBatch << " of screen with best." << endl;
					cout << os.str();
				}
			}// end for
		}// end if

		if (outputLevel >= 4){
			ostringstream os;
			os << "Finished System " << i << " with worker idx" << idx_i << endl;
			cout << os.str();
		}
	}// end for
#endif

	worker_last_batch_screened = worker_last_batch_recvd;
	if (outputLevel >= 4) {
		ostringstream oo;
		oo << "Worker " << myRank << " finished pairwise screening\n";
		cout << oo.str();
	}
}

void GoodSelAlgoV1::Worker_Simulate_Single_System(int j, int nReps) {
	problem->clear();
	SORngStream * p_RS = new SORngStream();
	if (p_RS->SetSeed(workerSeeds[j])==false) {
		cout << "Error seed with system " << endl;
		//				<< this->System2Idx(disc, dummy) << endl;
	}

	problem->Initialize_Single_System(worker_system_idx[j], nReps, p_RS);
	problem->Objective();

	// initialize
	//	problem->Set_Systems(1, &worker_system_idx[j], &nReps, workerSeeds[j]);
	//	problem->Initialize_Single_System(init_disc[j], init_cont, n, mySeed[j]);
	//compute objective function
	//	problem->run_system(0);
}

//void GoodSelAlgoV1rithm::Worker_Simulate_Multiple_System() {
//	// currently not in use
//
//	// simulate a number of systems, generate one replication from each system
//	// exit when a flag from master is detected
//	problem->clear();
//	s_curr = S_worker-1;
//	MPI::Status status;
//	// initialize problem with multiple systems
//	for (int j = 0; j < S_worker; ++j) {
//		problem->Add_System(init_disc[j], init_cont, worker_batch_sizes[j], mySeed[j]);
//	}
//	while(MPI::COMM_WORLD.Iprobe(0 , flag_tag, status) == false) {
//		s_curr = (s_curr+1)%S_worker;
//		problem->run_system(s_curr);
//		//send answers back to master
//		double fn = problem->getFn();
//		double sumsq = problem->getFnSumSq();
//		int sz = problem->getNumReplications();
//		MPI::COMM_WORLD.Send( &fn   	, 1, MPI::DOUBLE, 0, fn_tag);
//		MPI::COMM_WORLD.Send( &sumsq	, 1, MPI::DOUBLE, 0, sumsq_tag);
//		MPI::COMM_WORLD.Send( &sz   	, 1, MPI::INT   , 0, samplesize_tag);
//		MPI::COMM_WORLD.Send( &worker_system_idx[s_curr], 1, MPI::INT   , 0, sampleidx_tag);
//	}
//}
//void GoodSelAlgoV1::Worker_Initilize_Batches() {
//	// initialize problem with multiple systems
//	problem->clear();
//	problem->Set_Systems(S_worker, &worker_system_idx[0], &worker_batch_sizes[0],
//			mySeed[0]);
//	//	for (int j = 0; j < S_worker; ++j) {
//	//		problem->Add_System(worker_system_idx[j], worker_batch_sizes[j], mySeed[j]);
//	//	}
//}
//
//void GoodSelAlgoV1::Worker_Simulate_Batch() {
//	// simulate a number of systems,
//	// the number of replication from each system pre-determined by worker_batch_sizes
//
//	//	cout << "Worker begins simulating" << endl;
//	vector<SOAnswer> temp_worker_answers, prev_worker_answers; SOAnswer temp_answer;
//	vector<SOAnswer>::iterator iter;
//	//problem->clear();
//	//	s_curr = S_worker-1;
//	MPI::Status status;
//	temp_worker_answers.resize(S_worker);
//	//	cout << "111" << endl;
//	for (int j = 0; j < S_worker; ++j) {
//		if (!this->system_eliminated[worker_system_idx[j]]) {
//			problem->run_system(j);
//			//			if (worker_system_idx[j]==1888) cout << "Simulated sys 1888 for one batch "<<endl;
//			double fn = problem->getFn();
//			double sumsq = problem->getFnSumSq();
//			int sz = problem->getNumReplications();
//			vector<int> v = problem->getDiscX();
//			//			ostringstream oss;
//			//			oss << "Worker " << myRank << " simulates sys " << worker_system_idx[j] <<
//			//					", result = " << fn <<
//			//					"," << v[0] << "," <<v[1] << "," <<v[2] << "," <<v[3] << "," <<v[4] << "," <<
//			//					endl;
//			//			cout << oss.str();
//			temp_answer.clear();
//			temp_answer.addSample(sz, fn*sz, sumsq);
//			temp_worker_answers [j] = temp_answer;
//		}
//	}
//	//	cout << "222" << endl;
//	// for each of the simulated system in the current batch, add previous batch samples to the answer
//	assert ((int)worker_phase0_Results_keep.size()==worker_batches_simulated);
//	if (worker_batches_simulated > 0 ) {
//		prev_worker_answers = worker_phase0_Results_keep[worker_batches_simulated-1];
//		for (int i = 0; i < temp_worker_answers.size(); ++i) {
//			if (!this->system_eliminated[worker_system_idx[i]])
//				temp_worker_answers[i].addSample(prev_worker_answers[i]);
//		}
//	}
//	//	cout << "333" << endl;
//	this->worker_phase0_Results_keep.push_back(temp_worker_answers);
//	//	this->worker_Results_send_phase1.push_back(temp_worker_answers);
//	worker_batches_simulated += 1;
//	//	cout << "Worker finishes simulating" << endl;
//}

//void GoodSelAlgoV1rithm::Worker_Send_Batch(int stg) {
//	//currently not in use
//
//	// send answers from batch stg to master
//	worker_batch_send = worker_Results_send_phase1[stg];
//	Worker_Send_CurrentBatch();
//}

void GoodSelAlgoV1::Worker_Send_CurrentBatch() {
	// send the most current batch to the master

	MPI::Status status; SOResult tmp_answer;
	map<int, SOResult>::iterator iter_ans;
	int sz; bool cont = true;
	double fn, sumsq, simtime;
	int num_sys = S_worker;
	MPI::COMM_WORLD.Send (&num_sys,   1, MPI::INT,    0, sys_tag);
	if (worker_flag == 1) {
		//phase 00: send time
		MPI::COMM_WORLD.Send(&worker_phase00_send[0]    	, S_worker, MPI::DOUBLE, 0, runtime_tag);
		//		MPI::COMM_WORLD.Send(&worker_phase00_s2_send[0]    	, S_worker, MPI::DOUBLE, 0, s2_tag);
		//		MPI::COMM_WORLD.Send(&worker_phase0_t_send[0]    	, S_worker, MPI::DOUBLE, 0, runtime_tag);
	}
	else if (worker_flag == 2) {
		//phase 0 : send fn and sumsq
		MPI::COMM_WORLD.Send(&worker_phase0_fn_send[0]    	, S_worker, MPI::DOUBLE, 0, fn_tag);
		MPI::COMM_WORLD.Send(&worker_phase0_sumsq_send[0]  	, S_worker, MPI::DOUBLE, 0, sumsq_tag);
		MPI::COMM_WORLD.Send(&worker_phase0_t_send[0]    	, S_worker, MPI::DOUBLE, 0, runtime_tag);
	}
}


void GoodSelAlgoV1::Master_Phase1_Recv_Eliminated(int worker) {
	// return immediately if all systems on the worker has been eliminated
	//	if (this->Master_Phase1_isCoreEliminated(worker)) return;

	int numElim;
	double scr_t = 0.0;
	MPI::COMM_WORLD.Recv( &numElim, 1, MPI::INT, 	worker, num_elim_tag);
	MPI::COMM_WORLD.Recv( &scr_t,	1, MPI::DOUBLE, worker, screentime_tag);
	screen_t += scr_t;

	assert (this->Master_num_elim_each_core[worker-1] <= this->Master_phase1_num_sys_each_core[worker-1]);
	//receive the index of eliminated systems
	if (numElim > 0) {
		this->Master_num_elim_each_core[worker-1] += numElim;
		//		cout << "Master rrrr\n" ;
		MPI::COMM_WORLD.Recv( &Master_elim_sys_idx[0], numElim, MPI::INT, worker, elimidx_tag);
		MPI::COMM_WORLD.Recv( &Master_tmp_elim_sys_simcount[0], numElim, MPI::INT, worker, size_tag);
		if (outputLevel >= 4)
			cout << numElim << " sys eliminated on master by worker " << worker << endl;
		// mark system eliminated
		for (int i = 0; i < numElim; ++i) {
			if (!this->system_eliminated[Master_elim_sys_idx[i]]) {
				this->num_systems_eliminated += 1;
				if (outputLevel >= 3 ) {
					ostringstream oss;
					oss << "Sys " << setw(7) << setfill('0') <<
							Master_elim_sys_idx[i] << " eliminated on master by worker "
							<< worker << ", "   << S_Total-this->num_systems_eliminated <<
							" sys remaining" <<
							", t= " << this->getCurrentTime() << endl;
					cout << oss.str();
				}
				//				Master_simcount_batches[2] += (master_tmp_elim_sys_simcount[i] - n0);
				//				Master_simcount_systems[master_elim_sys_idx[i]] = master_tmp_elim_sys_simcount[i] - n0;
			}
			this->system_eliminated[Master_elim_sys_idx[i]] = 1;
			// remove all the data collected for this system
			Master_Results[Master_elim_sys_idx[i]].clear();
			Master_Temp_Results[Master_elim_sys_idx[i]].clear();
			Core2SysLists[worker-1].remove(Master_elim_sys_idx[i]);
		}

		//		cout <<  "Master ssss\n" ;
		if (Master_num_elim_each_core[worker-1] == Master_phase1_num_sys_each_core[worker-1]){
			core_eliminated[worker-1] = 1;
			Master_Phase1_Best_Results_ID[worker-1] = -1;
			Master_Phase1_Best_Results[worker-1].clear();
			assert(Core2SysLists[worker-1].size()==0);
			if (outputLevel > 0) {
				ostringstream oss;
				oss << "Core " << worker << " eliminated completely" <<
						", t= " << this->getCurrentTime() << endl;
				cout << oss.str();
			}
		}
		//		cout <<  "Master tttt\n" ;
	}
	return;
}

void GoodSelAlgoV1::Worker_Phase1_Send_Eliminated() {
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " sends elimination" << endl;
		cout << oss.str();
	}
	// send a list of system indexes eliminated since last communication
	// if there's no answer, simply send 0
	int i = 0, numElim = worker_eliminated_system_to_send.size();
	deque<int>::iterator iter, iter2;
	//	cout << "numElim = " << numElim << "S_worker = " <<
	//			S_worker << "num_systems_eliminated = " << num_systems_eliminated << endl;

	// if there's no answer and all systems eliminated on core, return immediately
	//	if (numElim == 0 && this->num_systems_eliminated == S_worker) return;

	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " eliminated " << numElim << " Sys\n";
		cout << oss.str();
	}

	MPI::COMM_WORLD.Send( &numElim, 	1, MPI::INT, 		0, num_elim_tag);
	MPI::COMM_WORLD.Send( &screen_t, 	1, MPI::DOUBLE, 	0, screentime_tag);

	// if there's no answer, return immediately
	if (numElim == 0) {
		return;
	}
	else {
		int temp_s [numElim];
		int temp_count [numElim];
		i = 0; iter2 = worker_eliminated_system_simcount.begin();
		for (iter=worker_eliminated_system_to_send.begin();
				iter != worker_eliminated_system_to_send.end(); iter2++, iter++, i++) {
			temp_s [i] = *iter;
			temp_count [i] = *iter2;
		}
		MPI::COMM_WORLD.Send( temp_s   	, numElim, MPI::INT, 0, elimidx_tag);
		MPI::COMM_WORLD.Send( temp_count  	, numElim, MPI::INT, 0, size_tag);
		worker_eliminated_system_to_send.clear();
	}
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " finished sending elimination, " << numElim << " sys eliminated" << endl;
		cout << oss.str();
	}
	return;
}

void GoodSelAlgoV1::Master_Phase1_Recv_Best(int worker) {
	assert(worker > 0 && worker < nP);
	int num_sys_elim, best_idx, batches_had, batches_screened, batches_torecv;
	SOResult temp_ans;
	MPI::COMM_WORLD.Recv( &num_sys_elim	, 1, MPI::INT, worker, sys_tag);
	assert(num_sys_elim == this->Master_num_elim_each_core[worker-1]);

	if (outputLevel >= 4)	cout << "Master receiving best system \n";

	// if not all systems have been eliminated, receives the best system from the worker
	// with its complete history
	if (num_sys_elim < (int) this->Master_phase1_num_sys_each_core[worker-1]) {
		//		cout << "Master yyy" << endl;
		// recv the number of batchs screened
		MPI::COMM_WORLD.Recv( &batches_screened, 1, MPI::INT, worker, batch_tag);
		// recv the index of the best system
		MPI::COMM_WORLD.Recv( &best_idx		, 1, MPI::INT, worker, idx_tag);

		if (best_idx == Master_Phase1_Best_Results_ID[worker-1]) {
			batches_had = Master_Phase1_Best_Results[worker-1].size();
		} else {
			batches_had = 0;
			Master_Phase1_Best_Results[worker-1].clear();
			Master_Phase1_Best_Results_ID[worker-1] = best_idx;
		}
		batches_torecv = batches_screened + 1 - batches_had;
		assert (batches_torecv > 0);
		MPI::COMM_WORLD.Send( &batches_torecv, 1, MPI::INT, worker, batch_tag);

		if (outputLevel >= 4) {
			ostringstream oss;
			oss << "Master recv best sys " << best_idx << " from worker " <<
					worker << ", batches " << (batches_had+1) << " to " << (batches_screened) << endl;
			cout << oss.str();
		}
		double fn[batches_torecv];
		double sumsq[batches_torecv];
		int sz[batches_torecv];
		//		cout << "Master www" << endl;
		//recv all of the history of the best system to the master
		MPI::COMM_WORLD.Recv( &fn   	, batches_torecv, MPI::DOUBLE, worker, fn_tag);
		MPI::COMM_WORLD.Recv( &sumsq	, batches_torecv, MPI::DOUBLE, worker, sumsq_tag);
		MPI::COMM_WORLD.Recv( &sz   	, batches_torecv, MPI::INT   , worker, samplesize_tag);
		//create a history of best answers
		for (int i = 0; i < batches_torecv; i++) {
			temp_ans.clear(); temp_ans.addSample(sz[i], ((fn[i]) * (sz[i])), sumsq[i]);
			Master_Phase1_Best_Results[worker-1].push_back(temp_ans);
		}
		//		cout << "Master vvv" << endl;
	} else {
		// all systems eliminated
		this->Master_Phase1_Best_Results_ID[worker-1] = -1;
		//		assert(this->Master_num_elim_each_core[worker-1] == (int) this->Core2SysLists[worker-1].size());
		assert(core_eliminated[worker-1]);
		//		this->Master_best_answers[worker-1].clear();
	}
	return;
}

void GoodSelAlgoV1::Worker_Phase1_Send_Best() {
	// worker sends the number of systems eliminated by itself, together with the best system
	// to the master (if not all systems have been eliminated on the worker)
	if (outputLevel >=3) {
		ostringstream oss;
		oss << "Worker " << myRank << " sends best sys" << endl;
		cout << oss.str();
	}

	// once again send num_systems_eliminated, to confirm with master
	MPI::COMM_WORLD.Send( &(this->num_systems_eliminated), 1, MPI::INT, 0, sys_tag);

	// only send best system if not all systems have been eliminated
	if (this->num_systems_eliminated < S_worker) {
		int i, best_i = -1;
		int best_idx = -1;
		// find the best system

		double best = -9999.0;
		for (i = 0; i < S_worker; i++) {
			//			if (iter->second.getSampleMean() > best)  {
			if ((!this->system_eliminated[worker_system_idx[i]])
					&& worker_phase1_results_keep[worker_system_idx[i]][worker_last_batch_screened].getSampleMean() > best) {
				best = worker_phase1_results_keep[worker_system_idx[i]][worker_last_batch_screened].getSampleMean();
				best_idx = worker_system_idx[i];
				best_i = i;
			}
		}
		if (outputLevel >= 4) {
			ostringstream oss;
			oss << "worker " << myRank << " sends best sys " << best_idx <<
					" to master, " << worker_last_batch_screened << "batches screened\n";
		}

		//send the current batch
		MPI::COMM_WORLD.Send( &worker_last_batch_screened	, 1, MPI::INT, 0, batch_tag);
		//send the index of the best system
		MPI::COMM_WORLD.Send( &best_idx				, 1, MPI::INT, 0, idx_tag);

		MPI::COMM_WORLD.Recv(&dummy		, 1, MPI::INT, 0, batch_tag);

		double fn[dummy];
		double sumsq[dummy];
		int sz[dummy];
		SOResult * bestAnswer;
		int kk = 0;
		for (int k = worker_last_batch_screened-dummy + 1; k <= worker_last_batch_screened; k++) {
			bestAnswer = &(worker_phase1_results_keep[best_idx][k]);
			fn[kk] 	= bestAnswer->getSampleMean();
			sumsq[kk]= bestAnswer->getSampleSumsq();
			sz[kk]	= bestAnswer->sample_size;
			kk++;
		}
		//send all of the history of the best system to the master
		MPI::COMM_WORLD.Send( &fn   	, dummy, MPI::DOUBLE, 0, fn_tag);
		MPI::COMM_WORLD.Send( &sumsq	, dummy, MPI::DOUBLE, 0, sumsq_tag);
		MPI::COMM_WORLD.Send( &sz   	, dummy, MPI::INT   , 0, samplesize_tag);
	}
	return;
}

void GoodSelAlgoV1::Master_Phase1_Send_OtherBest(int worker) {
	// sends the list of other systems, together with their histories, to the worker
	// system(core)/batch/answer

	assert(worker > 0 && worker < nP);
#if BEST_MAX == 0
	// count the number of best systems to send
	int num_best = 0; int num_batches, idx, nextSize, num_batches_tosend; double nextFn, nextSumSq;
	MPI::COMM_WORLD.Send(&core_eliminated[0], nP-1, MPI::INT, worker, sys_tag);
	MPI::COMM_WORLD.Send(&Master_Phase1_Best_Results_ID[0], nP-1, MPI::INT, worker, sampleidx_tag);
	deque<SOResult>::iterator iter_ans;
	for (int w = 1; w < nP; w++) {
		if(core_eliminated[w-1] || w == worker || Master_Phase1_Best_Results_ID[w-1] == -1) {
			// if core i eliminated or is worker itself, do not count
		} else {
			if (outputLevel >= 4)			cout <<  "M 111\n";
			idx = this->Master_Phase1_Best_Results_ID[w-1];
			//			MPI::COMM_WORLD.Send(&idx,     	1, MPI::INT,	worker, sampleidx_tag);
			num_batches = this->Master_Phase1_Best_Results[w-1].size();
			MPI::COMM_WORLD.Recv(&dummy,	 	1, MPI::INT,	worker, batch_tag);
			num_batches_tosend = num_batches - dummy ;
			if (outputLevel >= 4)	cout <<  "M 222\n";
			MPI::COMM_WORLD.Send(&num_batches_tosend, 1, MPI::INT,	worker, batch_tag);

			if (outputLevel >= 4) {
				ostringstream oss;
				oss << "Master send best sys " << idx << " of worker " << w << " to worker " <<
						worker << ", batches " << (dummy+1) << " to " << (num_batches) << endl;
				cout << oss.str();
			}
			if (outputLevel >= 4)
				cout <<  "M 333\n";
			for (iter_ans = Master_Phase1_Best_Results[w-1].begin()+(dummy);
					iter_ans != Master_Phase1_Best_Results[w-1].end(); ++iter_ans) {

				//			for (int j = 0; j < num_batches; ++j) {
				//				temp_ans = Master_best_answers[i][j];

				nextFn = (*iter_ans).getSampleMean();
				nextSumSq = (*iter_ans).getSampleSumsq();
				nextSize = (*iter_ans).sample_size;
				MPI::COMM_WORLD.Send(&nextFn,		1, MPI::DOUBLE,	worker, fn_tag);
				MPI::COMM_WORLD.Send(&nextSumSq, 	1, MPI::DOUBLE,	worker, sumsq_tag);
				MPI::COMM_WORLD.Send(&nextSize,    	1, MPI::INT, 	worker, samplesize_tag);
			}
			if (outputLevel >= 4)
				cout <<  "M 444\n";
		}
	}
#else
	MPI_Send(best_buffer, bestmax * (batchmax + 1),
			best_type, worker, best_tag, MPI_COMM_WORLD);
#endif
}

void GoodSelAlgoV1::Worker_Phase1_Recv_OtherBest() {
	//worker receives the list of best systems from other workers from the master

	assert(this->myRank>0);
	if(outputLevel >= 4) {
		ostringstream oss;
		oss << "Worker " << myRank << " recv other best" << endl;
		cout << oss.str();
	}

#if BEST_MAX == 0
	map<int, deque<SOResult> >::iterator it;
	SOResult temp_answer; temp_answer.clear();
	double nextFn, nextSumSq;
	// moved to outside of the loop
	deque<SOResult> emptydeque; emptydeque.clear();
	int nextIdx, nextSize, batchesCollected, batchesToCollect;
	//	MPI::COMM_WORLD.Recv(&num_best_sys_to_recv, 1, MPI::INT, 0, sys_tag);
	MPI::COMM_WORLD.Recv(&(core_eliminated [0]), nP-1, MPI::INT, 0, sys_tag);
	MPI::COMM_WORLD.Recv(&(Master_Phase1_Best_Results_ID[0]), nP-1, MPI::INT, 0, sampleidx_tag);

	if (outputLevel >= 4)	{
		ostringstream os0;
		os0 << myRank <<  " W 111\n";
		cout << os0.str();
	}
	indexes_recvd.clear();
	for (int w = 1; w < nP; w++) {
		if(core_eliminated[w-1] || w == myRank  || Master_Phase1_Best_Results_ID[w-1] == -1) {
			// if core i eliminated or is worker itself, do not count
		} else {
			if (outputLevel >= 4)cout <<  "W 222\n";
			//			MPI::COMM_WORLD.Recv(&nextIdx,     	1, MPI::INT,	0, sampleidx_tag);
			nextIdx = Master_Phase1_Best_Results_ID[w-1];
			indexes_recvd.insert(nextIdx);
			it = map_worker_results_received.find(nextIdx);
			if (outputLevel >= 4)cout <<  "W 333\n";
			if (it != map_worker_results_received.end() ) {
				batchesCollected = it->second.size();
				if (outputLevel >= 4)cout <<  "W 444a\n";
			} else {
				batchesCollected = 0;
				map_worker_results_received[nextIdx] = emptydeque;
				if (outputLevel >= 4)cout <<  "W 555a\n";
			}
			if (outputLevel >= 4)cout <<  "W 444\n";
			MPI::COMM_WORLD.Send(&batchesCollected, 1, MPI::INT,	0, batch_tag);
			MPI::COMM_WORLD.Recv(&batchesToCollect, 1, MPI::INT,	0, batch_tag);
			if (outputLevel >= 4)cout <<  "W 555\n";
			for (int k = 0; k < batchesToCollect; ++k) {
				MPI::COMM_WORLD.Recv(&nextFn,		1, MPI::DOUBLE,	0, fn_tag);
				MPI::COMM_WORLD.Recv(&nextSumSq, 	1, MPI::DOUBLE,	0, sumsq_tag);
				MPI::COMM_WORLD.Recv(&nextSize,    	1, MPI::INT, 	0, samplesize_tag);
				temp_answer.clear();
				temp_answer.addSample(nextSize, nextFn*nextSize, nextSumSq);
				map_worker_results_received[nextIdx].push_back(temp_answer);
			}
			if (outputLevel >= 4) cout <<  "W 666\n";
		}
	}
	if (outputLevel >= 4)cout <<  "W 777\n";
	if (map_worker_results_received.size()>0) {
		for (it = map_worker_results_received.begin(); it != map_worker_results_received.end(); it++) {
			if (indexes_recvd.find(it->first) == indexes_recvd.end()) {
				it->second.clear();
			}
		}
	}
	if (outputLevel >= 4)cout <<  "W 888\n";
	indexes_recvd.clear();
	if (outputLevel >= 4)cout <<  "W 999\n";
#else
	MPI_Recv(best_buffer, bestmax * (batchmax + 1), best_type, 0, best_tag,
			MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
}

//bool GoodSelAlgoV1::Master_Phase1_isCoreEliminated(int worker) {
//	return this->core_eliminated[worker-1];
//}

string GoodSelAlgoV1::toString_Param() {
	ostringstream ss;
	ss << "Alpha=" << alpha << " Delta="<<delta <<" eta=" << eta
			<< " n00=" << n1 << " n0=" << batchmax << " numSystems="
			<< problem->getNumSystems() <<endl;
	return ss.str();
}

string GoodSelAlgoV1::toString_BatchSizes() {
	string str;
	str += "System, BatchSize\n";
	for (int i = 0; i < (int) ( this->Phase1_Batch_Sizes.size() ); i++) {
		ostringstream ss ;
		ss << i << "," << this->Phase1_Batch_Sizes[i] << endl;
		str += ss.str();
	}
	return str;
}

string GoodSelAlgoV1::toString_Core2Sys() {
	list<int>::iterator iter;
	string str;
	assert((int) (this->Core2SysLists.size() ) == nP-1);
	str += "Core, Systems\n";
	for (int i = 0 ; i < (int) ( this->Core2SysLists.size() ); ++i) {
		ostringstream ss ;
		ss <<  setw(3) << setfill('0') <<  i+1 << ",";
		str += ss.str();
		for (iter = Core2SysLists[i].begin(); iter!=Core2SysLists[i].end(); iter++) {
			ostringstream ss1 ;
			ss1 <<  setw(7) << setfill('0') << *iter << ",";
			str += ss1.str();
		}
		str += "\n";
	}
	return str;
}

double GoodSelAlgoV1::getCurrentTime() {
	return MPI::Wtime()-t0 ;
}

void GoodSelAlgoV1::Master_PrintPhase0Summary() {
	ostringstream oss;
	oss << "----------------------" << endl;
	oss << "System,Size,mean,stdev,SimTime,S2" << endl;
	for (int j = 0; j<S_Total; j++) {
		oss << setw(7) << setfill('0') << j << "," <<
				Master_Results[j][0].toString() << "," <<
				Simulation_Times[j] << "," <<
				S2[j] << endl;
	}
	oss << "----------------------" << endl;
	cout << oss.str();
}

void GoodSelAlgoV1::report_winner() {
	ostringstream oss;
	if(this->num_systems_eliminated + 1 != this->S_Total) {
		cout << "Error! " << num_systems_eliminated <<
				" systems eliminated out of " << S_Total << "." << endl;
		assert(this->num_systems_eliminated + 1 == this->S_Total);
	}

	int winner;
	for (int j = 0; j < S_Total; j++ ) {
		if (this->system_eliminated[j]==0) {
			winner = j; break;
		}
	}
	//success
	double sim_time_total = Master_timing_phases[0]+
			Master_timing_phases[1]+Master_timing_phases[2]+Master_timing_phases[3];
	unsigned long sim_count_total = Master_simcount_phases[0]+
			Master_simcount_phases[1]+Master_simcount_phases[2]+
			Master_simcount_phases[3];
	double _t = this->getCurrentTime();
	oss << "algo,n0,n1,bsize,bmax,delta,seed,screenVer,core,time,RB,cov" <<
			",systems,phase2sys,simcount_0,simcount_1,simcount_2,simcount_3,"<<
			"totalsim,winner,simtime_0,simtime_1,simtime_2,simtime_3,simtime_total,"<<
			"simtime_util,screentime,screentime_util\n";
#if BEST_MAX == 0
	oss << "GSP," ;
#else
	oss << "GSP_LIMBEST,";
#endif
	oss << (useTimeEst?n0:0) << "," << n1 << "," << bsize << "," <<
			batchmax << "," << delta << "," <<
			seed_GLOBAL << "," << screenVersion << "," << nP << "," <<
			_t << "," <<
			RB_global << "," << burnin_param << "," << S_Total << "," <<
			num_systems_remain_after_phase1 << "," <<
			Master_simcount_phases[0]  << "," <<
			Master_simcount_phases[1]  << "," <<
			Master_simcount_phases[2]  << "," <<
			Master_simcount_phases[3]  << "," <<
			sim_count_total<< "," << winner << "," <<
			Master_timing_phases[0]  << "," <<
			Master_timing_phases[1]  << "," <<
			Master_timing_phases[2]  << "," <<
			Master_timing_phases[3]  << "," <<
			sim_time_total  << "," <<
			sim_time_total / _t / nP  << "," <<
			screen_t  << "," <<
			screen_t / _t / nP  << "," <<
			//				"," << num_systems_remain_after_phase0 <<
			endl;

	oss << "------------------------------------------------------" <<endl;
	oss << "Success! " ;
	oss << "Winner is system " << winner <<", t = " << this->getCurrentTime() << endl;
	oss << "------------------------------------------------------" <<endl;

	cout << oss.str();
}

