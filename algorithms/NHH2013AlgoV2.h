/**
 * @file NHH2013AlgoV2.h
 *
 * @brief Defines parallel NHH algorithm [Ni et al., 2013]
 */
#ifndef NHH2013ALGOV2_H_
#define NHH2013ALGOV2_H_

#include "SOAlgorithm.h"
#include <vector>
#include <deque>
#include <algorithm>
#include <set>
#include <list>
#include <assert.h>
using namespace std;

/** @addtogroup algo */
/*@{*/
/**
 * @brief Parallel NHH procedure.
 *
 * An efficient parallel ranking and selection procedure based on sequential screening.
 *
 * Originally proposed in
 *
 * 		Ni, E. C., S. R. Hunter, and S. G. Henderson. 2013.
 * 		"Ranking and Selection in a High Performance Computing Environment".
 * 		In Proceedings of the 2013 Winter Simulation Conference.
 *
 * See further discussions and tests on this procedure in
 *
 * 		Ni, E. C., S. G. Henderson, and S. R. Hunter. 2014.
 * 		"A Comparison of Two Parallel Ranking and Selection Procedures".
 * 		In Proceedings of the 2014 Winter Simulation Conference.
 *
 */
class NHH2013AlgoV2 : public SOAlgorithm {
public:
	NHH2013AlgoV2();
	~NHH2013AlgoV2() ;
	void initialize(SOProb * prob) ;
	void run();
	Matrix<unsigned long> workerSeeds;
	vector<int> worker_system_idx;
	vector<int> worker_stage_sizes;
protected:
	double alpha, delta, lambda, a, t0, t, TotalSimulationTime, timePerCore;
	int S_Total, n0, n1, bsize, num_systems_eliminated, num_systems_remain_after_phase0;
	int myRank, nP, S_worker;
	int num_core_terminated; //int s_curr;
	vector< list<int> > Core2SysLists; // // range [0 ~ nP-2][0 ~ S-1]
	vector<int> Sys2CoreList; // sys/core
	//	vector< list<int> > Sys2CoreLists; // range [0 ~ S-1][0 ~ nP-2]
	vector< int > Master_phase1_num_sys_each_core;
	vector<int> system_eliminated;
	vector<int> core_eliminated;
	vector<bool> core_terminated;
	//	vector<int> CoreReassigned; // range 0 ~ nP-1
	vector<int> rnd_perm;
	set<int> indexes_recvd;
	vector<SORngStream> WorkerStreams;
private:
	double getCurrentTime();
	int master_phase1_current_stage, master_phase1_current_sys;
	int worker_phase1_stage, worker_phase1_sys;
	double worker_phase1_fn, worker_phase1_sumsq;
	deque< deque<SOResult> > Master_Results; // sys (0~S_Total)/ stage/SOResult
	deque< map<int, SOResult> > Master_Temp_Results; // sys (0~S_Total)/ stage/SOResult
	vector<double> S2;
	vector<double> Simulation_Times;
	vector<int> Stage_Sizes;
	vector<double> worker_phase00_send;
	vector<double> worker_phase0_fn_send;
	vector<double> worker_phase0_sumsq_send;
	deque<vector<SOResult> > worker_phase0_Results_keep; //indexed from 0 onwards; stage/sys(worker: 0~S_worker-1)/SOResult
	vector<deque<SOResult> > worker_phase1_results_keep; //indexed from 0 onwards; sys(worker: 0~S_Total)/stage/SOResult
	vector<map<int, SOResult> > worker_Results_send_phase1; //indexed from 0 onwards
	map<int, deque<SOResult> > map_worker_results_received; //sys/stage/SOResult
	deque<int> worker_eliminated_system_to_send;
	deque<int> worker_eliminated_system_simcount;
	deque<int> Master_simcount_systems; //S_Total
	deque<double> Master_simcount_phases;  //3
	vector<int> Best_Results_idx;          // core<sys index>
	deque<deque<SOResult> > Master_best_Results;  // core<stage<result>>
	vector<int> Master_stages_screened_each_worker;     // core<sys index>
	//	vector<int> Master_stages_sent_each_worker;
	//	deque< deque<int> > Master_stages_recv_count; // worker/stage count(# systems received for this stage)
	vector<int> Master_num_elim_each_core;
	void report_winner();
	void Master_Routine();
	void Master_Phase00_PrepareList();
	void Master_Phase0_PrepareList();
	void Master_Phase0_pairwise_screen();
	void Master_Phase1_PrepareList();
	void Master_Phase00_0_Send(bool phase00);
	void Master_Phase00_0_Recv(bool phase00);
	void Master_Phase0_Recv_with_Screening();
	void Master_report_stage0();
	//	void Master_Phase1_Send();
	void Master_Phase1_Initial_Send();
	void Master_Phase1_Execute();
	void Master_Phase1_RecvAns(const int & worker);
	void Master_Phase1_RecvScreening(const int & worker);
	int  Master_Phase1_getNumNewStageAvail(const int & worker);
	void Master_Phase1_SendScreenAns(const int & worker, const int & numnewstages);
	void Master_Phase1_SendSimSys(const int & worker);
	void Master_Phase1_SendTermination(const int & worker);
	//	void Master_Phase1_WrapUp();
	void Master_Phase1_Recv_Eliminated(int worker);
	void Master_Phase1_Recv_Best(int worker);
	void Master_Phase1_Send_OtherBest(int worker);
	//	bool Master_Phase1_isCoreEliminated(int worker);
	void Master_PrintPhase0Summary();
	void Worker_Routine();
	void Worker_Phase00_0_Routine();
	void Worker_Phase0_Routine_with_Screening();
	//	void Worker_Phase0_Send_Results();
	void Worker_Phase1_Prepare();
	void Worker_Phase1_Execute();
	void Worker_Phase1_SendAns();
	void Worker_Phase1_SendScreening();
	void Worker_Phase1_RecvSimSys();
	void Worker_Phase1_RecvScreenAns();
	void Worker_Phase1_Clean();
	void Worker_Send_CurrentStage();
	void Worker_Recv_NumSys();
	void Worker_Recv_ListSys();
	//	void Worker_Recv_S2();
	void Worker_Phase1_Recv_OtherBest();
	void Worker_pairwise_screen_skipping();
	void Worker_pairwise_screen_no_skipping();
	void Worker_Phase1_Send_Eliminated();
	void Worker_Phase1_Send_Best();
	void Worker_Simulate_Single_System(int j, int nReos);
	//	void Worker_Simulate_Multiple_System();
	//	void Worker_Initilize_Stages();
	//	void Worker_Simulate_Stage();
	//	void Worker_Send_Stage(int stg);
	//	void Worker_Isend_Single_Result(int sys_idx, SOResult ans);
	unsigned long * currentSeed;
	unsigned long * tempSeed;
	int * master_elim_sys_idx;
	int * master_tmp_elim_sys_simcount;
	//	Matrix<int> init_disc;
	deque<map<int, int> > worker_otherbest_stages_screened; // records number of stages screened so far between any two systems, S_worker/S_others/
	double * init_cont;
	vector<int> master_phase0_num_sys_recv;
	int worker_stages_toSend, worker_stages_simulated, worker_stages_screened, worker_phase0_sys_sent;
	int worker_stages_recvd; // begins from 0 when used as index. 0 = stage 1
	int worker_flag, dummy;
	//	list<int> Master_Recvd_Workers_List;
	string toString_Param();
	string toString_Core2Sys();
	string toString_StageSizes();
	vector<MPI::Request> ReadyRecvRequests;
};
/*@}*/
#endif // NHH2013ALGOV2_H_
