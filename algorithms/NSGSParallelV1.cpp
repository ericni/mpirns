/*
Author: Nick Samson
Email: nes77@cornell.edu
NSGSParallelV1.cpp: Implements algorithm described in header
 */

/**
 * @file NSGSParallelV1.cpp
 *
 * @brief Implements a parallel version of NSGS [Nelson et al., 2001]
 */
extern int bsize_GLOBAL, seed_GLOBAL, n0_GLOBAL, screenVersion, outputLevel, algo;
extern int RB_global, n1_GLOBAL;
extern double delta_GLOBAL, useTimeEst, burnin_param;
double aggregator;

#define MASTER_FLAG(x) MPI::COMM_WORLD.Bcast(0, 1, MPI::INT, x)
#define WORKER_FLAG(x) MPI::COMM_WORLD.Recv(&recv_dump, 1, MPI::INT, 0, x)
#define MASTER_WAIT() MPI::COMM_WORLD.Gather(0, 1, MPI::INT, &recv_dump, 1, MPI::INT, 0)
#define WORKER_WAIT() MPI::COMM_WORLD.Gather(0, 1, MPI::INT, &recv_dump, 1, MPI::INT, 0)
#define LOG_VERBOSE(x) { strs << x; logger->log(strs.str(), VERBOSE); strs.str(""); }

#include "NSGSParallelV1.h"

namespace {

typedef map<int, SOResult>::iterator miter;


class Scheduler {

private:
	priority_queue<SchedSub> jobs;

public:

	///Converts a map of job/time values into a Scheduler
	Scheduler(map<int, double>& jobs){
		for (map<int, double>::iterator it = jobs.begin(); it != jobs.end(); ++it) {
			this->jobs.push(SchedSub(it->first, it->second));
		}
	}

	//Returns a map representing worker/job list
	//Use MPI rank for workers
	map<int, vector<int> > schedule_jobs(int workers){

		map<int, vector<int> > result;
		priority_queue<WorkerSub> worker_q;

		for (int i = 1; i < workers; i++) {
			result[i] = vector<int>();
			WorkerSub v(i, 0.0);
			worker_q.push(v);
		}

		while (!this->jobs.empty()) {
			WorkerSub worker_temp = worker_q.top();
			SchedSub sched_temp = this->jobs.top();
			worker_q.pop();
			this->jobs.pop();

			result[worker_temp.index].push_back(sched_temp.index);
			worker_temp.time += sched_temp.time;
			worker_q.push(worker_temp);
		}

		return result;
	}
};

}
NSGSParallelV1::NSGSParallelV1() {

	this->currentSeed = new unsigned long[6];
	this->tempSeed = new unsigned long[6];
	this->myRank = MPI::COMM_WORLD.Get_rank();
	this->nP = MPI::COMM_WORLD.Get_size();
	logger = new SimpleLogger(myRank, Logger::convertInt(outputLevel));

	logger->log("Successful initialization!", USEFUL);

}

NSGSParallelV1::~NSGSParallelV1() {

	logger->log("Running deconstructor", VERBOSE);


	delete[] tempSeed;
	delete[] currentSeed;
	delete logger;

	problem->clear();

	//cout << "Clean up completed." << endl;


}

void NSGSParallelV1::initialize(SOProb* prob) {
	SOAlgorithm::initialize(prob);
	S_Total = problem->getNumSystems();
	alpha = 0.025;
	this->nP = min(S_Total, nP);
	//t = -4.783;

	delta = delta_GLOBAL;

	lambda = delta / 2;

	n0 = n0_GLOBAL;
	bsize = n1_GLOBAL;
	a = ((double) bsize - 1.0) / 2.0 / delta
			* (pow(2.0 - 2.0 * pow(1.0 - alpha, 1.0 / ((double) S_Total - 1.0)), -2.0 / (bsize - 1.0)) - 1.0);

	//t = t_GLOBAL;
#if USE_BOOST_LIB
	boost::math::students_t tDist(bsize-1);
	t = boost::math::quantile( tDist, pow(1. - alpha, 1. / (S_Total - 1)) );
#else	
	t = gsl_cdf_tdist_Pinv(pow(1. - alpha, 1. / (S_Total - 1)), bsize - 1);
#endif
	strs << "t= " << t << endl;
	logger->log(strs.str(), VERBOSE);
	strs.str("");
	//n1 = n1_GLOBAL;
	this->num_systems_eliminated = 0;

	//    	h = RinottFunc::rinott(S_Total, .975, bsize - 1);
	//    	int tn0 = bsize;
	//    	while(h < .01) {
	//    		h = RinottFunc::rinott(S_Total, .975, tn0--);
	//    	}
	//	h = h_GLOBAL;
	h = RinottFunc::rinott(S_Total, .975, bsize - 1);
	strs << "h= " << h << endl;
	logger->log(strs.str(), VERBOSE);
	strs.str("");



	for (int i = 0; i < 6; i++) currentSeed[i] = (unsigned long) seed_GLOBAL;
	for (int i = 0; i < 6; i++) tempSeed[i] = 0;
	if (SORngStream::SetPackageSeed(currentSeed) == false)
		cerr << "Error seed for Master routine" << endl;
	this->pMasterStream = new SORngStream();

	logger->log("completed initialization.", USEFUL);
}

void NSGSParallelV1::run() {


	switch (myRank) {

	case 0:
		logger->log("entering master routine.", USEFUL);
		Master_Routine();
		logger->log("exiting master routine.", USEFUL);

		return;

	default:
		logger->log("entering worker routine.", USEFUL);
		if (myRank < nP) Worker_Routine();
		logger->log("exiting worker routine", USEFUL);
		return;


	}


}

void NSGSParallelV1::Master_Routine() {
	t0 = MPI::Wtime();

	Master_Phase_0();
	Master_Phase_1();
	Master_Phase_2();

	delete this->pMasterStream;

}

inline double NSGSParallelV1::getTime() {

	return MPI::Wtime() - t0;

}

void NSGSParallelV1::Worker_Routine() {


	Worker_Phase_0();



	Worker_Phase_1();



	Worker_Phase_2();



}

void NSGSParallelV1::Master_Phase_0() {

	Master_Phase_0_GenerateList();

	Master_Phase_0_Send();

	Master_Phase_0_Recv();



}

void NSGSParallelV1::Master_Phase_0_GenerateList() {

	logger->log("Beginning Phase 0 list generation.", USEFUL);

	vector<int> rnd_perm = vector<int>(S_Total);
	vector<int> Phase0List = vector<int>(nP);

	int tmp_i, tmp_j, s;
	for (int i = 0; i < S_Total; i++) rnd_perm[i] = i;
	for (int i = S_Total - 1; i >= 1; i--) {
		// use the next substream of pMasterStream to generate a random integer between 0 and i
		tmp_j = pMasterStream->RandInt(0, i);
		tmp_i = rnd_perm[tmp_j];
		rnd_perm[tmp_j] = rnd_perm[i];
		rnd_perm[i] = tmp_i;
	}
	pMasterStream->ResetNextSubstream();

	int num_systems_per_core = S_Total / (nP - 1);
	int num_systems_extra = S_Total % (nP - 1); // number of cores to get  num_systems_per_core+1 systems

	//compile core2syslist
	int curr_sys = 0;
	for (int i = 1; i < nP; ++i) {
		s = (i <= num_systems_extra) ? (num_systems_per_core + 1) : (num_systems_per_core);
		for (int j = 1; j <= s; ++j) {
			CoreToSys[i].push_back(rnd_perm[curr_sys++]);
		}
	}

	logger->log("Ending Phase 0 list generation.", USEFUL);
}

void NSGSParallelV1::Master_Phase_0_Send() {

	phase0_simulations = 0;

	vector<int>::iterator iter;
	int idx, temp_s;
	for (int i = 1; i <= nP - 1; i++) {
		temp_s = CoreToSys[i].size();
		MPI::COMM_WORLD.Send(&temp_s, 1, MPI::INT, i, sys_tag);

		int* v1 = new int[temp_s];
		int* v2 = new int[temp_s];
		unsigned long* seed_to_send = new unsigned long[temp_s * 6];
		int ss, k = 0;
		for (iter = CoreToSys[i].begin(); iter != CoreToSys[i].end(); iter++, k++) {
			//create a new stream and get its seed
			SORngStream rs;
			rs.GetState(tempSeed);
			for (ss = 0; ss < 6; ss++) {
				seed_to_send[k * 6 + ss] = tempSeed[ss];
			}


			idx = *iter;
			v1[k] = idx;
			v2[k] = n0;
			phase0_simulations += (double) n0;

		}
		MPI::COMM_WORLD.Send(v1, temp_s, MPI::INT, i, idx_tag);
		MPI::COMM_WORLD.Send(v2, temp_s, MPI::INT, i, size_tag);
		MPI::COMM_WORLD.Send(seed_to_send, 6 * temp_s, MPI::UNSIGNED_LONG, i, seed_tag);

		delete[] v1;
		delete[] v2;
		delete[] seed_to_send;

		logger->log("Phase 0 send completed.", USEFUL);
		this->strs << "Time: " << this->getTime();
		logger->log(strs.str(), VERBOSE);
		strs.str("");



	}
}

void NSGSParallelV1::Master_Phase_0_Recv() {

	Master_Requests_P0.resize(nP);

	//receives Answers from cores
	int nRecv = 0, from, numSys;
	//	int count, nextIdx, nextSize;
	double nextFn, nextSumSq, nextTime, s;
	vector<int>::iterator iter;
	MPI::Status status;

	int cores_recv = 0;
	while (cores_recv < nP - 1) {
		// iterate over cores to find the next core that has message to send
		if (MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE, sys_tag, status)) {
			from = status.Get_source();
			MPI::COMM_WORLD.Recv(&numSys, 1, MPI::INT, from, sys_tag);
			nRecv += numSys;

			double* master_times = new double[numSys];
			MPI::COMM_WORLD.Recv(master_times, numSys, MPI::DOUBLE, from, runtime_tag);
			int k = 0;
			for (iter = CoreToSys[from].begin(); iter != CoreToSys[from].end(); iter++, k++) {
				Simulation_Times[*iter] = master_times[k];
			}
			delete[] master_times;


			this->strs << "Received result from node " << setw(2) << setfill('0') << from
					<< ", time: " << this->getTime() << endl << nRecv << " systems recvd\n";
			logger->log(strs.str(), USEFUL);
			strs.str("");
			cores_recv++;

		}

	} // end while
	logger->log("Phase 0 recv completed.", USEFUL);
	this->strs << "Time: " << this->getTime();
	logger->log(strs.str(), VERBOSE);
	strs.str("");
}

void NSGSParallelV1::Worker_Phase_0() {

	int sz;
	double fn, sumsq, simtime;
	MPI::Status status;
	SOResult tmp_answer;
	MPI::COMM_WORLD.Recv(&S_worker, 1, MPI::INT, 0, sys_tag);
	int dummy;

	//worker receives the list of systems and seeds from master

	problem->clear();
	workerSeeds.clear();
	workerSeeds.resize(S_worker, 6, 0);

	worker_system_idx = new int[S_worker];
	worker_stage_sizes = new int[S_worker];

	worker_phase0_send = new double[S_Total];

	MPI::COMM_WORLD.Recv(worker_system_idx, S_worker, MPI::INT, 0, idx_tag);
	MPI::COMM_WORLD.Recv(worker_stage_sizes, S_worker, MPI::INT, 0, size_tag);
	MPI::COMM_WORLD.Recv(workerSeeds[0], 6 * S_worker, MPI::UNSIGNED_LONG, 0, seed_tag);

	// performs simulation, one replication for each system, and send results immediately back to the master
	//	bool message_received = false;
	for (int j = 0; j < S_worker; j++) {
		//simulate a single system
		this->Worker_Simulate_Single_System(j, worker_stage_sizes[j]);
		sz = worker_stage_sizes[j];
		fn = problem->getFn();
		sumsq = problem->getFnSumSq();
		simtime = problem->getSimulationTime();
		tmp_answer.clear();
		tmp_answer.addSample(sz, fn*sz, sumsq, simtime);

		worker_phase0_send[j] = simtime;





		strs << "Worker " << myRank << " finished its sys " << j << ", size = " << sz << endl;
		logger->log(strs.str(), VERBOSE);
		strs.str("");

	}

	//Instead of receiving signal from master, this iteration just lets the answer buffer on the master end

	//Send Phase 0 results
	int num_sys = S_worker;
	MPI::COMM_WORLD.Send(&num_sys, 1, MPI::INT, 0, sys_tag);

	//phase 0: send time
	MPI::COMM_WORLD.Send(worker_phase0_send, S_worker, MPI::DOUBLE, 0, runtime_tag);




	delete[] worker_phase0_send;
	delete[] worker_stage_sizes;
	delete[] worker_system_idx;

}

void NSGSParallelV1::Worker_Simulate_Single_System(int j, int nReps) {
	problem->clear();
	SORngStream * p_RS = new SORngStream();
	if (p_RS->SetSeed(workerSeeds[j])==false) {
		cout << "Error seed with system " << endl;
		//				<< this->System2Idx(disc, dummy) << endl;
	}

	problem->Initialize_Single_System(worker_system_idx[j], nReps, p_RS);
	problem->Objective();
	//    problem->clear();
	//    // initialize
	//    problem->Set_Systems(1, &worker_system_idx[j], &n, mySeed[j]);
	//    //compute objective function
	//    problem->run_system(0);
}

void NSGSParallelV1::Master_Phase_1() {


	Master_Phase_1_GenerateList();

	Master_Phase_1_Send();

	Master_Phase_1_Recv();

	/*switch (screenVersion) {

    case 8:

    break;


    default:
    logger->log("Invalid screen version. Terminating...", ESSENTIAL);
    throw INVALID_SCREEN;

    }*/

	Master_Phase_1_Survivor_Recv();



}

bool compareSim(const pair<int, double> &thus, const pair<int, double> &that) {
	return thus.second < that.second;
}

void NSGSParallelV1::Master_Phase_1_GenerateList() {

	LOG_VERBOSE("Entering Master Phase 1 Generate List");

	CoreToSys.clear();


	LOG_VERBOSE("Core to sys cleared");

	for (map<int, double>::iterator i = Simulation_Times.begin(); i != Simulation_Times.end();
			++i) {

		aggregator += i->second;

	}

	double time_average = aggregator / double(nP);

	LOG_VERBOSE("Time average determined: " << time_average);

	aggregator = 0;

	Scheduler sched = Scheduler(Simulation_Times);
	this->CoreToSys = sched.schedule_jobs(nP);

	//CoreToSys lists generated
	LOG_VERBOSE("CoreToSys lists generated")

}

void NSGSParallelV1::Master_Phase_1_Send() {

	//given the Core2SysLists, send job to respective cores, each system simulated numRep times
	typedef map<int, vector<int> >::iterator miter;
	phase1_simulations = 0;
	vector<int>::iterator iter;
	int idx, temp_s;
	int numRep = bsize;

	SORngStream rs;
	rs.GetState(tempSeed);
	MPI_Bcast(tempSeed, 6, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);

	//send a go flag and the number of systems to each core
	for (miter i = CoreToSys.begin(); i != CoreToSys.end(); ++i) {



		temp_s = i->second.size();
		vector<int>* temp = &(i->second);

		MPI::COMM_WORLD.Send(&temp_s, 1, MPI::INT, i->first, sys_tag);

		int* v1 = new int[temp_s];
		int* v2 = new int[temp_s];
		unsigned long* seed_to_send = new unsigned long[temp_s * 6];
		int ss, k = 0;
		for (iter = temp->begin(); iter != temp->end(); iter++, k++) {
			//create a new stream and get its seed
			SORngStream rs;
			rs.GetState(tempSeed);
			for (ss = 0; ss < 6; ss++) {
				seed_to_send[k * 6 + ss] = tempSeed[ss];
			}



			idx = *iter;
			v1[k] = idx;
			v2[k] = numRep;
			phase1_simulations += (double) numRep;

		}
		MPI::COMM_WORLD.Send(v1, temp_s, MPI::INT, i->first, idx_tag);
		MPI::COMM_WORLD.Send(v2, temp_s, MPI::INT, i->first, size_tag);
		MPI::COMM_WORLD.Send(seed_to_send, 6 * temp_s, MPI::UNSIGNED_LONG, i->first, seed_tag);


		delete[] v1;
		delete[] v2;
		delete[] seed_to_send;

	}



	logger->log("Phase 1 send completed.", USEFUL);
	this->strs << "Time: " << this->getTime();
	logger->log(strs.str(), VERBOSE);
	strs.str("");


}

void NSGSParallelV1::Master_Phase_1_Recv() {

	//receives Answers from cores
	int nRecv = 0, from, numSys;
	//	int count, nextIdx, nextSize;
	double nextFn, nextSumSq, nextTime, s;
	vector<int>::iterator iter;
	MPI::Status status;

	for (int i = 0; i < S_Total; i++) {
		Master_Results.stage1[i] = SOResult();
	}


	int cores_recv = 0;
	while (cores_recv < nP - 1) {
		// iterate over cores to find the next core that has message to send
		if (MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE, sys_tag, status)) {
			from = status.Get_source();
			MPI::COMM_WORLD.Recv(&numSys, 1, MPI::INT, from, sys_tag);
			nRecv += numSys;


			double* master_fn = new double[numSys];
			double* master_sumsq = new double[numSys];
			MPI::COMM_WORLD.Recv(master_fn, numSys, MPI::DOUBLE, from, fn_tag);
			MPI::COMM_WORLD.Recv(master_sumsq, numSys, MPI::DOUBLE, from, sumsq_tag);
			int i, k = 0;
			for (iter = CoreToSys[from].begin(); iter != CoreToSys[from].end(); iter++, k++) {
				i = *iter;
				Master_Results.stage1[i].addSample(bsize, master_fn[k] * bsize, master_sumsq[k], Simulation_Times[i]);
				s = Master_Results.stage1[i].getSampleStdev();
				S2[i] = s*s;
			}
			delete[] master_fn;
			delete[] master_sumsq;

			strs << "Recieved results from worker " << from << endl
					<< nRecv << " received" << endl
					<< "Time: " << this->getTime() << endl;
			logger->log(strs.str(), VERBOSE);
			strs.str("");

			cores_recv += 1;

		} //end if
	} // end while
	//	}
	strs << "Finished phase 1 receive" << endl
			<< "Time: " << this->getTime() << endl;
	logger->log(strs.str(), VERBOSE);
	strs.str("");
}

void NSGSParallelV1::Master_Phase_1_Survivor_Recv() {

	int numrecv;
	int core_recv = 0;
	int numelim = 0;

	for (int i = 0; i < S_Total; i++) {
		Master_Elim[i] = false;
	}

	while (core_recv < nP - 1) {

		for (int n = 1; n < nP; n++) {
			bool recv = MPI::COMM_WORLD.Iprobe(n, sys_num_tag);

			if (recv) {
				MPI::COMM_WORLD.Recv(&numrecv, 1, MPI::INT, n, sys_num_tag);

				int* idx_list = new int[numrecv];
				int* sys_elim = new int[numrecv];

				MPI::COMM_WORLD.Recv(idx_list, numrecv, MPI::INT, n, idx_tag);
				MPI::COMM_WORLD.Recv(sys_elim, numrecv, MPI::INT, n, ELIM);
				int thiselim = 0;
				for (int i = 0; i < numrecv; i++) {
					Master_Elim[idx_list[i]] = (sys_elim[i] == 1 ? true : false);
					(sys_elim[i] == 1 ? thiselim++ : 0);
				}



				strs << "received " << numrecv << " systems from " << n << endl
						<< thiselim << " systems eliminated. " << endl;
				logger->log(strs.str(), USEFUL);
				strs.str("");

				core_recv++;
				numelim += thiselim;

				strs << "Total systems eliminated: " << numelim << endl;
				logger->log(strs.str(), USEFUL);
				strs.str("");
			}
		}



	}

	logger->log("Survivors collected! Successfuly completed phase 1.", USEFUL);



}

//Worker Simulates systems and sends back all systems, followed by survivors only.

void NSGSParallelV1::Worker_Phase_1() {

	// receives the seed from Master
	MPI_Bcast(tempSeed, 6, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);


	if (SORngStream::SetPackageSeed(tempSeed) == false)
		cerr << "Error seed for Worker routine\n";

	for (int i = 0; i <= myRank; ++i) {
		SORngStream rs;
	}

	SORngStream rs;
	WorkerStreams.resize(S_Total);

	for (int i = 0; i < S_Total; ++i) {
		rs.ResetNextSubstream();
		rs.GetState(tempSeed);
		WorkerStreams[i].SetSeed(tempSeed);
	}




	int sz;
	double fn, sumsq, simtime;
	MPI::Status status;
	SOResult tmp_answer;

	MPI::COMM_WORLD.Recv(&S_worker, 1, MPI::INT, 0, sys_tag);
	int dummy;

	//worker receives the list of systems and seeds from master

	problem->clear();
	workerSeeds.clear();
	workerSeeds.resize(S_worker, 6, 0);

	worker_system_idx = new int[S_worker];
	worker_stage_sizes = new int[S_worker];

	worker_phase0_send = new double[S_Total];

	MPI::COMM_WORLD.Recv(worker_system_idx, S_worker, MPI::INT, 0, idx_tag);
	MPI::COMM_WORLD.Recv(worker_stage_sizes, S_worker, MPI::INT, 0, size_tag);
	MPI::COMM_WORLD.Recv(workerSeeds[0], 6 * S_worker, MPI::UNSIGNED_LONG, 0, seed_tag);

	strs << "Worker " << myRank << " is processing:" << endl;
	for (int i = 0; i < S_worker; i++) {
		strs << "System " << worker_system_idx[i] << endl;
	}
	logger->log(strs.str(), VERBOSE);
	strs.str("");

	worker_phase1_fn_send = new double[S_worker];
	worker_phase1_sumsq_send = new double[S_worker];

	//	worker_phase0_sys_sent = 0;

	// performs simulation, one replication for each system, and send results immediately back to the master
	//	bool message_received = false;
	for (int j = 0; j < S_worker; j++) {
		//simulate a single system
		this->Worker_Simulate_Single_System(j, worker_stage_sizes[j]);
		sz = worker_stage_sizes[j];
		fn = problem->getFn();
		sumsq = problem->getFnSumSq();
		simtime = problem->getSimulationTime();
		tmp_answer.clear();
		tmp_answer.addSample(sz, fn*sz, sumsq, simtime);

		strs << "Index " << worker_system_idx[j] << ": " << tmp_answer.getSampleMean()
                				<< "," << tmp_answer.getSampleSumsq() << endl;
		logger->log(strs.str(), VERBOSE);
		strs.str("");

		//Worker keeps local copy of system
		Worker_Results[worker_system_idx[j]] = tmp_answer;

		worker_phase1_fn_send[j] = fn;
		worker_phase1_sumsq_send[j] = sumsq;



		strs << "Worker " << myRank << " finished its sys " << j << ", size = " << sz << endl;
		logger->log(strs.str(), VERBOSE);
		strs.str("");

	}//end for

	// after finishing simulating all the systems, send remaining answers whenever master
	// is ready to receive

	map<int, SOResult>::iterator iter_ans;
	int num_sys = S_worker;
	MPI::COMM_WORLD.Send(&num_sys, 1, MPI::INT, 0, sys_tag);

	//phase 0 : send fn and sumsq
	MPI::COMM_WORLD.Send(worker_phase1_fn_send, S_worker, MPI::DOUBLE, 0, fn_tag);
	MPI::COMM_WORLD.Send(worker_phase1_sumsq_send, S_worker, MPI::DOUBLE, 0, sumsq_tag);

	delete[] worker_phase1_fn_send;
	delete[] worker_phase1_sumsq_send;


	// Now that worker has a local copy of all the systems it did, it can
	// perform screening.

	//TODO: Move the screen to a separate function.

	/********************************************************************/

	typedef map<int, SOResult>::iterator miter;
	list<int>::iterator iter;
	double Xbar_i, Xbar_j, ni, nj, tau, Y, W;

	for (miter it = Worker_Results.begin();
			it != Worker_Results.end();
			++it) {

		Worker_Phase_1_System_Eliminated[it->first] = false;

	}

	for (miter i = Worker_Results.begin(); i != Worker_Results.end(); ++i) {

		for (miter j = Worker_Results.begin(); j != Worker_Results.end(); ++j) {

			SOResult* temp_i = &(i->second);
			SOResult* temp_j = &(j->second);

			if (i->first == j->first) goto end_comp1;

			if (!Worker_Phase_1_System_Eliminated[i->first]) {

				Xbar_i = temp_i->getSampleMean() * this->minmax;
				Xbar_j = temp_j->getSampleMean() * this->minmax;
				ni = temp_i->sample_size;
				nj = temp_j->sample_size;

				double si = temp_i->getSampleStdev() * temp_i->getSampleStdev();
				double sj = temp_j->getSampleStdev() * temp_j->getSampleStdev();

				W = t * (pow((si) / double(nj) + sj / double(nj), 0.5));
				if (Xbar_i < Xbar_j - max(0.0, W - delta)) {
					// system i eliminated by system j
					Worker_Phase_1_System_Eliminated[i->first] = true;
					num_systems_eliminated += 1;

					strs << "Sys " << setw(7) << setfill('0') << j->first << " KO Sys "
							<< setw(7) << setfill('0') << i->first
							<< "; " << Xbar_j << "," << nj << "," << sj <<
							"," << Xbar_i << "," << ni << "," << si <<
							endl;
					logger->log(strs.str(), VERBOSE);
					strs.str("");


				}
			}
			if (!Worker_Phase_1_System_Eliminated[j->first]) {

				Xbar_i = temp_i->getSampleMean() * this->minmax;
				Xbar_j = temp_j->getSampleMean() * this->minmax;
				ni = temp_i->sample_size;
				nj = temp_j->sample_size;

				double si = temp_i->getSampleStdev() * temp_i->getSampleStdev();
				double sj = temp_j->getSampleStdev() * temp_j->getSampleStdev();

				W = t * (pow((si) / double(nj) + sj / double(nj), 0.5));

				if (Xbar_j < Xbar_i - max(0.0, W - delta)) {
					// system i eliminated by system j
					Worker_Phase_1_System_Eliminated[j->first] = true;
					num_systems_eliminated += 1;

					strs << "Sys " << setw(7) << setfill('0') << i->first << " KO Sys "
							<< setw(7) << setfill('0') << j->first
							<< "; " << Xbar_i << "," << ni << "," << si <<
							"," << Xbar_j << "," << nj << "," << sj <<
							endl;
					logger->log(strs.str(), VERBOSE);
					strs.str("");
				}
			}

			end_comp1:
			1;

		}
	}

	//Worker has screened local systems. Report survivors to master.

	//Send number of systems that system had
	MPI::COMM_WORLD.Ssend(&S_worker, 1, MPI::INT, 0, sys_num_tag);
	//Prepare send buffers

	int* idx_list = new int[S_worker];
	int* sys_elim = new int[S_worker];
	int idx_idx = 0;
	for (miter i = Worker_Results.begin(); i != Worker_Results.end(); ++i) {
		idx_list[idx_idx] = i->first;
		sys_elim[idx_idx] = Worker_Phase_1_System_Eliminated[i->first];
		idx_idx++;
	}

	MPI::COMM_WORLD.Ssend(idx_list, S_worker, MPI::INT, 0, idx_tag);
	MPI::COMM_WORLD.Ssend(sys_elim, S_worker, MPI::INT, 0, ELIM);

	delete[] idx_list;
	delete[] sys_elim;



}

bool compare(const pair<int, double> &A, const pair<int, double> &B) {
	return A.second < B.second;
}


void NSGSParallelV1::Master_Phase_2() {

	//Master completes all remaining pairwise screening tasks according to Step 4 in the
	//combined procedure of Nelson et al.

	typedef map<int, SOResult>::iterator siter;
	double Xbar_i, Xbar_j, ni, nj, tau, Y, W;

	if (screenVersion == 3) {
		for (siter i = Master_Results.stage1.begin(); i != Master_Results.stage1.end(); ++i) {


			for (siter j = Master_Results.stage1.begin(); j != Master_Results.stage1.end();
					++j) {

				if (i == j) goto end_comp2;

				if (!Master_Elim[i->first]) {
					Xbar_i = i->second.getSampleMean() * this->minmax;
					Xbar_j = j->second.getSampleMean() * this->minmax;

					ni = i->second.sample_size;
					nj = j->second.sample_size;

					double si = pow(i->second.getSampleStdev(), 2.0);
					double sj = pow(j->second.getSampleStdev(), 2.0);

					W = t * (pow((si) / double(nj) + sj / double(nj), 0.5));
					if (Xbar_i < Xbar_j - max(0.0, W - delta)) {
						// system i eliminated by system j
						Master_Elim[i->first] = true;


						strs << "Sys " << setw(7) << setfill('0') << j->first << " KO Sys "
								<< setw(7) << setfill('0') << i->first
								<< "; " << Xbar_j << "," << nj << "," << sj <<
								"," << Xbar_i << "," << ni << "," << si <<
								endl;
						logger->log(strs.str(), VERBOSE);
						strs.str("");
					}


				}
				if (!Master_Elim[j->first]) {

					Xbar_i = i->second.getSampleMean() * this->minmax;
					Xbar_j = j->second.getSampleMean() * this->minmax;

					ni = i->second.sample_size;
					nj = j->second.sample_size;

					double si = pow(i->second.getSampleStdev(), 2.0);
					double sj = pow(j->second.getSampleStdev(), 2.0);

					W = t * (pow((si) / double(nj) + sj / double(nj), 0.5));

					if (Xbar_j < Xbar_i - max(0.0, W - delta)) {

						Master_Elim[j->first] = true;


						strs << "Sys " << setw(7) << setfill('0') << i->first << " KO Sys "
								<< setw(7) << setfill('0') << j->first
								<< "; " << Xbar_i << "," << ni << "," << si <<
								"," << Xbar_j << "," << nj << "," << sj <<
								endl;
						logger->log(strs.str(), VERBOSE);
						strs.str("");

					}
				}

				end_comp2:
				1;



			}
		}
	}else if(screenVersion==8){
		Master_Phase_2_parallel_screen();
	}




	//Master load-balances the required remaining work into "batches" which are then
	//completed by the workers in an efficient manner

	typedef deque<int>::iterator soiter;

	map<int, int> n1map = map<int, int>();

	double comp = pow(h / delta, 2.0);
	int counts = 0;
	double t1 = 0;

	map<int, double> time_map;


	for (siter i = Master_Results.stage1.begin(); i != Master_Results.stage1.end(); ++i) {

		if (!Master_Elim[i->first]) {
			double var = Master_Results(i->first, ONE).getSampleStdev();
			var *= var;
			var *= comp;
			n1map[i->first] = max(bsize, int(ceil(var)));

			strs << "Master computes phase 2 sample size for sys " << i->first <<
					", n0= " << bsize << ", n1= " << int(ceil(var)) - bsize << ", h=" << h <<
					", delta = " << delta << ", comp = " << comp << ", var=" << var <<
					endl;
			logger->log(strs.str(), VERBOSE);
			strs.str("");
			counts++;
			time_map[i->first] = Simulation_Times[i->first];
			t1 += Simulation_Times[i->first];

			Master_Results.stage2[i->first] = Master_Results.stage1[i->first];
		} else {
			n1map[i->first] = 0;
		}

	}

	//Compile list of systems to send out

	const double average_time = t1 / double(nP);

	CoreToSys.clear();

	logger->log("Assigning systems to workers.", VERBOSE);

	Scheduler sched = Scheduler(time_map);
	CoreToSys = sched.schedule_jobs(nP);

	for (int i = 1; i < nP; i++) {

		int* n1s = new int[CoreToSys[i].size()];



		int size = CoreToSys[i].size();


		for (int m = 0; m < CoreToSys[i].size(); m++) {
			n1s[m] = n1map[CoreToSys[i][m]];
			phase2_simulations += (double) (n1s[m] - bsize);


			strs << "Master sends sys " << CoreToSys[i][m] <<
					" to worker " << i << ", size = " << n1s[m] - bsize << endl;
			logger->log(strs.str(), VERBOSE);
			strs.str("");
		}

		//size, idx, n1s, seeds

		MPI::COMM_WORLD.Send(&size, 1, MPI::INT, i, size_tag);
		MPI::COMM_WORLD.Send(&CoreToSys[i][0], size, MPI::INT, i, idx_tag);
		MPI::COMM_WORLD.Send(n1s, size, MPI::INT, i, N1_TAG);


		delete[] n1s;

		logger->log("Sent systems to worker", VERBOSE);



	}
	strs << "Phase 2 requires " << phase2_simulations << " simulations " << endl;
	logger->log(strs.str(), ESSENTIAL);
	strs.str("");

	//Master compiles all sample mean results as they come in and selects the system with
	//the largest sample mean as best once all sampling has been completed.




	int sys_recv = 0;

	while (sys_recv < nP - 1) {
		for (int i = 1; i < nP; i++) {
			if (MPI::COMM_WORLD.Iprobe(i, size_tag)) {
				int size;


				MPI::COMM_WORLD.Recv(&size, 1, MPI::INT, i, size_tag);

				int* indexes = new int[size];
				double* sumsq = new double[size];
				double* fn = new double[size];
				int* sizes = new int[size];

				//size, idx, sumsq, fn, sizes

				MPI::COMM_WORLD.Recv(indexes, size, MPI::INT, i, idx_tag);
				MPI::COMM_WORLD.Recv(sumsq, size, MPI::DOUBLE, i, sumsq_tag);
				MPI::COMM_WORLD.Recv(fn, size, MPI::DOUBLE, i, fn_tag);
				MPI::COMM_WORLD.Recv(sizes, size, MPI::INT, i, N1_TAG);

				for (int m = 0; m < size; m++) {
					if (sizes[m] != 0) {
						Master_Results.stage2[indexes[m]].addSample(sizes[m], fn[m] * sizes[m], sumsq[m]);
					}
					strs << "Master recvs sys " << indexes[m] <<
							" from worker " << i << ", size = " << sizes[m] << endl;
					logger->log(strs.str(), VERBOSE);
					strs.str("");
				}

				delete[] indexes;
				delete[] sumsq;
				delete[] fn;
				delete[] sizes;

				sys_recv++;
				logger->log("System received", VERBOSE);
			}
		}
	}

	typedef map<int, SOResult>::iterator miter;

	miter best_answer = Master_Results.stage2.begin();

	for (miter i = Master_Results.stage2.begin(); i != Master_Results.stage2.end(); ++i) {
		strs << "Comparing i [" << i->first << "," << i->second.getSampleMean() << "," <<
				i->second.sample_size << "] to best_answer ["
				<< best_answer->first << "," << best_answer->second.getSampleMean() << "," <<
				best_answer->second.sample_size << "] " << endl;
		logger->log(strs.str(), VERBOSE);
		strs.str("");
		if ( (i->second.getSampleMean() - best_answer->second.getSampleMean())
				* this->minmax > 0 )
			best_answer = i;
	}




	strs << "The best answer is index " << best_answer->first
			<< " with sample mean " << best_answer->second.getSampleMean() << endl;

	/*strs << endl << "Time elapsed = " << this->getTime() << endl;
    strs << "Cores used: " << nP << endl;
    strs << "Systems: " << S_Total << endl;
    strs << "Global Seed: " << seed_GLOBAL << endl;
    strs << "n0: " << bsize << endl;
    strs << "Phase 0 Simulations: " << phase0_simulations << endl;
    strs << "Phase 1 Simulations: " << phase1_simulations << endl;
    strs << "Phase 2 Simulations: " << phase2_simulations << endl;
    strs << "Total simulations: " << phase0_simulations + phase1_simulations + phase2_simulations << endl;*/

	double _t = this->getTime();
	strs << "algo,n0,n1,bsize,bmax,delta,seed,screenVer,core,time,RB,cov,systems,"<<
			"stage2sys,simcount_0,simcount_1,simcount_2,simcount_3,totalsim,winner,mean,h" << endl;
	strs << "NSGS," << n0 << "," << bsize << ","<< bsize << ",0," << delta << "," <<
			seed_GLOBAL << "," << screenVersion << "," <<
			nP << "," << _t << "," <<
			RB_global << "," << burnin_param << "," << S_Total << "," << counts << "," <<
			phase0_simulations << "," << phase1_simulations << "," <<
			phase2_simulations << ",0," <<
			phase0_simulations + phase1_simulations + phase2_simulations << "," <<
			best_answer->first << "," <<
			best_answer->second.getSampleMean() << "," << h << "," <<
			endl;

	strs << "------------------------------------------------------" <<endl;
	strs << "Success! " ;
	strs << "Winner is system " << best_answer->first <<", t = " << _t << endl;
	strs << "------------------------------------------------------" <<endl;
	logger->log(strs.str(), ESSENTIAL);
	strs.str("");

}

void NSGSParallelV1::Worker_Phase_2() {

	if (screenVersion == 8) Worker_Phase_2_parallel_screen();


	//size, idx, n1s, seeds

	int size;

	MPI::COMM_WORLD.Recv(&size, 1, MPI::INT, 0, size_tag);

	worker_system_idx = new int[size];

	MPI::COMM_WORLD.Recv(worker_system_idx, size, MPI::INT, 0, idx_tag);

	int* n1s = new int[size];
	workerSeeds.clear();
	workerSeeds.resize(size, 6, 0);



	MPI::COMM_WORLD.Recv(n1s, size, MPI::INT, 0, N1_TAG);





	//Perform additional simulations

	S_worker = size;
	SOResult tmp_answer;
	double* worker_phase2_fn_send = new double[size];
	double* worker_phase2_sumsq_send = new double[size];
	int* worker_phase2_n1_send = new int[size];

	for (int i = 0; i < S_worker; i++) {
		if (n1s[i] - bsize > 0) {

			problem->Initialize_Single_System(worker_system_idx[i],
					n1s[i] - bsize, &WorkerStreams[worker_system_idx[i]]);
			problem->Objective();
			double fn = problem->getFn();
			double sumsq = problem->getFnSumSq();


			tmp_answer.clear();
			tmp_answer.addSample((n1s[i] - bsize), fn * (n1s[i] - bsize), sumsq);

			strs << "Index " << worker_system_idx[i] << ": " << tmp_answer.getSampleMean()
                    				<< ", " << tmp_answer.getSampleStdev() << ", " << tmp_answer.sample_size
                    				<< endl;
			logger->log(strs.str(), VERBOSE);
			strs.str("");

			//Worker keeps local copy of system


			worker_phase2_fn_send[i] = fn;
			worker_phase2_sumsq_send[i] = sumsq;
			worker_phase2_n1_send[i] = n1s[i] - bsize;
		} else {
			worker_phase2_fn_send[i] = numeric_limits<double>::max();
			worker_phase2_sumsq_send[i] = numeric_limits<double>::max();
			worker_phase2_n1_send[i] = 0;
		}



		strs << "Worker " << myRank << " finished its sys " << i << endl;
		logger->log(strs.str(), VERBOSE);
		strs.str("");
	}

	//size, idx, sumsq, fn, sizes

	MPI::COMM_WORLD.Ssend(&S_worker, 1, MPI::INT, 0, size_tag);
	MPI::COMM_WORLD.Ssend(worker_system_idx, S_worker, MPI::INT, 0, idx_tag);
	MPI::COMM_WORLD.Ssend(worker_phase2_sumsq_send, S_worker, MPI::DOUBLE, 0, sumsq_tag);
	MPI::COMM_WORLD.Ssend(worker_phase2_fn_send, S_worker, MPI::DOUBLE, 0, fn_tag);
	MPI::COMM_WORLD.Ssend(worker_phase2_n1_send, S_worker, MPI::INT, 0, N1_TAG);

	delete[] worker_system_idx;
	delete[] worker_phase2_fn_send;
	delete[] worker_phase2_n1_send;
	delete[] worker_phase2_sumsq_send;





}


void NSGSParallelV1::Master_Phase_2_parallel_screen() {

    map<int, vector<int> > screen_map;

    for (int i = 1; i < nP; i++) {
        screen_map[i] = vector<int>();
    }

    vector<int> worker_indexes;
    vector<double> worker_means;
    vector<double> worker_stds;
    vector<int> worker_sizes;

    int count = 0;

    //Assign workers for comparison
    for (miter i = Master_Results.stage1.begin(); i != Master_Results.stage1.end(); ++i) {


        worker_indexes.push_back(i->first);
        worker_means.push_back(i->second.getSampleMean());
        worker_stds.push_back(i->second.getSampleStdev());
        worker_sizes.push_back(i->second.sample_size);
        screen_map[(((count++) % (nP - 1)) + 1)].push_back(i->first);

    }


    //Tell the workers which rows to screen
    for (int i = 1; i < nP; i++) {
        int size = screen_map[i].size();
        MPI::COMM_WORLD.Send(&size, 1, MPI::INT, i, size_tag);
        MPI::COMM_WORLD.Send(&screen_map[i][0], screen_map[i].size(), MPI::INT, i, sys_tag);
    }

    //Send the workers all the data for screening, could be smarter but extra processing
    //time is not worth the tiny amount of memory saved
    MPI::COMM_WORLD.Bcast(&S_Total, 1, MPI::INT, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_indexes[0], worker_indexes.size(), MPI::INT, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_means[0], worker_means.size(), MPI::DOUBLE, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_stds[0], worker_stds.size(), MPI::DOUBLE, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_sizes[0], worker_sizes.size(), MPI::INT, 0);
    MPI::COMM_WORLD.Barrier();

    vector<int> eliminated_idx;

    //Receive all the answers from workers
    for (int i = 1; i < nP; i++) {

        if (!MPI::COMM_WORLD.Iprobe(i, size_tag)) {
            i--;
        } else {
            int size;
            eliminated_idx.clear();
            MPI::COMM_WORLD.Recv(&size, 1, MPI::INT, i, size_tag);
            eliminated_idx.resize(size, 0);
            MPI::COMM_WORLD.Recv(&eliminated_idx[0], size, MPI::INT, i, elimidx_tag);

            for (vector<int>::iterator iter = eliminated_idx.begin(); iter != eliminated_idx.end(); ++iter) {
                if (!Master_Elim[*iter]) Master_Elim[*iter] = true;
            }


        }

    }

    MPI::COMM_WORLD.Barrier();





}

void NSGSParallelV1::Worker_Phase_2_parallel_screen() {

    vector<int> screen_idx;

    int size;

    MPI::COMM_WORLD.Recv(&size, 1, MPI::INT, 0, size_tag);
    screen_idx.resize(size, 0);
    MPI::COMM_WORLD.Recv(&screen_idx[0], size, MPI::INT, 0, sys_tag);

    vector<int> worker_idx;
    vector<double> worker_means;
    vector<double> worker_stds;
    vector<int> worker_sizes;
    map<int, WorkerTemp> sys_vec;



    int s;
    MPI::COMM_WORLD.Bcast(&s, 1, MPI::INT, 0);
    worker_idx.resize(s, 0);
    worker_means.resize(s, 0.0);
    worker_stds.resize(s, 0.0);
    worker_sizes.resize(s, 0.0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_idx[0], s, MPI::INT, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_means[0], s, MPI::DOUBLE, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_stds[0], s, MPI::DOUBLE, 0);
    MPI::COMM_WORLD.Barrier();
    MPI::COMM_WORLD.Bcast(&worker_sizes[0], s, MPI::INT, 0);
    MPI::COMM_WORLD.Barrier();

    for (int i = 0; i < worker_idx.size(); i++) {
        WorkerTemp temp;
        temp.idx = worker_idx[i];
        temp.mean = worker_means[i];
        temp.size = worker_sizes[i];
        temp.std = worker_stds[i];
        sys_vec[worker_idx[i]] = temp;
    }

    map<int, bool> elim;

    for (int i = 0; i < worker_idx.size(); i++) {
        elim[worker_idx[i]] = false;
    }

    typedef map<int, WorkerTemp>::iterator siter;
    double Xbar_i, Xbar_j, ni, nj, tau, Y, W, si, sj;
    //screen
    for (vector<int>::iterator iter = screen_idx.begin(); iter != screen_idx.end(); ++iter) {

        WorkerTemp* i = &sys_vec[*iter];

        for (siter jiter = sys_vec.begin(); jiter != sys_vec.end();
                ++jiter) {

            WorkerTemp* j = &(jiter->second);

            if (i->idx == j->idx) goto end_comp2;

            if (!elim[i->idx]) {
                Xbar_i = i->mean * this->minmax;
                Xbar_j = j->mean * this->minmax;

                ni = i->size;
                nj = j->size;

                double si = pow(i->std, 2.0);
                double sj = pow(j->std, 2.0);

                W = t * (pow((si) / double(nj) + sj / double(nj), 0.5));
                if (Xbar_i < Xbar_j - max(0.0, W - delta)) {
                    // system i eliminated by system j
                    elim[i->idx] = true;


                    strs << "Sys " << setw(7) << setfill('0') << j->idx << " KO Sys "
                            << setw(7) << setfill('0') << i->idx
                            << "; " << Xbar_j << "," << nj << "," << sj <<
                            "," << Xbar_i << "," << ni << "," << si <<
                            endl;
                    logger->log(strs.str(), VERBOSE);
                    strs.str("");
                }


            }
            if (!elim[j->idx]) {

                Xbar_i = i->mean * this->minmax;
                Xbar_j = j->mean * this->minmax;

                ni = i->size;
                nj = j->size;

                double si = pow(i->std, 2.0);
                double sj = pow(j->std, 2.0);

                W = t * (pow((si) / double(nj) + sj / double(nj), 0.5));

                if (Xbar_j < Xbar_i - max(0.0, W - delta)) {

                    elim[j->idx] = true;


                    strs << "Sys " << setw(7) << setfill('0') << i->idx << " KO Sys "
                            << setw(7) << setfill('0') << j->idx
                            << "; " << Xbar_i << "," << ni << "," << si <<
                            "," << Xbar_j << "," << nj << "," << sj <<
                            endl;
                    logger->log(strs.str(), VERBOSE);
                    strs.str("");

                }
            }

end_comp2:
            1;



        }
    }

    vector<int> elim_sys;

    for (map<int, bool>::iterator iter = elim.begin(); iter != elim.end(); ++iter) {
        if (iter->second) elim_sys.push_back(iter->first);
    }


    int ret_size = elim_sys.size();
    MPI::COMM_WORLD.Send(&ret_size, 1, MPI::INT, 0, size_tag);
    MPI::COMM_WORLD.Send(&elim_sys[0], ret_size, MPI::INT, 0, elimidx_tag);

    MPI::COMM_WORLD.Barrier();

}



