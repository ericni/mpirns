/**
 * @file SOAlgorithm.h
 *
 * @brief Defines SOAlgorithm interface
 *
 */
#ifndef SOALGORITHM_H_
#define SOALGORITHM_H_

#include "mpi.h"
#include <vector>
#include "SOProb.h"
#include "SORngStream.h"
#include <sstream>
#include <string>
#include <iostream>
using namespace std;

/**
 * @brief Set to 1 to use the boost library, set to 0 to use the GNU Scientific Library (GSL).
 */
#define USE_BOOST_LIB 1

#if USE_BOOST_LIB
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>
#include <boost/math/distributions/students_t.hpp>
#else
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#endif

/**
 * Enumeration used to identify various types of MPI messages.
 */
enum TagType{seed_tag, x_cont_tag, x_disc_tag, fn_tag, sumsq_tag, sampleidx_tag,
	samplesize_tag, num_elim_tag, elimidx_tag, flag_tag, idx_tag, size_tag, batch_tag,
	sys_tag, ready_master_tag, runtime_tag, s2_tag, stop_tag, reassign_tag, best_tag,
	ready_worker_tag, row_num_tag, sys_num_tag, systems_request_tag, result_tag,
	sys_req_size, result_size_tag, exec_tag, stage_tag, screentime_tag,
	TAGS_MAX
};

/**
 * @addtogroup interface
 *
 */
/*@{*/
/**
 * @brief SO algorithm interface
 *
 * A abstract class for SO Algorithms.
 *
 * @note Any specific SO algorithm should implement the SOAlgorithm::initialize and
 * SOAlgorithm::run functions.
 */
class SOAlgorithm {
public:
//	SOAlgorithm() {}

	virtual ~SOAlgorithm() {}

	/// Initializer
	/**
	 * Initialize the algorithm by passing a pointer to the SOProb instance
	 * to be solved.
	 * Additional parameters may also be initialized depending on the specific
	 * SO algorithm.
	 */
	virtual void initialize(SOProb * prob) {
		problem = prob;
		this->minmax = prob->properties.minmax;
	}

	/// Runs the SO algorithm, assuming it has already been initialized.
	virtual void run () {}
protected:
	SOProb * problem; ///< A SO problem to be solved.
	SORngStream * pMasterStream; ///< A RngStream reserved for randomness required by the master.
	int minmax; ///< +1 for maximization problems, and -1 for minimization problems.
};
/*@}*/
#endif /* SOALGORITHM_H_ */
