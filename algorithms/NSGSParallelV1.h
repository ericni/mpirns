/*
Author: Nick Samson
Email: nes77@cornell.edu
NSGSParallelV1: Implements a parallel version of NSGS [Nelson et al., 2001]
 */
/**
 * @file NSGSParallelV1.h
 *
 * @brief Defines a parallel version of NSGS [Nelson et al., 2001]
 */

#pragma once
#include "SOAlgorithm.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "RinottFunc.h"
#include <vector>
#include <deque>
#include <algorithm>
#include <list>
#include <assert.h>
#include <set>
#include <map>
#include <sstream>
#include <iterator>
#include <limits>
#include <queue>
#if USE_BOOST_LIB
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>
#else
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#endif

using namespace std;

//#define NSGS_DEBUG
#define INVALID_SCREEN 1

#ifdef NSGS_DEBUG
#define _bsize0 20
#define _bsize 50
#define _n1 50
#else
#define _n00 n00
#define _bsize bsize
#endif

using namespace std;

namespace{

///Extension flags for keeping tag system simple in NSGSAlg
enum ExtFlags { SIGNAL_0 = 9000, SIGNAL_1, SIGNAL_2, ELIM, N1_TAG, MEANS, SUMS };

typedef struct log_statement {
	int rank;
	string datum;
} log_s;


typedef vector<log_s>::iterator logiter;
typedef vector<string>::iterator stringiter;
typedef vector<MPI::Request>::iterator reqiter;


typedef struct __wComp {
	bool operator() (pair<int, SOResult> &thus, pair<int, SOResult> &that) {
		return thus.second.getSampleMean() < that.second.getSampleMean();
	}
} wComp;

const int LOG_TAG = 9000;
enum LoggerState { ESSENTIAL, USEFUL, VERBOSE };
enum Stage { ZERO, ONE, TWO };

class ResultStorage {
public:
	ResultStorage(){
		stage0 = map<int,SOResult>();
		stage1 = map<int,SOResult>();
		stage2 = map<int,SOResult>();
	}
	~ResultStorage() {
		stage0.clear();
		stage1.clear();
		stage2.clear();
	}

	SOResult operator() (int idx, Stage select) {
		switch (select) {
		case ZERO:
			return stage0[idx];

		case ONE:
			return stage1[idx];

		case TWO:
			return stage2[idx];

		default:
			return stage1[idx];
		}
	}

	map<int, SOResult> stage0;
	map<int, SOResult> stage1;
	map<int, SOResult> stage2;
};

/**
 * Declares a logger class which algorithms can use to log their work
Workers send their data to the master, which arranges the output
 */
class Logger {

public:

	Logger(int rank, LoggerState state){
		this->_rank = rank;
		this->logstate = state;
		this->_size = MPI::COMM_WORLD.Get_size();
	}
	virtual void log(string data, LoggerState state){

		if (this->logstate >= state) {

			switch (this->_rank) {

			case 0:
				cout << "Master: " << data << endl;
				this->flushlog();
				break;

			default:
				MPI::Request newreq = MPI::COMM_WORLD.Isend(data.c_str(), data.length() + 1, MPI::CHAR, 0, LOG_TAG);
				this->reqs.push_back(newreq);
				this->data.push_back(data);
				this->flushlog();
				break;

			}
		}
	}
	virtual void flushlog(){
		switch (this->_rank) {
		case 0:
			for (logiter it = this->ldata.begin(); it != this->ldata.end(); ++it) {
				cout << it->rank << ": " << it->datum << endl;
			}
			this->data.clear();
			break;

		default:
			vector<reqiter> reqptrs;
			vector<stringiter> strs;
			reqiter rb = this->reqs.begin();
			stringiter sb = this->data.begin();
			for (int i = 0; i < this->reqs.size(); i++) {
				if (reqs[i].Test()) {
					reqptrs.push_back(rb + i);
					strs.push_back(sb + i);
				}
			}

			for (int i = strs.size()-1; i >=0; i++) {
				this->reqs.erase(reqptrs[i]);
				this->data.erase(strs[i]);
			}
			reqptrs.clear();
			strs.clear();
			break;
		}
	}

	virtual bool recv(){
		bool go = true;
		bool returnstate = false;

		if (_rank != 0) go = false;

		while (go) {

			go = false;

			for (int temp = 1; temp < this->_size; temp++) {

				if (MPI::COMM_WORLD.Iprobe(temp, LOG_TAG)) {
					char buff[1000];
					MPI::COMM_WORLD.Recv(buff, 1000, MPI::CHAR, temp, LOG_TAG);
					log_s templ;
					templ.rank = temp;
					templ.datum = string(buff);
					this->ldata.push_back(templ);
					go = true;
					returnstate = true;
				}
			}

		}

		return returnstate;

	}
	static LoggerState convertInt(int stateNum){
		if (stateNum > 2) {
			return VERBOSE;
		}

		switch (stateNum) {
		case 0:
			return ESSENTIAL;

		case 1:
			return USEFUL;
		}

		return VERBOSE;
	}
	virtual bool simple() { return false; }
protected:

	ostringstream out;
	vector<MPI::Request> reqs;
	vector<string> data;
	LoggerState logstate;
	int _rank;
	int _size;
	vector<log_s> ldata;



};

class SimpleLogger : public Logger {

public:
	SimpleLogger(int rank, LoggerState state): super(rank, state) {}

	void log(string data, LoggerState state){

		if (this->logstate >= state) {
			out << "[" << this->_rank << "]" << ": " << data << endl;
			cout << out.str();
			out.str("");
		}

	}
	void flushlog() {};
	bool recv() { return true; }
	bool simple() { return true; }

private:
	typedef Logger super;
};

class SchedSub {

public:

	SchedSub(int ind, double tim) { this->index = ind; this->time = tim; }

	int index;
	double time;

	bool operator<(const SchedSub& that) const {
		return this->time < that.time;
	}
	bool operator==(const SchedSub& that) const {
		return this->time == that.time;
	}
	bool operator>(const SchedSub& that) const {
		return !(*(this)<that || *(this) == that);
	}
	bool operator>=(const SchedSub& that) const { return !(*(this) < that); };
	bool operator<=(const SchedSub& that) const { return !(*(this) > that); };
	bool operator!=(const SchedSub& that) const { return !(*(this) == that); };


};

class WorkerSub {

public:

	WorkerSub(int ind, double tim) { this->index = ind; this->time = tim; }

	int index;
	double time;

	bool operator<(const WorkerSub& that) const {
		return this->time > that.time;
	}
	bool operator==(const WorkerSub& that) const {
		return this->time == that.time;
	}
	bool operator>(const WorkerSub& that) const {
		return !(*(this)<that || *(this) == that);
	}
	bool operator>=(const WorkerSub& that) const { return !(*(this) < that); };
	bool operator<=(const WorkerSub& that) const { return !(*(this) > that); };
	bool operator!=(const WorkerSub& that) const { return !(*(this) == that); };



};
typedef struct WorkerTemp {
	int idx;
	double mean;
	double std;
	int size;
} WorkerTemp;


class Timer {
private:

	timeval startTime;

public:

	void start(){
		gettimeofday(&startTime, NULL);
	}

	double stop(){
		timeval endTime;
		long seconds, useconds;
		double duration;

		gettimeofday(&endTime, NULL);

		seconds  = endTime.tv_sec  - startTime.tv_sec;
		useconds = endTime.tv_usec - startTime.tv_usec;

		duration = seconds + useconds/1000000.0;

		return duration;
	}

	static void printTime(double duration){
		printf("%5.6f seconds\n", duration);
	}
};
}

/**
 * @addtogroup algo
 *
 */
/*@{*/
/**
 * @brief Parallel NSGS procedure.
 *
 * A parallel version of the NSGS procedure.
 *
 * 		Original paper by Nelson, B. L., J. Swann, D. Goldsman, and W. Song. 2001.
 * 		"Simple Procedures for Selecting the Best Simulated System When the
 * 		Number of Alternatives is Large".
 * 		Operations Research 49 (6): 950-963.
 *
 * Converted to parallel procedure by Susan R. Hunter and Eric Ni.
 *
 * Parallel implementation by Nick Samson.
 *
 * See discussions and tests on this procedure in
 *
 * 		Ni, E. C., S. G. Henderson, and S. R. Hunter. 2014.
 * 		"A Comparison of Two Parallel Ranking and Selection Procedures".
 * 		In Proceedings of the 2014 Winter Simulation Conference.
 */
class NSGSParallelV1 : public SOAlgorithm {

public:

	NSGSParallelV1();
	~NSGSParallelV1();
	void run();
	void initialize(SOProb* prob);


protected:
	double alpha, delta, lambda, a, t0, t, TotalSimulationTime, timePerCore, h;
	int S_Total, n0, bsize, num_systems_eliminated, num_systems_remain_after_phase0;
	double num_sim_runs, phase0_simulations, phase1_simulations, phase2_simulations;
	int myRank, nP, S_worker;
	unsigned long * currentSeed;
	unsigned long * tempSeed;
	deque<SOResult> Master_BestResults;
	ResultStorage Master_Results;
	map<int, SOResult> Worker_Results;
	map<int, vector<int> > CoreToSys;
	map<int, bool> Master_Elim;
	vector<SORngStream> WorkerStreams;
	Logger* logger;
	ostringstream strs;

	deque<MPI::Request> Master_Requests_P0;
	map<int,double> Simulation_Times;

	Matrix<unsigned long> workerSeeds;
	int* worker_system_idx;
	int* worker_stage_sizes;
	double* worker_phase1_fn_send;
	double* worker_phase1_sumsq_send;
	double* worker_phase0_send;

	map<int, int> Worker_Phase_1_System_Eliminated;

	map<int,double> S2;

	int recv_dump;


private:

	void Master_Routine();
	void Worker_Routine();

	//Get simulation times so workload can be distributed

	void Master_Phase_0();
	void Master_Phase_0_GenerateList();
	void Master_Phase_0_Send();
	void Master_Phase_0_Recv();

	void Worker_Phase_0();
	void Worker_Simulate_Single_System(int, int);

	//Phase 0 complete

	void Master_Phase_1();
	void Master_Phase_1_GenerateList();
	void Master_Phase_1_Send();
	void Master_Phase_1_Recv();
	void Master_Phase_1_Survivor_Recv();

	void Worker_Phase_1();

	void Master_Phase_2();
	void Worker_Phase_2();

	void Master_Phase_2_parallel_screen();
	void Worker_Phase_2_parallel_screen();

	inline double getTime();
};
/*@}*/
