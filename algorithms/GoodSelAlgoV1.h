/**
 * @file GoodSelAlgoV1.h
 *
 * @brief Defines a parallel SO algorithm with good selection guarantee [Ni et al., 2014]
 */
#ifndef GoodSelAlgoV1_H_
#define GoodSelAlgoV1_H_

#include "SOAlgorithm.h"
#include <vector>
#include <deque>
#include <algorithm>
#include <set>
#include <list>
#include <assert.h>
using namespace std;

/**
 * @brief Number of best systems to keep in each batch.
 *
 * Set to 0 to let the master keep one best system from each worker.
 * Otherwise, the master keeps the best BEST_ANS_MAX systems in each batch.
 *
 */
#define BEST_MAX 20

/**
 * @addtogroup algo
 *
 */
/*@{*/
/**
 * @brief Parallel good selection procedure.
 *
 * A parallel ranking and selection procedure providing good-selection guarantee.
 *
 * Originally proposed in
 *
 * 		Ni, E. C., S. G. Henderson, and S. R. Hunter. 2014.
 * 		"A Comparison of Two Parallel Ranking and Selection Procedures".
 * 		In Proceedings of the 2014 Winter Simulation Conference.
 *
 */
class GoodSelAlgoV1 : public SOAlgorithm {
public:
	GoodSelAlgoV1();
	~GoodSelAlgoV1() ;
	void initialize(SOProb * prob) ;
	void run();
	Matrix<unsigned long> workerSeeds;
	vector<int> worker_system_idx;
	vector<int> worker_batch_sizes;
protected:
	double alpha, delta, eta, t0, t, TotalSimulationTime, timePerCore;
	int S_Total, n0, n1, batchmax, bestmax, bsize, num_systems_eliminated;
	int num_systems_remain_after_phase1;
	int myRank, nP, S_worker;
	int num_core_terminated; //int s_curr;
	int num_sys_completed_phase2;
	double rinott_h;
	vector< list<int> > Core2SysLists; // // range [0 ~ nP-2][0 ~ S-1]
	vector<int> Sys2CoreList; // sys/core
	//	vector< list<int> > Sys2CoreLists; // range [0 ~ S-1][0 ~ nP-2]
	vector<int> Master_phase1_num_sys_each_core;
	vector<int> system_eliminated;
	vector<int> core_eliminated; // range 0 ~ nP-1
	vector<int> rnd_perm;
	set<int> indexes_recvd;
	vector<SORngStream> WorkerStreams;
#if BEST_MAX > 0
	typedef struct {
		double Xbar;
		int id, size;
	} BESTRESULT_T;
	struct greater_result{
		bool operator()(const BESTRESULT_T& a,const BESTRESULT_T& b) const{
			return a.Xbar > b.Xbar;
		}
	};
	BESTRESULT_T * best_buffer;
	MPI_Datatype best_type;
#endif
	typedef struct {
		int idx, batch;
		double fn, sumsq, runtime;
	} SINGLERESULT_T;
	MPI_Datatype singleresult_type;
	SINGLERESULT_T tmpResult;
private:
	double getCurrentTime();
	int master_phase1_current_batch, master_phase1_current_sys;
	int master_phase2_current_size, master_phase2_current_order, Master_Phase2_nSys;
	int worker_phase1_batch, worker_phase1_sys;
	int worker_phase2_size,  worker_phase2_sys;
	double worker_phase1_fn, worker_phase1_sumsq, worker_phase1_t;
	double worker_phase2_fn, worker_phase2_t, screen_t;
	deque< deque<SOResult> > Master_Results; // sys (0~S_Total)/ batch/SOResult
	deque< map<int, SOResult> > Master_Temp_Results; // sys (0~S_Total)/ batch/SOResult
	vector<double> S2;
	vector<double> Simulation_Times;
	vector<int> Phase1_Batch_Sizes;
	vector<int> Phase1_Max_Sizes;
	vector<int> Cores_Hibernated;
	vector<int> Master_Phase2_Num_Sys;
	vector<int> Master_Phase2_ID;
	vector<double> Master_Phase2_Mean;
	vector<int> Master_Phase2_Phase1Size;
	vector<int> Master_Phase2_AdditionalSize;
	vector<int> Master_Phase2_SentSize;
	vector<int> Master_Phase2_TotalSize;
	vector<int> Master_Phase2_ID2Order;
	vector<double> worker_phase00_send;
	vector<double> worker_phase00_s2_send;
	vector<double> worker_phase0_t_send;
	vector<double> worker_phase0_fn_send;
	vector<double> worker_phase0_sumsq_send;
	//	deque<vector<SOResult> > worker_phase0_results_keep; //indexed from 0 onwards; batch/sys(worker: 0~S_worker-1)/result
	vector<deque<SOResult> > worker_phase1_results_keep; //indexed from batch 0 onwards; sys(worker: 0~S_Total)/batch/result
	//	vector<map<int, SOResult> > worker_Results_send_phase1; //indexed from 0 onwards
	map<int, deque<SOResult> > map_worker_results_received; //sys/batch/result
	deque<int> worker_eliminated_system_to_send;
	deque<int> worker_eliminated_system_simcount;
	deque<unsigned long> Master_simcount_phases;  //4
	deque<double> Master_timing_phases;  //4
	vector<int> Master_Phase1_Best_Results_ID; // core<sys index>
	deque<deque<SOResult> > Master_Phase1_Best_Results;  // core<batch<ans>>
	vector<int> Master_last_batch_screened_each_worker;     // core<sys index>
	//	vector<int> Master_batches_sent_each_worker;
	//	deque< deque<int> > Master_batches_recv_count; // worker/batch count(# systems received for this batch)
	vector<int> Master_num_elim_each_core;
	void report_winner();
	void Master_Routine();
	void Master_Phase00_PrepareList();
	void Master_Phase0_PrepareList();
	//	void Master_Phase0_pairwise_screen();
	void Master_Phase1_PrepareList();
	void Master_Phase00_0_Send(bool phase00);
	void Master_Phase00_0_Recv(bool phase00);
	//	void Master_Phase0_Recv_with_Screening();
	//	void Master_report_Phase0();
	//	void Master_Phase1_Send();
	void Master_Phase1_Initial_Send();
	void Master_Phase1_Execute();
	void Master_Phase1_StoreResult(const int & otherWorker,
			const int & sysid, const int & w_batch,
			const double & w_fn, const double & w_sumsq);
	void Master_Phase1_SendContFlag(const int & from);
	void Master_SendExec(const int & from, const int & exec);
	void Master_Phase1_SendTerminateExec(const int & from);
	void Master_Phase1_SendSimExec(const int & from);
	void Master_Phase1_SendScreenExec(const int & from);
	void Master_Phase1_TerminateWorkers();
	void Master_Phase1_MoveReq(const int & from, const int & exec);
	void Master_Phase1_Clean();
	void Master_Phase2_Execute();
	void Master_Phase2_Find_Winner();
	void Master_Phase1_RecvResult(const int & worker);
	void Master_Phase1_WakeWorker(const int & otherWorker);
	void Master_Phase1_UpdateBestAnsBuffer( SOResult * result,
			const int & sysid, const int & currBatch);
	void Master_Phase1_RecvScreening(const int & worker);
	int  Master_Phase1_getNumNewBatchesAvail(const int worker);
	int  Master_Phase1_findNextSimSystem();
	void Master_Phase1_SendScreenAns(const int & worker, const int & nNewBatches);
	void Master_Phase1_SendSimSys(const int & worker);
	//	void Master_Phase1_SendTermination(const int & worker);
	//	void Master_Phase1_WrapUp();
	void Master_Phase1_Recv_Eliminated(int worker);
	void Master_Phase1_Recv_Best(int worker);
	void Master_Phase1_Send_OtherBest(int worker);
	void Master_Phase2_RecvResult(const int & worker);
	void Master_Phase2_SendSimSys(const int & worker);
	int  Master_Phase2_findNextSimSystem();
	//	bool Master_Phase1_isCoreEliminated(int worker);
	void Master_PrintPhase0Summary();
	void Worker_Routine();
	void Worker_Phase00_0_Routine();
	//	void Worker_Phase0_Routine_with_Screening();
	//	void Worker_Phase0_Send_Results();
	void Worker_Phase1_Prepare();
	void Worker_Phase1_Execute();
	int Worker_Phase1_RecvExecute();
	int Worker_Phase1_RecvInitFlag();
	int Worker_Phase1_RecvContFlag();
	void Worker_Phase1_SendResult();
	void Worker_Phase1_SendScreening();
	void Worker_Phase1_RecvSimSys();
	void Worker_Phase1_RecvScreenAns();
	void Worker_Phase1_Clean();
	void Worker_Phase2_Execute();
	void Worker_Phase2_SendResult();
	void Worker_Phase2_RecvSimSys();
	void Worker_Phase2_Clean();
	void Worker_Send_CurrentBatch();
	void Worker_Recv_NumSys();
	void Worker_Recv_ListSys();
	//	void Worker_Recv_S2();
	void Worker_Phase1_Recv_OtherBest();
	//	void Worker_pairwise_screen_skipping();
	void Worker_pairwise_screen_no_skipping();
	void Worker_Phase1_Send_Eliminated();
	void Worker_Phase1_Send_Best();
	void Worker_Simulate_Single_System(int j, int nReps);
	//	void Worker_Simulate_Multiple_System();
	//	void Worker_Simulate_Batch();
	//	void Worker_Send_Batch(int);
	//	void Worker_Isend_Single_Result(int sys_idx, SOResult ans);
	//	void Worker_Initilize_Batches();
	unsigned long * currentSeed;
	unsigned long * tempSeed;
	vector<int> Master_elim_sys_idx;
	vector<int> Master_tmp_elim_sys_simcount;
	vector<int> Worker2ReqID; // position of request for each worker, position starting from 1
	vector<int> WtoMmsg;
	//	Matrix<int> init_disc;
	deque<map<int, int> > worker_otherbest_batches_screened; // records number of batches screened so far between any two systems, S_worker/S_others/
	int worker_last_batch_screened;
	int worker_phase0_sys_sent, worker_batches_toSend, worker_batches_simulated;
	int worker_last_batch_recvd; // begins from 0 when used as index. 0 = batch 0
	int worker_flag, dummy;
	//	list<int> Master_Recvd_Workers_List;
	string toString_Param();
	string toString_Core2Sys();
	string toString_BatchSizes();
	vector<MPI::Request> ReadyRecvRequests;
	// new
	MPI::Request * reqs_ptr;
};
/*@}*/
#endif // GoodSelAlgoV1_H_
