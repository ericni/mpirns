/**
 * @file NHH2013AlgoV2.cpp
 *
 * @brief Implements parallel NHH algorithm [Ni et al., 2013]
 */
extern int bsize_GLOBAL, seed_GLOBAL, n0_GLOBAL, n1_GLOBAL;
extern int useTimeEst, screenVersion, outputLevel, algo, RB_global;
extern double delta_GLOBAL, burnin_param;

#include "NHH2013AlgoV2.h"

NHH2013AlgoV2::NHH2013AlgoV2() {
	//	alpha = 0.05; delta = 0.01; t0 = 10.; t=10.; num_systems_eliminated = 0; lambda = alpha/2.;
	//	nP = 2; a = 100.0; myRank = 0; S = 100; n0=5;
	this->currentSeed = new unsigned long [6];
	this->tempSeed    = new unsigned long [6];
	this->myRank = MPI::COMM_WORLD.Get_rank();	//get process rank
	this->nP = MPI::COMM_WORLD.Get_size();     	//get number of processes
}

NHH2013AlgoV2::~NHH2013AlgoV2() {
	delete [] currentSeed; delete [] tempSeed; problem->clear();
	delete [] master_elim_sys_idx;
	delete [] this->master_tmp_elim_sys_simcount;
}

void NHH2013AlgoV2::initialize(SOProb * prob) {
	SOAlgorithm::initialize(prob);
	S_Total = problem->getNumSystems();
	alpha = 0.05;
	// debug
	delta = delta_GLOBAL;
	//	delta = 1.0;
	lambda = delta/2;
	// a_l, lower bound in Hong(2006); recommended by the paper

	n0 = n0_GLOBAL;
	if (n1_GLOBAL <= 0)
		n1 = (int) ( ceil(5. * log((double)S_Total) / log(2.) ) );
	else
		n1 = n1_GLOBAL;
	a  = 	((double)n1-1.0)/2.0/delta
			*(pow(2.0-2.0*pow(1.0-alpha,1.0/((double)S_Total-1.0) ), -2.0/(n1-1.0) ) -1.0);
	// a_u, upper bound in Hong(2006)
	//	a= 	((double)n0-1.0)/2.0/delta
	//			*(pow(1.0-1.0*pow(1.0-alpha,1.0/((double)S-1.0) ), -2.0/(n0-1.0) ) -1.0);
	bsize = bsize_GLOBAL;
	S2.resize(S_Total, 0.0); // every core keeps a copy of S2
	dummy = 0; worker_flag = 0;
	system_eliminated.clear(); system_eliminated.resize(S_Total, 0);
	this->num_systems_eliminated = 0;
	worker_stages_recvd = 0; worker_stages_screened = 0;
	//	CORE_FLAGS_MIN = TAGS_MAX + 1;
	//	CORE_FLAGS_MAX = CORE_FLAGS_MIN + (nP * MAX_SEND_RECV_GRP) - 1;
	//	SYS_FLAGS_MIN  = CORE_FLAGS_MAX + 1;
	//	SYS_FLAGS_MAX  = SYS_FLAGS_MIN  + (S_Total * MAX_SEND_RECV_GRP) - 1;
	Stage_Sizes.resize(S_Total);
	core_eliminated.clear(); core_eliminated.resize(nP-1, 0);
	Best_Results_idx.clear(); Best_Results_idx.resize(nP-1,-1);

	if (myRank > 0) {
		this->master_tmp_elim_sys_simcount = new int [2];
		this->master_elim_sys_idx = new int [2];
		worker_phase1_results_keep.resize(S_Total);
	}

	if (myRank > 0) return;

	// the following are used by master only
	Master_best_Results.clear(); Master_best_Results.resize(nP-1);
	master_phase1_current_stage = 0;
	master_phase1_current_sys   = S_Total - 1;
	core_terminated.clear(); core_terminated.resize(nP-1, false);
	//	Master_best_Results.clear(); Master_best_Results.resize(nP-1);
	//	Master_stages_sent_each_worker.clear();
	Master_stages_screened_each_worker.clear();
	//	Master_stages_sent_each_worker.resize(nP-1, 0);
	Master_stages_screened_each_worker.resize(nP-1, 0);
	//	Master_stages_recv_count.resize(nP-1);
	Master_num_elim_each_core.clear(); Master_num_elim_each_core.resize(nP-1,0);
	Master_simcount_systems.resize(S_Total);
	Master_simcount_phases.clear();  Master_simcount_phases.resize(3,0);
	//	if (outputLevel > 0)
	//		cout << "n0 = " << n0 << endl;

	Simulation_Times.resize(S_Total, 0.0);
	master_elim_sys_idx = new int [S_Total];
	master_tmp_elim_sys_simcount = new int [S_Total];
	//	//	master_tmp_elim_sys_idx.resize(S_Total, 0);
	//	deque<SOAnswer> dso; dso.clear();
	Master_Results.resize(S_Total);
	Master_Temp_Results.resize(S_Total);
	list<int> l;
	this->Core2SysLists.clear(); this->Core2SysLists.resize(nP-1, l);
	//	this->Sys2CoreLists.clear(); this->Sys2CoreLists.resize(S_Total, l);

	for (int i = 0; i < 6; i++) currentSeed[i] = (unsigned long) seed_GLOBAL;
	for (int i = 0; i < 6; i++)    tempSeed[i] = 0;
	if (SORngStream::SetPackageSeed(currentSeed)==false)
		cerr << "Error seed for Master routine" << endl;
	this->pMasterStream = new SORngStream();
	ReadyRecvRequests.resize(nP-1);
}

void NHH2013AlgoV2::run() {
	if (myRank == 0) {
		// the master core creates seeds and solutions, sends to workers
		Master_Routine();
	}
	else {
		// the worker cores receive seeds and run simulation
		Worker_Routine();
	}
}

void NHH2013AlgoV2::Master_Routine() {
	t0 = MPI::Wtime();

#ifdef TEST_CONFIGURES
	problem->readSystem();
#endif

	if(outputLevel > 1)
		cout << toString_Param();
	// Phase 00
	Master_Phase00_PrepareList();
	Master_Phase00_0_Send(true);
	Master_Phase00_0_Recv(true);
	//	Master_PrintSummary();
	// End Phase 00

	switch(screenVersion) {
	//screen version 1: phase 0 screen at master level
	case 1:
	case 3:
	case 5:
		// Phase 0
		Master_Phase0_PrepareList();
		Master_Phase00_0_Send(false);
		Master_Phase00_0_Recv(false);
		if(outputLevel > 1)
			Master_PrintPhase0Summary();
		// End Phase 0

		//perform pairwise screening at master level
		Master_Phase0_pairwise_screen();
		if (this->num_systems_eliminated >= S_Total - 1)	{
			report_winner();
			for (int i = 1; i <= nP-1; i++) {
				int flag = 0;
				MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	i, flag_tag);
			}
			return;
		}
		break;
	case 2:
	case 4:
	case 6:
	case 8:
		//screen version 2: phase 0 screen at core level

		// Phase 0
		Master_Phase0_PrepareList();
		Master_Phase00_0_Send(false);
		Master_Phase0_Recv_with_Screening();
		break;
	default:
		cerr << "Incorrect screen version, terminating..." << endl;
		return ;
	}

	// Phase 1
	if (screenVersion == 5 || screenVersion == 6) {
		Master_report_stage0();
		if (outputLevel >= 1)
			this->Master_PrintPhase0Summary();
	}
	else {
		Master_Phase1_Initial_Send();
		Master_Phase1_PrepareList();
		//		Master_Phase1_Send();
		//Phase 2

		Master_Phase1_Execute();

		this->report_winner();
		//	Master_PrintSummary();
	}
	//stop here
	//	int stop = 0;
	//	for (int j = 1; j <= nP-1; j++ )
	//		MPI::COMM_WORLD.Send(&stop, 1, MPI::INT, j, flag_tag);
	delete this->pMasterStream;
	cout << "End of Master Routine" << endl;
}

void NHH2013AlgoV2::Master_Phase00_PrepareList() {
	//in phase 00, distribute systems randomly
	//perform Fisher-Yates shuffle to get a randomized core2syslist
	int tmp_i, tmp_j, s;
	rnd_perm.resize(S_Total);
	for (int i = 0; i < S_Total; i++) rnd_perm[i]=i;
	for (int i = S_Total - 1; i >= 1; i--) {
		// use the next substream of pMasterStream to generate a random integer between 0 and i
		tmp_j = pMasterStream->RandInt(0, i);
		tmp_i = rnd_perm[tmp_j]; rnd_perm[tmp_j] = rnd_perm[i]; rnd_perm[i] = tmp_i;
	}
	pMasterStream->ResetNextSubstream();

	int num_systems_per_core = S_Total/(nP-1);
	int num_systems_extra    = S_Total%(nP-1); // number of cores to get  num_systems_per_core+1 systems

	//compile core2syslist
	int curr_sys = 0;
	for (int i = 1; i < nP; ++i) {
		s = (i<=num_systems_extra)?(num_systems_per_core+1):(num_systems_per_core);
		for (int j = 1; j <= s; ++j){
			this->Core2SysLists[i-1].push_back(rnd_perm[curr_sys++]);
		}
	}

	if (outputLevel > 0)
		cout << "Phase 00 list prepared by Master" <<", t= " << this->getCurrentTime() << endl;
	//debug
	//	cout << this->toString_Core2Sys() << endl;
	this->Master_simcount_phases[0] = (double) (S_Total * this->n0);
}

void NHH2013AlgoV2::Master_Phase0_PrepareList() {
	//in phase 0, distribute systems evenly according to the runtime
	for (int i = 0; i<Core2SysLists.size(); i++) Core2SysLists[i].clear();


	TotalSimulationTime = 0.0;
	for (int i = 0; i < S_Total; ++i) {
		TotalSimulationTime += Simulation_Times[i];
	}
	timePerCore = TotalSimulationTime / (nP-1); double cumTime = 0.0;
	int currCore = 1;
	//debug
	//	cout << "2" << endl;

	for (int i = 0; i < S_Total; ++i) {
		cumTime += Simulation_Times[i];
		if (cumTime > timePerCore * currCore) currCore += 1;
		Core2SysLists[min(nP-1,currCore) - 1].push_back(i);
	}
	//debug
	if (outputLevel >= 4)
		cout << this->toString_Core2Sys() << endl;
	if (outputLevel > 0)
		cout << "Phase 0 list prepared by Master" <<", t= " << this->getCurrentTime() << endl;
	this->Master_simcount_phases[1] = (double) ( S_Total * this->n1 );
}

void NHH2013AlgoV2::Master_report_stage0() {
	num_systems_remain_after_phase0 = S_Total - this->num_systems_eliminated;
	int stop = 0;
	for (int j = 1; j <= nP-1; j++ )
		MPI::COMM_WORLD.Send(&stop, 1, MPI::INT, j, flag_tag);

	cout << "results," << n0_GLOBAL << "," << bsize_GLOBAL << "," << delta_GLOBAL << "," <<
			seed_GLOBAL << "," << screenVersion << "," << nP << "," <<
			this->getCurrentTime() <<
			"," << num_systems_remain_after_phase0 << endl;
}

void NHH2013AlgoV2::Master_Phase00_0_Send(bool phase00) {
	//given the Core2SysLists, send job to respective cores, each system simulated numRep times

	list<int>::iterator iter; int idx, temp_s; int numRep = phase00?n0:n1;
	//send a go flag and the number of systems to each core
	int go = phase00?1:2;
	for (int i = 1; i <= nP-1; i++) {
		temp_s = Core2SysLists[i-1].size();
		MPI::COMM_WORLD.Send(&go,		1, MPI::INT, i, flag_tag);
		MPI::COMM_WORLD.Send(&temp_s,	1, MPI::INT, i, sys_tag);

		int v1 [temp_s];
		int v2 [temp_s];
		unsigned long seed_to_send[temp_s*6];
		int ss, k = 0;
		for (iter = Core2SysLists[i-1].begin(); iter!=Core2SysLists[i-1].end(); iter++, k++) {
			//create a new stream and get its seed
			SORngStream rs;
			rs.GetState(tempSeed);
			for (ss = 0; ss < 6; ss++) {
				seed_to_send[k*6+ss]=tempSeed[ss];
			}

			//get the next system to simulate
			//			vector<int> temp_disc = problem->Idx2System_Disc(*iter);

			//send seed to the worker
			//			MPI::COMM_WORLD.Send(tempSeed, 6, MPI::UNSIGNED_LONG, i, seed_tag);

			idx = *iter;
			v1[k] = idx;
			v2[k] = numRep;
			//send system index to the worker
			//			MPI::COMM_WORLD.Send(&idx, 1, MPI::INT, i, idx_tag);

			//send number of replications required for each system
			//			MPI::COMM_WORLD.Send(&numRep, 1, MPI::INT, i, size_tag);

			//send discrete decision variables to worker
			//			if (problem->prop.d_disc > 0)
			//				MPI::COMM_WORLD.Send(&temp_disc[0], problem->prop.d_disc,
			//						MPI::INT, i, x_disc_tag);

		}
		MPI::COMM_WORLD.Send(v1, temp_s, MPI::INT, i, idx_tag);
		MPI::COMM_WORLD.Send(v2, temp_s, MPI::INT, i, size_tag);
		MPI::COMM_WORLD.Send(seed_to_send, 6*temp_s, MPI::UNSIGNED_LONG, i, seed_tag);
		//		cout << "Master sending to core " << i << endl;
	}

	if (outputLevel > 0) {
		if (phase00)
			cout << "Phase 00 send finished by Master" <<", t= " << this->getCurrentTime() << endl;
		else
			cout << "Phase 0 send finished by Master"  <<", t= " << this->getCurrentTime() << endl;
	}

}


void NHH2013AlgoV2::Master_Phase00_0_Recv(bool phase00) {
	// send buffered "ready to receive" message to workers
	for (int i=1; i<= nP-1; ++i ) {
		ReadyRecvRequests[i-1] = MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,i, ready_master_tag);
	}
	for (int i=1; i<= nP-1; ++i ) ReadyRecvRequests[i-1].Wait();

	//receives Answers from cores
	int nRecv = 0, from, numSys;
	//	int count, nextIdx, nextSize;
	double nextFn, nextSumSq, nextTime, s;
	list<int>::iterator iter;
	MPI::Status status;
	master_phase0_num_sys_recv.clear(); master_phase0_num_sys_recv.resize(nP-1, 0);

	//	if (algo == 3)
	//	{
	//		double fns[S_Total];
	//		double sumsqs[S_Total];
	//		double runtimes[S_Total];
	//		int samplesizes[S_Total];
	//		int bsize = 30;
	//		char buffer[S_Total][bsize];
	//
	//		// use pack
	//		MPI_Request reqs [S_Total];
	//		for (int i = 0; i < S_Total; i++) {
	//			MPI_Irecv(&buffer[i],     bsize, MPI_PACKED, MPI_ANY_SOURCE, get_system_tag(i, 0),
	//					MPI_COMM_WORLD, &reqs[i]);
	//		}
	//		if (outputLevel >= 3)
	//			cout << "Master finishes posting recv at " << this->getCurrentTime() << endl;
	//
	//		MPI_Waitall(S_Total, reqs, MPI_STATUSES_IGNORE);
	//		if (outputLevel >= 3)
	//			cout << "Master finishes waiting recv at " << this->getCurrentTime() << endl;
	//
	//		//	// do not use pack
	//		//	MPI_Request reqs [4 * S_Total];
	//		//	for (int i = 0; i < S_Total; i++) {
	//		//		MPI_Irecv(&fns[i],     1, MPI_DOUBLE, MPI_ANY_SOURCE, get_system_tag(i, fn_msg),
	//		//				MPI_COMM_WORLD, &reqs[i * 4 + 0]);
	//		//		MPI_Irecv(&sumsqs[i],  1, MPI_DOUBLE, MPI_ANY_SOURCE, get_system_tag(i, sumsq_msg),
	//		//				MPI_COMM_WORLD, &reqs[i * 4 + 1]);
	//		//		MPI_Irecv(&samplesizes[i], 1, MPI_INT, MPI_ANY_SOURCE, get_system_tag(i, samplesize_msg),
	//		//				MPI_COMM_WORLD, &reqs[i * 4 + 2]);
	//		//		MPI_Irecv(&runtimes[i], 1, MPI_DOUBLE, MPI_ANY_SOURCE, get_system_tag(i, runtime_msg),
	//		//				MPI_COMM_WORLD, &reqs[i * 4 + 3]);
	//		//	}
	//		//	MPI_Waitall(S_Total * 4, reqs, MPI_STATUSES_IGNORE);
	//		// use pack
	//		for (int i = 0; i < S_Total; i++) {
	//			int pos = 0;
	//			MPI_Unpack(buffer[i], bsize, &pos, &fns[i]        , 1, MPI_DOUBLE, MPI_COMM_WORLD);
	//			MPI_Unpack(buffer[i], bsize, &pos, &sumsqs[i]     , 1, MPI_DOUBLE, MPI_COMM_WORLD);
	//			MPI_Unpack(buffer[i], bsize, &pos, &samplesizes[i], 1, MPI_INT   , MPI_COMM_WORLD);
	//			MPI_Unpack(buffer[i], bsize, &pos, &runtimes[i]   , 1, MPI_DOUBLE, MPI_COMM_WORLD);
	//		}
	//		if (phase00) {
	//			for (int i = 0; i < S_Total; ++i) {
	//				Simulation_Times [i] = runtimes[i];
	//			}
	//		} else {
	//			for (int i = 0; i < S_Total; ++i) {
	//				Master_Results[i].resize(1); Master_Results[i][0].clear();
	//				Master_Results[i][0].addSample(samplesizes[i], fns[i]*samplesizes[i],
	//						sumsqs[i], runtimes[i]);
	//				s = Master_Results[i][0].getSampleStdev();
	//				S2[i] = s*s;
	//			}
	//		}
	//	}
	//	else if (algo == 2 )
	//	{
	while (nRecv < S_Total) {
		// iterate over cores to find the next core that has message to send
		if (MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE, sys_tag, status)) {
			from = status.Get_source();
			MPI::COMM_WORLD.Recv(&numSys,		1, MPI::INT	,	from, sys_tag);
			nRecv += numSys;
			if (phase00) {
				double master_times[numSys];
				MPI::COMM_WORLD.Recv(master_times,	numSys, MPI::DOUBLE,	from, runtime_tag);
				int k=0;
				for (iter = Core2SysLists[from-1].begin(); iter!=Core2SysLists[from-1].end(); iter++, k++){
					Simulation_Times[*iter] = master_times[k];
				}
			}else{
				double master_fn[numSys];
				double master_sumsq[numSys];
				MPI::COMM_WORLD.Recv(master_fn,	numSys, MPI::DOUBLE,	from, fn_tag);
				MPI::COMM_WORLD.Recv(master_sumsq,	numSys, MPI::DOUBLE,	from, sumsq_tag);
				int i, k=0;
				for (iter = Core2SysLists[from-1].begin(); iter!=Core2SysLists[from-1].end(); iter++, k++){
					i = *iter;
					Master_Results[i].resize(1); Master_Results[i][0].clear();
					Master_Results[i][0].addSample(n1, master_fn[k]*n1, master_sumsq[k], Simulation_Times[i]);
					s = Master_Results[i][0].getSampleStdev();
					S2[i] = s*s;
				}
			}
			if (outputLevel > 0 ) {
				cout << "Received result from node " << setw(2) << setfill('0') << from
						<<  ", time: " << this->getCurrentTime()<< endl;
				cout << nRecv << " systems recvd\n";
			}
			//				for (int k=0; k < numSys; ++k) {
			//					MPI::COMM_WORLD.Recv(&nextFn,		1, MPI::DOUBLE,	from, fn_tag);
			//					MPI::COMM_WORLD.Recv(&nextSumSq, 	1, MPI::DOUBLE,	from, sumsq_tag);
			//					MPI::COMM_WORLD.Recv(&nextSize,    	1, MPI::INT, 	from, samplesize_tag);
			//					MPI::COMM_WORLD.Recv(&nextIdx,     	1, MPI::INT,	from, sampleidx_tag);
			//					MPI::COMM_WORLD.Recv(&nextTime,    	1, MPI::DOUBLE,	from, runtime_tag);
			//					nRecv += 1;
			//					master_phase0_num_sys_recv[from-1] += 1;
			//					if (!phase00) {
			//						collectedAnswers[nextIdx].addSample(nextSize, nextFn*nextSize, nextSumSq,nextTime);
			//						s = collectedAnswers[nextIdx].getSampleStdev();
			//						S2[nextIdx] = s*s;
			//					}
			//					else Simulation_Times[nextIdx] = nextTime;
			//					if (outputLevel >= 3) {
			//						cout << "Received result for system " << setw(7) << setfill('0')<<  nextIdx
			//						<< " from node " << setw(2) << setfill('0') << from
			//						<<  ", time: " << this->getCurrentTime()<< endl;
			//						cout << nRecv << " systems recvd\n";
			//					}
			//				}
			//				if (master_phase0_num_sys_recv[from-1] < this->Core2SysLists[from-1].size()) {
			//					ReadyRecvRequests[from-1] = MPI::COMM_WORLD.Isend(&dummy,1, MPI::INT, from, ready_master_tag);
			//					if (outputLevel >= 3) {
			//						cout << "Master received " << master_phase0_num_sys_recv[from-1] << " sys from core "
			//								<<  setw(2) << setfill('0') << from
			//								<< ", need " << Core2SysLists[from-1].size() << ", sending ready_recv_tag" <<endl;
			//					}
			//				}
			//end if
		} //end if
	} // end while
	//	}
	if (outputLevel > 0) {
		if (phase00)
			cout << "Phase 00 recv finished by Master" <<", t= " << this->getCurrentTime() << endl;
		else
			cout << "Phase 0 recv finished by Master"  <<", t= " << this->getCurrentTime() << endl;
	}
}



void NHH2013AlgoV2::Master_Phase0_Recv_with_Screening() {
	int num_cores_received = 0;
	//	double nextFn, nextSumSq, nextTime;
	double s = 0.0;
	SOResult emptyanswer;
	double d_dummy[nP];
	int i_dummy[nP];
	MPI::COMM_WORLD.Allgather(&s    , 1, MPI::DOUBLE, d_dummy, 1,MPI::DOUBLE);
	MPI::COMM_WORLD.Allgather(&dummy, 1, MPI::INT   , i_dummy, 1,MPI::INT   );
	MPI::COMM_WORLD.Allgather(&dummy, 1, MPI::INT   , i_dummy, 1,MPI::INT   );
	MPI::COMM_WORLD.Allgather(&s    , 1, MPI::DOUBLE, d_dummy, 1,MPI::DOUBLE);
	//	bool isEliminated;
	int from, numSys;
	//	int nextSize,nextIdx;
	list<int>::iterator iter;
	MPI::Status status;
	//	this->system_eliminated.clear(); this->system_eliminated.resize(S_Total, true);
	while(num_cores_received < nP - 1) {
		if (MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE, sys_tag, status)) {
			from = status.Get_source();
			num_cores_received += 1;
			MPI::COMM_WORLD.Recv(&numSys,		1, MPI::INT	,	from, sys_tag);
			double master_fn[numSys];
			double master_sumsq[numSys];
			int   master_elim[numSys];
			MPI::COMM_WORLD.Recv(master_fn,		numSys, MPI::DOUBLE,	from, fn_tag);
			MPI::COMM_WORLD.Recv(master_sumsq,	numSys, MPI::DOUBLE,	from, sumsq_tag);
			MPI::COMM_WORLD.Recv(master_elim,	numSys, MPI::INT,		from, elimidx_tag);
			int k=0;
			for (iter = Core2SysLists[from-1].begin(); iter!=Core2SysLists[from-1].end(); iter++, k++){
				assert(Master_Results[*iter].size() == 0);
				Master_Results[*iter].push_back(emptyanswer);
				Master_Results[*iter][0].addSample(n1, master_fn[k]*n1, master_sumsq[k], Simulation_Times[*iter]);
				s = Master_Results[*iter][0].getSampleStdev();
				S2[*iter] = s*s;
				if (master_elim[k] && this->num_systems_eliminated < S_Total-1) {
					this->num_systems_eliminated += 1;
					if (outputLevel >= 4) {
						ostringstream oss;
						oss << "Sys " << setw(7) << setfill('0') <<
								*iter << " eliminated on master by worker "
								<< from << ", "   << S_Total-this->num_systems_eliminated <<" sys remaining" <<
								", t= " << this->getCurrentTime() << endl;
						cout << oss.str();
					}
					this->system_eliminated[*iter] = 1;
				}
				//				if (this->num_systems_eliminated < S_Total-1) // to avoid the last two sys knocked out at the same time
				//					this->system_eliminated[*iter] = master_elim[k];
			}
			//			MPI::COMM_WORLD.Send(&dummy,		1, MPI::INT,from, ready_worker_tag);
			//			for (int k=0; k < numSys; ++k) {
			//				MPI::COMM_WORLD.Recv(&nextFn,		1, MPI::DOUBLE,	from, fn_tag);
			//				MPI::COMM_WORLD.Recv(&nextSumSq, 	1, MPI::DOUBLE,	from, sumsq_tag);
			//				MPI::COMM_WORLD.Recv(&nextSize,    	1, MPI::INT, 	from, samplesize_tag);
			//				MPI::COMM_WORLD.Recv(&nextIdx,     	1, MPI::INT,	from, sampleidx_tag);
			//				MPI::COMM_WORLD.Recv(&isEliminated,	1, MPI::BOOL,	from, elimidx_tag);
			//				//				MPI::COMM_WORLD.Recv(&nextTime,    	1, MPI::DOUBLE,	from, runtime_tag);
			//
			//				//				cout << "Master recv " << nextFn << ", "<< nextSumSq << ", "<< nextSize << ", "<< nextIdx << ", "<< nextTime << endl;
			//				if (isEliminated && this->num_systems_eliminated < S_Total-1) {
			//					this->num_systems_eliminated += 1;
			//					if (outputLevel > 0) {
			//						ostringstream oss;
			//						oss << "Sys " << setw(7) << setfill('0') <<
			//								nextIdx << " eliminated on master by worker "
			//								<< from << ", "   << S_Total-this->num_systems_eliminated <<" sys remaining" <<
			//								", t= " << this->getCurrentTime() << endl;
			//						cout << oss.str();
			//					}
			//				}
			//				if (this->num_systems_eliminated < S_Total-1) // to avoid the last two sys knocked out at the same time
			//					this->system_eliminated[nextIdx] = isEliminated;
			//				collectedAnswers[nextIdx].addSample(nextSize, nextFn*nextSize, nextSumSq);
			//				s = collectedAnswers[nextIdx].getSampleStdev();
			//				S2[nextIdx] = s*s;
			//			}
		}
	}

	//	this->Master_PrintSummary();
	if (outputLevel > 0 )
		cout << "Phase 0 recv finished by Master" <<", t= " << this->getCurrentTime() << endl;
}

void NHH2013AlgoV2::Master_Phase0_pairwise_screen() {
	// FOR PHASE 0 USE ONLY
	// performs pairwise screening at the master level
	if(outputLevel > 0)
		cout << "Master performing pairwise screen in phase 0\n" ;
	list<int>::iterator iter;
	double Xbar_i, Xbar_j, ni, nj, tau, Y;
	for (int i = 0; i < S_Total; i++) {
		if (!(this->system_eliminated[i]) ) {
			Xbar_i = Master_Results[i][0].getSampleMean() * this->minmax;
			ni = Master_Results[i][0].sample_size;
			for (int j = 0; j < S_Total; j++) {
				if (this->system_eliminated[i]) break;
				if ( (!(this->system_eliminated[i])) && (i != j) ) {
					Xbar_j = Master_Results[j][0].getSampleMean() * this->minmax;
					if (Xbar_j > Xbar_i) {
						nj = Master_Results[j][0].sample_size;
						tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
						Y = tau*(Xbar_i-Xbar_j);
						if (Y < min(0.0, lambda*tau-a)) {
							// system i eliminated by system j
							this->system_eliminated[i] = 1;
							this->num_systems_eliminated += 1;
							if (outputLevel >= 4) {
								ostringstream oss;
								oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys " << setw(7) << setfill('0')  << i
										<<", " << S_Total - this->num_systems_eliminated <<" sys remaining, t= "
										<< this->getCurrentTime() << ";" << Xbar_j << "," << nj << "," << S2[j] <<
										"," << Xbar_i << "," << ni << "," << S2[i] <<endl;
								cout << oss.str();
							}
							//						//remove system i from each core simulating it
							//						for (iter=Sys2CoreLists[i].begin(); iter!=Sys2CoreLists[i].end(); iter++) {
							//							Core2SysLists[*iter].remove(i);
							//						}
							//						Sys2CoreLists[i].clear();

							//						if ((S - this->num_systems_eliminated <= 10)
							//								|| (((S - this->num_systems_eliminated) % 1000) == 0)) {
							//							cout << "---------- Snapshot when "<< S - this->num_systems_eliminated
							//									<< " system remaining ----------" << endl;
							//							this->Master_PrintSummary();
							//							cout << toString_Core2Sys() << endl;
							//						}
						}
					}
				}
			}
		}
		if(outputLevel > 2) {
			ostringstream oss;
			oss << "Master done screening sys " << setw(7) << setfill('0') << i  << ", t= "
					<< this->getCurrentTime() << endl;
			cout << oss.str();
		}
	}
	if(outputLevel > 0)
		cout << S_Total - this->num_systems_eliminated << " sys remaining after Phase 0\n" ;
}

void NHH2013AlgoV2::Master_Phase1_Initial_Send() {
	// in the beginning of Phase 1, master distributes
	list<int>::iterator iter; double s2;
	for (int i = 1; i < nP; ++i) {
		//send a flag to continue
		int go = 3;
		MPI::COMM_WORLD.Send(&go, 1, MPI::INT, i, flag_tag);
	}

	SORngStream rs;
	rs.GetState(tempSeed);
	MPI_Bcast(tempSeed,6,MPI_UNSIGNED_LONG,0,MPI_COMM_WORLD);
	MPI_Bcast(&S2[0],S_Total,MPI_DOUBLE,0,MPI_COMM_WORLD);
}

void NHH2013AlgoV2::Master_Phase1_PrepareList() {
	if (outputLevel > 0){
		cout << "Master preparing phase 1 list" << endl;
	}


	// in phase 1, determine a sequence of systems and their sample sizes
	// find the average variance
	double S_avg = 0.0;
	for (int i = 0; i < S_Total; ++i) {
		S_avg += sqrt(S2[i] / (useTimeEst?Simulation_Times[i]:1));
	}
	S_avg = S_avg/S_Total;

	//	cout << "aaa\n" ;
	for (int i = 0; i < S_Total; ++i) {
		Stage_Sizes[i]=(int)ceil(sqrt(S2[i])/S_avg*bsize/sqrt(useTimeEst?Simulation_Times[i]:1));
	}

	MPI_Bcast(&Stage_Sizes[0],S_Total,MPI_INT,0,MPI_COMM_WORLD);

	for (int i = 0; i<Core2SysLists.size(); i++) Core2SysLists[i].clear();
	Sys2CoreList.resize(S_Total);
	Master_phase1_num_sys_each_core.resize(nP-1);

	//	cout << "aaa\n" ;
	//	//perform Fisher�CYates shuffle to get a randomized core2syslist
	int tmp_i, tmp_j, s; list<int>::iterator iter;
	rnd_perm.resize(S_Total);
	for (int i = 0; i < S_Total; i++) rnd_perm[i]=i;
	for (int i = S_Total - 1; i >= 1; i--) {
		// use the next substream of pMasterStream to generate a random integer between 0 and i
		tmp_j = pMasterStream->RandInt(0, i);
		tmp_i = rnd_perm[tmp_j]; rnd_perm[tmp_j] = rnd_perm[i]; rnd_perm[i] = tmp_i;
	}

	pMasterStream->ResetNextSubstream();

	//	cout << "aaa\n" ;

	num_systems_remain_after_phase0 = S_Total - this->num_systems_eliminated;
	//	cout << num_systems_remain_after_phase0 << endl;

	MPI_Bcast(&num_systems_remain_after_phase0,1,MPI_INT,0,MPI_COMM_WORLD);
	int perms[num_systems_remain_after_phase0];
	int num_systems_per_core = (num_systems_remain_after_phase0)/(nP-1);
	int num_systems_extra    = (num_systems_remain_after_phase0)%(nP-1);
	//compile core2syslist
	int perm_sys = 0, curr_sys = 0;
	for (int w = 1; w < nP; ++w) {
		//		cout << i  << endl;
		s = (w<=num_systems_extra)?(num_systems_per_core+1):(num_systems_per_core);
		Master_phase1_num_sys_each_core [w-1] = s;
		//distribute systems
		for (int j = 1; j <= s; ++j){
			// skip eliminated systems
			while(this->system_eliminated[rnd_perm[curr_sys]]) curr_sys += 1;
			this->Core2SysLists[w-1].push_back(rnd_perm[curr_sys]);
			this->Sys2CoreList[rnd_perm[curr_sys]] = w;
			//			this->Sys2CoreLists[rnd_perm[curr_sys  ]].push_back(i-1);
			perms[perm_sys] = rnd_perm[curr_sys];
			curr_sys += 1; perm_sys += 1;
		}
		//		Master_stages_recv_count[i-1].clear();
		//		Master_stages_recv_count[i-1].push_back(s);
	}
	MPI_Bcast(perms,num_systems_remain_after_phase0,MPI_INT,0,MPI_COMM_WORLD);

	//	cout << "aaa\n" ;
	//
	//	num_systems_remain_after_phase0 = S_Total - this->num_systems_eliminated;
	//
	//	int num_systems_per_core = (S_Total-this->num_systems_eliminated)/(nP-1);
	//	int num_systems_extra    = (S_Total-this->num_systems_eliminated)%(nP-1);
	//	// number of cores to get 1 extra system
	//	double CoreSimulationTime = 0.0;
	//
	//	//compile core2syslist
	//	int curr_sys = 0;
	//	for (int i = 1; i < nP; ++i) {
	//		CoreSimulationTime = 0.0;
	//		s = (i<=num_systems_extra)?(num_systems_per_core+1):(num_systems_per_core);
	//
	//		//distribute systems
	//		for (int j = 1; j <= s; ++j){
	//			// skip eliminated systems
	//			while(this->system_eliminated[rnd_perm[curr_sys]]) curr_sys += 1;
	//			this->Core2SysLists[i-1].push_back(rnd_perm[curr_sys]);
	//			this->Sys2CoreLists[rnd_perm[curr_sys  ]].push_back(i-1);
	//			CoreSimulationTime += this->Simulation_Times[rnd_perm[curr_sys]];
	//			curr_sys += 1;
	//		}
	//		//compute number of replications in each stage
	//		for (iter = Core2SysLists[i-1].begin(); iter != Core2SysLists[i-1].end(); iter++){
	//			this->Stage_Sizes[*iter] = (int) ceil((CoreSimulationTime / s
	//					/ Simulation_Times[*iter] * n1));
	//		}
	//	}
	//	if (outputLevel > 1) {
	//		cout << this->toString_Core2Sys() << endl;
	//		cout << this->toString_StageSizes() << endl;
	//	}
	if (outputLevel > 0) {
		cout << "Phase 1 list prepared by Master" <<", t= " << this->getCurrentTime() << endl;
		cout << num_systems_remain_after_phase0 << " systems remain after phase 0\n";
		if (outputLevel >= 4)
			cout << this->toString_Core2Sys();
	}
}
//
//void NHH2013AlgoV2::Master_Phase1_Send() {
//	// in the beginning of Phase 1, master takes the Core2Sys and send to the cores together with
//	// the stage size information.
//	list<int>::iterator iter; double s2;
//	for (int i = 1; i < nP; ++i) {
//		//send a flag to continue
//		int go = 3;
//		MPI::COMM_WORLD.Send(&go, 1, MPI::INT, i, flag_tag);
//	}
//
//	for (int i = 1; i < nP; ++i) {
//		//determine the number of systems to send, and send this number to the seed
//		int temp_s = this->Core2SysLists[i-1].size();
//		MPI::COMM_WORLD.Send(&temp_s, 1, MPI::INT, i, sys_tag);
//
//		//for each of the systems to be send
//		int v1 [Core2SysLists[i-1].size()];
//		int v2 [Core2SysLists[i-1].size()];
//		int curr_sys, k = 0;
//		unsigned long seed_to_send[6*temp_s];
//		for (iter=Core2SysLists[i-1].begin(); iter!=Core2SysLists[i-1].end(); iter++, k++) {
//			curr_sys = *iter;
//			//create a new stream and get its seed
//			SORngStream rs;
//			rs.GetState(tempSeed);
//
//			for (int ss = 0; ss < 6; ss++) {
//				seed_to_send[k*6+ss]=tempSeed[ss];
//			}
//
//			//get the next system to simulate
//			//			vector<int> tmp_disc = problem->Idx2System_Disc(curr_sys); vector<double> initSol_cont;
//
//			//send seed to the worker
//			//			MPI::COMM_WORLD.Send(tempSeed, 6, MPI::UNSIGNED_LONG, i, seed_tag);
//
//			v1[k] = curr_sys;
//			v2[k] = Stage_Sizes[curr_sys];
//			//send system index to the worker
//			//			MPI::COMM_WORLD.Send(&curr_sys, 1, MPI::INT, i, idx_tag);
//			//send number of replications required for each system
//			//			MPI::COMM_WORLD.Send(&Stage_Sizes[curr_sys], 1, MPI::INT, i, size_tag);
//			//send discrete decision variables to worker
//			//			if (problem->prop.d_disc > 0)
//			//				MPI::COMM_WORLD.Send(&tmp_disc[0], problem->prop.d_disc,
//			//						MPI::INT, i, x_disc_tag);
//		}
//		MPI::COMM_WORLD.Send(v1, Core2SysLists[i-1].size(), MPI::INT, i, idx_tag);
//		MPI::COMM_WORLD.Send(v2, Core2SysLists[i-1].size(), MPI::INT, i, size_tag);
//		MPI::COMM_WORLD.Send(seed_to_send, 6*temp_s, MPI::UNSIGNED_LONG, i, seed_tag);
//		//		for (int k = 0; k < S_Total; k++) {
//		//			s2 = S2[k];
//		//			MPI::COMM_WORLD.Send(&s2, 1, MPI::DOUBLE, i, s2_tag);
//		//		}
//	}
//	if (outputLevel > 0)
//		cout << "master finishes sending in phase 1\n";
//	MPI_Bcast(&S2[0],S_Total,MPI_DOUBLE,0,MPI_COMM_WORLD);
//	if (outputLevel > 0)
//		cout << "Phase 1 send finished by Master\n" ;
//}

void NHH2013AlgoV2::Master_Phase1_Execute() {
	// Master Phase 1 routine
	// keep watching message queues, receive message and send the most recent
	// "best system from other cores" list to the workers

	//
	// send buffered "ready to receive" message to workers
	int exec, flag = 4; int from; num_core_terminated = 0;
	int numNewStageAvail = 0;
	for ( int i = 1; i < nP; ++i ) {
		//send flag 4 to kick off phase 1 simulation at workers
		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,i, flag_tag);
	}
	//	for ( int i = 1; i < nP; ++i ) {
	//		 send ready_master_tag to workers
	//		MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,i, ready_master_tag);
	//	}

	// new
	MPI::Request * reqs_ptr;
	MPI::Request * reqs_new;
	reqs_ptr = new MPI::Request[nP-1];
	int w;
	int reqs_worker[nP-1]; // position of request for each worker, position starting from 1
	//	MPI::Request reqs[nP-1];
	int WtoMmsg[nP-1];
	for ( int i = 1; i < nP; ++i ) {
		// post irecv request
		//		reqs[i-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[i-1], 1, MPI::INT, i, ready_worker_tag);
		reqs_ptr[i-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[i-1], 1, MPI::INT, i, ready_worker_tag);
		reqs_worker[i-1] = i;
	}
	// end new

	//	Master_Recvd_Workers_List.clear();
	int src = nP-1; MPI::Status status; int ac = 0;

	while (this->num_systems_eliminated < S_Total - 1) {
		//		if (num_core_terminated+Master_Recvd_Workers_List.size()>=nP-1) {
		//			for (list<int>::iterator iter=Master_Recvd_Workers_List.begin(); iter != Master_Recvd_Workers_List.end(); iter++) {
		//				from = *iter;
		//				//				MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,from, ready_master_tag);
		//				reqs[from-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[from-1], 1, MPI::INT, from, ready_worker_tag);
		//			}
		//			Master_Recvd_Workers_List.clear();
		//		}
		//		if (outputLevel >= 3){
		//			ostringstream oss;
		//
		//			oss << "\nWaiting for cores: "; ac = 0;
		//			for (int co = 1; co < nP; co++) {
		//				if (find(Master_Recvd_Workers_List.begin(),Master_Recvd_Workers_List.end(),co)==Master_Recvd_Workers_List.end()) {
		//					oss << co << "," ; ac++;
		//				}
		//			}
		//			oss << "Total = " << ac << endl;
		//			oss << "Master starts waiting at t = " << this->getCurrentTime() <<
		//					", " << S_Total-this->num_systems_eliminated << " Sys remaining" << endl;
		//			cout << oss.str();
		//			//			if (outputLevel >= 3 && S_Total-this->num_systems_eliminated  <= 50) {
		//			//				for (int co = 1; co < nP; co++) {
		//			//					if (!core_eliminated[co-1]) {
		//			//						ostringstream oss2;
		//			//						oss2 << "Sys " << this->Master_best_Results_idx[co-1] ;
		//			//						list<SOAnswer>::iterator iter_ans = Master_best_Results[co-1].end();
		//			//						iter_ans--;
		//			//						oss2 << ", n = " << (*iter_ans).sample_size << endl;
		//			//						cout << oss2.str();
		//			//					}
		//			//				}
		//			//			}
		//		}
		flag = 5;
		MPI::Request::Waitany(nP-1, reqs_ptr, status);
		from = status.Get_source();
		MPI::COMM_WORLD.Ssend(&flag,	1, MPI::INT,	from, flag_tag);
		if (outputLevel >= 3){
			ostringstream oss1;
			oss1 << "Master done waiting at t = " << this->getCurrentTime() << ", source = " << from << endl;
			if (from < 1 || from > nP-1) {
				ostringstream oss;
				oss << "error 1, from = " << from << endl;
				cout << oss.str();
			} else {
				cout << oss1.str();
			}
			//			ostringstream os;
			//			os << "Req_workers: ";
			//			for (int ww = 0; ww < nP-1; ww++) {
			//				os << reqs_worker[ww] <<" ";
			//			}
			//			os << endl;
			//			cout << os.str();
		}

		switch (WtoMmsg[from-1]) {
		case 1:
			// receive answer
			Master_Phase1_RecvAns(from);
			break;
		case 2:
			Master_Phase1_RecvScreening(from);
			break;
		case 0:
			break;
		}
		if (this->num_systems_eliminated >= S_Total - 1) {
			exec = 0;
			MPI::COMM_WORLD.Ssend(&exec,	1, MPI::INT,from, exec_tag);
			this->num_core_terminated++;
			//			Master_Phase1_SendTermination(worker);
		} else {
			numNewStageAvail = Master_Phase1_getNumNewStageAvail(from);
			if (numNewStageAvail == 0) {
				exec = 1;
				MPI::COMM_WORLD.Ssend(&exec,	1, MPI::INT,from, exec_tag);
				Master_Phase1_SendSimSys(from);
			} else {
				assert (numNewStageAvail > 0);
				exec = 2;
				MPI::COMM_WORLD.Ssend(&exec,	1, MPI::INT,from, exec_tag);
				Master_Phase1_SendScreenAns(from, numNewStageAvail);
			}
		}
		//mark the worker as received
		//		if (exec != 0)	{
		//			Master_Recvd_Workers_List.push_back(from);
		//		}
		//		reqs[from-1] = MPI::REQUEST_NULL;
		reqs_new = new MPI::Request[nP-1];
		w = 1;
		while (w < nP - 1) {
			if (w >= reqs_worker[from-1]) {
				reqs_new[w-1] = reqs_ptr[w];
			} else {
				reqs_new[w-1] = reqs_ptr[w-1];
			}
			w++;
		}
		for (w = 0; w < nP; w++) {
			if (reqs_worker[w] > reqs_worker[from-1])
				reqs_worker[w] -= 1;
		}
		if (exec != 0) {
			reqs_new[nP - 2] = MPI::COMM_WORLD.Irecv(&WtoMmsg[from-1], 1, MPI::INT, from, ready_worker_tag);
			reqs_worker[from-1] = nP - 1;
		} else {
			reqs_new[nP - 2] = MPI::REQUEST_NULL;
		}
		delete [] reqs_ptr;
		reqs_ptr = reqs_new;
		if (outputLevel >= 3){
			ostringstream os;
			os << "Req_workers: ";
			for (int ww = 0; ww < nP-1; ww++) {
				os << reqs_worker[ww] <<" ";
			}
			os << endl;
			cout << os.str();
		}
	}

	//	for (list<int>::iterator iter=Master_Recvd_Workers_List.begin(); iter != Master_Recvd_Workers_List.end(); iter++) {
	//		from = *iter;
	//		//		MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,from, ready_master_tag);
	//		reqs[from-1] = MPI::COMM_WORLD.Irecv(&WtoMmsg[from-1], 1, MPI::INT, from, ready_worker_tag);
	//	}

	while (num_core_terminated < nP-1) {
		MPI::Request::Waitany(nP-1, reqs_ptr, status);
		from = status.Get_source();
		if (from < 1 || from > nP-1) {
			ostringstream oss;
			oss << "error 111, from = " << from << endl;
			cout << oss.str();
		}
		flag = 6;
		MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	from, flag_tag);
		core_terminated[from-1] = true;
		reqs_ptr[reqs_worker[from-1]-1] = MPI::REQUEST_NULL;
		num_core_terminated++;
		if (outputLevel >= 3) {
			ostringstream oss1;
			oss1 << "Master sending termination instruction to core " << from << ", t = " << this->getCurrentTime() << endl;
			cout << oss1.str();
		}
	}
	delete[] reqs_ptr;
}


void NHH2013AlgoV2::Master_Phase1_SendSimSys(const int & worker) {
	// finds the next system to simulate
	master_phase1_current_sys ++ ;
	// if the next system exceeds S_Total, increase stage count by 1
	if (master_phase1_current_sys == S_Total) {
		master_phase1_current_stage++;
		master_phase1_current_sys = 0;
		if (outputLevel >= 4) {
			cout << "Master start sending stage " << master_phase1_current_stage << endl;
		}
	}
	while (this->system_eliminated[rnd_perm[master_phase1_current_sys]]) {
		master_phase1_current_sys ++ ;
		if (master_phase1_current_sys == S_Total) {
			master_phase1_current_stage++;
			master_phase1_current_sys = 0;
			if (outputLevel >= 4) {
				cout << "Master start sending stage " << master_phase1_current_stage << endl;
			}
		}
	}
	dummy = rnd_perm[master_phase1_current_sys];
	MPI::COMM_WORLD.Ssend(&dummy,	1, MPI::INT,worker, idx_tag);
	MPI::COMM_WORLD.Ssend(&master_phase1_current_stage,	1, MPI::INT,worker, stage_tag);
	this->Master_simcount_phases[2] += (double) (Stage_Sizes[dummy]);
	this->Master_simcount_systems[dummy] +=  Stage_Sizes[dummy];
	if (outputLevel >= 4)
	{
		ostringstream oss;
		oss << "Master send sys " << dummy << " stage "<<
				master_phase1_current_stage << " to worker " << worker << endl;
		cout << oss.str();
	}
	return;
}

void NHH2013AlgoV2::Master_Phase1_RecvAns(const int & worker) {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master receiving answer from worker " << worker << endl;
		cout << oss.str();
	}
	int w_sysidx, w_stage, sz;
	double w_fn, w_sumsq;
	map<int, SOResult>::iterator it, it2;
	MPI::COMM_WORLD.Recv(&w_sysidx,	1, MPI::INT,	worker, idx_tag);
	MPI::COMM_WORLD.Recv(&w_stage,	1, MPI::INT,	worker, stage_tag);
	MPI::COMM_WORLD.Recv(&w_fn,		1, MPI::DOUBLE,	worker, fn_tag);
	MPI::COMM_WORLD.Recv(&w_sumsq,	1, MPI::DOUBLE,	worker, sumsq_tag);
	// if system is already marked as eliminated, do nothing to collect the data
	if (this->system_eliminated[w_sysidx])	return;

	int other_worker = this->Sys2CoreList[w_sysidx];
	//	cout << "aaa\n";
	//	cout << w_sysidx << " " << w_stage << " " << Master_Results[w_sysidx].size() <<
	//			" " << Master_stages_screened_each_worker[worker-1] << endl;
	sz = Master_Results[w_sysidx].size() + Master_stages_screened_each_worker[other_worker-1];
	if (sz == w_stage) {
		//		cout << "ccc\n";

		// if every stage prior to this received one is present, add the new stage to the back

		Master_Results[w_sysidx].push_back(Master_Results[w_sysidx].back());
		Master_Results[w_sysidx].back().addSample(Stage_Sizes[w_sysidx], w_fn*Stage_Sizes[w_sysidx], w_sumsq);

		//		cout << "ddd\n";
		//		if (Master_stages_recv_count[worker-1].size() <= w_stage) {
		//			Master_stages_recv_count[worker-1].resize(w_stage+1, 0);
		//		}
		//		Master_stages_recv_count[worker-1][w_stage] += 1;
		sz++;

		//		cout << "eee\n";
		it = Master_Temp_Results[w_sysidx].begin();
		// check if the temp_Results has any further stage to add
		while( !Master_Temp_Results[w_sysidx].empty() && it->first <= sz) {
			//			cout << "ggg\n";
			assert ( it->first == sz) ;
			Master_Results[w_sysidx].push_back(Master_Results[w_sysidx].back());
			sz++;
			Master_Results[w_sysidx].back().addSample(it->second);
			Master_Temp_Results[w_sysidx].erase(it);
			it = Master_Temp_Results[w_sysidx].begin();
		}

		//		cout << "fff\n";
	} else {
		//		cout << "bbb\n";
		// if w_stage > sz, that is, there is a 'gap' between collected answer and this stage, add
		// the stage to the Master_Temp_Results map.
		if (!(w_stage > sz)) {
			cout << "w_stage " << w_stage << " sz " << sz << "  " << Master_Results[w_sysidx].size() <<
					" " << Master_stages_screened_each_worker[other_worker-1] << " sys " <<
					w_sysidx << " belong to worker " << other_worker << endl;
			assert (w_stage > sz);
		}
		assert(Master_Temp_Results[w_sysidx].find(w_stage) == Master_Temp_Results[w_sysidx].end());
		SOResult tmpans;
		tmpans.addSample(Stage_Sizes[w_sysidx], w_fn*Stage_Sizes[w_sysidx], w_sumsq);
		Master_Temp_Results[w_sysidx][w_stage] = tmpans;
	}
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master received answer from worker " << worker << " for sys " << w_sysidx <<endl;
		cout << oss.str();
	}
}

void NHH2013AlgoV2::Master_Phase1_SendScreenAns(const int & worker, const int & numnewstages) {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master send screening answers to worker " << worker << ", " <<
				numnewstages << " new stages" << endl;
		oss << "Worker has " <<Core2SysLists[worker-1].size() << " sys remaining\n";
		cout << oss.str();
	}
	assert(Core2SysLists[worker-1].size()>0);
	list<int>::iterator it;

	if (outputLevel >= 4)	cout << "MM 1111\n";
	MPI::COMM_WORLD.Ssend(&numnewstages,	1, MPI::INT,	worker, stage_tag);

	int num_sys_to_send = Core2SysLists[worker-1].size();
	MPI::COMM_WORLD.Ssend(&num_sys_to_send,	1, MPI::INT,	worker, sys_tag);
	int sys_idx_to_send[num_sys_to_send];
	double fns_to_send  [num_sys_to_send*numnewstages];
	double sumsq_to_send[num_sys_to_send*numnewstages];
	int szs_to_send  [num_sys_to_send*numnewstages];
	int curr_stage, curr_sys = 0;
	SOResult *tmp_ans ;
	if (outputLevel >= 4)	cout << "MM 2222\n";
	for (it = Core2SysLists[worker-1].begin(); it != Core2SysLists[worker-1].end(); it++) {
		assert(!this->system_eliminated[*it]);
		assert( (int)Master_Results[*it].size() >= numnewstages);
		sys_idx_to_send[curr_sys] = *it;
		for (curr_stage = 0; curr_stage < numnewstages; curr_stage++) {
			tmp_ans = &(Master_Results[*it][curr_stage+1]);
			//			tmp_ans = &(Master_Results[*it][curr_stage+Master_stages_screened_each_worker[worker-1]+1]);
			fns_to_send  [curr_sys*numnewstages+curr_stage]= tmp_ans->getSampleMean();
			sumsq_to_send[curr_sys*numnewstages+curr_stage]= tmp_ans->getSampleSumsq();
			szs_to_send  [curr_sys*numnewstages+curr_stage]= tmp_ans->sample_size;
		}
		curr_sys++;
		Master_Results[*it].erase ( Master_Results[*it].begin(), Master_Results[*it].begin()+numnewstages);

	}

	if (outputLevel >= 4)	cout << "MM 3333\n";
	Master_stages_screened_each_worker[worker-1] += numnewstages;
	MPI::COMM_WORLD.Ssend(sys_idx_to_send,	num_sys_to_send, MPI::INT,	worker, idx_tag);
	MPI::COMM_WORLD.Ssend(fns_to_send,	num_sys_to_send*numnewstages, MPI::DOUBLE,	worker, fn_tag);
	MPI::COMM_WORLD.Ssend(sumsq_to_send,	num_sys_to_send*numnewstages, MPI::DOUBLE,	worker, sumsq_tag);
	MPI::COMM_WORLD.Ssend(szs_to_send,	num_sys_to_send*numnewstages, MPI::INT,		worker, size_tag);


	if (outputLevel >= 4)	cout << "MM 4444\n";
	//	this->Master_Phase1_Recv_Best(worker);
	this->Master_Phase1_Send_OtherBest(worker);

	if (outputLevel >= 4)	cout << "MM 5555\n";
	return;
}

void NHH2013AlgoV2::Master_Phase1_RecvScreening(const int & worker) {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master receiving screening results from worker " << worker << endl;
		cout << oss.str();
	}
	Master_Phase1_Recv_Eliminated(worker);
	Master_Phase1_Recv_Best(worker);
	//	Master_Phase1_Send_OtherBest(worker);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "Master finished receiving screening results from worker " << worker << endl;
		cout << oss.str();
	}
}

int NHH2013AlgoV2::Master_Phase1_getNumNewStageAvail(const int & worker) {
	if (Core2SysLists[worker-1].size() == 0) return 0;
	int last_completed_stage = Master_stages_screened_each_worker[worker-1];
	list<int>::iterator it;
	bool stage_complete;
	do {
		stage_complete = true;
		for (it = Core2SysLists[worker-1].begin(); it != Core2SysLists[worker-1].end(); it++) {
			if (
					//	!system_eliminated[*it] &&
					(int)Master_Results[*it].size() +
					Master_stages_screened_each_worker[worker-1]  <=
					last_completed_stage + 1) {
				stage_complete=false;
				break;
			}
		}
		if (stage_complete)	last_completed_stage += 1;
	} while(stage_complete);
	if (outputLevel >= 4) {
		ostringstream os1;
		os1 << "Worker " << worker << " has last completed " << last_completed_stage <<
				" screened " << Master_stages_screened_each_worker[worker-1] << endl;
		cout << os1.str();
	}
	return (last_completed_stage-Master_stages_screened_each_worker[worker-1]);
}

void NHH2013AlgoV2::Master_Phase1_SendTermination(const int & worker) {
	//	int exec = 0;
	//	MPI::COMM_WORLD.Send(&exec,	1, MPI::INT,worker, exec_tag);
}

//void NHH2013AlgoV2::Master_Phase1_WrapUp() {
//	int from, flag; list<int>::iterator iter;
//	// send ready flag to remaining workers
//	for (iter=Master_Recvd_Workers_List.begin();iter!=Master_Recvd_Workers_List.end();iter++) {
//		from = *iter;
//		MPI::COMM_WORLD.Isend(&dummy,	1, MPI::INT,from, ready_master_tag);
//	}
//	Master_Recvd_Workers_List.clear();
//
//	assert (this->num_systems_eliminated >= S_Total - 1);
//	flag = 6;
//	for (int i = 1; i < nP; ++i) {
//		if (!core_terminated[i-1]) {
//			MPI::COMM_WORLD.Recv(&dummy, 	1, MPI::INT,	i, ready_worker_tag);
//			MPI::COMM_WORLD.Send(&flag,	1, MPI::INT,	i, flag_tag);
//		}
//	}
//}

void NHH2013AlgoV2::Worker_Routine() {
	// the worker cores receive seeds and run simulation
	bool cont = true;
	map<int, SOResult>::iterator iter_ans;
#ifdef TEST_CONFIGURES
	problem->readSystem();
#endif

	//phase 00 and phase 0 routine

	worker_stages_simulated = 0;
	worker_phase0_Results_keep.clear();

	do {
		//worker receives a flag from master
		MPI::COMM_WORLD.Recv(&worker_flag, 1, MPI::INT, 0, flag_tag);
		switch (worker_flag) {
		case 1:
			Worker_Phase00_0_Routine();
			break;
		case 2: {
			switch (screenVersion) {
			case 1:
			case 3:
			case 5:
				Worker_Phase00_0_Routine();
				break;
			case 2:
			case 4:
			case 6:
			case 8:
				Worker_Phase0_Routine_with_Screening();
				break;
			default:
				cerr << "Incorrect screen version, terminating..." << endl;
				return;
				break;
			}
			break;
		}
		case 3:
			// flag is 3: a signal to receive and initialize the system list for phase 1
			Worker_Phase1_Prepare();
			break;
		case 4:
			// flag is 4: a signal to execute the subroutine for phase 1
			Worker_Phase1_Execute();
			if (outputLevel >=2) {
				ostringstream oss;
				oss << "Worker " << myRank << " terminating from routine\n";
				cout << oss.str();
			}
			return;
		case 0:
			return;
		default:
			cout << "Worker " << myRank <<" unexpectedly received flag "
			<< worker_flag << endl;
			break;
		}
		//		if (worker_flag == 1) {
		//			Worker_Phase00_0_Routine();
		//		} else if (worker_flag == 2) {
		//			if (screenVersion == 1 || screenVersion == 3 || screenVersion == 5) {
		//				Worker_Phase00_0_Routine();
		//			} else if (screenVersion == 2 || screenVersion == 4 || screenVersion == 6) {
		//				Worker_Phase0_Routine_Version2_4();
		//			} else {
		//				cerr << "Incorrect screen version, terminating..." << endl;
		//				return;
		//			}
		//		} else if (worker_flag == 3) {
		//			// flag is 3: a signal to receive and initialize the system list for phase 1
		//			Worker_Phase1_Prepare();
		//		} else if (worker_flag == 4){
		//			// flag is 4: a signal to execute the subroutine for phase 1
		//			Worker_Phase1_Execute();
		//			if (outputLevel >=2) {
		//				ostringstream oss;
		//				oss << "Worker " << myRank << " terminating from routine\n";
		//				cout << oss.str();
		//			}
		//			return;
		//		} else if (worker_flag == 0) {
		//			return;
		//		} else{
		//			cout << "Worker " << myRank <<" unexpectedly received flag "
		//					<< worker_flag << endl;
		//		}
	} while(worker_flag >= 0);
	//
	//	//	cout << "Worker " << myRank <<" received worker_flag " << worker_flag << endl;
	//
	//	// phase 1: receive system list and stage sizes from master
	//	if(worker_flag == 5)
	//		// flag is 5: a signal to receive and initialize the system list for phase 1
	//		Worker_Phase1_Prepare();
	//	else if (worker_flag == 0)
	//		return;
	//	else cout << "Worker " << myRank <<" did not receive flag 5 as expected" << endl;
	//
	//	//	MPI::COMM_WORLD.Recv(&flag, 1, MPI::INT, 0, flag_tag);
	//	//	cout << "Worker " << myRank <<" received flag " << flag << endl;
	//	//	return;
	//
	//	MPI::COMM_WORLD.Recv(&worker_flag, 	1, MPI::INT,    0, flag_tag);
	//	if(worker_flag == 4)
	//		// flag is 4: a signal to execute the subroutine for phase 1
	//		Worker_Phase1_Execute();
	//	else if (worker_flag == 0)
	//		return;
	//	else
	//		cout << "Worker " << myRank <<" did not receive flag 4 as expected" << endl;
	//


	return;

	/*
	// phase 2: simulate each system 1 time before sending back, while listening to master's updates
	if(flag == 2) {
		// phase 2 routine

		// receives the number of systems for the worker
		this->Worker_Recv_NumSys();
		s_curr = S_worker-1;
		//worker receives systems and seeds from master
		this->Worker_Recv_ListSys();
		// finish receiving systems and seeds from the master

		while (flag == 2) {
			bool more_message = true;
			while (more_message) {
				// iterate over cores to find the next core that has message to send
				if (MPI::COMM_WORLD.Iprobe(0 , flag_tag, status)) {
					// if there is a flag_tag message from the master, it must be an update to systems,
					// or a signal to stop
					MPI::COMM_WORLD.Recv(&flag, 1, MPI::INT,    0, flag_tag);
					if (flag == 2) {
						// a signal to begin receiving a new list of systems.

						// receives the number of systems for the worker
						this->Worker_Recv_NumSys();
						s_curr = S_worker-1;
						//worker receives systems and seeds from master
						this->Worker_Recv_ListSys();
						// finish receiving systems and seeds from the master
					} else {
						// a signal to stop. quite the inner and the outer while loop
						more_message = false;
					}
				} else more_message = false;
			}
			if (flag == 2) {
				// no more message and no stop flag, continue to simulate the next system once and send to master
				this->Worker_Simulate_Multiple_System();
			}
		}
	}

	//exit here when flag != 2, in which case phase 2 is completed
	return;
	 */
}

/// Worker receives the number of systems to simulate from the master.
void NHH2013AlgoV2::Worker_Recv_NumSys() {

	assert(this->myRank>0);
	MPI::COMM_WORLD.Recv(&S_worker, 1, MPI::INT, 0, sys_tag);
}

///Worker receives the list of systems and seeds from master.
void NHH2013AlgoV2::Worker_Recv_ListSys() {

	//			unsigned long mySeed[s][6]; int idx[s];
	//			Matrix<int> init_disc(s, problem->prop.d_disc, 0);
	//			double * init_cont;
	//			int n[s];
	problem->clear();
	workerSeeds.clear(); workerSeeds.resize(S_worker, 6, 0);
	//	init_disc.clear(); init_disc.resize(S_worker, problem->prop.d_disc, 0);
	worker_system_idx.clear(); worker_system_idx.resize(S_worker, 0);
	worker_stage_sizes.clear(); worker_stage_sizes.resize(S_worker, 0);
	//	for (int j = 0; j < S_worker; j++) {
	//		MPI::COMM_WORLD.Recv(mySeed[j], 6, MPI::UNSIGNED_LONG, 0, seed_tag);
	//		MPI::COMM_WORLD.Recv(&worker_system_idx[j]  , 1, MPI::INT,           0, idx_tag);
	//		MPI::COMM_WORLD.Recv(&worker_stage_sizes[j]    , 1, MPI::INT,           0, size_tag);
	//receive discrete decision variables from master
	//		if (problem->prop.d_disc > 0) {
	//			MPI::COMM_WORLD.Recv(init_disc[j], problem->prop.d_disc, MPI::INT, 0, x_disc_tag);
	//		}

	//		//receive continuous decision variables from master
	//		if (problem->prop.d_cont > 0) {
	//			init_cont = new double [problem->prop.d_cont];
	//			MPI::COMM_WORLD.Recv(init_cont, problem->prop.d_cont, MPI::DOUBLE, 0, x_cont_tag);
	//		}
	//	}
	MPI::COMM_WORLD.Recv(&worker_system_idx[0]	, S_worker, MPI::INT,           0, idx_tag);
	MPI::COMM_WORLD.Recv(&worker_stage_sizes[0]	, S_worker, MPI::INT,           0, size_tag);
	MPI::COMM_WORLD.Recv(workerSeeds[0], 6*S_worker, MPI::UNSIGNED_LONG, 0, seed_tag);
	//	vector <int> v;
	//	for (int j = 0; j < S_worker; j++) {
	//		//compute discrete decision variable using index
	//		v = problem->Idx2System_Disc(worker_system_idx[j]);
	//		for (int k = 0 ; k < problem->prop.d_disc; k++) {
	//			init_disc[j][k] = v[k];
	//		}
	//	}
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " finished receiving \n";
		cout << oss.str();
	}
}
//
//void NHH2013AlgoV2::Worker_Recv_S2() {
//	// worker receives vector S2 from master
//	for (int k=0; k < S_Total; k++) {
//		MPI::COMM_WORLD.Recv(&S2[k], 1, MPI::DOUBLE, 0, s2_tag);
//	}
//}

void NHH2013AlgoV2::Worker_Phase00_0_Routine() {
	int sz;
	double fn, sumsq, simtime;
	MPI::Status status; SOResult tmp_Result;
	// receives the number of systems for the worker
	this->Worker_Recv_NumSys();
	//worker receives systems and seeds from master
	this->Worker_Recv_ListSys();
	// finish receiving systems and seeds from the master

	// clear answers collected by worker
	if (worker_flag == 1) {
		//phase 0
		worker_phase00_send.clear();
		worker_phase00_send.resize(S_worker);
	}
	if (worker_flag == 2){
		worker_phase00_send.clear();
		worker_phase0_fn_send.clear();
		worker_phase0_fn_send.resize(S_worker);
		worker_phase0_sumsq_send = worker_phase0_fn_send;
	}
	//	worker_phase0_sys_sent = 0;

	// performs simulation, one replication for each system, and send results immediately back to the master
	//	bool message_received = false;
	for (int j=0; j < S_worker; j++) {
		sz = worker_stage_sizes[j];
		//simulate a single system
		this->Worker_Simulate_Single_System(j, sz);
		fn = problem->getFn();
		sumsq = problem->getFnSumSq();
		simtime = problem->getSimulationTime();
		tmp_Result.clear(); tmp_Result.addSample(sz, fn*sz, sumsq, simtime);
		//		if (algo == 2)
		//		{
		if  (worker_flag == 1) {
			// phase 00
			worker_phase00_send[j] = simtime;
		} else {
			// phase 0
			worker_phase0_fn_send[j] = fn;
			worker_phase0_sumsq_send[j] = sumsq;
		}
		//			worker_stage_send[worker_system_idx[j]] = tmp_Result;
		//		}
		//		else
		//		{
		//			Worker_Isend_Single_Result(worker_system_idx[j], tmp_Result);
		//		}

		ostringstream oss;
		if (outputLevel >= 4) {
			oss << "Worker " << myRank << " finished its sys " << j << endl;
			cout << oss.str();
		}
	}//end for
	//	if (algo == 2){
	// after finishing simulating all the systems, send remaining answers whenever master
	// is ready to receive

	//		if (!worker_stage_send.empty()) {
	MPI::COMM_WORLD.Recv(&dummy,      1, MPI::INT,    0, ready_master_tag);
	Worker_Send_CurrentStage();
	//		}
	//	}
}

void NHH2013AlgoV2::Worker_Phase0_Routine_with_Screening() {
	bool cont = true; dummy = 1; double s;
	// receives the number of systems for the worker
	Worker_Recv_NumSys();
	//worker receives systems and seeds from master
	Worker_Recv_ListSys();

	//	Worker_Initilize_Stages();
	//	Worker_Simulate_Stage();
	//	double S2_send[S_worker];
	int   elim_send[S_worker];
	worker_phase0_fn_send.resize(S_worker);
	worker_phase0_sumsq_send = worker_phase0_fn_send;
	int best_idx, best_sz; double fn0, best_ssq, best_fn= -9.9e99 * this->minmax;
	int best_idxs[nP];
	int best_szs [nP];
	double best_ssqs[nP];
	double best_fns[nP];
	worker_phase0_Results_keep.resize(1);
	worker_phase0_Results_keep[0].resize(S_worker);
	for (int i = 0; i < S_worker ; ++i) {
		this->Worker_Simulate_Single_System(i, worker_stage_sizes[i]);
		dummy = worker_system_idx[i];
		SOResult *ans = &(this->worker_phase0_Results_keep[0][i]);
		ans->clear();
		ans->addSample(worker_stage_sizes[i],
				worker_stage_sizes[i] * problem->getFn(),
				problem->getFnSumSq());
		s = ans->getSampleStdev();
		S2[dummy] = s * s;
		fn0 = ans->getSampleMean();
		if ( (fn0 - best_fn) * this->minmax > 0 ) {
			best_fn = fn0;
			best_idx= dummy;
			best_sz = ans->sample_size;
			best_ssq= ans->getSampleSumsq();
		}
	}
	MPI::COMM_WORLD.Allgather(&best_fn , 1, MPI::DOUBLE, best_fns , 1,MPI::DOUBLE);
	MPI::COMM_WORLD.Allgather(&best_idx, 1, MPI::INT   , best_idxs, 1,MPI::INT   );
	MPI::COMM_WORLD.Allgather(&best_sz , 1, MPI::INT   , best_szs , 1,MPI::INT   );
	MPI::COMM_WORLD.Allgather(&best_ssq, 1, MPI::DOUBLE, best_ssqs, 1,MPI::DOUBLE);
	map_worker_results_received.clear();
	deque<SOResult> vans_temp;
	//	SOAnswer ans_temp;
	for (int i = 1; i <= nP-1; ++i) {
		vans_temp.clear(); vans_temp.resize(1); vans_temp[0].clear();
		vans_temp[0].addSample(best_szs[i], best_fns[i]*best_szs[i], best_ssqs[i]);
		map_worker_results_received[best_idxs[i]]=vans_temp;
		s = vans_temp[0].getSampleStdev();
		S2[best_idxs[i]] = s * s;
	}
	// so far, only one stage simulated: use skipping screening procedure will only screen with
	// stage 0 information
	this->worker_stages_simulated=1;
	Worker_pairwise_screen_skipping();
	int sys_left = S_worker - this->num_systems_eliminated;

	// count the number of sys left in the system_eliminated vector to double check
	int s_tmp = 0;
	for (int i = 0; i < S_worker; ++i) {
		if (!this->system_eliminated[this->worker_system_idx[i]]) {
			s_tmp+=1;
		}
	}
	assert(s_tmp == sys_left);

	//report phase 0 results (including screening) back to master
	SOResult *temp_ans; double fn, sumsq, simtime; int sz, temp_idx; bool isEliminated;
	//	MPI::COMM_WORLD.Send(&dummy,		1, MPI::INT,0, ready_worker_tag);
	MPI::COMM_WORLD.Isend(&S_worker,		1, MPI::INT,0, sys_tag);
	for (int i = 0; i < S_worker; ++i) {
		temp_idx = this->worker_system_idx[i];
		temp_ans = &(this->worker_phase0_Results_keep[0][i]);
		//		simtime = temp_ans.getSampleTime();
		//		sz = temp_ans.sample_size;
		worker_phase0_sumsq_send[i] = temp_ans->getSampleSumsq();
		worker_phase0_fn_send[i]    = temp_ans->getSampleMean();
		elim_send[i] = this->system_eliminated[temp_idx];
	}
	MPI::COMM_WORLD.Send( &worker_phase0_fn_send[0]		, S_worker, MPI::DOUBLE, 0, fn_tag);
	MPI::COMM_WORLD.Send( &worker_phase0_sumsq_send[0]	, S_worker, MPI::DOUBLE, 0, sumsq_tag);
	MPI::COMM_WORLD.Send( elim_send	, S_worker, MPI::INT	, 0, elimidx_tag);
}

void NHH2013AlgoV2::Worker_Phase1_Prepare() {
	if (outputLevel > 0) {
		ostringstream oss;
		oss << "worker " << myRank << " preparing phase 1" << endl;
		cout << oss.str();
	}

	// receives the seed from Master
	MPI_Bcast(tempSeed,6,MPI_UNSIGNED_LONG,0,MPI_COMM_WORLD);

	// receives the vector S2 from Master
	MPI_Bcast(&S2[0],S_Total,MPI_DOUBLE,0,MPI_COMM_WORLD);

	// receives the stage sizes from Master
	MPI_Bcast(&Stage_Sizes[0],S_Total,MPI_INT,0,MPI_COMM_WORLD);

	if (SORngStream::SetPackageSeed(tempSeed)==false)
		cerr << "Error seed for Worker routine\n";

	for (int i = 0; i <= myRank; ++i) {
		SORngStream rs;
	}

	SORngStream rs;
	WorkerStreams.resize(S_Total);

	for (int i = 0; i < S_Total; ++i) {
		rs.ResetNextSubstream();
		rs.GetState(tempSeed);
		WorkerStreams[i].SetSeed(tempSeed);
	}

	MPI_Bcast(&num_systems_remain_after_phase0,1,MPI_INT,0,MPI_COMM_WORLD);
	int perms[num_systems_remain_after_phase0];
	int num_systems_per_core = (num_systems_remain_after_phase0)/(nP-1);
	int num_systems_extra    = (num_systems_remain_after_phase0)%(nP-1);
	//	cout << "Worker" << num_systems_remain_after_phase0 << " " << num_systems_per_core << " " <<
	//			num_systems_extra << " "<< endl;
	MPI_Bcast(perms,num_systems_remain_after_phase0,MPI_INT,0,MPI_COMM_WORLD);
	int curr_sys = 0;
	for (int i = 1; i < myRank; ++i) {
		curr_sys += (num_systems_per_core+((i<=num_systems_extra)?1:0));
	}
	worker_system_idx.resize(num_systems_per_core+ ((myRank<=num_systems_extra)?1:0) );
	for(int i = 0; i < worker_system_idx.size(); ++i) {
		worker_system_idx[i] = perms[curr_sys + i];
	}
	if(outputLevel >= 4) {
		ostringstream oss;
		oss << "Worker " << myRank << " lists: " ;
		for (int i = 0; i < worker_system_idx.size(); i++) {
			oss << worker_system_idx[i] << ",";
		}
		oss << endl;
		cout << oss.str();
	}
	S_worker = worker_system_idx.size();
	worker_phase0_Results_keep.clear();
	worker_phase1_results_keep.clear();
	worker_phase1_results_keep.resize(S_Total);

	if (outputLevel >= 3){
		ostringstream oss;
		oss << "worker " << myRank << " finishes initializing in phase 1\n";
		cout << oss.str();
	}
	if (screenVersion == 3 || screenVersion == 4 || screenVersion == 8) {
		worker_otherbest_stages_screened.clear();
		worker_otherbest_stages_screened.resize(S_worker);
	}
}

void NHH2013AlgoV2::Worker_Phase1_Execute() {
	bool cont = true;  MPI::Status status;
	//begins simulating stage by stage
	//worker_stages_toSend = 0;
	this->num_systems_eliminated = 0; // set to zero because a new set of systems arrive
	this->worker_stages_recvd = 0;
	this->worker_stages_screened = 0;
	this->worker_phase0_Results_keep.clear();
	this->worker_eliminated_system_to_send.clear();
	this->worker_eliminated_system_simcount.clear();
	this->map_worker_results_received.clear();

	int flag, exec = 0;
	MPI::COMM_WORLD.Ssend(&exec, 1, MPI::INT, 0, ready_worker_tag);
	MPI::COMM_WORLD.Recv(&flag,	1, MPI::INT, 0, flag_tag);
	while(flag == 5 && cont) {
		switch(exec) {
		case 1:
			Worker_Phase1_SendAns();
			break;
		case 2:
			Worker_Phase1_SendScreening();
			break;
		case 0:
			break;
		}
		MPI::COMM_WORLD.Recv(&exec,	1, MPI::INT, 0, exec_tag);
		switch(exec) {
		case 1:
			Worker_Phase1_RecvSimSys();
			break;
		case 2:
			Worker_Phase1_RecvScreenAns();
			break;
		case 0:
			cont = false;
			Worker_Phase1_Clean();
			return;
		}
		MPI::COMM_WORLD.Ssend(&exec, 1, MPI::INT, 0, ready_worker_tag);
		MPI::COMM_WORLD.Recv(&flag,	1, MPI::INT, 0, flag_tag);
	}
}


void NHH2013AlgoV2::Worker_Phase1_SendAns() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " send answer of sys " <<
				worker_phase1_sys << " stage " << worker_phase1_stage <<
				" to master\n";
		cout << oss.str();
	}
	MPI::COMM_WORLD.Ssend(&worker_phase1_sys, 	1, MPI::INT,	0, idx_tag);
	MPI::COMM_WORLD.Ssend(&worker_phase1_stage,	1, MPI::INT,	0, stage_tag);
	MPI::COMM_WORLD.Ssend(&worker_phase1_fn,		1, MPI::DOUBLE,	0, fn_tag);
	MPI::COMM_WORLD.Ssend(&worker_phase1_sumsq,	1, MPI::DOUBLE,	0, sumsq_tag);
}

void NHH2013AlgoV2::Worker_Phase1_SendScreening() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " send screening results to master\n";
		cout << oss.str();
	}
	Worker_Phase1_Send_Eliminated();
	Worker_Phase1_Send_Best();
}

void NHH2013AlgoV2::Worker_Phase1_RecvSimSys() {
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " receiving a sys to simulate\n";
		cout << oss.str();
	}
	MPI::COMM_WORLD.Recv(&worker_phase1_sys  ,	1, MPI::INT,0, idx_tag);
	MPI::COMM_WORLD.Recv(&worker_phase1_stage,	1, MPI::INT,0, stage_tag);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " received sys " <<
				worker_phase1_sys << " stage " << worker_phase1_stage <<
				" from master to simulate\n";
		cout << oss.str();
	}
	problem->Initialize_Single_System(worker_phase1_sys,
			Stage_Sizes[worker_phase1_sys], &WorkerStreams[worker_phase1_sys]);
	problem->Objective();
	worker_phase1_fn = problem->getFn();
	worker_phase1_sumsq = problem->getFnSumSq();
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << "worker " << myRank << " finished simulating sys " <<
				worker_phase1_sys << " stage " << worker_phase1_stage <<
				"\n";
		cout << oss.str();
	}
}

void NHH2013AlgoV2::Worker_Phase1_RecvScreenAns() {
	int numStages, numSys, curr_stage, curr_sys;
	// receiving this worker's own systems
	MPI::COMM_WORLD.Recv(&numStages,		1, MPI::INT,	0, stage_tag);
	MPI::COMM_WORLD.Recv(&numSys,	1, MPI::INT,	0, sys_tag);
	if (outputLevel >= 4) {
		ostringstream oss;
		oss << myRank  << " " << worker_stages_recvd << " WW 1111\n";
		oss << "this worker " << myRank << " has " << S_worker - this->num_systems_eliminated <<
				" Sys remaining\n";
		cout << oss.str();
	}
	assert (numSys == S_worker - this->num_systems_eliminated );
	int    sys_idx_to_recv[numSys];
	double fns_to_recv  [numSys*numStages];
	double sumsq_to_recv[numSys*numStages];
	int    szs_to_recv  [numSys*numStages];
	MPI::COMM_WORLD.Recv(sys_idx_to_recv,	numSys, MPI::INT,	0, idx_tag);
	MPI::COMM_WORLD.Recv(fns_to_recv,	numSys*numStages, MPI::DOUBLE,	0, fn_tag);
	if (outputLevel >= 4){
		cout << "WW 2222\n";
	}
	MPI::COMM_WORLD.Recv(sumsq_to_recv,	numSys*numStages, MPI::DOUBLE,	0, sumsq_tag);
	MPI::COMM_WORLD.Recv(szs_to_recv,	numSys*numStages, MPI::INT,		0, size_tag);
	if (outputLevel >= 4)	cout << "WW 3333\n";
	SOResult tmp_ans;
	tmp_ans.clear();
	for (curr_sys = 0; curr_sys < numSys; curr_sys++){
		for (curr_stage = 0; curr_stage < numStages; curr_stage++) {
			worker_phase1_results_keep[sys_idx_to_recv[curr_sys]].push_back(tmp_ans);
			worker_phase1_results_keep[sys_idx_to_recv[curr_sys]].back().addSample(szs_to_recv[curr_sys * numStages + curr_stage],
					fns_to_recv[curr_sys * numStages + curr_stage]*szs_to_recv[curr_sys * numStages + curr_stage],
					sumsq_to_recv[curr_sys * numStages + curr_stage] );
		}
	}
	if (outputLevel >= 4)
		cout << "WW 4444\n";
	this->worker_stages_recvd += numStages;
	// receiving others' best
	Worker_Phase1_Recv_OtherBest();
	if (outputLevel >= 4) {
		cout << "WW 5555now\n";
	}
	// performs pairwise screening
	Worker_pairwise_screen_no_skipping();
	if (outputLevel >= 4)	cout << "WW 6666\n";
}

void NHH2013AlgoV2::Worker_Phase1_Clean() {
	this->worker_phase0_Results_keep.clear();
	this->worker_phase1_results_keep.clear();
	this->map_worker_results_received.clear();
	this->worker_eliminated_system_to_send.clear();
	this->worker_eliminated_system_simcount.clear();
	this->worker_system_idx.clear();
	this->worker_otherbest_stages_screened.clear();
	//	this->init_disc.clear();
	worker_stage_sizes.clear();
	if (outputLevel >= 2) {
		ostringstream oss;
		oss << "Worker " << myRank << " received termination instruction from master" << endl;
		cout << oss.str();
	}
}

void NHH2013AlgoV2::Worker_pairwise_screen_skipping() {
	// performs pairwise screening at the worker level
	// for phase 0

	//	cout << "Worker " << myRank << " begins pairwise screen" << endl;

	// return if all systems eliminated
	if (this->num_systems_eliminated == this->S_worker) return;

	assert(worker_stages_simulated == (int)worker_phase0_Results_keep.size());
	vector<SOResult> *myAnswers_now = &worker_phase0_Results_keep[worker_stages_simulated-1];

	map<int, deque<SOResult> >::iterator iter_other;
	list<int>::iterator iter;
	double Xbar_i, Xbar_j, ni, nj, tau, Y; int idx_i, i, idx_j, j, otherStage;
	// use myAnswers_now to screen sys i
	for ( idx_i = 0; idx_i  < myAnswers_now->size(); idx_i++){
		i = worker_system_idx[idx_i];
		if (!(this->system_eliminated[i]) ) {
			Xbar_i = (*myAnswers_now)[idx_i].getSampleMean() * this->minmax;
			ni = (*myAnswers_now)[idx_i].sample_size;
			for ( idx_j = 0; idx_j < myAnswers_now->size(); idx_j++ ) {
				if (this->system_eliminated[i]) break;
				j = worker_system_idx[idx_j];
				if (i != j) {
					Xbar_j = (*myAnswers_now)[idx_j].getSampleMean() * this->minmax;
					if (Xbar_j > Xbar_i) {
						nj = (*myAnswers_now)[idx_j].sample_size;
						tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
						Y = tau*(Xbar_i-Xbar_j);
						if (Y < min(0.0, lambda*tau-a)) {
							// system i eliminated by system j
							this->system_eliminated[i] = 1;
							num_systems_eliminated += 1;
							worker_eliminated_system_to_send.push_back(i);
							worker_eliminated_system_simcount.push_back(ni);
							if(outputLevel >= 4) {
								ostringstream oss;
								oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
										<< setw(7)	<< setfill('0')  << i <<","
										<< " on core " << setw(3) << setfill('0') << myRank
										<< "; " << Xbar_j << "," << nj << "," << S2[j] <<
										"," << Xbar_i << "," << ni << "," << S2[i] <<
										endl;
								cout << oss.str();
							}
						}
					}
				}
			}
		}
	}

	//	cout << "111" << endl;
	if (!map_worker_results_received.empty()){
		// use otherAnswers to screen sys i
		for ( int k = 0; k < S_worker; k++){
			i = this->worker_system_idx[k];
			if (!(this->system_eliminated[i]) ) {
				for ( iter_other = map_worker_results_received.begin();
						iter_other !=map_worker_results_received.end(); iter_other++ ) {
					if (this->system_eliminated[i]) break;
					j = iter_other->first;
					//					cout << "222" << endl;
					if (i != j) {
						//						cout << "555" << endl;
						otherStage = min((int)(iter_other->second.size()), worker_stages_simulated)-1;
						//						cout << "otherstage = " << otherStage <<endl;
						//						cout << "666" << endl;
						if (otherStage >= 0) {
							Xbar_j = iter_other->second[otherStage].getSampleMean() * this->minmax;
							if (Xbar_j > Xbar_i) {
								nj = iter_other->second[otherStage].sample_size;
								Xbar_i = worker_phase0_Results_keep[otherStage][k].getSampleMean() * this->minmax;
								ni = worker_phase0_Results_keep[otherStage][k].sample_size;
								tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
								Y = tau*(Xbar_i-Xbar_j);
								//							cout << "777" << endl;
								if (Y < min(0.0, lambda*tau-a)) {
									// system i eliminated by system j
									this->system_eliminated[i] = 1;
									num_systems_eliminated += 1;
									worker_eliminated_system_to_send.push_back(i);
									worker_eliminated_system_simcount.push_back(ni);
									if(outputLevel >= 4) {
										ostringstream oss;
										oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
												<< setw(7)	<< setfill('0')  << i <<","
												<< " on core " << setw(3) << setfill('0') << myRank
												<< "; " << Xbar_j << "," << nj << "," << S2[j] <<
												"," << Xbar_i << "," << ni << "," << S2[i] <<
												endl;
										cout << oss.str();
									}
								}
							}
						}
					}
				}
			}
		}
	}

}

void NHH2013AlgoV2::Worker_pairwise_screen_no_skipping() {
	// performs pairwise screening at the worker level
	if (outputLevel >= 4)	{
		cout << "WW 5555x\n";
	}
	if (outputLevel >= 3) {
		ostringstream o0, o1,o2,o3,o4;
		o0 << "Worker " << this->myRank << " begins pairwise screen\n" ;
		cout << o0.str().c_str();
		o1 <<"S_worker = " << this->S_worker << "\n";
		cout << o1.str().c_str();
		o2 << "num_systems_eliminated = " << this->num_systems_eliminated << "\n";
		cout << o2.str().c_str();
		o3 << "worker_stages_screened = " << this->worker_stages_screened << "\n";
		cout << o3.str().c_str();
		o4 << "worker_stages_recvd = " << this->worker_stages_recvd << "\n";
		cout << o4.str().c_str();
	}
	//	assert(S_worker == worker_system_idx.size());
	// return if all systems eliminated
	if (this->num_systems_eliminated == this->S_worker) {
		return;
	}
	if (this->num_systems_eliminated >= this->S_worker - 1 &&
			(int)this->map_worker_results_received.size() == 0) {
		return;
	}
	map<int, deque<SOResult> >::iterator iter_other;
	list<int>::iterator iter;
	double Xbar_i, Xbar_j, ni, nj, tau, Y;
	int idx_i, idx_j, i, j, otherStage;
	int stage_screening;
	for ( idx_i = 0; idx_i < S_worker; idx_i++){
		i = worker_system_idx[idx_i];
		if (!(this->system_eliminated[i]) ) {
			if (outputLevel >= 4){
				ostringstream osss;
				osss << "Sys i " << i << endl;
				cout << osss.str();
			}
			for ( idx_j = 0; idx_j < S_worker; idx_j++){
				if (this->system_eliminated[i]) break;
				j = worker_system_idx[idx_j];
				if (i != j && !this->system_eliminated[j])  {
					if (outputLevel >= 4){
						ostringstream osss;
						osss << "Sys j " << j << endl;
						cout << osss.str();
					}
					for (stage_screening = worker_stages_screened; stage_screening < worker_stages_recvd; stage_screening++) {
						if (this->system_eliminated[i]) break;
						Xbar_i = worker_phase1_results_keep[i][stage_screening].getSampleMean() * this->minmax;
						Xbar_j = worker_phase1_results_keep[j][stage_screening].getSampleMean() * this->minmax;
						if (Xbar_j > Xbar_i) {
							ni = worker_phase1_results_keep[i][stage_screening].sample_size;
							nj = worker_phase1_results_keep[j][stage_screening].sample_size;
							tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
							Y = tau*(Xbar_i-Xbar_j);
							if (Y < min(0.0, lambda*tau-a)) {
								// system i eliminated by system j
								if(outputLevel >= 4) {
									ostringstream oss;
									oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
											<< setw(7)	<< setfill('0')  << i <<","
											<< " on core " << myRank
											<< "; " << Xbar_j << "," << nj << "," << S2[j] <<
											"," << Xbar_i << "," << ni << "," << S2[i] <<
											endl;
									cout << oss.str();
								}
								this->system_eliminated[i] = 1;
								num_systems_eliminated += 1;
								worker_eliminated_system_to_send.push_back(i);
								worker_eliminated_system_simcount.push_back(ni);
							}
						}
					}

				}
			}
		}
	}


	if (outputLevel >= 4)  {
		ostringstream oss;
		oss << "Worker " << myRank << " done self screening\n";
		cout << oss.str();
	}
	int ijscreened;
	map<int,int>::iterator it;
	//	cout << "111" << endl;
	if (!map_worker_results_received.empty()){
		// use otherAnswers to screen sys i
		for ( int idx_i = 0; idx_i < S_worker; idx_i++){
			i = this->worker_system_idx[idx_i];
			if (!(this->system_eliminated[i]) ) {
				if (outputLevel >= 4){
					ostringstream os;
					os << "Sys i " << i << endl;
					cout << os.str();
				}
				for ( iter_other = map_worker_results_received.begin();
						iter_other !=map_worker_results_received.end(); iter_other++ ) {
					if (this->system_eliminated[i]) break;
					j = iter_other->first;
					//					cout << "222" << endl;
					if (i != j) {
						if (outputLevel >= 4){
							ostringstream osss;
							osss << "Other Sys j " << j << endl;
							cout << osss.str();
						}
						//						cout << "555" << endl;
						otherStage = min((int)iter_other->second.size(), worker_stages_recvd)-1;
						//						cout << "otherstage = " << otherStage <<endl;
						//						cout << "666" << endl;
						if (otherStage >= 0) {
							it = worker_otherbest_stages_screened[idx_i].find(j);
							if (it == worker_otherbest_stages_screened[idx_i].end()) {
								worker_otherbest_stages_screened[idx_i][j] = -1;
								ijscreened = -1;
							} else {
								ijscreened = it->second;
							}
							for ( stage_screening = ijscreened+1;
									stage_screening <= otherStage; stage_screening++) {

								//	cout << "Stage " << stage_screening << endl;
								if (this->system_eliminated[i] ) break;
								Xbar_j = iter_other->second[stage_screening].getSampleMean() * this->minmax;
								Xbar_i = worker_phase1_results_keep[i][stage_screening].getSampleMean() * this->minmax;
								if (Xbar_j > Xbar_i) {
									ni = worker_phase1_results_keep[i][stage_screening].sample_size;
									nj = iter_other->second[stage_screening].sample_size;
									tau = 1.0/( (S2[i]/ni)+(S2[j]/nj) );
									Y = tau*(Xbar_i-Xbar_j);
									//								cout << "777" << endl;
									if (Y < min(0.0, lambda*tau-a)) {
										// system i eliminated by system j
										this->system_eliminated[i] = 1;
										num_systems_eliminated += 1;
										worker_eliminated_system_to_send.push_back(i);
										worker_eliminated_system_simcount.push_back(ni);
										if(outputLevel >= 4 || (outputLevel == 3 && this->num_systems_eliminated >= S_worker - 1)) {
											ostringstream oss;
											oss << "Sys " << setw(7) << setfill('0') << j  << " KO Sys "
													<< setw(7)	<< setfill('0')  << i <<","
													<< " on core " << myRank
													<< " at stage " << stage_screening << " of " << otherStage << ", "
													<< "othersize=" << (int)iter_other->second.size() << ", workersize=" << worker_stages_recvd-1 << ", "
													<< Xbar_j << "," << nj << "," << S2[j] <<
													"," << Xbar_i << "," << ni << "," << S2[i]
													                                        << endl;
											cout << oss.str();
										}
									}// end if
								}
							}//end for
							it->second = otherStage;
						}// end if
					}
				}
			}
		}
	}

	worker_stages_screened = worker_stages_recvd;
	if (outputLevel >= 4) {
		ostringstream oo;
		oo << "Worker " << myRank << " finished pariwise screening\n";
		cout << oo.str();
	}
}

void NHH2013AlgoV2::Worker_Simulate_Single_System(int j, int nReps) {
	problem->clear();
	SORngStream * p_RS = new SORngStream();
	if (p_RS->SetSeed(workerSeeds[j])==false) {
		cout << "Error seed with system " << endl;
		//				<< this->System2Idx(disc, dummy) << endl;
	}

	problem->Initialize_Single_System(worker_system_idx[j], nReps, p_RS);
	problem->Objective();

	// initialize
	//	problem->Set_Systems(1, &worker_system_idx[j], &nReps, workerSeeds[j]);
	//	problem->Initialize_Single_System(init_disc[j], init_cont, n, mySeed[j]);
	//compute objective function
	//	problem->run_system(0);
}

//void NHH2013AlgoV2rithm::Worker_Simulate_Multiple_System() {
//	// currently not in use
//
//	// simulate a number of systems, generate one replication from each system
//	// exit when a flag from master is detected
//	problem->clear();
//	s_curr = S_worker-1;
//	MPI::Status status;
//	// initialize problem with multiple systems
//	for (int j = 0; j < S_worker; ++j) {
//		problem->Add_System(init_disc[j], init_cont, worker_stage_sizes[j], mySeed[j]);
//	}
//	while(MPI::COMM_WORLD.Iprobe(0 , flag_tag, status) == false) {
//		s_curr = (s_curr+1)%S_worker;
//		problem->run_system(s_curr);
//		//send answers back to master
//		double fn = problem->getFn();
//		double sumsq = problem->getFnSumSq();
//		int sz = problem->getNumReplications();
//		MPI::COMM_WORLD.Send( &fn   	, 1, MPI::DOUBLE, 0, fn_tag);
//		MPI::COMM_WORLD.Send( &sumsq	, 1, MPI::DOUBLE, 0, sumsq_tag);
//		MPI::COMM_WORLD.Send( &sz   	, 1, MPI::INT   , 0, samplesize_tag);
//		MPI::COMM_WORLD.Send( &worker_system_idx[s_curr], 1, MPI::INT   , 0, sampleidx_tag);
//	}
//}
//void NHH2013AlgoV2::Worker_Initilize_Stages() {
//	// initialize problem with multiple systems
//	problem->clear();
//	problem->Set_Systems(S_worker, &worker_system_idx[0], &worker_stage_sizes[0],
//			workerSeeds[0]);
//	//	for (int j = 0; j < S_worker; ++j) {
//	//		problem->Add_System(worker_system_idx[j], worker_stage_sizes[j], mySeed[j]);
//	//	}
//}

//void NHH2013AlgoV2::Worker_Simulate_Stage() {
//	// simulate a number of systems,
//	// the number of replication from each system pre-determined by worker_stage_sizes
//
//	//	cout << "Worker begins simulating" << endl;
//	vector<SOAnswer> temp_worker_Results, prev_worker_Results; SOAnswer temp_Result;
//	vector<SOAnswer>::iterator iter;
//	//problem->clear();
//	//	s_curr = S_worker-1;
//	MPI::Status status;
//	temp_worker_Results.resize(S_worker);
//	//	cout << "111" << endl;
//	for (int j = 0; j < S_worker; ++j) {
//		if (!this->system_eliminated[worker_system_idx[j]]) {
//			problem->run_system(j);
//			//			if (worker_system_idx[j]==1888) cout << "Simulated sys 1888 for one stage "<<endl;
//			double fn = problem->getFn();
//			double sumsq = problem->getFnSumSq();
//			int sz = problem->getNumReplications();
//			vector<int> v = problem->getDiscX();
//			//			ostringstream oss;
//			//			oss << "Worker " << myRank << " simulates sys " << worker_system_idx[j] <<
//			//					", result = " << fn <<
//			//					"," << v[0] << "," <<v[1] << "," <<v[2] << "," <<v[3] << "," <<v[4] << "," <<
//			//					endl;
//			//			cout << oss.str();
//			temp_Result.clear();
//			temp_Result.addSample(sz, fn*sz, sumsq);
//			temp_worker_Results [j] = temp_Result;
//		}
//	}
//	//	cout << "222" << endl;
//	// for each of the simulated system in the current stage, add previous stage samples to the answer
//	assert ((int)worker_phase0_Results_keep.size()==worker_stages_simulated);
//	if (worker_stages_simulated > 0 ) {
//		prev_worker_Results = worker_phase0_Results_keep[worker_stages_simulated-1];
//		for (int i = 0; i < temp_worker_Results.size(); ++i) {
//			if (!this->system_eliminated[worker_system_idx[i]])
//				temp_worker_Results[i].addSample(prev_worker_Results[i]);
//		}
//	}
//	//	cout << "333" << endl;
//	this->worker_phase0_Results_keep.push_back(temp_worker_Results);
//	//	this->worker_Results_send_phase1.push_back(temp_worker_Results);
//	worker_stages_simulated += 1;
//	//	cout << "Worker finishes simulating" << endl;
//}

//void NHH2013AlgoV2rithm::Worker_Send_Stage(int stg) {
//	//currently not in use
//
//	// send answers from stage stg to master
//	worker_stage_send = worker_Results_send_phase1[stg];
//	Worker_Send_CurrentStage();
//}

void NHH2013AlgoV2::Worker_Send_CurrentStage() {
	// send the most current stage to the master

	MPI::Status status; SOResult tmp_Result;
	map<int, SOResult>::iterator iter_ans;
	int sz; bool cont = true;
	double fn, sumsq, simtime;
	int num_sys = S_worker;
	MPI::COMM_WORLD.Send (&num_sys,   1, MPI::INT,    0, sys_tag);
	if (worker_flag == 1) {
		//phase 00: send time
		MPI::COMM_WORLD.Send(&worker_phase00_send[0]    	, S_worker, MPI::DOUBLE, 0, runtime_tag);
	} else if (worker_flag == 2) {
		//phase 0 : send fn and sumsq
		MPI::COMM_WORLD.Send(&worker_phase0_fn_send[0]    	, S_worker, MPI::DOUBLE, 0, fn_tag);
		MPI::COMM_WORLD.Send(&worker_phase0_sumsq_send[0]  	, S_worker, MPI::DOUBLE, 0, sumsq_tag);
	}
}


void NHH2013AlgoV2::Master_Phase1_Recv_Eliminated(int worker) {
	// return immediately if all systems on the worker has been eliminated
	//	if (this->Master_Phase1_isCoreEliminated(worker)) return;

	int numElim;
	MPI::COMM_WORLD.Recv( &numElim   	, 1, MPI::INT, worker, num_elim_tag);

	assert (this->Master_num_elim_each_core[worker-1] <= this->Master_phase1_num_sys_each_core[worker-1]);
	//receive the index of eliminated systems
	if (numElim > 0) {
		this->Master_num_elim_each_core[worker-1] += numElim;
		//		cout << "Master rrrr\n" ;
		MPI::COMM_WORLD.Recv( master_elim_sys_idx, numElim, MPI::INT, worker, elimidx_tag);
		MPI::COMM_WORLD.Recv( master_tmp_elim_sys_simcount, numElim, MPI::INT, worker, size_tag);
		if (outputLevel >= 4)
			cout << numElim << " sys eliminated on master by worker " << worker << endl;
		// mark system eliminated
		for (int i = 0; i < numElim; ++i) {
			if (!this->system_eliminated[master_elim_sys_idx[i]]) {
				this->num_systems_eliminated += 1;
				if (outputLevel >= 3 ) {
					ostringstream oss;
					oss << "Sys " << setw(7) << setfill('0') <<
							master_elim_sys_idx[i] << " eliminated on master by worker "
							<< worker << ", "   << S_Total-this->num_systems_eliminated <<" sys remaining" <<
							", t= " << this->getCurrentTime() << endl;
					cout << oss.str();
				}
				//				Master_simcount_stages[2] += (master_tmp_elim_sys_simcount[i] - n0);
				//				Master_simcount_systems[master_elim_sys_idx[i]] = master_tmp_elim_sys_simcount[i] - n0;
			}
			this->system_eliminated[master_elim_sys_idx[i]] = 1;
			// remove all the data collected for this system
			Master_Results[master_elim_sys_idx[i]].clear();
			Master_Temp_Results[master_elim_sys_idx[i]].clear();
			Core2SysLists[worker-1].remove(master_elim_sys_idx[i]);
		}

		//		cout <<  "Master ssss\n" ;
		if (Master_num_elim_each_core[worker-1] == Master_phase1_num_sys_each_core[worker-1]){
			core_eliminated[worker-1] = 1;
			Best_Results_idx[worker-1] = -1;
			Master_best_Results[worker-1].clear();
			assert(Core2SysLists[worker-1].size()==0);
			if (outputLevel > 0) {
				ostringstream oss;
				oss << "Core " << worker << " eliminated completely" <<
						", t= " << this->getCurrentTime() << endl;
				cout << oss.str();
			}
		}
		//		cout <<  "Master tttt\n" ;
	}
	return;
}

void NHH2013AlgoV2::Worker_Phase1_Send_Eliminated() {
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " sends elimination" << endl;
		cout << oss.str();
	}
	// send a list of system indexes eliminated since last communication
	// if there's no answer, simply send 0
	int i = 0, numElim = worker_eliminated_system_to_send.size();
	deque<int>::iterator iter, iter2;
	//	cout << "numElim = " << numElim << "S_worker = " <<
	//			S_worker << "num_systems_eliminated = " << num_systems_eliminated << endl;

	// if there's no answer and all systems eliminated on core, return immediately
	//	if (numElim == 0 && this->num_systems_eliminated == S_worker) return;

	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " eliminated " << numElim << " Sys\n";
		cout << oss.str();
	}

	MPI::COMM_WORLD.Ssend( &numElim   	, 1, MPI::INT, 0, num_elim_tag);

	// if there's no answer, return immediately
	if (numElim == 0) {
		return;
	}
	else {
		int temp_s [numElim];
		int temp_count [numElim];
		i = 0; iter2 = worker_eliminated_system_simcount.begin();
		for (iter=worker_eliminated_system_to_send.begin();
				iter != worker_eliminated_system_to_send.end(); iter2++, iter++, i++) {
			temp_s [i] = *iter;
			temp_count [i] = *iter2;
		}
		MPI::COMM_WORLD.Ssend( temp_s   	, numElim, MPI::INT, 0, elimidx_tag);
		MPI::COMM_WORLD.Ssend( temp_count  	, numElim, MPI::INT, 0, size_tag);
		worker_eliminated_system_to_send.clear();
	}
	if (outputLevel >= 3) {
		ostringstream oss;
		oss << "Worker " << myRank << " finished sending elimination, " << numElim << " sys eliminated" << endl;
		cout << oss.str();
	}
	return;
}

void NHH2013AlgoV2::Master_Phase1_Recv_Best(int worker) {
	int num_sys_elim, best_idx, stages_had, stage_screened, stages_torecv;
	SOResult temp_ans;
	MPI::COMM_WORLD.Recv( &num_sys_elim	, 1, MPI::INT, worker, sys_tag);
	assert(num_sys_elim == this->Master_num_elim_each_core[worker-1]);

	//	cout << "Master xxx" << endl;
	// if not all systems have been eliminated, receives the best system from the worker
	// with its complete history
	if (num_sys_elim < (int) this->Master_phase1_num_sys_each_core[worker-1]) {
		//		cout << "Master yyy" << endl;
		// recv the number of stages screened
		MPI::COMM_WORLD.Recv( &stage_screened, 1, MPI::INT, worker, stage_tag);
		// recv the index of the best system
		MPI::COMM_WORLD.Recv( &best_idx		, 1, MPI::INT, worker, idx_tag);

		if (best_idx == Best_Results_idx[worker-1]) {
			stages_had = Master_best_Results[worker-1].size();
		} else {
			stages_had = 0;
			Master_best_Results[worker-1].clear();
			Best_Results_idx[worker-1] = best_idx;
		}
		stages_torecv = stage_screened - stages_had;
		assert (stages_torecv > 0);
		MPI::COMM_WORLD.Ssend( &stages_torecv, 1, MPI::INT, worker, stage_tag);

		if (outputLevel >= 4) {
			ostringstream oss;
			oss << "Master recv best sys " << best_idx << " from worker " <<
					worker << ", stages " << (stages_had+1) << " to " << (stage_screened) << endl;
			cout << oss.str();
		}
		double fn[stages_torecv];
		double sumsq[stages_torecv];
		int sz[stages_torecv];
		//		cout << "Master www" << endl;
		//recv all of the history of the best system to the master
		MPI::COMM_WORLD.Recv( &fn   	, stages_torecv, MPI::DOUBLE, worker, fn_tag);
		MPI::COMM_WORLD.Recv( &sumsq	, stages_torecv, MPI::DOUBLE, worker, sumsq_tag);
		MPI::COMM_WORLD.Recv( &sz   	, stages_torecv, MPI::INT   , worker, samplesize_tag);
		//create a history of best answers
		for (int i = 0; i < stages_torecv; i++) {
			temp_ans.clear(); temp_ans.addSample(sz[i], ((fn[i]) * (sz[i])), sumsq[i]);
			Master_best_Results[worker-1].push_back(temp_ans);
		}
		//		cout << "Master vvv" << endl;
	} else {
		// all systems eliminated
		this->Best_Results_idx[worker-1] = -1;
		//		assert(this->Master_num_elim_each_core[worker-1] == (int) this->Core2SysLists[worker-1].size());
		assert(core_eliminated[worker-1]);
		//		this->Master_best_Results[worker-1].clear();
	}
	return;
}

void NHH2013AlgoV2::Worker_Phase1_Send_Best() {
	// worker sends the number of systems eliminated by itself, together with the best system
	// to the master (if not all systems have been eliminated on the worker)
	if (outputLevel >=3) {
		ostringstream oss;
		oss << "Worker " << myRank << " sends best sys" << endl;
		cout << oss.str();
	}

	// once again send num_systems_eliminated, to confirm with master
	MPI::COMM_WORLD.Ssend( &(this->num_systems_eliminated), 1, MPI::INT, 0, sys_tag);

	// only send best system if not all systems have been eliminated
	if (this->num_systems_eliminated < S_worker) {
		int i, best_i = -1;
		int best_idx = -1;
		// find the best system

		double best = -9.9e99 * this->minmax;
		for (i = 0; i < S_worker; i++) {
			//			if (iter->second.getSampleMean() > best)  {
			if ((!this->system_eliminated[worker_system_idx[i]])
					&&
					( worker_phase1_results_keep[worker_system_idx[i]][worker_stages_screened-1].getSampleMean() -
							best) * this->minmax > 0 ) {
				best = worker_phase1_results_keep[worker_system_idx[i]][worker_stages_screened-1].getSampleMean();
				best_idx = worker_system_idx[i];
				best_i = i;
			}
		}
		if (outputLevel >= 4) {
			ostringstream oss;
			oss << "worker " << myRank << " sends best sys " << best_idx <<
					" to master, " << worker_stages_screened << "stages screened\n";
		}

		//send the current stage
		MPI::COMM_WORLD.Ssend( &worker_stages_screened	, 1, MPI::INT, 0, stage_tag);
		//send the index of the best system
		MPI::COMM_WORLD.Ssend( &best_idx				, 1, MPI::INT, 0, idx_tag);

		MPI::COMM_WORLD.Recv(&dummy		, 1, MPI::INT, 0, stage_tag);

		double fn[dummy];
		double sumsq[dummy];
		int sz[dummy];
		SOResult * bestAnswer;
		int kk = 0;
		for (int k = worker_stages_screened-dummy; k < worker_stages_screened; k++) {
			bestAnswer = &(worker_phase1_results_keep[best_idx][k]);
			fn[kk] 	= bestAnswer->getSampleMean();
			sumsq[kk]= bestAnswer->getSampleSumsq();
			sz[kk]	= bestAnswer->sample_size;
			kk++;
		}
		//send all of the history of the best system to the master
		MPI::COMM_WORLD.Ssend( &fn   	, dummy, MPI::DOUBLE, 0, fn_tag);
		MPI::COMM_WORLD.Ssend( &sumsq	, dummy, MPI::DOUBLE, 0, sumsq_tag);
		MPI::COMM_WORLD.Ssend( &sz   	, dummy, MPI::INT   , 0, samplesize_tag);
	}
	return;
}

void NHH2013AlgoV2::Master_Phase1_Send_OtherBest(int worker) {
	// sends the list of other systems, together with their histories, to the worker
	// system(core)/stage/answer

	//	// count the number of best systems to send
	int num_best = 0; int num_stages, idx, nextSize, num_stages_tosend; double nextFn, nextSumSq;
	MPI::COMM_WORLD.Ssend(&core_eliminated[0], nP-1, MPI::INT, worker, sys_tag);
	MPI::COMM_WORLD.Ssend(&Best_Results_idx[0], nP-1, MPI::INT, worker, sampleidx_tag);
	deque<SOResult>::iterator iter_ans;
	for (int w = 1; w < nP; w++) {
		if(core_eliminated[w-1] || w == worker || Best_Results_idx[w-1] == -1) {
			// if core i eliminated or is worker itself, do not count
		} else {
			if (outputLevel >= 4)			cout <<  "M 111\n";
			idx = this->Best_Results_idx[w-1];
			//			MPI::COMM_WORLD.Send(&idx,     	1, MPI::INT,	worker, sampleidx_tag);
			num_stages = this->Master_best_Results[w-1].size();
			MPI::COMM_WORLD.Recv(&dummy,	 	1, MPI::INT,	worker, stage_tag);
			num_stages_tosend = num_stages - dummy ;
			if (outputLevel >= 4)	cout <<  "M 222\n";
			MPI::COMM_WORLD.Send(&num_stages_tosend, 1, MPI::INT,	worker, stage_tag);

			if (outputLevel >= 4) {
				ostringstream oss;
				oss << "Master send best sys " << idx << " of worker " << w << " to worker " <<
						worker << ", stages " << (dummy+1) << " to " << (num_stages) << endl;
				cout << oss.str();
			}
			if (outputLevel >= 4)
				cout <<  "M 333\n";
			for (iter_ans = Master_best_Results[w-1].begin()+(dummy);
					iter_ans != Master_best_Results[w-1].end(); ++iter_ans) {

				//			for (int j = 0; j < num_stages; ++j) {
				//				temp_ans = Master_best_Results[i][j];

				nextFn = (*iter_ans).getSampleMean();
				nextSumSq = (*iter_ans).getSampleSumsq();
				nextSize = (*iter_ans).sample_size;
				MPI::COMM_WORLD.Send(&nextFn,		1, MPI::DOUBLE,	worker, fn_tag);
				MPI::COMM_WORLD.Send(&nextSumSq, 	1, MPI::DOUBLE,	worker, sumsq_tag);
				MPI::COMM_WORLD.Send(&nextSize,    	1, MPI::INT, 	worker, samplesize_tag);
			}
			if (outputLevel >= 4)
				cout <<  "M 444\n";
		}
	}

}

void NHH2013AlgoV2::Worker_Phase1_Recv_OtherBest() {
	//	cout << "Worker " << myRank << " recv other best" << endl;
	//worker receives the list of best systems from other workers from the master

	assert(this->myRank>0);
	map<int, deque<SOResult> >::iterator it;
	SOResult temp_Result; temp_Result.clear();
	double nextFn, nextSumSq;
	// moved to outside of the loop
	deque<SOResult> emptydeque; emptydeque.clear();
	int nextIdx, nextSize, stagesCollected, stagesToCollect;
	//	MPI::COMM_WORLD.Recv(&num_best_sys_to_recv, 1, MPI::INT, 0, sys_tag);
	MPI::COMM_WORLD.Recv(&(core_eliminated [0]), nP-1, MPI::INT, 0, sys_tag);
	MPI::COMM_WORLD.Recv(&(Best_Results_idx[0]), nP-1, MPI::INT, 0, sampleidx_tag);

	if (outputLevel >= 4)	{
		ostringstream os0;
		os0 << myRank <<  " W 111\n";
		cout << os0.str();
	}
	indexes_recvd.clear();
	for (int w = 1; w < nP; w++) {
		if(core_eliminated[w-1] || w == myRank  || Best_Results_idx[w-1] == -1) {
			// if core i eliminated or is worker itself, do not count
		} else {
			if (outputLevel >= 4)cout <<  "W 222\n";
			//			MPI::COMM_WORLD.Recv(&nextIdx,     	1, MPI::INT,	0, sampleidx_tag);
			nextIdx = Best_Results_idx[w-1];
			indexes_recvd.insert(nextIdx);
			it = map_worker_results_received.find(nextIdx);
			if (outputLevel >= 4)cout <<  "W 333\n";
			if (it != map_worker_results_received.end() ) {
				stagesCollected = it->second.size();
				if (outputLevel >= 4)cout <<  "W 444a\n";
			} else {
				stagesCollected = 0;
				map_worker_results_received[nextIdx] = emptydeque;
				if (outputLevel >= 4)cout <<  "W 555a\n";
			}
			if (outputLevel >= 4)cout <<  "W 444\n";
			MPI::COMM_WORLD.Ssend(&stagesCollected, 1, MPI::INT,	0, stage_tag);
			MPI::COMM_WORLD.Recv(&stagesToCollect, 1, MPI::INT,	0, stage_tag);
			if (outputLevel >= 4)cout <<  "W 555\n";
			for (int k = 0; k < stagesToCollect; ++k) {
				MPI::COMM_WORLD.Recv(&nextFn,		1, MPI::DOUBLE,	0, fn_tag);
				MPI::COMM_WORLD.Recv(&nextSumSq, 	1, MPI::DOUBLE,	0, sumsq_tag);
				MPI::COMM_WORLD.Recv(&nextSize,    	1, MPI::INT, 	0, samplesize_tag);
				temp_Result.clear();
				temp_Result.addSample(nextSize, nextFn*nextSize, nextSumSq);
				map_worker_results_received[nextIdx].push_back(temp_Result);
			}
			if (outputLevel >= 4)cout <<  "W 666\n";
		}
	}
	if (outputLevel >= 4)cout <<  "W 777\n";
	if (map_worker_results_received.size()>0) {
		for (it = map_worker_results_received.begin(); it != map_worker_results_received.end(); it++) {
			if (indexes_recvd.find(it->first) == indexes_recvd.end()) {
				it->second.clear();
			}
		}
	}
	if (outputLevel >= 4)cout <<  "W 888\n";
	indexes_recvd.clear();
	if (outputLevel >= 4)cout <<  "W 999\n";
	return;
}

//bool NHH2013AlgoV2::Master_Phase1_isCoreEliminated(int worker) {
//	return this->core_eliminated[worker-1];
//}

string NHH2013AlgoV2::toString_Param() {
	ostringstream ss;
	ss << "Alpha=" << alpha << " Delta="<<delta << " lambda=" << lambda <<" a=" << a
			<< " n00=" << n0 << " n0=" << n1 << " numSystems="<< problem->getNumSystems() <<endl;
	return ss.str();
}

string NHH2013AlgoV2::toString_StageSizes() {
	string str;
	str += "System, StageSize\n";
	for (int i = 0; i < (int) ( this->Stage_Sizes.size() ); i++) {
		ostringstream ss ;
		ss << i << "," << this->Stage_Sizes[i] << endl;
		str += ss.str();
	}
	return str;
}
string NHH2013AlgoV2::toString_Core2Sys() {
	list<int>::iterator iter;
	string str;
	assert((int) (this->Core2SysLists.size() ) == nP-1);
	str += "Core, Systems\n";
	for (int i = 0 ; i < (int) ( this->Core2SysLists.size() ); ++i) {
		ostringstream ss ;
		ss <<  setw(3) << setfill('0') <<  i+1 << ",";
		str += ss.str();
		for (iter = Core2SysLists[i].begin(); iter!=Core2SysLists[i].end(); iter++) {
			ostringstream ss1 ;
			ss1 <<  setw(7) << setfill('0') << *iter << ",";
			str += ss1.str();
		}
		str += "\n";
	}
	return str;
}

double NHH2013AlgoV2::getCurrentTime() {
	return MPI::Wtime()-t0 ;
}

void NHH2013AlgoV2::Master_PrintPhase0Summary() {
	ostringstream oss;
	oss << "----------------------" << endl;
	oss << "System,Size,mean,stdev,SimTime,S2" << endl;
	for (int j = 0; j<S_Total; j++) {
		oss << setw(7) << setfill('0') << j << "," <<
				Master_Results[j][0].toString() << "," << S2[j] << endl;
	}
	oss << "----------------------" << endl;
	cout << oss.str();
}

void NHH2013AlgoV2::report_winner() {
	ostringstream oss;
	assert(this->num_systems_eliminated + 1 == this->S_Total);
	int winner;
	for (int j = 0; j < S_Total; j++ ) {
		if (this->system_eliminated[j]==0) {
			winner = j; break;
		}
	}
	//success
	if (outputLevel >= 0) {
		oss << "algo,n0,n1,bsize,bmax,delta,seed,screenVer,core,time,RB,cov," <<
				"systems,stage2sys,simcount_0,simcount_1,simcount_2,simcount_3,totalsim,winner\n";
		oss << "NHH," << n0_GLOBAL << "," << n1 << "," << bsize << ",0," <<
				delta_GLOBAL << "," << seed_GLOBAL << "," <<
				screenVersion << "," << nP << "," <<
				getCurrentTime() << "," <<
				RB_global << "," << burnin_param << "," << S_Total << "," << num_systems_remain_after_phase0 << "," <<
				Master_simcount_phases[0]  << "," <<
				Master_simcount_phases[1]  << "," <<
				Master_simcount_phases[2]  << ",0," <<
				Master_simcount_phases[0]+Master_simcount_phases[1]+Master_simcount_phases[2] <<
				"," <<	winner << ",," <<
				//				"," << num_systems_remain_after_phase0 <<
				endl;
	}

	oss << "------------------------------------------------------" <<endl;
	oss << "Success! " ;
	oss << "Winner is system " << winner <<", t = " << getCurrentTime() << endl;
	oss << "------------------------------------------------------" <<endl;

	cout << oss.str();
}
