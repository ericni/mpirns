# Parallel Ranking and Selection Using MPI
----------

## Introduction

This package contains MPI implementations of various parallel ranking and selection procedures. 

* Procedure GSP (Ni, Ciocan, Henderson and Hunter, 2015)
* Procedure NHH (Ni, Hunter and Henderson, 2013)  
* Procedure NSGSp (Ni, Henderson and Hunter, 2014)

A unified C++ interface for test problems is provided together with C++ implementations of a few test problems originated from [SimOpt](http://simopt.org/).  

* Throughput Maximization [\[Wiki\]](http://simopt.org/wiki/index.php?title=Throughput_Maximization)
* Baby Call Center [\[Wiki\]](http://simopt.org/wiki/index.php?title=Baby_Call_Center)

## Documentation

Complete documentation of the package is now available [here](http://people.orie.cornell.edu/cn254/MPIRnS_docs/).

## Papers

Eric C. Ni, Dragos F. Ciocan, Shane G. Henderson and Susan R. Hunter. (2015)  
**Efficient Ranking and Selection in Parallel Computing Environments** [\[link\]](http://arxiv.org/abs/1506.04986)  
*Working Paper* 

Eric C. Ni, Shane G. Henderson and Susan R. Hunter. (2014)  
**A Comparison of two parallel ranking and selection procedures** [\[pdf\]](http://people.orie.cornell.edu/cn254/documents/wsc2014-final.pdf) [\[slides\]](http://people.orie.cornell.edu/cn254/documents/wsc2014-slides.pdf)  
*Proceedings of the 2014 Winter Simulation Conference* 

Eric C. Ni, Susan R. Hunter and Shane G. Henderson. (2013)  
**Ranking and selection in a high performance computing environment** [\[pdf\]](http://people.orie.cornell.edu/cn254/documents/inv128s4-file1.pdf) [\[slides\]](http://people.orie.cornell.edu/cn254/documents/NHH_WSC_13.pdf)  
*Proceedings of the 2013 Winter Simulation Conference* 
  
## Resources

* [Simulation Optimization Library](http://simopt.org/)
* [The Extreme Science and Engineering Discovery Environment (XSEDE)](https://www.xsede.org/)  
* [Texas Advanced Computing Center(TACC)](https://www.tacc.utexas.edu/)  
* [The Message Passing Interface (MPI) standard](http://www.mcs.anl.gov/research/projects/mpi/)