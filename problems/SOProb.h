/**
 * @file SOProb.h
 *
 * @brief Defines SOProb interfaces
 *
 */


//#define TEST_CONFIGURES 1

#ifndef SOPROB_H
#define SOPROB_H

#include "mpi.h"
#include "SORngStream.h"
#include "Matrix.h"
#include <vector> 
#include <deque>
#include <sstream>
#include <iomanip>
#include <locale>
#include <map>
#include <algorithm>
using namespace std;

/**
 * @brief SO problem properties container
 *
 * A class to store general properties of SO problems.
 *
 * Based on discussion in
 *
 * 		Pasupathy, R. and S. G. Henderson. 2011.
 * 		"SimOpt: A testbed of simulation-optimization problems".
 * 		In Proceedings of the 2011 Winter Simulation Conference.
 *
  */
class SOProperties {
public:
	int minmax; ///< +1 for maximization problems, and -1 for minimization problems.

	int d_disc;	///< The dimension of integer  (discrete)   decision variables.
	int d_cont;	///< The dimension of floating (continuous) decision variables.
	//NOTE: total dimension (# of decision variables) is thus d_disc + d_cont
	int m;  ///< Number of constraints

	//As opposed to the original Matlab framework, VarNature is unnessasary if
	//discrete and continuous decision variables are stored in different
	//vectors.
	//vector<int> VarNature; // a d dimensional column vector where the jth
	//component gives the nature of the jth decision variable: continuous (0),
	//integer-ordered (1) or categorical (2)

	vector<int>    VarUB_disc; ///<  Upper/lower bounds of discrete/continuous decision variables
	vector<int>    VarLB_disc; ///<  Upper/lower bounds of discrete/continuous decision variables
	vector<double> VarUB_cont; ///<  Upper/lower bounds of discrete/continuous decision variables
	vector<double> VarLB_cont; ///<  Upper/lower bounds of discrete/continuous decision variables


	/// Equals 1 if function gradient estimates are available, and 0 otherwise
	bool FnGradAvail;	

	/**
	 * @brief Number of constraints with gradient available
	 *
	 *  Number of constraints for which gradient estimates of the left-hand side
	of the constraints are available. Any such differentiable constraints
	are assumed to come first in the collection of m constraints in the
	simulation implementation
	 */
	int	NumConstraintGradAvail; 

	/**
	 * @brief Bound on obtimal solution value
	 *
	 * A bound (upper bound for maximization problems, lower bound for
	 minimization problems) on the optimal solution value, or NaN if no such
	 bound is known.
	 */
	double ObjBd; 	

	/**
	 * @brief Optimal solution
	 *
	 * A d-dimensional column vector giving an optimal solution if known, and is
	empty if no optimal solution is known.
	 *
	 */
	vector<double> OptimalSol;  

	/**
	 * @brief Simulation budget
	 *
	 *
	A column vector containing a collection of suggested budgets. (Recall that
	a budget is the amount of computer time, measured as specified in the
	problem description, allowed for the optimization problem to complete its
	search.)
	 */
	vector<int> budget;

	/// Number of random number substreams needed
	int NumSubstreams;

	/// Total number of systems considered
	int NumSystems;

	/// Clears properties
	void clear() {
		this->budget.clear();
		this->OptimalSol.clear();
		this->VarLB_cont.clear();
		this->VarUB_cont.clear();
		this->VarLB_disc.clear();
		this->VarUB_disc.clear();
	}
};

/**
 * 		@brief SO problem result container
 *
 * 		A class to store and manage SO problem results
 *
  */
class SOResult {
public:

	/// Sample size
	int sample_size;
	/// Sample sum of objective value
	double sample_sum;
	/// Sample sum of squared objective value
	double sample_sumsq;
	/// Sample simulation completion time
	double sample_time;

	/// Empty constructor
	SOResult(): sample_size(0), sample_sum(0.0), sample_sumsq(0.0), sample_time(0.0) {}

	/**
	 * @brief Constructor with values
	 *
	 * @param sz Sample size
	 * @param sum Sum of objective value
	 * @param sumsq Sum of squared
	 * @param tm Completion time
	 */
	SOResult(int sz,double sum,double sumsq, double tm):sample_size(sz),sample_sum(sum),
			sample_sumsq(sumsq), sample_time(tm){}

	/// Assignment constructor with another object
	SOResult(const SOResult & rhs) {
		this->sample_size = rhs.sample_size; this->sample_sum = rhs.sample_sum;
		this->sample_sumsq= rhs.sample_sumsq;this->sample_time= rhs.sample_time;
	}

	/// Assignment operator
	SOResult & operator=(const SOResult &rhs) {
		this->sample_size = rhs.sample_size; this->sample_sum = rhs.sample_sum;
		this->sample_sumsq= rhs.sample_sumsq;this->sample_time= rhs.sample_time;
		return *this;
	}

	/// Clears the object
	void clear() {
		sample_size = 0; sample_sum = 0.0; sample_sumsq = 0.0; sample_time = 0.0;
	}

	/**
	 * @brief Update the current answer with additional data
	 */
	///@{
	void addSample(SOResult soa) {
		addSample(soa.sample_size, soa.sample_sum, soa.sample_sumsq, soa.sample_time);
	}

	void addSample(int size, double sum, double sumsq) {
		if (size == 0) return;
		if (sample_size == 0) {
			sample_size = size;
			sample_sum = sum;
			sample_sumsq= sumsq;
			return;
		}
		sample_size += size;
		sample_sum += sum;
		sample_sumsq += sumsq;
	}

	void addSample(int size, double sum, double sumsq, double tm) {
		if (size == 0) return;
		if (sample_size == 0) {
			sample_size = size;
			sample_sum = sum;
			sample_sumsq= sumsq;
			sample_time = tm;
			return;
		}
		sample_size += size;
		sample_sum += sum;
		sample_sumsq += sumsq;
		sample_time += tm;
	}
	///@}

	/**
	 * @brief Converts answer to a c++ String
	 *
	 * @return
	 */
	string toString() {
		ostringstream ss;
		ss << sample_size << "," << getSampleMean() << "," << getSampleStdev();
		return ss.str();
	}

	/// Compute sample mean
	double getSampleMean() {
		if (sample_size == 0 ) return 0;
		return sample_sum/sample_size;
	}

	/// Compute sample standard deviation
	double getSampleStdev() {
		if (sample_size == 0 ) return 0.0;
		double s2 = (sample_sumsq / (sample_size - 1.)) -
				(sample_sum * sample_sum / (sample_size - 1.) / (double)sample_size) ;
		return sqrt(s2);
	}

	/// Get simulation time
	double getSampleTime() {
		if (sample_size == 0 ) return 0.0;
		return sample_time;
	}

	/// Get sample sum of squared value
	double getSampleSumsq() {
		if (sample_size == 0 ) return 0.0;
		return sample_sumsq;
	}
};

/**
 * @addtogroup interface
 *
 */
/*@{*/
/**
 * @brief SO problem interface.
 *
 * An abstract class used to describe simulation optimization problems.
 *
 * Based on discussion in
 *
 * 		Pasupathy, R. and S. G. Henderson. 2011.
 * 		"SimOpt: A testbed of simulation-optimization problems".
 * 		In Proceedings of the 2011 Winter Simulation Conference.
 *
 * @note To add a new (parallel) simulation optimization test problem, write a class
 * that inherits SOProb (see, for example, TpMax).
 * The class should implement the SOProb::Objective function and use a constructor
 * to initialize the SOProb::properties object as necessary.
 *
 * @note To solve the test problem with an existing SOAlgorithm, refer to the algorithm's
 * documentation and implement additional SOProb functions when necessary. For
 * instance, to use GoodSelAlgoV1 which uses indices internally to manage systems,
 * it is essential to implement either SOProb::Idx2System_Disc or
 * SOProb::Idx2System_Cont so that the index can be converted to the right decision
 * variables used in simulation.
 *
  */
class SOProb {
public:
	virtual ~SOProb() { }

	/// Clears the problem
	/**
	 * Return to the state after initialization. After clear is called,
	 * the problem should be solvable by another SOAlgorithm.
	 *
	 */
	void clear() {
		this->x_cont.clear();
		this->x_disc.clear();
	}

	/// A SOProperties object that stores the problem properties.
	SOProperties properties;

	///@{
	/**
	 * @brief (Optional) Get initial solutions.
	 *
	 * Create a number of initial solutions returned by Sol_disc and Sol_cont,
	 * given the random seed.
	 */
	virtual void getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
			const int &NumInitSol) = 0;
	///@}

	//virtual void getNextSystem(vector<int> & v1, vector<double> &v2) {}

	///@{
	/**
	 * @brief Converts a system index to discrete decision variables.
	 */
	virtual vector<int> Idx2System_Disc(const int& idx) {
		vector<int> v;
		return v;
	}
	///Converts a system index to continuous decision variables.
	virtual vector<double> Idx2System_Cont(const int& idx) {
		vector<double> v;
		return v;
	}
	/// (Optional) Converts a system (as characterized by its decision variables) to its index.
	virtual int System2Idx(vector<int> sys_disc, vector<double> sys_cont) = 0;
	///@}

	//@{
	/**
	 * @brief Directly sets decision variables for current replication.
	 *
	 * @param otherX (An array of) decision variable(s) to be set.
	 */
	void setContX(double *otherX) {
		x_cont.clear();
		x_cont.resize(this->properties.d_cont);
		for (int i=0; i<this->properties.d_cont; i++) x_cont[i] = otherX[i];
	}

	void setDiscX(int *otherX) {
		x_disc.clear();
		x_disc.resize(this->properties.d_disc);
		for (int i=0; i<this->properties.d_disc; i++) x_disc[i] = otherX[i];
	}
	//@}
	//
	//vector<int> getDiscX() {
	//	return x_disc;
	//}

	//@{
	/// (Optional) Converts the problem to a c++ string.
	string result2str() {
		ostringstream ss;
		ss << "Discrete decision variables:";
		for (int i=0; i<properties.d_disc; i++)
			ss << " " << x_disc[i];

		ss << endl << "Continuous decision variables:";
		for (int i=0; i<properties.d_cont; i++)
			ss << " " << fixed << setprecision (3) << x_cont[i];

		ss << endl << "Fn = " << fixed << setprecision (3) << this->fn << endl;
		ss << "FnVar = " << fixed << setprecision (3) << this->FnVar << endl;
		ss << "Constraints:";
		for (int i=0; i<properties.m; i++)
			ss << " " << constraint[i];
		ss << endl << endl;
		return ss.str();
	}
	//@}

	///@{
	/// @see fn
	double getFn() const {
		return fn;
	}

	/// @see FnVar
	double getFnVar() const {
		return FnVar;
	}

	/// @see FnSumSq
	double getFnSumSq() const {
		return FnSumSq;
	}

	/// @see numReplications
	int getNumReplications() const {
		return _numReps;
	}

	/// @see SOProperties::NumSystems
	int getNumSystems() const {
		return properties.NumSystems;
	}

	/// @see Simulation_Time
	double getSimulationTime() const {
		return Simulation_Time;
	}
	///@}

	///@{
	/**
	 * @brief Initialize single instance.
	 *
	 * Initialize the problem instance using given decision variables, number of replications
	 * and seed.
	 *
	 * @param inDisc Pointer to discrete decision variables to be initialized.
	 * @param inCont Pointer to continuous decision variables to be initialized.
	 * @param nRep Number of replications.
	 * @param ptr_Stream Pointer to the SORngStream to use.
	 */
	virtual void Initialize_Single_System(int * inDisc, double * inCont, int nRep,
			SORngStream * ptr_Stream) {
		this->_pStream = ptr_Stream;
		this->setDiscX(inDisc);
		this->setContX(inCont);
		this->_numReps = nRep;
	}

	/**
	 * @brief Initialize single instance by index.
	 *
	 * Initialize the problem instance using given system index, number of replications
	 * and seed.
	 *
	 * @param idx Index of the system to be initialized.
	 * @param nRep Number of replications.
	 * @param ptr_Stream Pointer to the SORngStream to use.
	 */
	virtual void Initialize_Single_System(int idx, int nRep,
			SORngStream * ptr_Stream) {
		this->x_disc = Idx2System_Disc(idx);
		this->x_cont = Idx2System_Cont(idx);
		this->_numReps = nRep;
		this->_pStream = ptr_Stream;
		this->_index = idx;
	}
	///@}

	/// Evaluate the objective function
	virtual void Objective() {}

protected:

	/**
	 * @brief (Optional) Simulation status.
	 *
	 * status is an integer that tracks the status of the simulation run.
	 */
	int status;

	/// (Optional) index of the system currently being simulated.
	int _index;

	//@{
	/**
	 * @brief Decision variables.
	 *
	 * x_disc, x_cont are column vectors of dimension d_disc, d_cont, respectively, giving
	 * the solution at which to simulate.
	 */
	vector<int> x_disc;
	vector<double> x_cont;
	//@}

	/// The number of replications(batch size) in the current simulation run.
	int _numReps;

	/// Pointer to the SORngStream to be used for simulation.
	SORngStream * _pStream;

	/**
	 * @brief Simulation time estimate.
	 *
	 * Records the amount of time it takes to run the simulation.
	 */
	double Simulation_Time;

	/**
	 * @brief Objective function estimate.
	 *
	 * fn is the real-valued estimate of the function value at the point x. The value NaN
	 * is returned if the point x is "hard" infeasible in the sense that the x values
	 * cannot be used in the simulation, as opposed to "soft" infeasible when the problem
	 * constraints are not satisfied.
	 */
	double fn;

	/**
	 * @brief Variance estimate.
	 *
	 * FnVar is a real-valued estimate of the variance of the estimator fn. In the common
	 * case where n i.i.d. replications are performed, this quantity equals s^2/n, where
	 * s^2 is the sample variance of the n observations. If a variance estimate is not
	 * computed, this is returned as NaN.
	 */
	double FnVar;

	/// Sum of squared value from all replications.
	double FnSumSq;

	/**
	 * @brief Gradient estimate.
	 *
	 * 	FnGrad is a d-dimensional vector giving an estimate of the gradient of the
	 * 	function at x, returned as a d-vector or empty if gradients
	 * 	are unavailable or inappropriate.
	 */
	vector<double> FnGrad;

	/**
	 * @brief Constraint estimate.
	 *
	 * 	constraint is an m-dimensional vector giving the estimated left-hand side (LHS)
	 * 	of inequality constraints that are assumed to be of the form LHS>=0. If m = 0
	 * 	so there are no constraints, then an empty vector is returned.
	 */
	vector<double> constraint;

	/**
	 * @brief Covariance of gradient estimate.
	 *
	 * FnGradCov is a d*d estimated covariance matrix of the function gradient estimate,
	 * including any scaling to account for the number of replications (see FnVar above).
	 * An empty matrix is returned if the gradient covariance is not available or not
	 * appropriate.
	 */
	Matrix<double> FnGradCov;
	/**
	 * @brief Covariance of constraint estimate.
	 *
	 * ConstraintCov is the estimated covariance matrix of the constraint LHS estimates.
	 * This could have zero rows/columns corresponding to deterministic constraints. It
	 * is returned as empty if there are no constraints, or if constraint covariance
	 * estimates are not computed.
	 */
	Matrix<double> ConstraintCov;
	/**
	 *
	 * @brief Gradients of constraints estimate.
	 *
	 * ConstraintGrad is a d*k matrix giving the estimated gradients of the
	 * k = NumConstraintGradAvail constraint LHS values for which gradients are
	 * available, one in each column. If there are no constraints then the an empty
	 * matrix is returned.
	 */
	Matrix<double> ConstraintVar;


};
/*@}*/
#endif
