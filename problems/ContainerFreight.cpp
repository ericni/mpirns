/**
 * @file ContainerFreight.cpp
 *
 * @brief Implements ContainerFreight (Throughput Maximization) test problem class
 *
 */
#include "ContainerFreight.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <list>
using namespace std;

/**
 * @brief Default Constructor.
 *
 * Reads parameter RB from global variable RB_global.
 * Also initializes parameters.
 */
ContainerFreight::ContainerFreight() {
	param = new ContainerFreightParams();
	//	param->B    = 50;
	//	param->R 	= 50;
	//	param->B    = 20;
	//	param->R 	= 20;
	//	prop.NumSystems = 57624;
	//	prop.NumSystems = 3249;
	//	param->B	= 128;
	//	param->R	= 128;
	//	prop.NumSystems = 1016127;
	int dim = 4;
	param->M	= RB_global;
	properties.NumSystems  = Combination::Choose(param->M - 101 + 3, 3);
	double ls[] = {52.8, 11.7, 13.0, 22.5};
	double us[] = {67.0, 46.0, 92.0, 34.0};
	param->lambdas.resize(dim);
	param->mus.resize(dim);
	param->LB.resize(dim);
	for (int i = 0; i < dim; ++i) {
		param->lambdas[i] = ls[i]/60.;
		param->mus[i] = 1./us[i];
		param->LB[i] = (int) ceil( param->lambdas[i] / param->mus[i] );
	}

	//Number of jobs to be released in simulation
	param->njobs = 600 * 60; ///length of total simulation in minutes
	param->burnin= 100 * 60;	///length of burn-in period in minutes
	/*
	 * batch means
	 */
	//param->m    = 500;		//batch size
	properties.minmax = -1;
	properties.d_disc = dim;
	properties.d_cont = 0;
	properties.m = 0;
	properties.VarUB_disc.clear(); properties.VarUB_disc.resize(properties.d_disc, 150);
	properties.VarLB_disc.clear(); properties.VarLB_disc.resize(properties.d_disc, 0 );
	properties.VarUB_cont.clear();
	properties.VarLB_cont.clear();
	properties.FnGradAvail = 0;
	properties.NumConstraintGradAvail = 0;
	properties.ObjBd = 0.0;
	properties.budget.resize(3);
	properties.budget[0] = 20;
	properties.budget[1] = 1;
	properties.budget[2] = 10000;
	properties.OptimalSol.clear();
	//	NumRemainingSystems = prop.NumSystems;
	_index = -1;
}

ContainerFreight::~ContainerFreight() {
	this->clear();
	delete param;
}

/// Currently not in use.
void ContainerFreight::getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
		const int &NumInitSol){
	// NOT IMPLEMENTED

	Sol_cont.clear(); // this problem has no continuous decision variablee
	if (NumInitSol <= 0) {
		Sol_disc.clear();
		//do nothing (probably send a warning message)
	} else if (NumInitSol == 1) {
		Sol_disc.resize(1, 17);	Sol_disc.fill(8);
	} else {
		Sol_disc.resize(NumInitSol, 17);
		SORngStream UStream;
		for (int j=0; j <NumInitSol; j++) {
			for (int i=0; i < 17; i++ ) {
				//Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][15]= 30;
				Sol_disc[j][16]= 50;
			}
		}
	}
}

void ContainerFreight::Objective(){
	// initially set fn and FnVar to NaN, status to -1
	double t0 = MPI::Wtime();
	this->fn =  *( double* )nan;
	this->FnVar =  *( double* )nan;
	status = -1;

	int i,j;
	vector<int> burnins;
	vector<int> x = x_disc;
	if(burnin_param > 0) {
		double mu = log(param->burnin) - burnin_param / 2.;
		double sig = sqrt(burnin_param);
		burnins.resize(_numReps, 0);
		for (i = 0; i < _numReps; ++i)  {
			int v = (int) exp( _pStream->RandNormal(0., 1.) * sig + mu );
			burnins[i] = (v > 20000) ? 20000:v;
		}
	}
	double Tmax = param->njobs;
	int _njobs, _burnin;
	int nTypes = this->properties.d_disc;  // default = 4
	const int _diff = param->njobs - param->burnin;

	//debug
	bool db  = false;

	vector<double> avg_wait_time (_numReps,0.0);
	double total_wait_time = 0.0;
	this->FnSumSq = 0.0;
	Matrix<int> Poisson( _numReps, nTypes );

	for ( int k = 0; k < _numReps; ++k ) {
		_burnin = (burnin_param > 0) ? burnins[k] : param->burnin;
		Tmax = _njobs = _burnin + _diff;
		for ( j = 0; j < nTypes; ++j ) {
			Poisson[k][j]=_pStream->
					RandPoisson(param->lambdas[j] * Tmax);
		}
	}

	vector< vector<double> > Arrv;
	vector< vector<double> > Svcs;
	vector< vector<double> > Exit;
	Arrv.resize(nTypes);
	Svcs.resize(nTypes);
	Exit.resize(nTypes);

	for (int k=0; k<_numReps; ++k) {
		_burnin = (burnin_param > 0) ? burnins[k] : param->burnin;
		Tmax = _njobs = _burnin + _diff;
		for (j = 0; j < nTypes; ++j ) {
			int sz = Poisson[k][j];
			//			maxSize = maxSize < sz ? sz : maxSize;
			Arrv[j].resize(sz);
			Svcs[j].resize(sz);
			Exit[j].clear();
			Exit[j].resize(sz, 0.0);
			for ( i = 0; i < sz; ++i ) {
				Arrv[j][i] = _pStream->RandU(0.0, Tmax);
				Svcs[j][i] = _pStream->RandExponential(param->mus[j]);
			}
			sort(Arrv[j].begin(), Arrv[j].end());

			/*************
			 * Old Simulation: using sort, poor complexity
			 */
			//			for (i = 0; i < sz; ++i ) {
			//				double t = Svcs[j][i];
			//				if(i < x[j]) {
			//					Exit[j][i] = Arrv[j][i] + t;
			//				} else {
			//					vector<double> temp = Exit[j];
			//					sort(temp.begin(), temp.end());
			//					if(temp[sz - x[j]] > Arrv[j][i])
			//						Exit[j][i] = temp[sz - x[j]] + t;
			//					else
			//						Exit[j][i] = Arrv[j][i] + t;
			//				}
			//
			//				if (outputLevel > 2)  {
			//					ostringstream oss;
			//					oss << "i = " << i << endl;
			//					cout << oss.str();
			//				}
			//			}

			/**********
			 * New simulation: using priority queue to simulate a MMc queue
			 */
			priority_queue< MMCEvent, vector<MMCEvent>, MMCComparison > event_q;
			queue< MMCEvent, list<MMCEvent> > wait_q;
			int numJobsInService = 0;
			double currentTime = 0;
			MMCEvent currentEvent;
			for (i = 0; i < sz; ++i) {
				event_q.push( MMCEvent( i, Arrv[j][i], Arrv[j][i] ) );
			}
			while(!event_q.empty()) {
				currentEvent = event_q.top();
				currentTime = currentEvent.event_time;
				event_q.pop();
				if(currentEvent.type == arrival_event) {
					if (numJobsInService < x[j]) {
						++numJobsInService;
						assert(currentEvent.departure_time < 0);
						currentEvent.type = departure_event;
						Exit[j][currentEvent.id]
						        = currentEvent.event_time
						        = currentEvent.departure_time
						        = currentTime + Svcs[j][currentEvent.id];
						event_q.push(currentEvent);
					} else
						wait_q.push(currentEvent);
				} else if (currentEvent.type == departure_event) {
					if(!wait_q.empty()) {
						assert( numJobsInService == x[j] );
						currentEvent = wait_q.front();
						wait_q.pop();
						assert(currentEvent.departure_time < 0);
						currentEvent.type = departure_event;
						Exit[j][currentEvent.id]
						        = currentEvent.event_time
						        = currentEvent.departure_time
						        = currentTime +	Svcs[j][currentEvent.id];
						event_q.push(currentEvent);
					} else
						--numJobsInService;
				} else {
					//corrupted
					throw "wrong event type!";
				}
			}
		}

		double totalTime = 0.0;
		int totalJobs = 0;
		for ( j = 0; j < nTypes; ++j ) {
			int sz = Poisson[k][j];
			double timePerType = 0.0;
			int jobPerType = 0;
			for (i=0; i<sz; ++i) {
				if (Arrv[j][i] >= _burnin) {
					timePerType = timePerType + Exit[j][i] - Arrv[j][i];
					jobPerType += 1;
				}
			}
			if (outputLevel >= 4) {
				ostringstream oss;
				oss << "RepStat," << this->_index << "," << k <<
						"," << j <<
						"," << timePerType << "," << jobPerType << endl;
				cout << oss.str();
			}
			totalTime += timePerType;
			totalJobs += jobPerType;
		}
		avg_wait_time[k] = totalTime / totalJobs;
		total_wait_time += avg_wait_time[k];
		this->FnSumSq += avg_wait_time[k]*avg_wait_time[k];
	}
	if (db) cout << endl;
	///////////////////////
	// end simulation
	///////////////////////

	//final answer
	fn = total_wait_time / _numReps;
	FnVar = 0.0;
	for (int k=0; k<_numReps; k++)  FnVar += pow(avg_wait_time[k] - fn, 2);
	FnVar = FnVar / (_numReps-1);

	Simulation_Time = MPI::Wtime()-t0;

	//mark success simulation run
	status = 0;
}

vector<int> ContainerFreight::Idx2System_Disc(const int& idx) {
	//finds the system corresponding to the index
	int dim = properties.d_disc;
	int base = 0;
	int i;
	for ( i = 0; i < dim; ++i ) base += param->LB[i];
	long * ans = Combination(param->M - base + dim - 1, dim - 1).Element(idx).getData();
	for ( i = 0; i < dim - 1; ++i ) ans[i] += 1;

	vector <int> x;
	x.resize(dim);
	x[0] = ans[0] - 1 + param->LB[0];
	x[dim - 1] = param->M - x[0];
	for (i = 1; i < dim - 1; ++i ) {
		x[i] = ans[i] - ans[i-1] - 1 + param->LB[i];
		x[dim - 1] -= x[i];
	}
	return x;
}

int ContainerFreight::System2Idx(vector<int> sys_disc,
		vector<double> sys_cont) {
	// NOT IMPLEMENTED
	return -1;
}
