/**
 * @file TpMaxLEQ.h
 *
 * @brief Defines TpMaxLEQ (Throughput Maximization) test problem class
 *
 */
#ifndef TPMAXLEQ_H
#define TPMAXLEQ_H

#include "SOProb.h"
#include "TpMax.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <assert.h>

using namespace std;

/**
 * @addtogroup prob
 */
/*@{*/
/**
 * @brief Throughput Maximization test problem LEQ Alternative.
 *
 * This is an alternative implementation of TpMax, with the exception that
 * the resources (process rate and buffer size) are not required to be exhausted.
 * Such difference is mainly reflected in the re-implemented
 * TpMaxLEQ::Idx2System_Disc function.
 *
 * As a result, under the same R and B parameters this yields more inferior
 * alternatives, making the mean configuration more solvable for
 * iterative-screening-based methods.
 *
 * @see TpMax
 */
class TpMaxLEQ : public TpMax {
public:
	TpMaxLEQ ();
	//	~TpMaxLEQ ();
	vector<int> Idx2System_Disc(const int& idx);
	//	void Objective();

	///@{
	void getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
			const int &NumInitSol);
	///@}

private:
	///@{
	int System2Idx(vector<int> sys_disc, vector<double> sys_cont);
	void enumerateSystem();
	void nextComb(int offset, int k, int rb);
	void readSystem();
	///@}
};
/*@}*/

#endif
