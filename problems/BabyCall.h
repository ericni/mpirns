/**
 * @file BabyCall.h
 *
 * @brief Defines BabyCall (Baby Call center) test problem class
 *
 */
#include "SOProb.h"

#ifndef BABYCALL_H
#define BABYCALL_H

/**
 * @addtogroup prob
 */
/*@{*/
/**
 * @brief A Baby Call Center Simulator
 *
 * Problem description can be found at
 *
 * http://simopt.org/wiki/index.php?title=Baby_Call_Center
 *
 * Simulation code written originally by German Gutierrez in Matlab
 *
 * Converted to C++ by Eric Ni
 */
class BabyCall : public SOProb {
public:
	BabyCall ();
	~BabyCall ();
	//	void Initialize_Single_System(int * inDisc, double * inCont, int nRep,
	//			unsigned long * otherSeed);
	void getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
			const int &NumInitSol);
	void Objective();
private:
	struct BabyCallParams;
	BabyCallParams* param;
};
/*@}*/
#endif
