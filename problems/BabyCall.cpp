/**
 * @file BabyCall.cpp
 *
 * @brief Implements BabyCall (Baby Call center) test problem class
 *
 */

#include "BabyCall.h"
#include <cmath>
#include <iostream>
using namespace std;

/**
 * @brief Problem parameters.
 */
struct BabyCall::BabyCallParams {
	double lambdaMax;		///< Maximum arrival rate used in Accept-Reject.

	//@{
	/// Parameters for the service/patience time (Gamma) distributions.
	double serviceShape;
	double serviceScale;
	double patienceShape;
	double patienceScale;
	//@}

	int TrunkMax;			///< Maximum trunk size.
	double Salary;			///< Agents' salary in $/hr/agent.
	int nDays;				///< Number of days to simulate.
	double dayLength;		///< Length of a day's operation in hours.
	int nShifts;			///< Number of agent shifts considered.
	int nHours;				///< Number of hours in a day's operation.
	int MaxRandNum;			///< Number of random numbers to be generated with each stream.
	/// Call is considered delayed if not answered within this threshold. In hours.
	double DelayThreshold;
};

BabyCall::BabyCall() {
	param = new BabyCallParams();
	param->lambdaMax    = 1000.;
	param->serviceShape = 18.;
	param->serviceScale = 1./3./60.;
	param->patienceShape= 4.;
	param->patienceScale= 1./2./.60;
	param->TrunkMax     =150;
	param->Salary       =18.;
	param->dayLength	=16.5;
	param->nShifts 	 	=17;
	param->nHours	 	=17;
	param->DelayThreshold=20./60./60.;
	param->MaxRandNum	=100000;
	properties.NumSubstreams = 5;
	properties.minmax = -1;
	properties.d_disc = 17;
	properties.d_cont = 0;
	properties.m = 16;
	properties.VarUB_disc.clear(); properties.VarUB_disc.resize(properties.d_disc, 150);
	properties.VarLB_disc.clear(); properties.VarLB_disc.resize(properties.d_disc, 0 );
	properties.VarUB_cont.clear();
	properties.VarLB_cont.clear();
	properties.FnGradAvail = 0;
	properties.NumConstraintGradAvail = 0;
	properties.ObjBd = 0.0;
	properties.budget.resize(3);
	properties.budget[0] = 100;
	properties.budget[1] = 1000;
	properties.budget[2] = 10000;
	properties.OptimalSol.clear();
}

BabyCall::~BabyCall() {
	this->clear();
	delete param;
}
//
//void BabyCall::Initialize_Single_System(int * inDisc, double * inCont, int nRep,
//		SORngStream * ptr_Stream) {
//	this->pStream = ptr_Stream;
//	this->setDiscX(inDisc);
//	this->setContX(inCont);
//	this->numReplications = nRep;
//}

void BabyCall::getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
		const int &NumInitSol){
	Sol_cont.clear(); // this problem has no continuous decision variablee
	if (NumInitSol <= 0) {
		Sol_disc.clear();
		//do nothing (probably send a warning message)
	} else if (NumInitSol == 1) {
		Sol_disc.resize(1, properties.d_disc);	Sol_disc.fill(8);
	} else {
		Sol_disc.resize(NumInitSol, properties.d_disc);
		SORngStream UStream;
		for (int j=0; j <NumInitSol; j++) {
			for (int i=0; i < properties.d_disc; i++ ) {
				//Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][15]= 30;
				Sol_disc[j][16]= 50;
			}
		}
	}
}

//for debugging purposes
namespace {
void printVec (vector<double> v) {
	int i = 0;
	for (i=0; i <(int) v.size(); ++i) {
		cout << v[i] << ' ';
	}
	cout <<endl;
}
}

void BabyCall::Objective(){
	// initially set fn and FnVar to NaN, status to -1
	this->fn =  *( double* )nan; this->FnVar =  *( double* )nan;
	status = -1;

	param->nDays = _numReps;
	double PI = 4.0*atan(1.0);
	int nAgents = 0;
	int i, j, k; double a;
	for (i=0; i < param->nShifts;i++) nAgents += x_disc[i];
	Matrix<double> PerformanceMeasure (param->nHours, param->nDays);
	vector<double> TotalCost(param->nDays,0.0);
	vector<int> L(param->nDays,1); //lunchbreak counter
	vector<int> D(param->nDays,1); //dummytime counter
	vector<int> S(param->nDays,1); //service time counter
	vector<int> P(param->nDays,1); //patience time counter
	vector<int> A(param->nDays,1); //Accept-reject counter
	vector<double> b_tmp, b, b_tmp2;
	vector<double> callsPerHour(param->nHours,0.0),successfulPerHour ;
	int hour, callTaken, minAgent, nCalls, num, success;
	double totalHours, nextCall, dummy, funValue, minTime, serviceTime, patienceTime;
	double GrandCost = 0;
	Matrix<double> Agents (nAgents, 6);
	Matrix<double> Calls  (4, param->MaxRandNum);
	ConstraintVar.resize(properties.m,properties.m);
	constraint.resize(properties.m);
	fill(constraint.begin(), constraint.end(),0.0);

	// create random number streams
	//	SORngStream pStream;
	//LunchStream,DummyStream,ServiceStream,PatienceStream,ARStream;

	// generate random data
	// 1)lunch times, which is assumed fixed under the current version
	Matrix<double> LunchTimes (param->nDays, nAgents, 4.);

	// 2)dummy times for A/R
	_pStream->ResetNextSubstream();
	Matrix<double> Dummy (param->nDays, param->MaxRandNum);
	for (i=0;i<param->nDays;i++) {
		for (j=0;j<param->MaxRandNum;j++) {
			Dummy[i][j] = _pStream->RandExponential(param->lambdaMax);
		}
	}

	// 3)service times
	_pStream->ResetNextSubstream();
	Matrix<double> sTime (param->nDays,param->MaxRandNum);
	for (i=0;i<param->nDays;i++) {
		for (j=0;j<param->MaxRandNum;j++) {
			sTime[i][j]=_pStream->RandGamma(param->serviceShape,param->serviceScale);
		}
	}

	// 4)patience times
	_pStream->ResetNextSubstream();
	Matrix<double> pTime (param->nDays,param->MaxRandNum);
	for (i=0;i<param->nDays;i++) {
		for (j=0;j<param->MaxRandNum;j++) {
			//			v=0;
			//			for (k=0;k<param->patienceShape;k++){
			//				u = PatienceStream.RandU01();
			//				v += -(log(u));
			//			}
			pTime[i][j]= _pStream->RandGamma(param->patienceShape ,
					param->patienceScale);
			//			pTime[i][j] = v*param->patienceScale;
		}
	}

	// 5)A/R scheme
	_pStream->ResetNextSubstream();
	Matrix <double> AR	(param->nDays,param->MaxRandNum);
	for (i=0;i<param->nDays;i++) {
		for (j=0;j<param->MaxRandNum;j++) {
			AR[i][j] = _pStream->RandU01();
		}
	}
	////////////////////////////////////
	// done generating random numbers
	///////////////////////////////////

	///////////////////////
	// begin simulation
	///////////////////////
	for (k=0;k<param->nDays;++k) {
		// Matrix Agents will contain current state of the system for all
		// agents. First column has time agent i starts working, col.2 has time
		// at which he has scheduled break, col.3 contains time of next
		// availability and col. 4 the time at which he left the job. col.5
		// is binary, 1 if agent already took break, 0 o.w.
		Agents.fill(0.0);
		// Calls will contain the time of arrival (first row), time at which the
		// call was taken by an agent (row 2, 0 if the call is never taken) and
		// time at which it departed the system(either blocked, patience time
		// exhausted or serviced)
		Calls.fill(0.0);

		//create start times and break times for all agents based on x, the
		//decision variable on shift allocation
		num=0;
		for (i=0;i < param->nShifts;i++) {
			for (j=0;j<x_disc[i];j++) {
				Agents[num][1] = ((double) i)/2;
				Agents[num][2] = ((double) i)/2+LunchTimes[k][L[k]];
				L[k] += 1;
				Agents[num][3] = ((double) i)/2;
				num += 1;
			}
		}

		// create first arrival time using Accept/Reject for nonstationary
		// Poisson. (same algorithm used in loop later)
		nextCall = 0.0;
		dummy = 0.0;
		while (nextCall<=0.0) {
			dummy += Dummy[k][D[k]];
			D[k]+=1;
			funValue = 500*sin(((3*PI*dummy)-(16*PI))/32)+500;
			a = AR[k][A[k]];
			A[k]+=1;
			if (a <= funValue/1000) {
				nextCall=dummy;
				nCalls = 1;
				Calls[1][nCalls-1]=nextCall;
			}
		}

		// b is a vector of varying length that maintains departure times for
		// the calls
		b.clear(); b.push_back(0.0);
		// while calls continue to arrive
		while (nextCall < param->dayLength) {
			Calls[1][nCalls-1]=nextCall;
			minTime = 25;
			minAgent = -1;
			callTaken = 0;

			// check trunk line availability using departure time per call
			// sort(b.begin(), b.end());
			if ((int)b.size()>param->TrunkMax)  {
				b_tmp.clear();
				b_tmp.resize(param->TrunkMax);
				for(int kk = 0; kk<param->TrunkMax; ++kk)
					b_tmp[kk] = b[(int)b.size() - param->TrunkMax + kk];
				//				b_tmp.assign(b.end()-param->TrunkMax, b.end());
				b = b_tmp;
				if (b[0] > nextCall) {
					//call blocked, leaves the system immediately
					callTaken = 1;
					Calls[3][nCalls-1] = nextCall;
				}
			}
			/*
			len = b.size();
			b_tmp.clear(); b_tmp.resize(param->TrunkMax);
			fill(b_tmp.begin(),b_tmp.end(),0.0);
			for (i=len - param->TrunkMax+1 ; i <= len;i++) {
				if (1<i) {
					b_tmp[i-1] = i;
				}else {
					b_tmp[i-1] = 1;
				}
			}
			b_tmp2.clear();
			for (i=0; i<(int) b_tmp.size();i++)
				b_tmp2.push_back(b[b_tmp[i]-1]);

			b = b_tmp2; len = b.size();
			j=len-param->TrunkMax+1;
			if (1> j) j = 1;
			if (b[j-1]>= nextCall) {
				//call blocked, leaves the system immediately
				Calls[3][nCalls-1] = nextCall;
				callTaken = 1;
			}
			 */
			//printVec(b);

			// service and patience time for this call
			serviceTime = sTime[k][S[k]];
			patienceTime= pTime[k][P[k]];
			S[k]+=1; P[k]+=1;

			for (i=0; i < nAgents; i++) {
				//update whether agents took breaks or left in between the past
				//two calls. If so, update time of next availability or time
				//of agent departure.
				//NOTE: If time of break or finish time happened while agent
				//was in a call, this times have already been considered and
				//either a "time left" or a "break taken" notice has been
				//posted.
				if (Agents[i][3] <= nextCall && Agents[i][4] <= 0.0 && callTaken ==0) {
					//agent left for break
					if (Agents[i][2] <= nextCall && Agents[i][5] <= 0.0) {
						Agents[i][3] = Agents [i][2]+0.5;
						Agents[i][5] = 1.0;
					}
					//agent left his job already
					if (Agents[i][1] + 8.5 <= nextCall && Agents[i][1] + 8.5<16.5) {
						Agents[i][4] = Agents[i][1]+8.5;
					}
				}//end if

				//if agent available, take the call. update time of next availability
				if (Agents[i][3] <=nextCall && Agents[i][4] <= 0.0 && callTaken==0) {
					Calls[2][nCalls-1]=nextCall;
					Agents[i][3]=nextCall+serviceTime;
					callTaken=1;
					Calls[3][nCalls-1] = nextCall+serviceTime;

					//if scheduled break while servicing call, break will begin
					//as soon as     the call is completed
					if (Agents[i][3] >= Agents[i][2] && Agents[i][5] <= 0.0) {
						Agents[i][3] += .5;
						Agents[i][5]  = 1.0;
					}

					//If scheduled to leave while completing the call, agent
					//leaves once ca    ll is finished. However, if Agent is one
					//of the last ones he is not allowed to leave until     all
					//calls have been serviced.
					if (Agents[i][3] >= Agents[i][1]+8.5 && Agents[i][1]+8.5<16.5) {
						Agents[i][4] = Agents[i][3];
					}
					break;
				}//end if

				//if call has not been taken, keep track next available agent
				if (Agents[i][3] <= minTime && Agents[i][4]<=0.0) {
					minTime = Agents [i][3];
					minAgent = i;
				}//end if
			}// end for

			//no immediately available agents. if patience time has not been
			//exhausted, next available agent will take the call
			if (minTime < nextCall + patienceTime && callTaken == 0 && minAgent >=0)  {
				//Agent takes call
				Calls [2][nCalls-1] = Agents[minAgent][3];
				Agents[minAgent][3] += serviceTime;
				Calls [3][nCalls-1] = Agents[minAgent][3];
				//if scheduled to berak or leave while in service, agent does so.
				if (Agents[minAgent][3] >= Agents[minAgent][2]
				                                            && Agents[minAgent][5] <= 0.0){
					Agents[minAgent][3]+=0.5;
					Agents[minAgent][5] =1.0;
				}
				//allow agent to leave job
				if(Agents[minAgent][3]>=Agents[minAgent][1]+8.5
						&&Agents[minAgent][1]+8.5<16.5)
					Agents[minAgent][4]=Agents[minAgent][3];

			} else if (callTaken==0){
				//if patience time exhausted, call leaves the system as soon as
				//patience time elapses
				Calls[3][nCalls-1]=nextCall+patienceTime;
			}//end if

			//generate time of next arrival and update counters
			success=0;
			while (success==0) {
				dummy += Dummy[k][D[k]];
				D[k]+=1;
				funValue = 500*sin(((3*PI*dummy)-(16*PI))/32)+500;
				a = AR[k][A[k]];
				A[k]+=1;
				if (a<=funValue/1000) {
					success=1;
					nextCall=dummy;
				}
			}

			b.push_back(Calls[3][nCalls-1]);
			nCalls+=1;
		}// end while

		//update departure times for last agents
		for (i=0; i<nAgents; i++){
			if (Agents[i][4]<=0.0) {
				if (Agents[i][3]>16.5) Agents[i][4]=Agents[i][3];
				else Agents[i][4] = Agents[i][1]+8.5;
			}
		}


		fill(callsPerHour.begin(),callsPerHour.end(),0);
		successfulPerHour = callsPerHour;
		// loop through calls to calculate performance measure
		for (i=0;i<nCalls;i++){
			hour = (int) (floor(Calls[1][i]));
			if (hour<param->nHours){
				callsPerHour[hour] +=1;
				if(Calls[2][i] - Calls[1][i] <= param->DelayThreshold &&
						Calls[2][i]-Calls[1][i]>=0.)
					successfulPerHour[hour]+=1;
			}
		}

		// Total Cost, assuming break is paid
		totalHours = 0.0;
		for (i=0;i<nAgents;i++)
			totalHours += (Agents[i][4]-Agents[i][1]);
		TotalCost[k] = param->Salary * totalHours;
		GrandCost += TotalCost[k];
		for (i=0;i<param->nHours;i++) {
			PerformanceMeasure[i][k] = (double)(successfulPerHour[i]/(double)callsPerHour[i]-0.8);
		}
		//cout << "Day " << k << " Call " << nCalls <<endl;
	}//end kth day simulation

	//final answer
	fn = GrandCost/param->nDays;
	//cout << "fn, GrandCost, nDays: " << fn << " " << GrandCost << " " << param->nDays << endl;
	FnVar=0;
	for (k=0; k<param->nDays; k++)  FnVar += pow(TotalCost[k]-fn,2);
	FnVar = FnVar / (param->nDays-1);
	// to compute constraint and ConstraintVar here.....
	for (i=0;i<properties.m;i++){
		for (k=0;k<param->nDays;k++) {
			constraint[i] += PerformanceMeasure[i][k];
		}
		constraint[i] /= param->nDays;

	}

	Calls.clear();
	Agents.clear();
	PerformanceMeasure.clear();

	//mark success simulation run
	status = 0;
}
