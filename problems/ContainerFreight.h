/**
 * @file ContainerFreight.h
 *
 * @brief Defines ContainerFreight (Throughput Maximization) test problem class
 *
 */
#ifndef CONTAINERFREIGHT_H
#define CONTAINERFREIGHT_H

#include "SOProb.h"
#include "Combination.h"
#include <fstream>
#include <assert.h>

extern int bsize_GLOBAL, seed_GLOBAL, n0_GLOBAL, outputLevel, algo, RB_global;
extern double burnin_param;

/**
 * @addtogroup prob
 */
/*@{*/
/**
 * @brief Container Freight test problem.
 *
 * Problem description can be found at
 * http://simopt.org/wiki/index.php?title=Container_Freight
 *
 * Simulation code written originally by German Gutierrez in Matlab, available
 * on the SimOpt page.
 *
 * Converted to C++ by Eric Ni
 */
class ContainerFreight : public SOProb {
public:
	ContainerFreight ();
	virtual ~ContainerFreight ();
	virtual vector<int> Idx2System_Disc(const int& idx);
	void Objective();

	///@{
	void getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
			const int &NumInitSol);
	///@}


	/**
	 * @brief Problem parameters.
	 */
	struct ContainerFreightParams{
		int M;		///< Number of available docks.
		int burnin; ///< Length of burn-in period in minutes.
		int njobs; 	///< Length of total simulation in minutes.
		vector<int> LB;
		vector<double> lambdas;
		vector<double> mus;
		/*
		 * batch means
		 */
		/*
		int m;      // batch size
		int b;      // number of batches
		 */
	};
	/// A ContainerFreight::Parameters object that stores problem parameters.
	ContainerFreightParams* param;
protected:
	///@{
	int System2Idx(vector<int> sys_disc, vector<double> sys_cont);
	void pretty_print();
	///@}
};
/*@}*/

/**
 * Enumeration used to identify various types of simulated events
 */
enum MMCEventType{
	arrival_event, departure_event, null_event
};

/**
 * A simple class to model arrival/departure event in an M/M/c queue
 */
class MMCEvent {
public:
	MMCEvent():
		id(-1), type(null_event), event_time(-1),
		arrival_time(-1), departure_time(-1) {}
	MMCEvent( int _id,
			double _event_t,
			double _arr_t,
			double _dep_t = -1,
			int _type = arrival_event ):
				id(_id),
				arrival_time(_arr_t),
				departure_time(_dep_t),
				type(_type),
				event_time (_event_t) {}
	int id; ///< Index of the job, used to locate service time
	int type; ///< Type of event, either arrival_event or departure_event
	double event_time; ///< Time at which the event occurs (in minute)
	double arrival_time; ///< Arrival time (in minute)
	double departure_time; ///< Departure time (in minute)
};

/**
 * A simple comparator of two MMCEvent objects
 */
class MMCComparison
{
public:
	bool operator() (MMCEvent lhs, MMCEvent rhs) {
		return lhs.event_time > rhs.event_time;
	}
};

#endif
