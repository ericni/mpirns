#include <cmath>
#include <glpk.h>
#include <Matrix.h>
#include "SOProb.h"
using namespace std;

struct Newsvendor::Parameters {
	int q;	// #products
	int p;	// #resources
	Vector c;	// cost vector for resources
	Vector v;	// profit margins
	// A[i][j] = amount of resource i required
	// to produce one unit of product j
	Matrix A;

	// distribution of demand:
	Vector mu;	// mean
	Vector sigma;	// standard deviation
	Matrix rho;	// correlation matrix
	Matrix Sigma;	// covariance matrix
	Matrix L;	// LL' = Sigma
};

Newsvendor::Newsvendor() {
	parameters = new Parameters();
	parameters->p = 5;
	parameters->q = 5;

	static const double c[] = { 1.0, 1.0, 1.0, 1.0, 1.0 };
	parameters->c = arraytoVector(c, parameters->p);

	static const double v[] = { 6.0, 5.0, 4.0, 3.0, 2.0 };
	parameters->v = arraytoVector(v, parameters->q);

	for(int i=0; i<parameters->p; ++i) {
		Vector Ai(parameters->q, 0.0);	parameters->A.push_back(Ai);
		for(int j=0; j<=i; ++j)	parameters->A[i][j] = 1.0;	}

	static const double mu[] = { 10.0, 15.0, 5.0, 8.0, 10.0 };
	parameters->mu = arraytoVector(mu, parameters->q);

	static const double sigma[] = { 5.0, 10.0, 2.0, 3.0, 6.0 };
	parameters->sigma = arraytoVector(sigma, parameters->q);

	static const double rho[] = {
			 1.0,	-0.2,	 0.3,	 0.5,	 0.1,
			-0.2,	 1.0,	-0.1,	-0.3,	-0.1,
			 0.3,	-0.1,	 1.0,	 0.6,	 0.2,
			 0.5,	-0.3,	 0.6,	 1.0,	 0.05,
			 0.1,	-0.1,	 0.2,	 0.05,	 1.0
		};
	parameters->rho = arraytoMatrix(rho, parameters->q, parameters->q);
	parameters->Sigma = parameters->rho;
	for(int i=0; i<parameters->q; ++i)	for(int j=0; j<parameters->q; ++j)
		parameters->Sigma[i][j] *= parameters->sigma[i] * parameters->sigma[j];
	Vector eval;	Matrix evec;
	eigendecomposition( parameters->Sigma, eval, evec );
	for(int i=0; i<eval.size(); ++i)	eval[i] = sqrt(eval[i]);
	parameters->L = multiply(evec, diag(eval));

	static const double lb[] = {8, 18, 22, 29, 36};
	static const double ub[] = {22, 61, 71, 86, 110};
	LB = arraytoVector(lb, parameters->p);
	UB = arraytoVector(ub, parameters->q);
}

double Newsvendor::Objective( Vector x ) {
	Vector D;
	for(int i=0; i<parameters->q; ++i)	D.push_back(s.RandGaussian());
	D = add( parameters->mu, multiply(parameters->L, D) );

	//	max		v'y
	//	s.t.	Ay	<=	X	(capacity constraints)
	//			y	<=	D	(demand constraints)
	//			y	>=	0

	glp_prob *lp = glp_create_prob();
	glp_set_prob_name(lp, "Newsvendor");
	glp_set_obj_dir(lp, GLP_MAX);

	glp_add_rows(lp, parameters->p);
	for(int i=0; i<parameters->p; ++i)
		glp_set_row_bnds(lp, i+1, GLP_UP, 0.0, x[i]);
	glp_add_cols(lp, parameters->q);
	for(int i=0; i<parameters->q; ++i) {
		glp_set_col_bnds(lp, i+1, GLP_DB, 0.0, D[i]);
		glp_set_obj_coef(lp, i+1, parameters->v[i]);
	}

	int *ia = new int[parameters->p * parameters->q + 1];
	int *ja = new int[parameters->p * parameters->q + 1];
	double *ar = new double[parameters->p * parameters->q + 1];
	for(int i=0; i<parameters->p; ++i)	for(int j=0; j<parameters->q; ++j) {
		int index = i * parameters->q + j + 1;
		ia[index] = i+1;	ja[index] = j+1;	ar[index] = parameters->A[i][j];
	}
	glp_load_matrix(lp, parameters->p * parameters->q, ia, ja, ar);

	glp_smcp parm;	glp_init_smcp(&parm);	parm.msg_lev = GLP_MSG_OFF;
	glp_simplex(lp, &parm);	double vy = glp_get_obj_val(lp);

	delete[] ia;	delete[] ja;	delete[] ar;
	glp_delete_prob(lp);

	return vy - multiply( parameters->c, x);
}
