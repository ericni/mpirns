
/**
 * @file TpMaxLEQ.cpp
 *
 * @brief Implements TpMax (Throughput Maximization) test problem class
 *
*/
#include "TpMaxLEQ.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

namespace {
/// For debugging purposes
void printVec (vector<double> v) {
	for (int i=0; i <(int) v.size(); ++i) {
		cout << v[i] << ' ';
	}
	cout << endl;
}

/// For debugging purposes
string toString_Vec (vector<double> v) {
	ostringstream oss;
	for (int i=0; i <(int) v.size(); ++i) {
		oss << v[i] << ' ';
	}
	oss << endl;
	return oss.str();
}

/// For debugging purposes
int nChoosek( int n, int k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}
}

//struct TpMaxLEQ::Parameters {
//	int B;		// max number of buffers at each station
//	int R;		// max number of rates   at each station
//	int nstages;// number of stages
//	int burnin; // number of burn-in jobs
//	int njobs; 	// number of jobs to be released in simulation
//	/*
//	 * batch means
//	 */
//	/*
//	int m;      // batch size
//	int b;      // number of batches
//	 */
//};

/**
 * @brief Default Constructor.
 *
 * Initializes parameters.
 */
TpMaxLEQ::TpMaxLEQ() {
	param = new TpMaxParams();
	param->B    = 8;
	param->R 	= 7;
	param->B    = RB_global;
	param->R 	= RB_global - 1;
	param->nstages = 4;
	properties.NumSystems  = (int) pow((double)param->R, param->nstages);
	properties.NumSystems *= (int) pow((double)param->B, param->nstages-1);
	param->njobs   = 2050;
	//Number of jobs to be released in simulation
	param->burnin= 2000;	//number of burn-in jobs
	/*
	 * batch means
	 */
	//param->m    = 500;		//batch size
	properties.minmax = 1;
	properties.d_disc = 2*param->nstages-1;
	properties.d_cont = 0;
	properties.m = 0;
	properties.VarUB_disc.clear(); properties.VarUB_disc.resize(properties.d_disc, 150);
	properties.VarLB_disc.clear(); properties.VarLB_disc.resize(properties.d_disc, 0 );
	properties.VarUB_cont.clear();
	properties.VarLB_cont.clear();
	properties.FnGradAvail = 0;
	properties.NumConstraintGradAvail = 0;
	properties.ObjBd = 0.0;
	properties.budget.resize(3);
	properties.budget[0] = 20;
	properties.budget[1] = 1;
	properties.budget[2] = 10000;
	properties.OptimalSol.clear();
//	NumRemainingSystems = prop.NumSystems;
	_index = 0;
	this->Simulation_Time = 1.0;
}
//
//TpMaxLEQ::~TpMaxLEQ() {
//	this->clear();
//	delete param;
//}

void TpMaxLEQ::getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
		const int &NumInitSol){
	Sol_cont.clear(); // this problem has no continuous decision variablee
	if (NumInitSol <= 0) {
		Sol_disc.clear();
		//do nothing (probably send a warning message)
	} else if (NumInitSol == 1) {
		Sol_disc.resize(1, 17);	Sol_disc.fill(8);
	} else {
		Sol_disc.resize(NumInitSol, 17);
		SORngStream UStream;
		for (int j=0; j <NumInitSol; j++) {
			for (int i=0; i < 17; i++ ) {
				//Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][15]= 30;
				Sol_disc[j][16]= 50;
			}
		}
	}
}

//void TpMaxLEQ::Initialize_Single_System(int * inDisc, double * inCont, int nRep,
//		//		unsigned long * otherSeed,
//		SORngStream * ptr_Stream) {
//	this->setDiscX(inDisc);
//	this->setContX(inCont);
//	this->numReplications = nRep;
//	this->pStream = ptr_Stream;
//	/*
//	 * batch means
//	 */
//	/*
//	param->b = nRep;
//	param->njobs= param->burnin+param->m*param->b;
//	//Number of jobs to be released in simulation
//	 */
//}

//void TpMaxLEQ::Initialize_Single_System(int idx, int nRep,
//		//		unsigned long * otherSeed,
//		SORngStream * ptr_Stream) {
//	this->x_disc = this->Idx2System_Disc(idx);
//	//	this->x_cont = this->Idx2System_Cont(idx);
//	this->numReplications = nRep;
//	this->pStream = ptr_Stream;
//	/*
//	 * batch means
//	 */
//	/*
//	param->b = nRep;
//	param->njobs= param->burnin+param->m*param->b;
//	//Number of jobs to be released in simulation
//	 */
//}
//
//void TpMaxLEQ::Add_System(int* inDisc, double* inCont, int nRep,
//		unsigned long * otherSeed) {
//	vector<int> disc;
//	for (int i = 0; i < this->prop.d_disc; i++)
//		disc.push_back(inDisc[i]);
//	SORngStream * ptr = new SORngStream();
//	if (ptr->SetSeed(otherSeed)==false) {
//		vector <double> dummy;
//		cout << "Error seed with system " << endl;
//		//				<< this->System2Idx(disc, dummy) << endl;
//	}
//	this->x_disc_mat.push_back(disc);
//	this->numReplications_vec.push_back(nRep);
//	this->pStream_vec.push_back(ptr);
//}
//
//
//
//void TpMaxLEQ::Set_Systems(int numSys, int * idxs, int * nReps, unsigned long * otherSeed ) {
//	//	vector<int> disc;
//	//	disc.resize(prop.d_disc,0);
//	this->x_disc_mat.clear();
//	this->sys_idxs.resize(numSys);
//	std::copy ( idxs, idxs+numSys, sys_idxs.begin() );
//	//	for (int i = 0; i < numSys; ++i) {
//	//		sys_idxs[i] = idxs[i];
//	//	}
//	//	this->x_disc_mat.resize(numSys, disc);
//	//	for (int i = 0; i < numSys; ++i) {
//	//		x_disc_mat[i] = this->Idx2System_Disc(idxs[i]);
//	//	}
//
//
//
//	numReplications_vec.resize(numSys, 0);
//	std::copy ( nReps, nReps+numSys, numReplications_vec.begin() );
//
//	this->pStream_vec.resize(numSys);
//	for (int i = 0; i < numSys; ++i) {
//		SORngStream * ptr = new SORngStream();
//		if (ptr->SetSeed(&otherSeed[6*i])==false) {
//			vector <double> dummy;
//			cout << "Error seed with system " << endl;
//			//				<< this->System2Idx(disc, dummy) << endl;
//		}
//		pStream_vec[i] = ptr;
//	}
//
//}
//
//void TpMaxLEQ::run_system(int s) {
//	double * dummyptr = NULL;
//	vector <int > v = this->Idx2System_Disc(sys_idxs[s]);
//	this->Initialize_Single_System(&v[0], dummyptr, numReplications_vec[s], pStream_vec[s]);
//	this->Objective();
//}

//
//void TpMaxLEQ::Objective(){
//	//Returns average throughput as defined in problem statement, Mean and Sample Variance
//	//of the time between the last 50 job releases.
//	// initially set fn and FnVar to NaN, status to -1
//	double t0 = MPI::Wtime();
//	this->fn =  *( double* )nan;
//	this->FnVar =  *( double* )nan;
//	status = -1;
//
//	int i,j; double t;
//
//	//decompose the vector of discrete decision variables, x_disc, into two
//	//vectors representing the solution
//	vector<int> r(x_disc.begin(),x_disc.begin()+param->nstages); 	//rates of service times
//	vector<int> b(x_disc.begin()+param->nstages,x_disc.end()); 	//buffer spaces before stage i
//
//	//debug
//	bool db  = false;
//	//if (r[0] == 7 && r[1] == 7 && r[2] == 6 && b[0] == 8 && b[1] == 12) db=true;
//
//	//Matrix<double> ServiceT  (param->nstages, param->njobs+1, 0.0);
//
//	Matrix<double> sTime (param->njobs, param->nstages, 0.0);
//	vector<double> tp (numReplications,0.0);
//	double tp_total = 0.0;
//	this->FnSumSq = 0.0;
//
//	// create random number stream
//	//	SORngStream ServiceStream;
//
//
//	for (int k=0; k<numReplications; ++k) {
//		Matrix<double> ExitTimes (param->nstages, param->njobs+1, 0.0);
//		//matrix with the time at which each job leaves stage i (row i).
//		// generate random data
//		pStream->ResetNextSubstream();
//		for (i=0;i<param->njobs;i++) {
//			for (j=0;j<param->nstages;j++) {
//				sTime[i][j]=pStream->RandExponential((double) r[j]);
//			}
//		}
//
//		////////////////////////////////////
//		// done generating random numbers
//		///////////////////////////////////
//
//		///////////////////////
//		// begin simulation
//		///////////////////////
//
//		for (i=1; i <= param->njobs; ++i) {
//			t = sTime[i-1][0];
//			//First stage(indexed 0): if time at which the i--b(0)th job left stage 1 is less
//			//than the time at which this job is ready to leave stage 0 (there is
//			//buffer space available) job leaves stage 0 as soon as it is ready.
//			if (ExitTimes[1][max(0,i-b[0])] <= ExitTimes[0][i-1]+t) {
//				ExitTimes[0][i] = ExitTimes[0][i-1]+t;
//			}else {
//				//No buffer space available, job leaves as soon as buffer space is
//				//available.
//				ExitTimes[0][i] = ExitTimes[1][max(0,i-b[0])];
//			}
//
//			for (j=2; j<=param->nstages-1; j++) {
//				t = sTime[i-1][j-1];
//				//if (time at which previous order left > time this order left
//				//previous stage)Then there must be a queue
//				if(ExitTimes[j-1][i-1]>ExitTimes[j-2][i]) {
//					//if there is buffer space available
//					if(ExitTimes[j][max(0,i-b[j-1])] <= ExitTimes[j-1][i-1]+t) {
//						ExitTimes[j-1][i] = ExitTimes[j-1][i-1]+t;
//					} else {
//						//No buffer space available, job leaves as soon as buffer space is available.
//						ExitTimes[j-1][i] = ExitTimes[j][max(0,i-b[j-1])];
//					}
//				} else {
//					//There is no queue, job starts to be worked on as soon as it leaves previous stage.
//					if (ExitTimes[j][max(0,i-b[j-1])] <= ExitTimes[j-2][i]+t) {
//						//If there is buffer space available, job leaves as soon as ready
//						ExitTimes[j-1][i] = ExitTimes[j-2][i]+t;
//					} else{
//						//No buffer space available, job leaves as soon as buffer space is available.
//						ExitTimes[j-1][i] = ExitTimes[j][max(0,i-b[j-1])];
//					}
//				}
//			}
//
//			//last stage: all jobs leave as soon as ready
//			t=sTime[i-1][param->nstages-1];
//			//if there is not a queue (i.e. previous job finished before current job leaves previous stage)
//			if (ExitTimes[param->nstages-1][i-1] <= ExitTimes[param->nstages-2][i]) {
//				//leaves as soon as ready, entered to be worked on as soon as finished in previous stage
//				ExitTimes[param->nstages-1][i] = ExitTimes[param->nstages-2][i]+t;
//			} else {
//				//Entered to be worked on after previous job was finished, leaves as soon as ready.
//				ExitTimes[param->nstages-1][i] = ExitTimes[param->nstages-1][i-1]+t;
//			}
//		}// end for loop of the ith job
//		tp[k] = ((double) (param->njobs-param->burnin))/(ExitTimes[param->nstages-1][param->njobs]-ExitTimes[param->nstages-1][param->burnin]);
//		tp_total += tp[k];
//		this->FnSumSq += tp[k]*tp[k];
//		ExitTimes.clear();
//		if (db) {
//			cout << tp[k] << "," ;
//		}
//	}
//	if (db) cout << endl;
//	///////////////////////
//	// end simulation
//	///////////////////////
//
//
//	///////////////////////
//	// compute statistics
//	///////////////////////
//
//	/*
//	 * batch means
//	 */
//	/*
//	int first, last;
//	for (int k=0; k<param->b; k++) {
//		first = param->burnin+k*param->m;
//		last  = first + param->m;
//		tp[k] = (double)param->m/(ExitTimes[0][last]-ExitTimes[0][first]);
//		tp_total += tp[k];
//	}
//	 */
//
//	//final answer
//	/*
//	 * batch means
//	 */
//	//fn = ((double)(param->njobs-param->burnin))/(ExitTimes[0][param->njobs] - ExitTimes[0][param->burnin]);
//	fn = tp_total/numReplications;
//	FnVar = 0.0;
//	for (int k=0; k<numReplications; k++)  FnVar += pow(tp[k]-fn,2);
//	FnVar = FnVar / (numReplications-1);
//
//	Simulation_Time = MPI::Wtime()-t0;
//
//	//mark success simulation run
//	status = 0;
//}

/// (Deprecated) Enumerate systems.
void TpMaxLEQ::enumerateSystem() {
	int n=param->nstages, B=param->B, R=param->R, i;
	ostringstream s;
	s << "TpSystem_" << n << "_" << B << "_" << R << ".txt";
	outfile.open(s.str().c_str());
	Rs.clear(); Bs.clear(); Rcomb.clear(); Bcomb.clear(); 	_index = 0;

	for (i=1; i<=R-1; i++) Rs.push_back(i);
	for (i=1; i<=B-1; i++) Bs.push_back(i);

	this->nextComb(0, n-1, 1);

	outfile.close();
	Rs.clear(); Bs.clear(); Rcomb.clear(); Bcomb.clear(); 	_index = 0;
}

/// (Deprecated) Finds the next combination. Used by system enumerator.
void TpMaxLEQ::nextComb(int offset, int k, int rb) {
	if (k == 0) {
		if (rb == 1)
			this->nextComb(0, Rcomb.size()-1, 2);
		else pretty_print();
		return;
	}
	if (rb == 1) {
		for (int i = offset; i <= (int)Rs.size() - k; ++i) {
			Rcomb.push_back(Rs[i]);
			this->nextComb(i+1, k-1, rb);
			Rcomb.pop_back();
		}
	}else if (rb == 2) {
		for (int i = offset; i <= (int)Bs.size() - k; ++i) {
			Bcomb.push_back(Bs[i]);
			this->nextComb(i+1, k-1, rb);
			Bcomb.pop_back();
		}
	}
}

/**
 * @brief (Deprecated) Read a list of systems from file.
 *
 * If the file is not found, writes the file and the reads it.
 */
void TpMaxLEQ::readSystem() {
	Systems.clear();	Sys2IdxMap.clear();
	string line;
	int n=param->nstages, B=param->B, R=param->R;
	ostringstream s;
	s << "TpSystem_" << n << "_" << B << "_" << R << ".txt";
	infile.open(s.str().c_str());
	if (infile.is_open()) {
		while(!infile.eof()) {
			getline(infile, line);
			stringstream s(line);
			int i, j, tmp; vector<int> v;
			s >> i;
			for (j = 0; j<this->properties.d_disc; ++j) {
				s >> tmp;
				v.push_back(tmp);
			}
			Systems.push_back(v);
			Sys2IdxMap.insert(pair<vector<int>, int>(v, i));
		}
		infile.close();
	} else {
		//if system enumeration file is not found, create one and load
		infile.close();
		this->enumerateSystem();
		this->readSystem();
	}
}

vector<int> TpMaxLEQ::Idx2System_Disc(const int& idx) {
	int tmp_idx = idx;
	//finds the system corresponding to the index
	vector <int> v ;
	int i;
	v.resize(properties.d_disc);
	if (idx >= properties.NumSystems || idx < 0) {
		cerr << "System idx " << idx << " out of bounds" << endl;
		//		Old - reading file
		//		return Systems[0];
		//		New
	} else {
		//rates
		for (i = 0; i < param->nstages; i++) {
			v[i] = tmp_idx % param->R;
			tmp_idx -= v[i];
			tmp_idx /= param->R;
			v[i]++;
		}
		//buffers
		for (i = param->nstages; i < properties.d_disc; i++ ) {
			v[i] = tmp_idx % param->B;
			tmp_idx -= v[i];
			tmp_idx /= param->B;
			v[i]++;
		}
	}
	return v;
}

/// (Deprecated) Converts a system (as characterized by its decision variables) to its index.
int TpMaxLEQ::System2Idx(vector<int> sys_disc, vector<double> sys_cont) {
	//converts system back to index. -1 indicates that the system is not found
	if (Sys2IdxMap.count(sys_disc) < 1) {
		cerr << "Unable to find system in Sys2IdxMap" << endl;
		return -1;
	}
	else return Sys2IdxMap.find(sys_disc)->second;
}
//
//void TpMaxLEQ::getNextSystem(vector<int> & v1, vector<double> &v2) {
//	//return in vector v the next system to test
//	v1 = this->Idx2System_Disc( prop.NumSystems - NumRemainingSystems-- );
//}
