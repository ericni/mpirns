/**
 * @file TpMax.h
 *
 * @brief Defines TpMax (Throughput Maximization) test problem class
 *
*/
#ifndef TPMAX_H
#define TPMAX_H

#include "SOProb.h"
#include <fstream>
#include <assert.h>

extern int bsize_GLOBAL, seed_GLOBAL, n0_GLOBAL, outputLevel, algo, RB_global;
extern double burnin_param;

/**
 * @addtogroup prob
 */
/*@{*/
/**
 * @brief Throughput Maximization test problem.
 *
 * Problem description can be found at
 * http://simopt.org/wiki/index.php?title=Throughput_Maximization
 *
 * Simulation code written originally by German Gutierrez in Matlab, available
 * on the SimOpt page.
 *
 * Converted to C++ by Eric Ni
 */
class TpMax : public SOProb {
public:
	TpMax ();
	virtual ~TpMax ();
	virtual vector<int> Idx2System_Disc(const int& idx);
	void Objective();

	///@{
	void getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
			const int &NumInitSol);
	///@}


	/**
	 * @brief Problem parameters.
	 */
	struct TpMaxParams{
		int B;		///< Number of available buffers.
		int R;		///< Number of units of available resources.
		int nstages;///< Number of stages.
		int burnin; ///< Number of burn-in jobs.
		int njobs; 	///< Number of jobs to be released in simulation.
		/*
		 * batch means
		 */
		/*
		int m;      // batch size
		int b;      // number of batches
		 */
	};
	/// A TpMax::Parameters object that stores problem parameters.
	TpMaxParams* param;
protected:

	///@{
	virtual int System2Idx(vector<int> sys_disc, vector<double> sys_cont);
	virtual void enumerateSystem();
	virtual void nextComb(int offset, int k, int rb);
	virtual void readSystem();
	void pretty_print() ;
	///@}

	//@{
	/// (Deprecated) Used by system enumerator
	vector<int> Rs, Bs, Rcomb, Bcomb;
	ofstream outfile;
	ifstream infile;
	//@}
	/// (Deprecated) A list of decision variables ordered by system index.
	deque< vector<int> > Systems;

	/// (Deprecated) Maps decision variables to index.
	map<vector<int>, int> Sys2IdxMap;
};
/*@}*/
#endif
