/**
 * @file TpMax.cpp
 *
 * @brief Implements TpMax (Throughput Maximization) test problem class
 *
 */
#include "TpMax.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

namespace {
///for debugging purposes
void printVec (vector<double> v) {
	for (int i=0; i <(int) v.size(); ++i) {
		cout << v[i] << ' ';
	}
	cout << endl;
}

///for debugging purposes
string toString_Vec (vector<double> v) {
	ostringstream oss;
	for (int i=0; i <(int) v.size(); ++i) {
		oss << v[i] << ' ';
	}
	oss << endl;
	return oss.str();
}

///for debugging purposes
int nChoosek( int n, int k )
{
	if (k > n) return 0;
	if (k * 2 > n) k = n-k;
	if (k == 0) return 1;

	int result = n;
	for( int i = 2; i <= k; ++i ) {
		result *= (n-i+1);
		result /= i;
	}
	return result;
}
}

/**
 * @brief Default Constructor.
 *
 * Reads parameter RB from global variable RB_global.
 * Also initializes parameters.
 */
TpMax::TpMax() {
	param = new TpMaxParams();
	//	param->B    = 50;
	//	param->R 	= 50;
	//	param->B    = 20;
	//	param->R 	= 20;
	//	prop.NumSystems = 57624;
	//	prop.NumSystems = 3249;
	//	param->B	= 128;
	//	param->R	= 128;
	//	prop.NumSystems = 1016127;
	param->nstages = 3;		//Number of stages
	param->B	= RB_global;
	param->R	= RB_global;
	properties.NumSystems  = nChoosek(param->R-1, param->nstages-1);
	properties.NumSystems *= nChoosek(param->B-1, param->nstages-2);
#ifdef TEST_CONFIGURES
	properties.NumSystems = 3000;
#endif
	param->njobs   = 2050;
	//Number of jobs to be released in simulation
	param->burnin= 2000;	//number of burn-in jobs
	/*
	 * batch means
	 */
	//param->m    = 500;		//batch size
	properties.minmax = 1;
	properties.d_disc = 2*param->nstages-1;
	properties.d_cont = 0;
	properties.m = 0;
	properties.VarUB_disc.clear(); properties.VarUB_disc.resize(properties.d_disc, 150);
	properties.VarLB_disc.clear(); properties.VarLB_disc.resize(properties.d_disc, 0 );
	properties.VarUB_cont.clear();
	properties.VarLB_cont.clear();
	properties.FnGradAvail = 0;
	properties.NumConstraintGradAvail = 0;
	properties.ObjBd = 0.0;
	properties.budget.resize(3);
	properties.budget[0] = 20;
	properties.budget[1] = 1;
	properties.budget[2] = 10000;
	properties.OptimalSol.clear();
	//	NumRemainingSystems = prop.NumSystems;
	_index = -1;
}

TpMax::~TpMax() {
	this->clear();
	delete param;
}

/// Currently not in use.
void TpMax::getInitialSolution (Matrix<int> &Sol_disc, Matrix<double> &Sol_cont,
		const int &NumInitSol){
	Sol_cont.clear(); // this problem has no continuous decision variablee
	if (NumInitSol <= 0) {
		Sol_disc.clear();
		//do nothing (probably send a warning message)
	} else if (NumInitSol == 1) {
		Sol_disc.resize(1, 17);	Sol_disc.fill(8);
	} else {
		Sol_disc.resize(NumInitSol, 17);
		SORngStream UStream;
		for (int j=0; j <NumInitSol; j++) {
			for (int i=0; i < 17; i++ ) {
				//Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][i] = UStream.RandInt(0,10);
				Sol_disc[j][15]= 30;
				Sol_disc[j][16]= 50;
			}
		}
	}
}

//void TpMax::Initialize_Single_System(int * inDisc, double * inCont, int nRep,
//		//		unsigned long * otherSeed,
//		SORngStream * ptr_Stream) {
//	this->setDiscX(inDisc);
//	this->setContX(inCont);
//	this->numReplications = nRep;
//	this->pStream = ptr_Stream;
//	/*
//	 * batch means
//	 */
//	/*
//	param->b = nRep;
//	param->njobs= param->burnin+param->m*param->b;
//	//Number of jobs to be released in simulation
//	 */
//}
//
//void TpMax::Initialize_Single_System(int idx, int nRep,
//		//		unsigned long * otherSeed,
//		SORngStream * ptr_Stream) {
//	this->x_disc = this->Idx2System_Disc(idx);
//	//	this->x_cont = this->Idx2System_Cont(idx);
//	this->numReplications = nRep;
//	this->pStream = ptr_Stream;
//	/*
//	 * batch means
//	 */
//	/*
//	param->b = nRep;
//	param->njobs= param->burnin+param->m*param->b;
//	//Number of jobs to be released in simulation
//	 */
//}
//
//void TpMax::Add_System(int* inDisc, double* inCont, int nRep,
//		unsigned long * otherSeed) {
//	vector<int> disc;
//	for (int i = 0; i < this->prop.d_disc; i++)
//		disc.push_back(inDisc[i]);
//	SORngStream * ptr = new SORngStream();
//	if (ptr->SetSeed(otherSeed)==false) {
//		vector <double> dummy;
//		cout << "Error seed with system " << endl;
//		//				<< this->System2Idx(disc, dummy) << endl;
//	}
//	this->x_disc_mat.push_back(disc);
//	this->numReplications_vec.push_back(nRep);
//	this->pStream_vec.push_back(ptr);
//}
//
//void TpMax::Set_Systems(int numSys, int * idxs, int * nReps, unsigned long * otherSeed ) {
//	//	vector<int> disc;
//	//	disc.resize(prop.d_disc,0);
//	this->x_disc_mat.clear();
//	this->sys_idxs.resize(numSys);
//	std::copy ( idxs, idxs+numSys, sys_idxs.begin() );
//	//	for (int i = 0; i < numSys; ++i) {
//	//		sys_idxs[i] = idxs[i];
//	//	}
//	//	this->x_disc_mat.resize(numSys, disc);
//	//	for (int i = 0; i < numSys; ++i) {
//	//		x_disc_mat[i] = this->Idx2System_Disc(idxs[i]);
//	//	}
//
//
//	//	cout << "Added 2\n";
//
//	numReplications_vec.resize(numSys, 0);
//	std::copy ( nReps, nReps+numSys, numReplications_vec.begin() );
//
//	//	cout << "Added 3\n";
//	this->pStream_vec.resize(numSys);
//	for (int i = 0; i < numSys; ++i) {
//		SORngStream * ptr = new SORngStream();
//		if (ptr->SetSeed(&otherSeed[6*i])==false) {
//			cout << "Error seed with system " << endl;
//			//				<< this->System2Idx(disc, dummy) << endl;
//		}
//		pStream_vec[i] = ptr;
//	}
//
//	//	cout << "Added 4\n";
//}
//
//void TpMax::run_system(int s) {
//	double * dummyptr = NULL;
//	vector <int > v = this->Idx2System_Disc(sys_idxs[s]);
//	this->Initialize_Single_System(&v[0], dummyptr, numReplications_vec[s], pStream_vec[s]);
//	this->Objective();
//}


void TpMax::Objective(){
	//Returns average throughput as defined in problem statement, Mean and Sample Variance
	//of the time between the last 50 job releases.
	// initially set fn and FnVar to NaN, status to -1
	double t0 = MPI::Wtime();
	this->fn =  *( double* )nan;
	this->FnVar =  *( double* )nan;
	status = -1;

	int i,j; double t;


	vector<int> burnins;
	if(burnin_param > 0) {
		double mu = log(param->burnin) - burnin_param / 2.;
		double sig = sqrt(burnin_param);
		burnins.resize(_numReps, 0);
		for (i = 0; i < _numReps; ++i)  {
			int v = (int) exp( _pStream->RandNormal(0., 1.) * sig + mu );
			burnins[i] = (v > 20000) ? 20000:v;
		}
	}
	int _njobs, _burnin;
	const int _diff = param->njobs - param->burnin;
	const int _nstages = param->nstages;

	//decompose the vector of discrete decision variables, x_disc, into two
	//vectors representing the solution
	vector<int> r(x_disc.begin(),x_disc.begin()+_nstages); 	//rates of service times
	vector<int> b(x_disc.begin()+_nstages,x_disc.end()); 	//buffer spaces before stage i

	//debug
	bool db  = false;
	//if (r[0] == 7 && r[1] == 7 && r[2] == 6 && b[0] == 8 && b[1] == 12) db=true;

	//Matrix<double> ServiceT  (_nstages, param->njobs+1, 0.0);

	vector<double> tp (_numReps,0.0);
	double tp_total = 0.0;
	this->FnSumSq = 0.0;

	for (int k=0; k<_numReps; ++k) {
		_burnin = (burnin_param > 0) ? burnins[k] : param->burnin;
		_njobs = _burnin + _diff;
		Matrix<double> sTime (_njobs, _nstages, 0.0);
		Matrix<double> ExitTimes (_nstages, _njobs+1, 0.0);
		//matrix with the time at which each job leaves stage i (row i).
		// generate random data

		//		pStream->ResetNextSubstream();
		for (i=0;i<_njobs;i++) {
			for (j=0;j<_nstages;j++) {
				sTime[i][j]=_pStream->RandExponential((double) r[j]);
			}
		}

		///////////////////////
		// begin simulation
		///////////////////////

		for (i=1; i <= _njobs; ++i) {
			t = sTime[i-1][0];
			//First stage(indexed 0): if time at which the i--b(0)th job left stage 1 is less
			//than the time at which this job is ready to leave stage 0 (there is
			//buffer space available) job leaves stage 0 as soon as it is ready.
			if (ExitTimes[1][max(0,i-b[0])] <= ExitTimes[0][i-1]+t) {
				ExitTimes[0][i] = ExitTimes[0][i-1]+t;
			}else {
				//No buffer space available, job leaves as soon as buffer space is
				//available.
				ExitTimes[0][i] = ExitTimes[1][max(0,i-b[0])];
			}

			for (j=2; j<=_nstages-1; j++) {
				t = sTime[i-1][j-1];
				//if (time at which previous order left > time this order left
				//previous stage)Then there must be a queue
				if(ExitTimes[j-1][i-1]>ExitTimes[j-2][i]) {
					//if there is buffer space available
					if(ExitTimes[j][max(0,i-b[j-1])] <= ExitTimes[j-1][i-1]+t) {
						ExitTimes[j-1][i] = ExitTimes[j-1][i-1]+t;
					} else {
						//No buffer space available, job leaves as soon as buffer space is available.
						ExitTimes[j-1][i] = ExitTimes[j][max(0,i-b[j-1])];
					}
				} else {
					//There is no queue, job starts to be worked on as soon as it leaves previous stage.
					if (ExitTimes[j][max(0,i-b[j-1])] <= ExitTimes[j-2][i]+t) {
						//If there is buffer space available, job leaves as soon as ready
						ExitTimes[j-1][i] = ExitTimes[j-2][i]+t;
					} else{
						//No buffer space available, job leaves as soon as buffer space is available.
						ExitTimes[j-1][i] = ExitTimes[j][max(0,i-b[j-1])];
					}
				}
			}

			//last stage: all jobs leave as soon as ready
			t=sTime[i-1][_nstages-1];
			//if there is not a queue (i.e. previous job finished before current job leaves previous stage)
			if (ExitTimes[_nstages-1][i-1] <= ExitTimes[_nstages-2][i]) {
				//leaves as soon as ready, entered to be worked on as soon as finished in previous stage
				ExitTimes[_nstages-1][i] = ExitTimes[_nstages-2][i]+t;
			} else {
				//Entered to be worked on after previous job was finished, leaves as soon as ready.
				ExitTimes[_nstages-1][i] = ExitTimes[_nstages-1][i-1]+t;
			}
		}// end for loop of the ith job
		tp[k] = ((double) (_njobs-_burnin))/(ExitTimes[_nstages-1][_njobs]-ExitTimes[_nstages-1][_burnin]);
		tp_total += tp[k];
		this->FnSumSq += tp[k]*tp[k];
		ExitTimes.clear();
		if (db) {
			cout << tp[k] << "," ;
		}
	}
	if (db) cout << endl;
	///////////////////////
	// end simulation
	///////////////////////

	//final answer
	fn = tp_total/_numReps;
	FnVar = 0.0;
	for (int k=0; k<_numReps; k++)  FnVar += pow(tp[k]-fn,2);
	FnVar = FnVar / (_numReps-1);

	Simulation_Time = MPI::Wtime()-t0;

	//mark success simulation run
	status = 0;
}

/// (Deprecated) Enumerate systems.
void TpMax::enumerateSystem() {
	int n=param->nstages, B=param->B, R=param->R, i;
	ostringstream s;
	s << "TpSystem_" << n << "_" << B << "_" << R << ".txt";
	outfile.open(s.str().c_str());
	Rs.clear(); Bs.clear(); Rcomb.clear(); Bcomb.clear(); 	_index = 0;

	for (i=1; i<=R-1; i++) Rs.push_back(i);
	for (i=1; i<=B-1; i++) Bs.push_back(i);

	nextComb(0, n-1, 1);

	outfile.close();
	Rs.clear(); Bs.clear(); Rcomb.clear(); Bcomb.clear(); 	_index = 0;
}

/// (Deprecated) Prints out a set of decision variable. Used by system enumerator.
void TpMax::pretty_print() {
	outfile << (_index++) << " ";
	int curr;
	curr = Rcomb[0];
	for (int i = 0; i < (int)Rcomb.size(); ++i) {
		if (i == 0) curr = Rcomb[0];
		else curr = Rcomb[i]-Rcomb[i-1];
		outfile << curr   << " ";
	}
	outfile << param->R - Rcomb.back() << " ";
	curr = Bcomb[0];
	for (int i = 0; i < (int)Bcomb.size(); ++i) {
		if (i==0) curr = Bcomb[0];
		else curr = Bcomb[i]-Bcomb[i-1];
		outfile << curr << " ";
	}
	outfile << param->B - Bcomb.back();
	outfile << endl;
}

/// (Deprecated) Finds the next combination. Used by system enumerator.
void TpMax::nextComb(int offset, int k, int rb) {
	if (k == 0) {
		if (rb == 1)
			nextComb(0, Rcomb.size()-1, 2);
		else pretty_print();
		return;
	}
	if (rb == 1) {
		for (int i = offset; i <= (int)Rs.size() - k; ++i) {
			Rcomb.push_back(Rs[i]);
			nextComb(i+1, k-1, rb);
			Rcomb.pop_back();
		}
	}else if (rb == 2) {
		for (int i = offset; i <= (int)Bs.size() - k; ++i) {
			Bcomb.push_back(Bs[i]);
			nextComb(i+1, k-1, rb);
			Bcomb.pop_back();
		}
	}
}

/**
 * @brief (Deprecated) Read a list of systems from file.
 *
 * If the file is not found, writes the file and the reads it.
 */
void TpMax::readSystem() {
	Systems.clear();	Sys2IdxMap.clear();
	string line;
	int n=param->nstages, B=param->B, R=param->R;
	ostringstream s;
#ifdef TEST_CONFIGURES
	s << "TpSystem_" << n << "_" << B << "_" << R << ".txt" << TEST_CONFIGURES;
#endif
	//	cout << s.str().c_str() << "\n";
	infile.open(s.str().c_str());
	if (infile.is_open()) {
		while(!infile.eof() && Systems.size() < properties.NumSystems) {
			getline(infile, line);
			stringstream s(line);
			int i, j, tmp; vector<int> v;
			s >> i;
			for (j = 0; j<properties.d_disc; ++j) {
				s >> tmp;
				v.push_back(tmp);
			}
			Systems.push_back(v);
			Sys2IdxMap.insert(pair<vector<int>, int>(v, i));
		}
		infile.close();
	} else {
		//if system enumeration file is not found, create one and load
		infile.close();
		this->enumerateSystem();
		this->readSystem();
	}
}

vector<int> TpMax::Idx2System_Disc(const int& idx) {
	//finds the system corresponding to the index
	vector <int> v ;
	v.resize(properties.d_disc);
	if (idx >= properties.NumSystems || idx < 0) {
		cerr << "System idx " << idx << " out of bounds" << endl;

#ifndef TEST_CONFIGURES
		return v;
#else
		return Systems[0];
#endif
		return v;
	} else {
		//		Old - reading file
		//		return Systems[idx];
		//		New
#ifndef TEST_CONFIGURES
		int rr = param->R * 2 - 3;
		int n  = idx / (param->R-1) + 1;
		v[3] = idx % (param->R-1) + 1;
		v[4] = param->B - v[3];
		v[0] = (int) ceil( ( (double)rr - sqrt( (double) rr*rr-8.*n) )  / 2);
		v[1] = param->R - 1 + (n - (rr-v[0]) * v[0] /2) - v[0];
		v[2] = param->R - v[0] - v[1];
		return v;
#else
		return Systems[idx];
#endif
	}
	return v;
}

/// (Deprecated) Converts a system (as characterized by its decision variables) to its index.
int TpMax::System2Idx(vector<int> sys_disc, vector<double> sys_cont) {
	//converts system back to index. -1 indicates that the system is not found
	if (Sys2IdxMap.count(sys_disc) < 1) {
		cerr << "Unable to find system in Sys2IdxMap" << endl;
		return -1;
	}
	else return Sys2IdxMap.find(sys_disc)->second;
}
//
//void TpMax::getNextSystem(vector<int> & v1, vector<double> &v2) {
//	//return in vector v the next system to test
//	v1 = this->Idx2System_Disc( prop.NumSystems - NumRemainingSystems-- );
//}
