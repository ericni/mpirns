/**
 * @file runMPI.cpp
 *
 * @brief Main function uses MPI to distribute SO tasks.
 *
  */

/** @defgroup interface Interfaces
 *
 * @brief Interfaces that can be inherited from.
 *
 */
/** @defgroup algo Algorithms
 *
 * @brief Implementations of several (parallel) simulation optimization algorithms.
 *
 */
/**
 * @defgroup prob Test problems
 *
 * @brief Test problem implementations
 */

/** @defgroup driver Driver
 *
 * @brief main function to drive the package.
 *
 */
/**
 * @defgroup util Utilities
 *
 * @brief Various utilities.
 */
int bsize_GLOBAL, s1max_GLOBAL, seed_GLOBAL, n1_GLOBAL, n0_GLOBAL, outputLevel, algo, RB_global;
int useTimeEst;
double delta_GLOBAL, burnin_param;
int screenVersion;

#include "mpi.h"
#include <sstream>
#include <iostream>
//#include "NHH2013Algo.h"
#include "NHH2013AlgoV2.h"
#include "Matrix.h"
#include "TpMaxLEQ.h"
#include "TpMax.h"
#include "ContainerFreight.h"
//#include "LH2011Algorithm.h"
#include "SORngStream.h"
#include "SOAlgorithm.h"
#include "NSGSParallelV1.h"
#include "GoodSelAlgoV1.h"
using namespace std;

/**
 * @addtogroup driver
 */
/*@{*/
/**
 * @param argc
 * @param args
 * @return
 */
int main(int argc, char* args[]) {
	if (argc > 13 || argc < 11) {
		cerr << "Incorrect number of arguments! " << endl;
		return 0;
	}
	n0_GLOBAL 		= atoi(args[1]);
	n1_GLOBAL 		= atoi(args[2]);
	bsize_GLOBAL	= atoi(args[3]);
	s1max_GLOBAL 	= atoi(args[4]);
	delta_GLOBAL 	= atof(args[5]);
	seed_GLOBAL 	= atof(args[6]);
	screenVersion 	= atoi(args[7]);
	outputLevel 	= atoi(args[8]);
	algo 			= atoi(args[9]);
	RB_global    	= atoi(args[10]);
	useTimeEst 		= (argc >= 12)? atoi(args[11]) : 1;
	burnin_param	= (argc >= 13)? atof(args[12]) : -1.0;


	MPI::Init(); //start MPI

	//	int main_myRank = MPI::COMM_WORLD.Get_rank();	//get process rank
	//	int main_nP = MPI::COMM_WORLD.Get_size();     	//get number of processes

	SOProb * problem = new TpMax();
	SOAlgorithm * algorithm = NULL;
	if (algo == 1) {
		//		algorithm = new LH2011Algorithm();
	}
	else if (algo == 2) {
		//algorithm = new NHH2013Algo();
	}
	else if (algo == 3) {
		algorithm = new NHH2013AlgoV2();
	}
	else if (algo == 4) {
		algorithm = new NSGSParallelV1();
	}
	else if (algo == 5) {
		algorithm = new GoodSelAlgoV1();
	}
	algorithm->initialize(problem);

	algorithm->run();

	MPI_Barrier(MPI_COMM_WORLD);
	delete algorithm;

	MPI::Finalize();
	return 0;
}
/*@}*/
