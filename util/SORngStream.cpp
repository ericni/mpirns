/**
 * @file SORngStream.cpp
 *
 * @brief Implements SORngStream class
 *
 */
#include "SORngStream.h"
#include <iostream>
using namespace std;

//anonymous namespace copied from RngStream.cpp for own use
namespace
{
const double m1   =       4294967087.0;
const double m2   =       4294944443.0;
const double norm =       1.0 / (m1 + 1.0);
const double a12  =       1403580.0;
const double a13n =       810728.0;
const double a21  =       527612.0;
const double a23n =       1370589.0;
const double two17 =      131072.0;
const double two53 =      9007199254740992.0;
const double fact =       5.9604644775390625e-8;     /* 1 / 2^24  */

// The following are the transition matrices of the two MRG components
// (in matrix form), raised to the powers -1, 1, 2^76, and 2^127, resp.

const double InvA1[3][3] = {          // Inverse of A1p0
		{ 184888585.0,   0.0,  1945170933.0 },
		{         1.0,   0.0,           0.0 },
		{         0.0,   1.0,           0.0 }
};

const double InvA2[3][3] = {          // Inverse of A2p0
		{      0.0,  360363334.0,  4225571728.0 },
		{      1.0,          0.0,           0.0 },
		{      0.0,          1.0,           0.0 }
};

const double A1p0[3][3] = {
		{       0.0,        1.0,       0.0 },
		{       0.0,        0.0,       1.0 },
		{ -810728.0,  1403580.0,       0.0 }
};

const double A2p0[3][3] = {
		{        0.0,        1.0,       0.0 },
		{        0.0,        0.0,       1.0 },
		{ -1370589.0,        0.0,  527612.0 }
};

const double A1p76[3][3] = {
		{      82758667.0, 1871391091.0, 4127413238.0 },
		{    3672831523.0,   69195019.0, 1871391091.0 },
		{    3672091415.0, 3528743235.0,   69195019.0 }
};

const double A2p76[3][3] = {
		{    1511326704.0, 3759209742.0, 1610795712.0 },
		{    4292754251.0, 1511326704.0, 3889917532.0 },
		{    3859662829.0, 4292754251.0, 3708466080.0 }
};

const double A1p127[3][3] = {
		{    2427906178.0, 3580155704.0,  949770784.0 },
		{     226153695.0, 1230515664.0, 3580155704.0 },
		{    1988835001.0,  986791581.0, 1230515664.0 }
};

const double A2p127[3][3] = {
		{    1464411153.0,  277697599.0, 1610723613.0 },
		{      32183930.0, 1464411153.0, 1022607788.0 },
		{    2824425944.0,   32183930.0, 2093834863.0 }
};



//-------------------------------------------------------------------------
// Return (a*s + c) MOD m; a, s, c and m must be < 2^35
//
double MultModM (double a, double s, double c, double m)
{
	double v;
	long a1;

	v = a * s + c;

	if (v >= two53 || v <= -two53) {
		a1 = static_cast<long> (a / two17);    a -= a1 * two17;
		v  = a1 * s;
		a1 = static_cast<long> (v / m);     v -= a1 * m;
		v = v * two17 + a * s + c;
	}

	a1 = static_cast<long> (v / m);
	/* in case v < 0)*/
	if ((v -= a1 * m) < 0.0) return v += m;   else return v;
}


//-------------------------------------------------------------------------
// Compute the vector v = A*s MOD m. Assume that -m < s[i] < m.
// Works also when v = s.
//
void MatVecModM (const double A[3][3], const double s[3], double v[3],
		double m)
{
	int i;
	double x[3];               // Necessary if v = s

	for (i = 0; i < 3; ++i) {
		x[i] = MultModM (A[i][0], s[0], 0.0, m);
		x[i] = MultModM (A[i][1], s[1], x[i], m);
		x[i] = MultModM (A[i][2], s[2], x[i], m);
	}
	for (i = 0; i < 3; ++i)
		v[i] = x[i];
}


//-------------------------------------------------------------------------
// Compute the matrix C = A*B MOD m. Assume that -m < s[i] < m.
// Note: works also if A = C or B = C or A = B = C.
//
void MatMatModM (const double A[3][3], const double B[3][3],
		double C[3][3], double m)
{
	int i, j;
	double V[3], W[3][3];

	for (i = 0; i < 3; ++i) {
		for (j = 0; j < 3; ++j)
			V[j] = B[j][i];
		MatVecModM (A, V, V, m);
		for (j = 0; j < 3; ++j)
			W[j][i] = V[j];
	}
	for (i = 0; i < 3; ++i)
		for (j = 0; j < 3; ++j)
			C[i][j] = W[i][j];
}


//-------------------------------------------------------------------------
// Compute the matrix B = (A^(2^e) Mod m);  works also if A = B.
//
void MatTwoPowModM (const double A[3][3], double B[3][3], double m, long e)
{
	int i, j;

	/* initialize: B = A */
	if (A != B) {
		for (i = 0; i < 3; ++i)
			for (j = 0; j < 3; ++j)
				B[i][j] = A[i][j];
	}
	/* Compute B = A^(2^e) mod m */
	for (i = 0; i < e; i++)
		MatMatModM (B, B, B, m);
}


//-------------------------------------------------------------------------
// Compute the matrix B = (A^n Mod m);  works even if A = B.
//
void MatPowModM (const double A[3][3], double B[3][3], double m, long n)
{
	int i, j;
	double W[3][3];

	/* initialize: W = A; B = I */
	for (i = 0; i < 3; ++i)
		for (j = 0; j < 3; ++j) {
			W[i][j] = A[i][j];
			B[i][j] = 0.0;
		}
	for (j = 0; j < 3; ++j)
		B[j][j] = 1.0;

	/* Compute B = A^n mod m using the binary decomposition of n */
	while (n > 0) {
		if (n % 2) MatMatModM (W, B, B, m);
		MatMatModM (W, W, W, m);
		n /= 2;
	}
}


//-------------------------------------------------------------------------
// Check that the seeds are legitimate values. Returns 0 if legal seeds,
// -1 otherwise.
//
int CheckSeed (const unsigned long seed[6])
{
	int i;

	for (i = 0; i < 3; ++i) {
		if (seed[i] >= m1) {
			cerr << "****************************************\n"
					<< "ERROR: Seed[" << i << "] >= 4294967087, Seed is not set."
					<< "\n****************************************\n\n";
			return (-1);
		}
	}
	for (i = 3; i < 6; ++i) {
		if (seed[i] >= m2) {
			cerr << "*****************************************\n"
					<< "ERROR: Seed[" << i << "] >= 4294944443, Seed is not set."
					<< "\n*****************************************\n\n";
			return (-1);
		}
	}
	if (seed[0] == 0 && seed[1] == 0 && seed[2] == 0) {
		cerr << "****************************\n"
				<< "ERROR: First 3 seeds = 0.\n"
				<< "****************************\n\n";
		return (-1);
	}
	if (seed[3] == 0 && seed[4] == 0 && seed[5] == 0) {
		cerr << "****************************\n"
				<< "ERROR: Last 3 seeds = 0.\n"
				<< "****************************\n\n";
		return (-1);
	}

	return 0;
}

} // end of anonymous namespace


/**
 * @brief Generates a normal random number
 *
 * @param mu Mean
 * @param sigma Standard deviation
 * @return A normal random number with mean mu and standard deviation sigma
 */
double SORngStream::RandNormal (double mu, double sigma)
{
	static double Y[2];
	double U[2];
	U[0] = RandU01();   U[1] = RandU01();
	double R = sqrt(-2.0*log(U[0])), Theta = 2.0*M_PI*U[1];
	Y[0] = R*sin(Theta);    Y[1] = R*cos(Theta);
	return mu+sigma*Y[0];
};

/**
 * @brief Generates a general uniform random number
 *
 * @param a
 * @param b
 * @return A uniform random number between a and b.
 */
double SORngStream::RandU (double a, double b)
{
	return a + (b - a) * RandU01();
};

/**
 * @brief Generates an Exponential random number
 *
 * @param lambda
 * @return An exponential random number with parameter lambda
 */
double SORngStream::RandExponential (double lambda) {
	double u = RandU01();
	return -(log(u)/lambda);
};


/**
 * @brief Generates a Gamma random number
 *
  Originally the algorithm was devised by Marsaglia in 2000:
    G. Marsaglia, W. W. Tsang: A simple method for gereating Gamma variables
	  ACM Transactions on mathematical software, Vol. 26, No. 3, Sept. 2000

  Original code by
	Rajib Bhattacharjea (raj.b@gatech.edu)
	and
	Hadi Arbabi (marbabi@cs.odu.edu)

 * @param shape
 * @param scale
 * @return A Gamma random number with parameters shape and scale
 */
double SORngStream::RandGamma (double shape, double scale) {
	if (shape < 1) {
		double u = RandU01();
		return RandGamma(1.0+shape, scale) * pow (u, 1.0 / shape);
	}

	double x,v,u;
	double d= shape - 1.0/3.0;
	double c= (1.0/3.0) / sqrt(d);

	while(1) {
		do {
			x = RandNormal();
			v = 1. + c * x;
		} while (v <= 0);

		v = v*v*v;
		u = RandU01();
		if (u < 1. -0.0331 *(x*x)*(x*x)) {
			break;
		}
		if (log(u) < 0.5 * x * x + d * (1. - v + log(v))){
			break;
		}
	}
	return scale * d * v;
}


/**
 * @brief Generates a Poisson random number for large lambda
 *
 * "Rejection method PA" from "The Computer Generation of
 *  Poisson Random Variables" by A. C. Atkinson,
 *  Journal of the Royal Statistical Society Series C
 *  (Applied Statistics) Vol. 28, No. 1. (1979)
 *  The article is on pages 29-35.
 *  The algorithm given here is on page 32.
 *
 *  Code by John D. Cook
 *  Retrieved 30 July, 2015 from
 *  http://www.johndcook.com/blog/csharp_poisson/
 *
 * @param lambda
 * @return A Poisson random number with parameter lambda
 */
int SORngStream::RandPoisson_large_lambda (double lambda) {
    double c = 0.767 - 3.36/lambda;
    double beta = M_PI/sqrt(3.0*lambda);
    double alpha = beta*lambda;
    double k = log(c) - lambda - log(beta);

    while(true)
    {
        double u = this->RandU01();
        double x = (alpha - log((1.0 - u)/u))/beta;
        int n = (int) floor(x + 0.5);
        if (n < 0)
            continue;
        double v = this->RandU01();
        double y = alpha - beta*x;
        double temp = 1.0 + exp(y);
        double lhs = y + log(v/(temp*temp));
        double rhs = k + n*log(lambda) - lgamma( n+1 );
        if (lhs <= rhs)
            return n;
    }

    throw "corrupted\n";
    return -1;
}

/**
 * @brief Generates a Poisson random number for small lambda
 *
 *
 * Algorithm due to Donald Knuth, 1969.
 *
 * Code by John D. Cook
 * Retrieved 30 July, 2015 from
 * http://www.johndcook.com/blog/csharp_poisson/
 *
 * @param lambda
 * @return A Poisson random number with parameter lambda
 */
int SORngStream::RandPoisson_small_lambda (double lambda) {
    double p = 1.0;
    double L = exp(-lambda);
    int k = 0;
    do
    {
        k++;
        p *= this->RandU01();
    }
    while (p > L);
    return k - 1;
}

/**
 * @brief Generates a Poisson random number by selecting the appropriate method based on lambda
 *
 * @param lambda
 * @return A Poisson random number with parameter lambda
 */
int SORngStream::RandPoisson (double lambda) {
	return (lambda < 30) ?
			RandPoisson_small_lambda(lambda) :
			RandPoisson_large_lambda(lambda);
}

/**
 * @brief Advances random seed by 2^127 states.
 *
 * Manually advance the input array seedIn 2^127 states to seedOut which occupies
   the same relative location in the next stream.

 * Original code by Andrew Karl, Randy Eubank, Dennis Young: "Parallel Random
	Number Generation in C++ and R Using RngStream".
 *
 * @param seedIn The input array
 * @param seedOut The output array, which is 2^127 states away from seedIn
 */
void SORngStream::AdvanceSeed (unsigned long seedIn[6], unsigned long seedOut[6]) {
	double tempIn[6]; double tempOut[6];
	for (int i=0; i<6; ++i) {
		tempIn[i] = seedIn[i];
	}
	MatVecModM ( A1p127 , tempIn , tempOut , m1 ) ;
	MatVecModM ( A2p127 , &tempIn [3] , &tempOut [3] , m2 ) ;
	for (int i=0; i<6; ++i) {
		seedOut[i] = tempOut[i];
	}
}


