/**
 * @file SORngStream.h
 *
 * @brief Defines SORngStream class
 *
 */
#include <RngStream.h>
#include <cmath> 

#ifndef SORNGSTREAM_H
#define SORNGSTREAM_H

#ifndef M_PI
#define M_PI           3.14159265358979323846 //< Value of PI
#endif

/**
 * @addtogroup util
 *
 */
/*@{*/
/**
 * @brief Extends RngStream class to include more distributions used for simulation optimization
 *
 */
class SORngStream : public RngStream
{
public:
	double RandU(double a = 0.0, double b = 1.0);
	double RandNormal (double mu = 0.0, double sigma = 1.0 );
	double RandExponential(double lambda = 1.0);
	double RandGamma (double shape = 1.0, double scale = 1.0);
	int RandPoisson(double lambda);
	static void AdvanceSeed (unsigned long seedIn[], unsigned long seedOut[]);
private:
	int RandPoisson_large_lambda (double lambda);
	int RandPoisson_small_lambda (double lambda);
};
/*@}*/
#endif
