/*
 * Combination.h
 *
 *  Created on: Jul 29, 2015
 *      Author: cn254
 *      https://msdn.microsoft.com/en-us/library/Aa289166(v=VS.71).aspx
 */

#include<cstdlib>
#include<string>
#include<exception>
#include <sstream>
using namespace std;

#ifndef COMBINATION_H_
#define COMBINATION_H_

class Combination {
private:
	long n;
	long k;
	long* data;
	static long LargestV(long a, long b, long x);
public:
	Combination(long n, long k);
	Combination(long n, long k, long* a, long k_array);
	bool IsValid();
	string ToString();
	Combination Successor();
	static long Choose(long n, long k);
	Combination Element(long m);
	virtual ~Combination();

	long *getData() const {
		return data;
	}
};

#endif /* COMBINATION_H_ */
