/**
 * @file Matrix.h
 *
 * @brief Defines Matrix class
*/

#ifndef SOMATRIX_H_
#define SOMATRIX_H_
#include <algorithm>
#include <memory>
#include <vector>

/**
 * @addtogroup util
 *
 */
/*@{*/
/**
 * @brief A simple implementation of 2d row-oriented matrix using std::vector.
 */
template<class T>
class Matrix {
	typedef T value_type;
	typedef std::vector<value_type> Container;
public:
	/// Default constructor.
	Matrix() : _b(0) {}

	/// Default destructor.
	~Matrix()   {_data.clear();}

	/**
	 * @brief Construct an a by b Matrix with optional initial value.
	 *
	 * @param a Number of rows.
	 * @param b Number of columns.
	 * @param initial (Optional) initial value.
	 */
	Matrix(int a, int b, value_type const& initial=value_type())
	: _b(0)
	{
		resize(a, b, initial);
	}

	/// Copy constructor.
	Matrix(Matrix const& other)
	: _data(other._data), _b(other._b)
	{}

	/// Copy from another Matrix object.
	Matrix& operator=(Matrix copy) {
		swap(*this, copy);
		return *this;
	}

	/// Empty the content while maintaining the dimension and memory space.
	bool empty() const { return _data.empty(); }
	/// Clear data and release memory.
	void clear() { _data.clear(); _b = 0; }

	/// Returns number of rows.
	int dim_a() const { return _b ? _data.size() / _b : 0; }

	/// Returns number of columns.
	int dim_b() const { return _b; }

	/// Returns a pointer to row a.
	value_type* operator[](int a) {
		return &_data[a * _b];
	};

	/// Returns a pointer to row a.
	value_type const* operator[](int a) const {
		return &_data[a * _b];
	};

	/**
	 * @brief Resize the Matrix to a by b with (optional) initial value.
	 *
	 * @param a Number of rows.
	 * @param b Number of columns.
	 * @param initial (Optional) initial value.
	 */
	void resize(int a, int b, value_type const& initial=value_type()) {
		if (a == 0) {
			b = 0;
		}
		_data.resize(a * b, initial);
		_b = b;
	};

	/// Fill Matrix with value.
	void fill( value_type const& initial=value_type() ) {
		std::fill (_data.begin(), _data.end(), initial);
	};

	/// Swap the content of two Matrices.
	friend void swap(Matrix& a, Matrix& b) {
		using std::swap;
		swap(a._data, b._data);
		swap(a._b,    b._b);
	};

	/// Output to stream.
	template<class Stream>
	friend Stream& operator<<(Stream& s, Matrix const& value) {
		s << "<Matrix at " << &value << " dimensions "
				<< value.dim_a() << 'x' << value.dim_b();
		if (!value.empty()) {
			bool first = true;
			typedef typename Container::const_iterator Iter;
			Iter i = value._data.begin(), end = value._data.end();
			while (i != end) {
				s << (first ? " [[" : "], [");
				first = false;
				s << *i;
				++i;
				for (int b = value._b - 1; b; --b) {
					s << ", " << *i;
					++i;
				}
			}
			s << "]]";
		}
		s << '>';
		return s;
	};

private:
	/// Container for data.
	Container _data;
	/// Number of columns.
	int _b;
};
/*@}*/
#endif
