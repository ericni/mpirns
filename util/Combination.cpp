/*
 * Combination.cpp
 *
 *  Created on: Jul 29, 2015
 *      Author: cn254
 *      https://msdn.microsoft.com/en-us/library/Aa289166(v=VS.71).aspx
 */

#include "Combination.h"

Combination::Combination(long n, long k) {
	if (n < 0 || k < 0) // normally n >= k
		throw "Negative parameter in constructor";
	this->n = n;
	this->k = k;
	this->data = new long[k];
	for (long i = 0; i < k; ++i)
		this->data[i] = i;
}

Combination::Combination(long n, long k, long * a, long k_array) {
	if (k != k_array)
		throw "Array length does not equal k";

	this->n = n;
	this->k = k;
	this->data = new long[k];
	for (long i = 0; i < k_array; ++i)
		this->data[i] = a[i];

	if (!this->IsValid())
		throw "Bad value from array";
}

bool Combination::IsValid() {
	for (long i = 0; i < this->k; ++i)
	{
		if (this->data[i] < 0 || this->data[i] > this->n - 1)
			return false; // value out of range

		for (long j = i+1; j < this->k; ++j)
			if (this->data[i] >= this->data[j])
				return false; // duplicate or not lexicographic
	}

	return true;
}

string Combination::ToString() {
	string s = "{ ";
	for (long i = 0; i < this->k; ++i) {
		ostringstream ss;
		ss << this->data[i];
		s += ss.str() + " ";
	}

	s += "}";
	return s;

}

Combination Combination::Successor() {
	if (this->data[0] == this->n - this->k)
		throw "exception";

	Combination ans = Combination(this->n, this->k);

	long i;
	for (i = 0; i < this->k; ++i)
		ans.data[i] = this->data[i];

	for (i = this->k - 1; i > 0 && ans.data[i] == this->n - this->k + i; --i)
		;

	++ans.data[i];

	for (long j = i; j < this->k - 1; ++j)
		ans.data[j+1] = ans.data[j] + 1;

	return ans;
}

long Combination::Choose(long n, long k) {

	if (n < 0 || k < 0)
		throw "Invalid negative parameter in Choose()";
	if (n < k)
		return 0;  // special case
	if (n == k)
		return 1;

	long delta, iMax;

	if (k < n-k) // ex: Choose(100,3)
	{
		delta = n-k;
		iMax = k;
	}
	else         // ex: Choose(100,97)
	{
		delta = k;
		iMax = n-k;
	}

	long ans = delta + 1;

	for (long i = 2; i <= iMax; ++i)
	{
		ans = (ans * (delta + i)) / i;
	}

	return ans;
}

long Combination::LargestV(long a, long b, long x) {
	long v = a - 1;

	while (Choose(v,b) > x)
		--v;

	return v;
}

Combination Combination::Element(long m) {
	long* ans = new long[this->k];

	long a = this->n;
	long b = this->k;
	long x = (Choose(this->n, this->k) - 1) - m; // x is the "dual" of m

	for (long i = 0; i < this->k; ++i)
	{
		ans[i] = LargestV(a,b,x); // largest value v, where v < a and vCb < x
		x = x - Choose(ans[i],b);
		a = ans[i];
		b = b-1;
	}

	for (long i = 0; i < this->k; ++i)
	{
		ans[i] = (n-1) - ans[i];
	}

	return Combination(this->n, this->k, ans, this->k);
}

Combination::~Combination() {
}
